Client Job Engine
=================

###Core Engine SDK
This module is module is essentially the non-UI client side compute engine.  The compute-lib
folder contains the customer core sdk library.  The library is generated from the 
compute-customer-SDK Project in github, specifically from the core-engine-sdk-<version> module.


###AppEngine Endpoints
This module is an android library for the consumer android app, It contains API Handlers
to Google AppEngine Endpoints.  It depends heavily on client libraries that are generated from 
the UnocerosServer-JobEngine Project specifically from the consumer-endpoints module.

###Deployment Notes:
The `com.unoceros.engine.Globals.java` must be updated with the specific google project console id
this app is meant to communicated with via means of google cloud messaging, GCM.


