package com.unoceros.consumer.data;

import com.google.api.client.util.DateTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DailyComputeData implements Serializable{
	private static final long serialVersionUID = -3862705866764300897L;
	private String key;
	private String date;
	private List<ComputeStats> computeStats;
	
	public DailyComputeData(){
		this.computeStats = new ArrayList<ComputeStats>();
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public List<ComputeStats> getComputeStats() {
		return computeStats;
	}
	public void setComputeStats(List<ComputeStats> computeStats) {
		this.computeStats = computeStats;
	}
	public void addStats(ComputeStats stats){
		this.computeStats.add(stats);
	}
	
	public class ComputeStats implements Serializable{
		private static final long serialVersionUID = -2361996461475885276L;
		private String jobId;
        private String customerId;
        private String projectId;
        private String pluginId;
		private long startMark;
		private long duration;
        private long startOfDay;
        private long endOfDay;
        private String status;
        private DateTime date;

        public String getJobId() {
			return jobId;
		}
        public void setJobId(String jobId) {
            this.jobId = jobId;
        }
		public void setProjectId(String projectId) {
			this.projectId = projectId;
		}
        public String getProjectId() {
            return projectId;
        }
        public String getPluginId() {
            return pluginId;
        }
        public void setPluginId(String pluginId) {
            this.pluginId = pluginId;
        }
        public String getCustomerId() {
            return customerId;
        }
        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }
        public long getStartMark() {
			return startMark;
		}
		public void setStartMark(long startMark) {
			this.startMark = startMark;
		}
		public long getDuration() {
			return duration;
		}
		public void setDuration(long duration) {
			this.duration = duration;
		}
        public long getStartOfDay() {
            return startOfDay;
        }
        public void setStartOfDay(long startOfDay) {
            this.startOfDay = startOfDay;
        }
        public long getEndOfDay() {
            return endOfDay;
        }
        public void setEndOfDay(long endOfDay) {
            this.endOfDay = endOfDay;
        }
        public String getStatus() {
            return status;
        }
        public void setStatus(String status) {
            this.status = status;
        }
        public void setDate(DateTime date) {
            this.date = date;
        }
        public DateTime getDate() {
            return date;
        }
    }
}
