package com.unoceros.consumer.lib;

import android.app.Application;
import android.content.Context;

import com.unoceros.consumer.util.Log;
import com.unoceros.kit.JobPayload;
import com.unoceros.kit.task.ComputeTask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import dalvik.system.DexClassLoader;


/**
 * Created by Amit
 */
public class ExecutableLoader {

    private static final String TAG = "ExecutableLoader";
    //private static final int BUF_SIZE = 8 * 1024;
    
    private static ExecutableLoader instance;
    private static Map<String, ClassLoader> mLoadedExecutable;

    //private Application mContext;
    
    private ExecutableLoader(Application context){
        //mContext = context;
        mLoadedExecutable = new HashMap<>();
        ComputeTask task = new ComputeTask() {
            @Override
            public void execute() {}
        };
        Log.d(TAG, "Making Sure Dependency is Included: " + task);
    }

    public static ExecutableLoader getInstance(Application context){
        if(instance == null)
            instance = new ExecutableLoader(context);

        return instance;
    }

    public ClassLoader loadDexPlugin(Application context, JobPayload payload,
            String pluginPath, String prefix, String pluginId) 
            throws IOException, IllegalAccessException, ClassNotFoundException, InstantiationException {

        ClassLoader classLoader = mLoadedExecutable.get(pluginId);
        if(classLoader == null) {

            //load the class loader and retrieve the loaded plugin, the mainclass is set in playload
            classLoader = getDexClassLoader(context, payload, pluginPath, prefix, pluginId);
            
            //now try retrieving the mainclass name from payload
            String mainClassName = payload.getMainClassName(); //"com.ejiro.lib.WordCounterTask"
            Class<?> loadedPluginClazz = classLoader.loadClass(mainClassName);
            Object libInstance = loadedPluginClazz.newInstance();
            /*
            try {
                Method method = loadedPluginClazz.getMethod("getPayload"); //"getName");
                Object result = method.invoke(libInstance);
                Log.d(TAG, "method test: "+result);
            }catch (Exception e){
                Log.e(TAG, e.getMessage(), e);
            }*/
            
            if(libInstance == null) {
                if(payload.getExOutputFileDir() != null)
                    payload.getExOutputFileDir().delete();
                throw new InstantiationException("Plugin not loaded");
            }else {
                //save the loaded plugin to the hash map
                mLoadedExecutable.put(pluginId, classLoader);
            }
        }else{
            String fileName = prefix+"_"+pluginId;
            File executablePluginFile = new File(context.getDir(pluginPath, Context.MODE_PRIVATE), fileName);
            setExecutablePayloadFile(payload, executablePluginFile);
        }
        
        return classLoader;
    }

    private ClassLoader getDexClassLoader(Application context, JobPayload payload,
            String pluginPath, String prefix, String pluginId) throws IOException, IllegalAccessException {

        String fileName = prefix+"_"+pluginId;
        File executablePluginFile = new File(context.getDir(pluginPath, Context.MODE_PRIVATE), fileName);
        
        //download and install on "plugin" path
        boolean shouldDownloadPlugin = !executablePluginFile.exists();
        if(shouldDownloadPlugin) {
            Log.e(TAG, "downloading executable..."+payload.getSignedExecUrl());
            downloadFile(payload.getSignedExecUrl(),
                    payload.getExContentType(),
                    executablePluginFile);
        }
        Log.e(TAG, "Dex exist? "+executablePluginFile.exists()+", path: "+executablePluginFile.getAbsolutePath());
        setExecutablePayloadFile(payload, executablePluginFile);
        
        // Internal storage where the DexClassLoader writes the optimized dex file to
        final File optimizedDexOutputPath = context.getDir("outdex", Context.MODE_PRIVATE);

        /*
        try {
            List<URL> urls = new ArrayList<>();
            urls.add(executablePluginFile.toURI().toURL());
            PathClassLoader myClassLoader = new PathClassLoader(urls.get(0).toString(), context.getClassLoader());
            Class<?> clazz = Class.forName("com.ejiro.lib.UnocerosTask", true, myClassLoader);
            Object instance = clazz.newInstance();
            Method method = clazz.getMethod("getName");
            Object result = method.invoke(instance);
            Log.e(TAG, result+"");
        }catch (Exception e){
            Log.e(TAG, e.getMessage(), e);
        }
        */
        DexClassLoader cl = new DexClassLoader(executablePluginFile.getAbsolutePath(),
                optimizedDexOutputPath.getAbsolutePath(),
                null,
                context.getClassLoader());

        return cl;
    }

    private void setExecutablePayloadFile(JobPayload payload, File executablePluginFile) throws IOException, 
            IllegalAccessException{
        //we open the jar file
        JarFile jarFile = new JarFile(executablePluginFile);
        //we recover the manifest
        Manifest manifest = jarFile.getManifest();
        //we recover the class name of our pause-able task thanks to ours manifest
        String mainClassName = manifest.getMainAttributes().getValue("Main-Class");
        Log.d(TAG, "Main-Class: "+ mainClassName);
        payload.setMainClassName(mainClassName);

        //set executable file dir
        payload.setExOutputFileDir(executablePluginFile);
    }
    
    private void downloadFile(String signedUrl, String contentType, File outfile) throws IOException {
        try {
            URL url = new URL(signedUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", contentType);

            //add request header
            int responseCode = connection.getResponseCode();
            Log.d(TAG, "Sending 'GET' request to URL : " + url);
            Log.d(TAG, "Response Code : " + responseCode);

            //opens input stream from the HTTP connection
            InputStream inputStream = connection.getInputStream();

            //opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(outfile);

            int bytesRead = -1;
            byte[] buffer = new byte[4096];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            //close streams
            outputStream.close();
            inputStream.close();
            Log.d(TAG, "Done downloading request file into: "+outfile);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            throw new IOException(e.getMessage());
        }
    }

    /*
    private void saveToStorage(Application context, String fileName, String destPath,
                               InputStream input) throws IOException {

        File outfile = new File(context.getDir(destPath, Context.MODE_PRIVATE),fileName);
        if (!outfile.exists()) {
            outfile.createNewFile();
        }
        FileOutputStream output = new FileOutputStream(outfile);
        byte[] buffer = new byte[BUF_SIZE];
        int readLen;
        while((readLen = input.read(buffer)) != -1){
            output.write(buffer, 0, readLen);
        }
        output.flush();
        input.close();
        output.close();
    }

    private void copyFromAssetToStorage(Application context, String fileName,
                                        String assetsPath, String destPath) throws IOException {

        File outfile = new File(context.getDir(destPath, Context.MODE_PRIVATE),fileName);
        if (!outfile.exists()) {
            outfile.createNewFile();
        }
        FileOutputStream output = new FileOutputStream(outfile);
        byte[] buffer = new byte[BUF_SIZE];
        int readLen;
        InputStream input = context.getAssets().open(assetsPath+fileName);
        while((readLen = input.read(buffer)) != -1){
            output.write(buffer, 0, readLen);
        }
        output.flush();
        input.close();
        output.close();
    }
    */
}
