package com.unoceros.consumer.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.telephony.TelephonyManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.util.DateTime;
import com.google.gson.Gson;
import com.unoceros.client.engine.ApiServiceHandler;
import com.unoceros.client.engine.Globals;
import com.unoceros.client.engine.computetimeendpoint.Computetimeendpoint;
import com.unoceros.client.engine.computetimeendpoint.model.ComputeTime;
import com.unoceros.client.engine.nodestatusinfoendpoint.model.NodeStatusInfo;
import com.unoceros.client.engine.sync.SyncForegroundReceiver;
import com.unoceros.consumer.data.DailyComputeData;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.util.SyncUtility;
import com.unoceros.engine.JobManager;
import com.unoceros.engine.TaskScheduler;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

public class EngineUtility {

	public static final String TAG = "CloudAppUtility";
    public static final long SYNC_INTERVAL_MILLISECONDS = AlarmManager.INTERVAL_FIFTEEN_MINUTES;

    /**
     * True if Job state is stopped/interrupted (meaning it cannot resume from the paused state)
     *
     * @return
     */
    public static boolean jobStateIsStopped() {
        return JobManager.instance().getState() == TaskScheduler.JobState.STOPPED;
    }

    /**
     * True if Job is currently running or temporary paused, False if it is finished (meaning all scheduled task are completed)
     *
     * @param context
     * @return
     */
    public static boolean isComputing(Context context) {
        boolean isComputing = JobManager.instance().isJobRunning();
        return isComputing;
    }

    public static boolean isComputeServiceRunning(Context context, Class<?> serviceClass) {
	    ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (serviceClass.getName().equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}
	
	/**
	 * Gets the current registration ID for application on GCM service, if there
	 * is one.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	public static  String getRegistrationId(Context context){
		final SharedPreferences prefs = EngineUtility.getGcmPreferences(context);
		String registrationId = prefs.getString(Globals.GCM_PREFS_PROPERTY_REG_ID, "");
		if (registrationId == null || registrationId.equals("")){
			Log.i(Globals.TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(Globals.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = EngineUtility.getAppVersionCode(context);
		if (registeredVersion != currentVersion){
			Log.i(Globals.TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}
	
	/**
	 * Stores the registration ID and the app versionCode in the application's
	 * {@code SharedPreferences}. 
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	public static void storeRegistrationId(Context context, String regId){
		final SharedPreferences prefs = EngineUtility.getGcmPreferences(context);
		int appVersion = EngineUtility.getAppVersionCode(context);
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Globals.GCM_PREFS_PROPERTY_REG_ID, regId);
		editor.putInt(Globals.PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	/**
	 * Removes the registration ID from the application's 
	 * {@code SharedPreferences}.
	 * @param context 
	 * 		the application context
	 */
	public static void removeRegistrationId(Context context){
		final SharedPreferences prefs = EngineUtility.getGcmPreferences(context);
		int appVersionCode = EngineUtility.getAppVersionCode(context);
		Log.i(TAG, "Removing GCM regId on app version " + appVersionCode);
		SharedPreferences.Editor editor = prefs.edit();
		editor.remove(Globals.GCM_PREFS_PROPERTY_REG_ID);
		editor.commit();
	}
	
	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private static SharedPreferences getGcmPreferences(Context context){
		// This sample app persists the registration ID in shared preferences,
		// but how you store the regID in your app is up to you.
		return context.getSharedPreferences(Globals.GCM_PREFS_NAME, Context.MODE_PRIVATE);
	}
	
	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	public static int getAppVersionCode(Context context){
		int appVersion = 0;
		try{
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			appVersion = packageInfo.versionCode;
		}catch (NameNotFoundException e){
			// should never happen
			//throw new RuntimeException("Could not get package name: " + e);
		}
		
		return appVersion;
	}
	
	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	public static boolean checkPlayServices(Activity context){
        try {
            int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
            if (resultCode != ConnectionResult.SUCCESS) {
                if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                    GooglePlayServicesUtil.getErrorDialog(resultCode, context, Globals.PLAY_SERVICES_RESOLUTION_REQUEST).show();
                } else {
                    Log.d(Globals.TAG, "This device is not supported.");
                    return false;
                }
                return false;
            }
        }catch (Exception e){
            Log.e(Globals.TAG, "Error checking play services: "+e.getMessage());
            return false;
        }
		return true;
	}

    public static PendingIntent startSyncService(Context context, boolean setAlarm, boolean oneShot){
        Intent intent = new Intent(context, SyncForegroundReceiver.class);
        intent.setAction(AppUtility.SYNC_ACTION_SERVICE);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        if(setAlarm) {
            AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
            //cancel before resetting
            alarmMgr.cancel(alarmIntent);
            if(oneShot){
                long startFromNow = System.currentTimeMillis()+2000;
                alarmMgr.set(AlarmManager.RTC_WAKEUP, startFromNow, alarmIntent);
            }else {
                /*
                long startFromNow = SystemClock.elapsedRealtime()+2000;
                alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                        startFromNow,
                        SYNC_INTERVAL_MILLISECONDS,
                        alarmIntent);
                 */
                long startFromNow = System.currentTimeMillis()+2000;
                alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP,
                        startFromNow,
                        SYNC_INTERVAL_MILLISECONDS,
                        alarmIntent);
            }
        }

        return alarmIntent;
    }

    public static void stopSyncService(Context context){
        // If the alarm has been set, cancel it.
        PendingIntent alarmIntent = startSyncService(context, false, false);
        if(alarmIntent != null)
            alarmIntent.cancel();
    }

    public static void updateNodeStatusUpdate(Application application, ApiServiceHandler endpointService, boolean isComputing){
        Context context = application.getApplicationContext();
        try {
            //get device status for DeviceInfo
            boolean canCompute = SyncUtility.canCompute(context);
            boolean isActive = (canCompute && !isComputing);
            DateTime currentDateTime = new DateTime(new Date(), TimeZone.getTimeZone("UTC"));

            //memory stats
            Runtime runtime = Runtime.getRuntime();
            long maxMemory = runtime.maxMemory();
            long freeMemory = runtime.freeMemory();
            
            //disk stats
            long freeInternalDiskSpace = AppUtility.freeDiskMemory(false);
            long freeExternalDiskSpace = AppUtility.freeDiskMemory(true);
            long totalInternalDiskSpace = AppUtility.totalDiskMemory(false);
            long totalExternalDiskSpace = AppUtility.totalDiskMemory(true);

            String arch = System.getProperty("os.arch");
            boolean isOnWIFI = SyncUtility.isConnectedToWiFi(context);

            TelephonyManager manager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
            String carrierName = manager.getNetworkOperatorName();

            String location = "";
            try {
                location = AppUtility.getLastKnowLocation(context).toString();
            }catch (Exception e){
                e.printStackTrace();
            }

            NodeStatusInfo nodeStatus = new NodeStatusInfo();
            nodeStatus.setId(UUID.randomUUID().toString())
                    .setNodeId(endpointService.getDeviceUUID())
                    .setAppVersion(AppUtility.getVersionName(application))
                    .setComputeEngineStatus(isActive)
                    .setMeetsComputeThreshold(canCompute)
                    .setComputing(isComputing)
                    .setBatteryLevel((double) SyncUtility.batteryLevel(context))
                    .setBatteryPrefLevel((double) SyncUtility.batteryPrefLevel(context))
                    .setCharging(SyncUtility.isCharging(context))
                    .setReportedDate(currentDateTime)
                    .setLoggedDate(currentDateTime) //this value will be replaced in the back-end
                    .setTimeZone(TimeZone.getDefault().getID())
                    .setMaxMemory(maxMemory)
                    .setFreeMemory(freeMemory)
                    .setArchitecture(arch)
                    .setOnWiFi(isOnWIFI)
                    .setCarrierName(carrierName)
                    .setLastKnowLocation(location)
                    .setFreeInternalDiskSpace(freeInternalDiskSpace)
                    .setFreeExternalDiskSpace(freeExternalDiskSpace)
                    .setTotalInternalDiskSpace(totalInternalDiskSpace)
                    .setTotalExternalDiskSpace(totalExternalDiskSpace)
                    .setNodeBrand(URLEncoder.encode(android.os.Build.BRAND, "UTF-8"))
                    .setNodeManufacturer(URLEncoder.encode(android.os.Build.MANUFACTURER, "UTF-8"))
                    .setNodeModel(URLEncoder.encode(android.os.Build.MODEL, "UTF-8"))
                    .setNodeModelVersion(URLEncoder.encode(android.os.Build.VERSION.CODENAME, "UTF-8"));
            endpointService.getNodeStatusInfoEndpoint().addNodeStatusInfo(nodeStatus).execute();

            //Log.d(TAG, "Successfully logged node status to endpoint");
        }catch (Exception e){
            //store locally for later retrieval
            Log.e(TAG, "Error logging node status to endpoint", e);
        }
    }

    public static List<String> getExecutablePlugins(File dir){
        List<String> pluginIds = new ArrayList<>();
        FilenameFilter pluginFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                String lowercaseName = name.toLowerCase();
                if (lowercaseName.startsWith("plugin_")) {
                    return true;
                } else {
                    return false;
                }
            }
        };

        File[] files = dir.listFiles(pluginFilter);
        for (File file : files) {
            if (!file.isDirectory() && file.canExecute()) {
                String pluginName = file.getName();
                Log.d(TAG, "Plugin: "+pluginName);
                pluginIds.add(pluginName);
            }
        }
        return pluginIds;
    }

    /**
     * STORE STATS TO REMOTE ENDPOINTS
     * @param endpointService
     * @param pref
     */
    public static void storeComputeStatsToRemote(ApiServiceHandler endpointService, SharedPreferences pref){
        Date depositDate = new Date(); //getStartOfDay();
        String dateKey = SyncUtility.getFormatedDate(depositDate); //2014-01-30
        String deviceId = endpointService.getDeviceUUID(); //deviceInfo.getDeviceInfoID();
        String key = String.valueOf((deviceId+dateKey).hashCode());
        String gson_str = pref.getString(key, null);
        DailyComputeData cdata = null;
        if(gson_str != null){
            Gson gson = new Gson();
            cdata = gson.fromJson(gson_str, DailyComputeData.class);
            List<DailyComputeData.ComputeStats> stats = cdata.getComputeStats();

            com.unoceros.consumer.util.Log.d(TAG, "syncRemoteStorage:: storing to remote storage - Gson: " + gson_str);

            //update compute time
            Computetimeendpoint endpoint = endpointService.getComputeTimeEndpoint();
            boolean isErrored = false;
            for(DailyComputeData.ComputeStats stat: stats) {
                ComputeTime computeTime = new ComputeTime();
                computeTime.setId(UUID.randomUUID().toString());
                computeTime.setJobId(stat.getJobId());
                computeTime.setCustomerId(stat.getCustomerId());
                computeTime.setPluginId(stat.getPluginId());
                computeTime.setProjectId(stat.getProjectId());
                computeTime.setStatus(stat.getStatus());
                computeTime.setStartTime(stat.getStartMark()+"");
                computeTime.setDeviceId(deviceId);
                computeTime.setDate(stat.getDate());
                computeTime.setDuration(stat.getDuration());
                computeTime.setStartOfDay(stat.getStartOfDay());
                computeTime.setEndOfDay(stat.getEndOfDay());
                try {
                    endpoint.insertComputeTime(computeTime).execute();
                } catch (IOException e) {
                    com.unoceros.consumer.util.Log.e(TAG, "Error syncing compute stats to remote: " + e.getMessage(), e);
                    isErrored = true;
                }
            }
            if(!isErrored){
                pref.edit().remove(key).apply();
            }
        }
    }
}