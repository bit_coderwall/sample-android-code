package com.unoceros.client.task;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.unoceros.consumer.utils.EngineUtility;

public class JobWakefulReceiver extends WakefulBroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
        //reset sync alarm if rebooted
        boolean resetSyncingService = false;
        String action = intent.getAction();
        if (Intent.ACTION_BOOT_COMPLETED.equalsIgnoreCase(action) ||
                Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE.equals(action)) {
            resetSyncingService = true;
        }else if(Intent.ACTION_PACKAGE_REPLACED.equalsIgnoreCase(action) &&
                intent.getData().getSchemeSpecificPart().equals(context.getPackageName())){
            resetSyncingService = true;
        }

        if(resetSyncingService) {
            EngineUtility.startSyncService(context, true, false);
        }
	}
}