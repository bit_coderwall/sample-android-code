package com.unoceros.client.task;

import android.app.Application;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.unoceros.client.engine.ApiServiceHandler;
import com.unoceros.client.engine.GcmBroadcastReceiver;
import com.unoceros.client.engine.messageEndpoint.MessageEndpoint;
import com.unoceros.client.engine.messageEndpoint.model.TaskPayloadInfo;
import com.unoceros.client.engine.sync.SyncForegroundReceiver;
import com.unoceros.client.engine.sync.SyncHelper;
import com.unoceros.consumer.lib.ExecutableLoader;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.util.SyncUtility;
import com.unoceros.consumer.utils.EngineUtility;
import com.unoceros.engine.JobManager;
import com.unoceros.kit.JobPayload;
import com.unoceros.kit.exception.IllegalJobRequestException;
import com.unoceros.kit.service.PausableTask;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

public class JobComputeIntent extends IntentService implements JobManager.OnDoneListener{
	private static final String TAG = "JobComputeIntent";
	
	private JobManager manager;
	private Intent workIntent;

	public JobComputeIntent() {
		super("JobComputeIntent");
		manager = JobManager.instance();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return super.onStartCommand(intent,flags,startId);
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		this.workIntent = workIntent;
		
		// Do the job that requires the app to keep the CPU running.
		Bundle extras = workIntent.getExtras();
		boolean isStarting = extras.getBoolean("isStarting", false);
		String message = workIntent.getStringExtra("message");
        String data = workIntent.getStringExtra("data");
        String deviceId = workIntent.getStringExtra("deviceId");

        //get payload object from intent data
        JobPayload payload = parsePushTaskData(deviceId, data, message);

		if(isStarting){
            Log.d(TAG, "Device can compute requested task, it meets compute threshold requirements");
            try {
                //try and start running the requested compute task with payload
                Thread.sleep(20);
                manager.startDoingJob(payload, this);
            } catch (InterruptedException | IllegalJobRequestException e) {
                Log.e(TAG, "Error occurred for job request: ", e);
            }
        }

		if(!isStarting){
            Log.d(TAG, "Device cannot compute requested task, it does not meets compute threshold requirements");
			try {
                //validate if a job request was initiated, but unfortunately this devices
                //might not in the right "can" compute state
                validateNodeComputeState(payload);

                //try and stop any currently running task
				Thread.sleep(20);
                if(manager.isJobRunning())
				    manager.stopDoingJob();
			} catch (InterruptedException | IllegalJobRequestException e) {
                Log.e(TAG, "Error occurred for job request: ", e);
			}
		}
	}

    private JobPayload parsePushTaskData(String deviceId, String data, String message) {
        Log.i(TAG, "Starting task for node: " + deviceId);

        JobPayload payload = new JobPayload();
        try {
            payload.setMessage(message);

            //create json object with data
            JSONObject jobData = null;
            if(data != null && !data.equals(""))
                jobData = new JSONObject(data);

            if (jobData != null) {
                //String bucket = jobData.isNull("bucket")?null:jobData.getString("bucket");
                String projectId = jobData.isNull("projectId") ? null : jobData.getString("projectId");
                String customerId = jobData.isNull("customerId") ? null : jobData.getString("customerId");
                String jobId = jobData.isNull("jobId") ? null : jobData.getString("jobId");
                String pluginId = jobData.isNull("pluginId") ? null : jobData.getString("pluginId");
                String taskInfoId = jobData.isNull("taskInfoId") ? null : jobData.getString("taskInfoId");
                String taskNumber = jobData.isNull("taskNumber") ? null : jobData.getString("taskNumber");
                
                String signedInput = jobData.isNull("signIn") ? null : jobData.getString("signIn");
                String signedExec = jobData.isNull("signEx") ? null : jobData.getString("signEx");
                String signedOutput = jobData.isNull("signOut") ? null : jobData.getString("signOut");

                payload.setCustomerId(customerId);
                payload.setProjectId(projectId);
                payload.setJobId(jobId);
                payload.setPluginId(pluginId);
                payload.setTaskInfoId(taskInfoId);
                
                payload.setSignedInputUrl(signedInput);
                payload.setSignedExecUrl(signedExec);
                payload.setSignedOutputUrl(signedOutput);

                JSONObject filesData = jobData.isNull("files") ? null : jobData.getJSONObject("files");
                if (filesData != null) {
                    String inPath = filesData.isNull("inPath") ? null : filesData.getString("inPath");
                    String inType = filesData.isNull("inType") ? null : filesData.getString("inType");
                    String inEncoding = filesData.isNull("inEncoding") ? null : filesData.getString("inEncoding");
                    String exPath = filesData.isNull("exPath") ? null : filesData.getString("exPath");
                    String exType = filesData.isNull("exType") ? null : filesData.getString("exType");
                    String exEncoding = filesData.isNull("exEncoding") ? null : filesData.getString("exEncoding");

                    payload.setInFilePath(inPath);
                    payload.setInContentType(inType);
                    payload.setInEncoding(inEncoding);
                    payload.setExFilePath(exPath);
                    payload.setExContentType(exType);
                    payload.setExEncoding(exEncoding);

                    //system file path
                    Context context = this.getApplication().getApplicationContext();
                    String jobNameSpace = "task"+File.separator+jobId;
                    String computePath = "compute"; //jobNameSpace+File.separator+"compute"; //"/data/data/"+context.getPackageName()+"/files/compute";
                    String pluginPath = "plugin"; //jobNameSpace+File.separator+"plugin"; //"/data/data/"+context.getPackageName()+"/files/plugins";
                    String inFileName = "input_" + taskNumber;
                    String exFileName = "plugin_" + pluginId; //"executable.jar";
                    String resultFileName = "result_" + taskNumber;

                    File inOutfile = new File(context.getDir(computePath, Context.MODE_PRIVATE), inFileName);
                    payload.setInOutputFileDir(inOutfile);
                    if(inOutfile == null || !inOutfile.exists())
                        Log.e(TAG, "Input File is: "+inOutfile);
                    
                    File resultfile = new File(context.getDir(computePath, Context.MODE_PRIVATE), resultFileName);
                    payload.setResultFileDir(resultfile);
                    if(resultfile == null || !resultfile.exists())
                        Log.e(TAG, "Result File is: "+resultfile);
                    
                    //do this last
                    try {
                        //init executable file prior to loading it class-loader
                        //File exOutfile = new File(context.getDir(pluginPath, Context.MODE_PRIVATE), exFileName);
                        //payload.setExOutputFileDir(exOutfile);
                        
                        ExecutableLoader loader = ExecutableLoader.getInstance(getApplication());
                        ClassLoader classLoader = loader.loadDexPlugin(getApplication(), payload, pluginPath, "plugin", pluginId);
                        payload.setClassLoader(classLoader);
                        File exOutfile = payload.getExOutputFileDir();
                        if(classLoader != null)
                            Log.e(TAG, "Executable ClassLoader is: "+classLoader);
                        
                        if(exOutfile == null || !exOutfile.exists())
                            Log.e(TAG, "Executable File is: "+exOutfile);
                    }catch (Throwable e){ /** MUST BE THROWABLE TO CATCH FATAL ERROR*/
                        //yikes devices could not load executable
                        //TODO report to endpoints
                        Log.e(TAG, "Device could not load an optimized dex plugin: "+pluginId, e);
                        if(payload.getExOutputFileDir() != null)
                            payload.getExOutputFileDir().delete();
                        payload = null;
                    }
                }

                Log.d(TAG, jobData.toString());
            }
        } catch (Exception e1) {
            Log.e(TAG, "Error parsing push task data for payload: ", e1);
            payload = null;
        }
        return payload;
    }

    private void purgePayloadFromDisk(JobPayload payload){
        if(payload != null) {
            Log.d(TAG, "Purging temporary reference for task: "+payload.getTaskInfoId());
            //File input = payload.getInOutputFileDir();
            //if(input != null)
            //    input.delete();
            
            //File output = payload.getInOutputFileDir();
            //if(output != null)
            //    output.delete();
        }
        
    }

    /**
     * Handles request to do task on a node that does not meet compute threshold.
     * @param payload
     */
    private void validateNodeComputeState(JobPayload payload){
        try{
            if(payload != null && payload.getTaskInfoId() != null) {
                ApiServiceHandler endpointServices = ApiServiceHandler.instance(getApplication());
                String deviceId = endpointServices.getDeviceUUID();

                endpointServices.getMessageInfoEndpoint()
                        .computeFailed(deviceId)
                        .setFailedTaskId(payload.getTaskInfoId())
                        .execute();
            }
        }catch (Exception e){
            Log.e(TAG, "Error - notifying backend of failed compute task: ", e);
        }

        purgePayloadFromDisk(payload);
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        releaseWakeLock();
    }

    @Override
    public void onStart() {
        ApiServiceHandler endpointServices = ApiServiceHandler.instance(getApplication());
        boolean canCompute = true;
        boolean isComputing = true;

        //update node
        EngineUtility.updateNodeStatusUpdate(getApplication(), endpointServices, isComputing);

        //report start of compute stats
        SyncHelper sync = new SyncHelper(this.getApplication());
        sync.onStartOfCompute(getApplicationContext(), isComputing, canCompute);

        // Release the wake lock provided by the WakefulBroadcastReceiver.
        releaseWakeLock();
    }

    @Override
	public void onDone(JobPayload payload, long totalTime) {
		//get device status for DeviceInfo
		boolean canCompute = SyncUtility.canCompute(getApplicationContext());
		boolean isComputing = EngineUtility.isComputing(getApplicationContext());

        //sync with remote
		SyncHelper sync = new SyncHelper(this.getApplication());
		sync.onEndOfCompute(getApplicationContext(), payload, totalTime, isComputing, canCompute);

        //notify dispatching system of successful compute and request new task if available
        requestNewTask(getApplication(), sync, payload, false, canCompute);
        purgePayloadFromDisk(payload);
        
		// Release the wake lock provided by the WakefulBroadcastReceiver.
	    releaseWakeLock();
	}

    @Override
    public void onError(Exception exception, JobPayload payload, long totalTime) {
        //get device status for DeviceInfo
        boolean canCompute = SyncUtility.canCompute(getApplicationContext());
        boolean isComputing = EngineUtility.isComputing(getApplicationContext());

        //sync with remote
        SyncHelper sync = new SyncHelper(this.getApplication());
        sync.onEndOfCompute(getApplicationContext(), payload, totalTime, isComputing, canCompute);

        // Release the wake lock provided by the WakefulBroadcastReceiver.
        if(exception != null)
            Log.e(TAG, "Error occurred while performing compute task: " + exception.getMessage(), exception);

        //notify fault tolerant system of failed compute and request new task if available
        requestNewTask(getApplication(), sync, payload, true, canCompute);
        purgePayloadFromDisk(payload);
        
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        releaseWakeLock();
    }

    private static void requestNewTask(Application context, SyncHelper sync, JobPayload payload,
                                       boolean computeFailed,
                                       boolean canCompute){
        try {
            TaskPayloadInfo taskPayloadInfo = JobComputeIntent.getNextScheduleTask(context, payload, computeFailed);

            //TODO pull in next scheduled job payload from remote server to continue doing work
            if(taskPayloadInfo != null && taskPayloadInfo.getPayload() != null) {
                ApiServiceHandler endpointServices = ApiServiceHandler.instance(context);
                String deviceId = endpointServices.getDeviceUUID();

                //deviceInfo is null, lets attempt to sync compute data locally
                boolean isComputing = true; //cause it is about to pull in another compute task
                EngineUtility.updateNodeStatusUpdate(context, endpointServices, isComputing);
                sync.onSyncService(context, null, payload, isComputing, canCompute);

                //update status of dispatched task
                String taskInfoId = taskPayloadInfo.getId();
                taskPayloadInfo.setState(PausableTask.TaskState.COMPUTING.toString());
                Log.d(TAG, "New task dispatched with task id: "+taskInfoId);

                //change taskInfo state
                try {
                    endpointServices.getMessageInfoEndpoint()
                            .updateTaskPayloadInfoState(deviceId, taskInfoId, PausableTask.TaskState.COMPUTING.toString())
                            .execute();
                }catch (IOException e){
                    Log.e(TAG, "Error Updating new dispatch task to COMPUTING state: ", e);
                }

                //compose intent and make a new service request
                String data = taskPayloadInfo.getPayload().getValue();
                String message = "pulled scheduled task";

                Bundle bundle = new Bundle();
                bundle.putBoolean("isStarting", true);
                Intent intent = new Intent(context, JobComputeIntent.class);
                intent.putExtras(bundle);
                intent.putExtra("message", message);
                intent.putExtra("data", data);
                intent.putExtra("deviceId", deviceId);

                SyncForegroundReceiver.startWakefulService(context, intent);
            }else{
                Log.e(TAG, "No scheduled task for node: "+ AppUtility.getUniqueUserID(context));

                //schedule a oneshot remote sync service, remotely(if network allows)
                EngineUtility.startSyncService(context, true, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static TaskPayloadInfo getNextScheduleTask(Application application, JobPayload payload, boolean computeFailed){
        try {
            boolean canCompute = SyncUtility.canCompute(application);
            ApiServiceHandler endpointServices = ApiServiceHandler.instance(application);
            String deviceId = endpointServices.getDeviceUUID();
            MessageEndpoint messageEndpoint = endpointServices.getMessageInfoEndpoint();
            if(!canCompute) {
                //report computed task - either failure of success
                String taskInfoId = payload != null ? payload.getTaskInfoId() : null;
                messageEndpoint.reportProcessedTaskInfo(deviceId)
                        .setCompletedTaskId(computeFailed?null:taskInfoId)
                        .setFailedTaskId(computeFailed?taskInfoId:null)
                        .execute();
            }else {
                if (computeFailed) {
                    String failedId = payload != null ? payload.getTaskInfoId() : null;
                    TaskPayloadInfo taskPayloadInfo = messageEndpoint.pullTaskPayload(deviceId)
                            .setCompletedTaskId(null)
                            .setFailedTaskId(failedId)
                            .execute();

                    Log.e(TAG, "Compute failed task with task id: " + failedId);

                    //don't re-compute failed node
                    if (taskPayloadInfo != null && taskPayloadInfo.getId().equals(failedId))
                        return null;

                    return taskPayloadInfo;
                } else {
                    String completedId = payload != null ? payload.getTaskInfoId() : null;
                    TaskPayloadInfo taskPayloadInfo = messageEndpoint.pullTaskPayload(deviceId)
                            .setCompletedTaskId(completedId)
                            .setFailedTaskId(null)
                            .execute();

                    Log.d(TAG, "Compute completed task with task id: " + completedId);

                    //don't re-compute completed node
                    if (taskPayloadInfo != null && taskPayloadInfo.getId().equals(completedId))
                        return null;

                    return taskPayloadInfo;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "Error occurred pulling in new task", e);
        }

        return null;
    }

    private void releaseWakeLock(){
        if(workIntent != null){
            JobWakefulReceiver.completeWakefulIntent(workIntent);
            GcmBroadcastReceiver.completeWakefulIntent(workIntent);
        }
    }
}
