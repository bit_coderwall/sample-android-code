package com.unoceros.client.engine;

public class Globals{
	
    public static final String TAG = "GLOBAL";

    //SANDBOX
    //public static final String SERVICE_CLIENT_ID = "103587488127-5nt2ncibv73mdeipcfoakh0vvtd72itt.apps.googleusercontent.com";
    //public static final String GCM_SENDER_ID = "103587488127";

    //PRODUCTION
    //public static final String SERVICE_CLIENT_ID = "685865805554-584059pri8pl9hdilivqlr7si9uaihts.apps.googleusercontent.com";
    public static final String SERVICE_CLIENT_ID = "685865805554-b4mbsbs886lfdukl8usk827j86co25jk.apps.googleusercontent.com";
    public static final String GCM_SENDER_ID = "685865805554";

    //GCM Registration
    public static final String GCM_PREFS_NAME = "GCM_PREFS";
    public static final String GCM_PREFS_PROPERTY_REG_ID = "registration_id";
    public static final long GCM_TIME_TO_LIVE = 60L * 60L * 24L * 7L * 4L; // 4 Weeks
    
    //OTHERS
	public static final String PROPERTY_APP_VERSION = "appVersionCode";
	public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
}

