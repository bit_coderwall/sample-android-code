package com.unoceros.client.engine.sync;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.unoceros.client.engine.ApiServiceHandler;
import com.unoceros.client.engine.deviceinfoendpoint.model.DeviceInfo;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;


/**
 * Created by Amit
 */
public class SyncAdProfileHandler extends IntentService {

    private static final String TAG = "SyncAdProfileHandler";

    public SyncAdProfileHandler(){
        super("SyncAdProfileHandler");
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String affiliateId = intent.getStringExtra("affiliateId");

        //End point service
        ApiServiceHandler endpointService = ApiServiceHandler.instance(this.getApplication());

        try {
            //get the device info for this device from the back-end
            DeviceInfo deviceInfo = endpointService.getDeviceInfoEndpoint()
                    .getDeviceInfo(endpointService.getDeviceUUID()).execute();

            if(deviceInfo != null){
                //if affiliate id is not null
                if(!TextUtils.isEmpty(affiliateId)){
                    //save to local store
                    SharedPreferences mPref = getApplicationContext().getSharedPreferences(AppUtility.SYNC_PREF, Context.MODE_PRIVATE);
                    mPref.edit().putString("affiliateId", affiliateId).apply();

                    //update with new id and post back to endpoint
                    deviceInfo.setTrackingID(affiliateId);
                    endpointService.updateDeviceInfo(deviceInfo);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        SyncForegroundReceiver.completeWakefulIntent(intent);
    }
}
