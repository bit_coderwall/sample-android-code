package com.unoceros.client.engine.sync;

import java.io.IOException;
import java.util.Date;

import android.app.Application;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.api.client.util.DateTime;
import com.unoceros.client.engine.ApiServiceHandler;
import com.unoceros.client.engine.deviceinfoendpoint.model.DeviceInfo;
import com.unoceros.client.engine.deviceinfoendpoint.model.UserInfo;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.util.SyncUtility;
import com.unoceros.consumer.utils.EngineUtility;
import com.unoceros.engine.JobManager;
import com.unoceros.kit.JobPayload;
import com.unoceros.kit.service.ApplicationVersion;

public class SyncIntentService extends IntentService{

	private static final String TAG = "AlarmIntentService";

	public SyncIntentService() {
		super("AlarmIntentService");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return super.onStartCommand(intent,flags,startId);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
        //End point service
        ApiServiceHandler endpointService = ApiServiceHandler.instance(this.getApplication());

        //extras and context
        //Bundle extras = intent.getExtras();
        Application application = this.getApplication();

        //handleDeviceUpdate
        handleDeviceUpdate(application, intent, endpointService);

        //handleNodeUpdate
        boolean isComputing = EngineUtility.isComputing(application.getApplicationContext());
        EngineUtility.updateNodeStatusUpdate(application, endpointService, isComputing);
    }

    private void handleDeviceUpdate(Application application, Intent intent, ApiServiceHandler endpointService){
		Context context = application.getApplicationContext();
        DeviceInfo deviceInfo = null;
		try {
			//get the device info for this device from the back-end
			deviceInfo = endpointService.getDeviceInfoEndpoint()
					.getDeviceInfo(endpointService.getDeviceUUID()).execute();
		} catch (IOException e) {
            Log.e(TAG, "Error getting deviceInfo object from endpoint", e);
        }

        //get device status for DeviceInfo
        boolean canCompute = SyncUtility.canCompute(context);
        boolean isComputing = EngineUtility.isComputing(context);
        boolean isActive = (canCompute && !isComputing);

		if(deviceInfo != null){
			// Notify end-points with persistent data
			// Requires the app to keep the CPU running for a little bit.

			//update deviceInfo
			deviceInfo.setCanCompute(canCompute)
				.setIsComputing(isComputing)
				.setIsActive(isActive)
                .setVersion(endpointService.getDeviceUUID());

            //update affiliate id
            String affiliateId = intent.getStringExtra("affiliateId");
            if(deviceInfo.getTrackingID() == null)
                deviceInfo.setTrackingID(affiliateId);

            //update userInfo
			UserInfo user = deviceInfo.getUser(); //google endpoints
            com.unoceros.consumer.model.UserInfo userInfo = AppController.getInstance().getUserInfo();//unoceros model
			if(user == null){
                String userId = userInfo.getUserId()==null?AppUtility.getUniqueUserID(context):deviceInfo.getDeviceInfoID();
				user = new UserInfo();
				user.setId(userId) //set the unique user id
                    .setCreatedDate(new DateTime(new Date())); //and created sdate
				deviceInfo.setUser(user);

			}
            String userName = userInfo.getUserName();
            String userImage = userInfo.getUserImageUrl();
            String userEmail = userInfo.getUserEmail();
            final SharedPreferences prefs = context.getSharedPreferences(AppUtility.INSTALLATION_PREF_FILE, MODE_PRIVATE);
            if(userName == null || userName.equals(""))
                userName = prefs.getString("username", null);
            if(userEmail == null || userEmail.equals(""))
                userEmail = prefs.getString("useremail", null);
            if(userImage == null || userImage.equals(""))
                userImage = prefs.getString("userimage", null);
            user.setName(userName)
				.setEmail(userEmail)
				.setAgeRange(userInfo.getAge() + "")
                .setImageURL(userImage);
			
			//now try posting device update back to the end-points
			try {
				//should there be some randomness to this?? 
				//any reason for denial of service??
				//potentially many devices will be schedule to post back as this specific time
                String appVersion = AppUtility.getVersionName(application);
                String engineVersion = String.valueOf(ApplicationVersion.version);
                deviceInfo.setAppVersion(appVersion);
                deviceInfo.setEngineVersion(engineVersion);
				endpointService.updateDeviceInfo(deviceInfo);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			try{
				Thread.sleep(2000);
				//sync computed credit bank info and compute time info back to the endpoints
				//not this has to be done after the update to the deviceInfo otherwise the update will
				//eventually wipe away any update

                JobPayload payload = JobManager.instance().getPayload();
                SyncHelper sync = new SyncHelper(this.getApplication());
                sync.onSyncService(context, deviceInfo, payload, isComputing, canCompute);
                /*if(!canCompute) {
                    //just notify user to charge their phone???
                    NotificationHelper.notifyPaused(context);
                }*/
			}catch(Exception e){
				e.printStackTrace();
			}
		}else{
            //deviceInfo is null, lets attempt to sync data locally
            JobPayload payload = JobManager.instance().getPayload();
            SyncHelper sync = new SyncHelper(this.getApplication());
            sync.onSyncService(context, deviceInfo, payload, isComputing, canCompute);
        }
		
		//complete task
		SyncForegroundReceiver.completeWakefulIntent(intent);
	}
}
