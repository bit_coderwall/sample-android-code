package com.unoceros.client.engine;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.TextUtils;

import com.unoceros.client.engine.sync.SyncForegroundReceiver;
import com.unoceros.consumer.adapter.PushMessageListAdapter;
import com.unoceros.consumer.model.PushMessage;
import com.unoceros.consumer.model.db.PushMessagesDAO;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.util.NotificationHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver{

    private static final String TAG = "GcmBroadcastReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals("com.google.android.c2dm.intent.REGISTRATION")) {
            handleRegistration(context, intent);
        } else if (intent.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) {
            handleMessage(context, intent);
        }
	}

    private void handleMessage(Context context, Intent intent){
        //Assume job notification intent
        if(!AppUtility.haveNetworkConnection(context)){
            //message for notification
            String msg = context.getString(R.string.no_network_when_job_request_occurred);
            //NotificationHelper.notifyNoInternet(context, msg, true);
            Log.d(TAG, msg);
        }else {
            String message = intent.getStringExtra("message");
            String testing = intent.getStringExtra("testing");
            String jobData = intent.getStringExtra("data");
            if(!TextUtils.isEmpty(message) && TextUtils.isEmpty(jobData)){
                //direct push notification that will be displaced to the user
                PushMessage pushMessage = new PushMessage();
                try {
                    /**
                     * {"key":{"kind":"MessageData","id":0,"name":"ea6bbaa5-39c9-4d88-836c-22d525e8e85e"},
                     * * "message":"Please download the new app!",
                     * * "details":{"value":"Yo people, Unoceros just released a new version of the compute engine app, please take time to update to the latest and take full advantages of the new features.  It\u0027s full of new features and performance improvement. Thanks, happy earning power.  Check http://unoceros.com"},
                     * * "timestamp":1424514689436}* 
                     */
                    JSONObject jsonObject = new JSONObject(message);
                    if(jsonObject != null){
                        String messageId = jsonObject.isNull("key") ? null : jsonObject.getJSONObject("key").getString("name");
                        String messageHeadline = jsonObject.isNull("message") ? null : jsonObject.getString("message");
                        String messageDetails = jsonObject.isNull("details") ? null : jsonObject.getJSONObject("details").getString("value");
                        Long timestamp = jsonObject.isNull("timestamp") ? null : jsonObject.getLong("timestamp");
                        pushMessage.setMessageId(messageId);
                        pushMessage.setHeadline(messageHeadline);
                        pushMessage.setDetails(messageDetails);
                        pushMessage.setTimestamp(timestamp);
                        pushMessage.setColor(PushMessageListAdapter.getRandomColor(messageHeadline));
                        
                        PushMessagesDAO persistentDAO = new PushMessagesDAO(context);
                        persistentDAO.open();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        String date = sdf.format(new Date());
                        persistentDAO.insertByDate(date, pushMessage, true);
                        persistentDAO.close();
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
                
                Bundle messageBundle = new Bundle();
                messageBundle.putParcelable("pushMessage", pushMessage);
                NotificationHelper.notifyPushMessage(context, messageBundle);
            }else {
                if(AppUtility.signedWithDebugKey(context) || AppUtility.isDebuggable(context)
                        || "true".equalsIgnoreCase(testing+"".trim())) {
                    //start the compute engine to do a job task defined in the intent
                    String messageId = UUID.randomUUID().toString(); //intent.getStringExtra("messageId");
                    PushMessage pushMessage = new PushMessage();
                    pushMessage.setMessageId(messageId);
                    pushMessage.setHeadline("Unoceros Beta [benchmarking test in progress]");
                    pushMessage.setDetails("Notification can be turned off from within the Settings Panel: \n" + jobData);
                    pushMessage.setTimestamp((new Date().getTime()));
                    pushMessage.setColor(PushMessageListAdapter.getRandomColor("Unoceros"));
                    
                    PushMessagesDAO persistentDAO = new PushMessagesDAO(context);
                    persistentDAO.open();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    String date = sdf.format(new Date());
                    persistentDAO.insertByDate(date, pushMessage, true);
                    persistentDAO.close();
                    
                    Bundle messageBundle = new Bundle();
                    messageBundle.putParcelable("pushMessage", pushMessage);
                    NotificationHelper.notifyPushMessage(context, messageBundle);
                }
                
                SyncForegroundReceiver.startJobIntent(context, intent);
            }
        }
    }

    private void handleRegistration(Context context, Intent intent){
        String registration = intent.getStringExtra("registration_id");
        if (intent.getStringExtra("error") != null) {
            // Registration failed, should try again later.
        } else if (intent.getStringExtra("unregistered") != null) {
            // unregistration done, new messages from the authorized sender will be rejected
        } else if (registration != null) {
            // Send the registration ID to the 3rd party site that is sending the messages.
            // This should be done in a separate thread.
            // When done, remember that all registration is done.

            // update the intent to gcm registration handler
            Intent service = new Intent(context, GcmRegistrationHandler.class);
            // Start the service, keeping the device awake while the service is
            // launching.
            GcmBroadcastReceiver.startWakefulService(context, service);
        }
    }
}
