package com.unoceros.client.engine.sync;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.unoceros.client.engine.RemoteRewardService;
import com.unoceros.client.engine.RemoteStreamingService;
import com.unoceros.client.task.JobComputeIntent;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.SyncUtility;
import com.unoceros.consumer.utils.EngineUtility;

public class SyncForegroundReceiver extends WakefulBroadcastReceiver{
	@Override
	public void onReceive(Context context, Intent intent) {
        // Get extra data included in the Intent
        String action = intent.getAction();
        if(AppUtility.SYNC_ACTION_SETUP.equalsIgnoreCase(action)) {
            //setup sync service
            //String message = intent.getStringExtra("message");
            EngineUtility.startSyncService(context, true, false);
        }else if(AppUtility.SYNC_ACTION_SERVICE.equalsIgnoreCase(action)) {
            //analytics tracker
            UserInfo userInfo = AppController.getInstance().getUserInfo();
            TrackingManager.getInstance(AppController.getInstance()).trackComputeAvailability(userInfo);

            boolean canCompute = SyncUtility.canCompute(context);
            boolean jobStateIsStopped = EngineUtility.jobStateIsStopped();
            if(canCompute && jobStateIsStopped) {
                //attempt to resume a job
                SyncForegroundReceiver.startJobIntent(context, intent);
            }else if(!canCompute){
                //attempt to stop a job
                SyncForegroundReceiver.stopJobIntent(context, intent);
            }

            //start wake-full intent to sync with remote service
            /*if(!AppUtility.haveNetworkConnection(context)) {
                //message for notification
                String msg = context.getString(R.string.no_network_when_scheduled_sync_occurred);
                NotificationHelper.notifyNoInternet(context, msg, true);
            }*/

            //lets attempt to sync computing stats, stats will be save locally if
            //remote syncing has issues
            Intent service = new Intent(context, SyncIntentService.class);
            SyncForegroundReceiver.startWakefulService(context, service);
        }else if(AppUtility.SYNC_ACTION_PARTNER_CREATED.equalsIgnoreCase(action)){
            //an affiliate profile was created, the id is included in the intent
            String affiliateId = intent.getStringExtra("affiliateId");
            Intent service = new Intent(context, SyncAdProfileHandler.class);
            service.putExtra("affiliateId", affiliateId);
            SyncForegroundReceiver.startWakefulService(context, service);
        }else if(AppUtility.SYNC_ACTION_CREDIT_WITHDRAW_MADE.equalsIgnoreCase(action)){
            //a credit withdraw request, perhaps a user claimed an offer
            double amount = intent.getDoubleExtra("amount", 0);
            String transactionId = intent.getStringExtra("transactionId");
            Intent service = new Intent(context, SyncCreditWithdrawHandler.class);
            service.putExtra("amount", amount);
            service.putExtra("transactionId", transactionId);
            SyncForegroundReceiver.startWakefulService(context, service);
        }else if(AppUtility.ACTION_GET_ACTIVITY_STREAMING.equalsIgnoreCase(action)){
            int limit = intent.getIntExtra("limit", 5);
            int offset = intent.getIntExtra("offset", 0);
            String order = intent.getStringExtra("order");
            Intent service = new Intent(context, RemoteStreamingService.class);
            service.putExtra("limit", limit);
            service.putExtra("offset", offset);
            service.putExtra("order", order);
            SyncForegroundReceiver.startWakefulService(context, service);
        }else if(AppUtility.ACTION_GET_REWARD_REDEMPTION_CODE.equalsIgnoreCase(action)){
            Campaign campaign = intent.getParcelableExtra("campaign");
            String transactionId = intent.getStringExtra("transactionId");
            Intent service = new Intent(context, RemoteRewardService.class);
            service.setAction(AppUtility.ACTION_GET_REWARD_REDEMPTION_CODE);
            Bundle extras = new Bundle();
            extras.putParcelable("campaign", campaign);
            extras.putString("transactionId", transactionId);
            service.putExtras(extras);
            SyncForegroundReceiver.startWakefulService(context, service);
        }else if(AppUtility.ACTION_INSERT_OFFER_BY_DATE.equalsIgnoreCase(action)){
            Campaign campaign = intent.getParcelableExtra("campaign");
            String date = intent.getStringExtra("date");
            String uuid = intent.getStringExtra("uuid");
            String transactionId = intent.getStringExtra("transactionId");
            Intent service = new Intent(context, RemoteRewardService.class);
            service.setAction(AppUtility.ACTION_INSERT_OFFER_BY_DATE);
            Bundle extras = new Bundle();
            extras.putParcelable("campaign", campaign);
            extras.putString("date", date);
            extras.putString("uuid", uuid);
            extras.putString("transactionId", transactionId);
            service.putExtras(extras);
            SyncForegroundReceiver.startWakefulService(context, service);
        }else if(AppUtility.ACTION_SYNC_REMOTE_OFFER_WITH_DEVICE.equalsIgnoreCase(action)){
            Intent service = new Intent(context, RemoteRewardService.class);
            service.setAction(AppUtility.ACTION_SYNC_REMOTE_OFFER_WITH_DEVICE);
            SyncForegroundReceiver.startWakefulService(context, service);
        }
	}

    public static void startJobIntent(Context context, Intent intent){
        //String action = intent.getAction();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
        String messageType = gcm.getMessageType(intent);
        // Filter messages based on message type. It is likely that GCM will be extended in the future
        // with new message types, so just ignore message types you're not interested in, or that you
        // don't recognize.
        if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
            // It's an error.
        } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
            // Deleted messages on the server.
        } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
            // It's a regular GCM message, do some work.

            //This is the Intent to deliver to the intent service, in our case
            //intent to compute job request.
            Intent service = new Intent(context, JobComputeIntent.class);
            Bundle extras = intent.getExtras();
            extras.putBoolean("isStarting", true);
            service.putExtras(extras);

            // Start the service, keeping the device awake while the service is
            // launching.
            startWakefulService(context, service);
        }
    }

    public static void stopJobIntent(Context context, Intent intent){
        //String action = intent.getAction();

        //This is the Intent to deliver to the intent service, in our case
        //intent to compute job request.
        Intent service = new Intent(context, JobComputeIntent.class);
        Bundle extras = intent.getExtras();
        extras.putBoolean("isStarting", false);
        service.putExtras(extras);

        // Start the service, keeping the device awake while the service is
        // launching.
        startWakefulService(context, service);
    }
}
