package com.unoceros.client.engine;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.unoceros.consumer.util.Log;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

public class GcmRegistrationHandler extends IntentService {

	private Handler handler;
	public GcmRegistrationHandler() {
		super("GcmMessageHandler");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		handler = new Handler();
	}
	
	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();

		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);
		String message = "Received : (" + messageType + ")  " + extras.getString("title");
		showToast(message);
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	public void showToast(final String message){
		handler.post(new Runnable() {
			public void run() {

                Log.i("GCM", message);
			}
		});

	}
}
