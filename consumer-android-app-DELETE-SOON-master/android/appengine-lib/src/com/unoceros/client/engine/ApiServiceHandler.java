package com.unoceros.client.engine;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.regex.Pattern;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Patterns;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.unoceros.client.engine.computetimeendpoint.Computetimeendpoint;
import com.unoceros.client.engine.creditbankendpoint.Creditbankendpoint;
import com.unoceros.client.engine.creditbankendpoint.model.CreditBank;
import com.unoceros.client.engine.depositmapendpoint.Depositmapendpoint;
import com.unoceros.client.engine.deviceinfoendpoint.Deviceinfoendpoint;
import com.unoceros.client.engine.deviceinfoendpoint.model.DeviceInfo;
import com.unoceros.client.engine.messageEndpoint.MessageEndpoint;
import com.unoceros.client.engine.nodestatusinfoendpoint.Nodestatusinfoendpoint;
import com.unoceros.client.engine.offerinfoendpoint.Offerinfoendpoint;
import com.unoceros.client.engine.rewardcodeinfoendpoint.Rewardcodeinfoendpoint;
import com.unoceros.consumer.InviteOnlyActivity;
import com.unoceros.consumer.SignUpSlickblueActivity;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.services.ApplicationServices;
import com.unoceros.consumer.services.CreditServices;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.DeviceUuidFactory;
import com.unoceros.consumer.util.Log;
import com.unoceros.kit.service.ApplicationVersion;

public class ApiServiceHandler {

	private static final String TAG = "ApiServiceHandler";
	private Deviceinfoendpoint deviceInfoEndpoint;
	private Creditbankendpoint creditbankEndpoint;
	private Depositmapendpoint depositEndpoint;
    private Computetimeendpoint computeEndpoint;
    private Rewardcodeinfoendpoint rewardsEndpoint;
    private Offerinfoendpoint offersEndpoint;
    private Nodestatusinfoendpoint nodeStatusEndpoint;
    private MessageEndpoint messageEndpoint;

	private DeviceUuidFactory deviceUUID;
	
	private static ApiServiceHandler instance;
	
	private ApiServiceHandler(Application application){
		//unique user id
		deviceUUID = DeviceUuidFactory.instance(application);
		
		//set application name
		String applicationName = application.getString(R.string.app_name);

        //OAuth credentials
        HttpRequestInitializer credential = ApiServiceHandler.createCredentialFromDevice(application);
        /*
        if(AppUtility.signedWithDebugKey(application)) {
            credential = new HttpRequestInitializer() {
                public void initialize(HttpRequest httpRequest) {
                }
            };
        }*/
        
		//deviceInfoEndpoint
		Deviceinfoendpoint.Builder deviceEndpointBuilder = new Deviceinfoendpoint.Builder(
				AndroidHttp.newCompatibleTransport(), new JacksonFactory(), credential).setApplicationName(applicationName);
		deviceInfoEndpoint = CloudEndpointUtils.updateBuilder(deviceEndpointBuilder).build();
		
		//CreditbankEndpoint
        Creditbankendpoint.Builder creditbankEndpointBuilder = new Creditbankendpoint.Builder(
                AndroidHttp.newCompatibleTransport(), new JacksonFactory(), credential).setApplicationName(applicationName);
		creditbankEndpoint = CloudEndpointUtils.updateBuilder(creditbankEndpointBuilder).build();

        //Computetimeendpoint
        Computetimeendpoint.Builder computeTimeEndpointBuilder = new Computetimeendpoint.Builder(
                AndroidHttp.newCompatibleTransport(), new JacksonFactory(), credential).setApplicationName(applicationName);
        computeEndpoint = CloudEndpointUtils.updateBuilder(computeTimeEndpointBuilder).build();

        //DepositEndpoint
		Depositmapendpoint.Builder depositEndpointBuilder = new Depositmapendpoint.Builder(
				AndroidHttp.newCompatibleTransport(), new JacksonFactory(), credential).setApplicationName(applicationName);
		depositEndpoint = CloudEndpointUtils.updateBuilder(depositEndpointBuilder).build();

        //RewardsCodeEndpoint
        Rewardcodeinfoendpoint.Builder rewardsEndpointBuilder = new Rewardcodeinfoendpoint.Builder(
                AndroidHttp.newCompatibleTransport(), new JacksonFactory(), credential).setApplicationName(applicationName);
        rewardsEndpoint = CloudEndpointUtils.updateBuilder(rewardsEndpointBuilder).build();

        //OffersEndpoint
        Offerinfoendpoint.Builder offersEndpointBuilder = new Offerinfoendpoint.Builder(
                AndroidHttp.newCompatibleTransport(), new JacksonFactory(), credential).setApplicationName(applicationName);
        offersEndpoint = CloudEndpointUtils.updateBuilder(offersEndpointBuilder).build();

        //NodeStatusEndpoint
        Nodestatusinfoendpoint.Builder nodeStatusEndpointBuilder = new Nodestatusinfoendpoint.Builder(
                AndroidHttp.newCompatibleTransport(), new JacksonFactory(), credential).setApplicationName(applicationName);
        nodeStatusEndpoint = CloudEndpointUtils.updateBuilder(nodeStatusEndpointBuilder).build();

        //MessageEndpoint
        MessageEndpoint.Builder messageEndpointBuilder = new MessageEndpoint.Builder(
                AndroidHttp.newCompatibleTransport(), new JacksonFactory(), credential).setApplicationName(applicationName);
        messageEndpoint = CloudEndpointUtils.updateBuilder(messageEndpointBuilder).build();
    }

	public static ApiServiceHandler instance(Application application){
		if(instance == null)
			instance = new ApiServiceHandler(application);
		
		return instance;
	}

    public static String getAccountName(Context context){
        //Get all accounts from my Android Phone
        String validGoogleEmailAccount = null;
        String validGoogleUserName = null;
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(context).getAccountsByType("com.google"); //.getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                //Just store mail if contain gmail.com
                validGoogleUserName = account.name;
                if (account.name.contains("gmail.com") && account.type.contains("com.google")){
                    validGoogleEmailAccount = account.name;
                }
            }
        }

        //check if user name contains @gmail.com
        if(validGoogleUserName!=null && !validGoogleUserName.contains("@")){
            validGoogleUserName = validGoogleUserName+"@gmail.com";
        }
        Log.d(TAG, "Google Play Service Account User: "+validGoogleUserName);

        //validated authentication account
        if(validGoogleEmailAccount == null && validGoogleUserName != null){
            validGoogleEmailAccount = validGoogleUserName;
        }

        Log.d(TAG, "Google Play Service Account Name: " + validGoogleEmailAccount);
        return validGoogleEmailAccount;
    }

    public static GoogleAccountCredential createCredentialFromDevice(Context context){
        String validGoogleEmailAccount = getAccountName(context);

        //Build Credential with valid google account
        GoogleAccountCredential credential = GoogleAccountCredential.usingAudience(context,
                "server:client_id:"+Globals.SERVICE_CLIENT_ID);
        credential.setSelectedAccountName(validGoogleEmailAccount);
        
        return credential;
    }
    
    public static GoogleCredential createCredentialForServiceAccount(
            HttpTransport transport,
            JacksonFactory jsonFactory,
            String serviceAccountId,
            Collection<String> serviceAccountScopes,
            File p12File) throws GeneralSecurityException, IOException {

        return new GoogleCredential.Builder().setTransport(transport)
                .setJsonFactory(jsonFactory)
                .setServiceAccountId(serviceAccountId)
                .setServiceAccountScopes(serviceAccountScopes)
                .setServiceAccountPrivateKeyFromP12File(p12File)
                .build();
    }

    public static GoogleCredential createCredentialForServiceAccountImpersonateUser(
            HttpTransport transport,
            JacksonFactory jsonFactory,
            String serviceAccountId,
            Collection<String> serviceAccountScopes,
            File p12File,
            String serviceAccountUser) throws GeneralSecurityException, IOException {

        return new GoogleCredential.Builder().setTransport(transport)
                .setJsonFactory(jsonFactory)
                .setServiceAccountId(serviceAccountId)
                .setServiceAccountScopes(serviceAccountScopes)
                .setServiceAccountPrivateKeyFromP12File(p12File)
                .setServiceAccountUser(serviceAccountUser)
                .build();
    }

    /**
     * Build auhtentication token from AccountManager
     */
    public static void buildAuthToken(final Activity activity, final String accountName,
                                  final boolean invalidateCurrent){

        final AccountManager mgr = AccountManager.get(activity);
        Account account;
        if (accountName == null)
            account = mgr.getAccountsByType("com.google")[0];
        else
            account = new Account(accountName,"com.google");

        final String accountType = account.type;
        String authTokeType = "oauth2:https://www.googleapis.com/auth/userinfo.email"; //"ah";
        final AccountManagerFuture<Bundle> accountManagerFuture =
                mgr.getAuthToken(account, authTokeType, null, activity, null, null);

        new AsyncTask<Void, Integer, Bundle>() {
            @Override
            protected Bundle doInBackground(Void... params) {
                Bundle authTokenBundle = null;
                try {
                    authTokenBundle = accountManagerFuture.getResult();
                }catch (Exception e){
                    Log.e(TAG, e.getMessage(), e);
                }
                return authTokenBundle;
            }

            @Override
            protected void onPostExecute(Bundle authTokenBundle) {
                if(authTokenBundle != null) {
                    String authToken = authTokenBundle.get(AccountManager.KEY_AUTHTOKEN).toString();
                    Log.d(TAG, authToken);
                    if (invalidateCurrent) {
                        mgr.invalidateAuthToken(accountType, authToken);
                        buildAuthToken(activity, accountName, false);
                    }
                    //Todo store authToken preference file
                }
            }
        }.execute();

    }

    public Deviceinfoendpoint getDeviceInfoEndpoint() {
		return deviceInfoEndpoint;
	}

	public Creditbankendpoint getCreditBankInfoEndpoint() {
		return creditbankEndpoint;
	}
	
	public Depositmapendpoint getDepositMapEndpoint() {
		return depositEndpoint;
	}

    public Computetimeendpoint getComputeTimeEndpoint() {
        return computeEndpoint;
    }

    public Rewardcodeinfoendpoint getRewardCodeInfoEndpoint() {
        return rewardsEndpoint;
    }

    public Offerinfoendpoint getOfferInfoEndpoint() {
        return offersEndpoint;
    }

    public Nodestatusinfoendpoint getNodeStatusInfoEndpoint() {
        return nodeStatusEndpoint;
    }

    public MessageEndpoint getMessageInfoEndpoint() {
        return messageEndpoint;
    }

	public String getDeviceUUID(){
		return deviceUUID.getDeviceUuid().toString();
	}
	
	/**
	 * Called back when a registration token has been received from the Google
	 * Cloud Messaging service.
	 * 
	 * Sends the registration ID to the server via endpoint
	 * 
	 * @param context
	 *            the Context
	 */
	public void sendRegistrationIdToBackend(Context context, String registrationID){
		boolean alreadyRegisteredWithEndpointServer = false;
        boolean registrationIDUpdatedWithEndpointServer = false;
        String msg = "none";
        boolean isError = false;
		try {
			/*
			 * Using cloud endpoints, see if the device has already been
			 * registered with the backend
			 */
			DeviceInfo existingInfo = getDeviceInfoEndpoint().getDeviceInfo(getDeviceUUID()).execute();

			if (existingInfo != null && registrationID.equals(existingInfo.getRegistrationID())) {
                alreadyRegisteredWithEndpointServer = true;
            }

            if(existingInfo != null){
                //save affiliate id to local store
                String affiliateId = existingInfo.getTrackingID();
                SharedPreferences mPref = context.getSharedPreferences(AppUtility.SYNC_PREF, Context.MODE_PRIVATE);
                mPref.edit().putString("affiliateId", affiliateId).apply();

                //Sync With Credit Services
                CreditBank bank = getCreditBankInfoEndpoint().getCreditBank(existingInfo.getDeviceInfoID()).execute();
                if(bank != null) {
                    double overallBalance = bank.getOverallTotalBalance() != null ? bank.getOverallTotalBalance() : 0.0;
                    double totalBalance = bank.getTotalBalance() != null ? bank.getTotalBalance() : 0.0;
                    double todayBalance = bank.getTodayBalance() != null ? bank.getTodayBalance() : 0.0;
                    double todayAverage = bank.getAverage() != null ? bank.getAverage() : 0.0;

                    //get a handle to the credit service/bank account
                    //and attempt to sync with remote info
                    CreditServices creditServices = ApplicationServices.instance().getCreditService();
                    creditServices.invokedOverrideByAdmin(overallBalance, totalBalance, todayBalance, todayAverage, 0.0);
                }else{
                    //hmm? shouldn't happen, but regardless less create a new remote credit account
                    //init new credit banking
                    initializeNewCreditBank(context);
                }

                //update registration for this device
                registrationIDUpdatedWithEndpointServer = true;
                existingInfo.setRegistrationID(registrationID);

                //update device profile
                getDeviceInfoEndpoint().addDeviceInfo(existingInfo).execute();
                Log.i(TAG, existingInfo.toPrettyString());
            }
		} catch (Exception e) {
			//Authentication error
            msg = "Registration Error: Trying to authenticate with Unoceros network.";
		}

		try {
			if (!alreadyRegisteredWithEndpointServer && !registrationIDUpdatedWithEndpointServer) {
				/*
				 * We are not registered as yet. Send an endpoint message
				 * containing the GCM registration id and some of the device's
				 * product information over to the backend. Then, we'll be
				 * registered.
				 */
                String arch = System.getProperty("os.arch");
				DeviceInfo deviceInfo = new DeviceInfo();
				deviceInfo.setDeviceInfoID(getDeviceUUID())
						.setRegistrationID(registrationID)
						.setTimestamp(System.currentTimeMillis())
						.setBrand(URLEncoder.encode(android.os.Build.BRAND, "UTF-8"))
						.setCpu(arch)
						.setManufacturer(URLEncoder.encode(android.os.Build.MANUFACTURER, "UTF-8"))
						.setModel(URLEncoder.encode(android.os.Build.MODEL, "UTF-8"))
						.setVersion(URLEncoder.encode(android.os.Build.VERSION.CODENAME, "UTF-8"))
                        .setAppVersion(AppUtility.getVersionName(AppController.getInstance()))
                        .setEngineVersion(String.valueOf(ApplicationVersion.version))
                        .setCanCompute(false)
                        .setIsComputing(false)
                        .setIsActive(false);

                //init new credit banking
                initializeNewCreditBank(context);

                //do this last in case it fails
                getDeviceInfoEndpoint().addDeviceInfo(deviceInfo).execute();
                Log.i(TAG, deviceInfo.toPrettyString());
			}
		} catch (Exception e) {
            msg = "Registration Error: "+e.getMessage();
            isError = true;
			Log.e(TAG, msg +", "+getDeviceInfoEndpoint().getRootUrl(), e);
		}

        //broadcast registration intent
        Intent intent = new Intent("registration");
        intent.putExtra("regId", registrationID);
        intent.putExtra("message", msg);
        intent.putExtra("isError", isError);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
	}

    private void initializeNewCreditBank(Context context){
        //Credit device with initial unicoins
        double initialTotal = context.getResources().getFraction(R.fraction.unoceros_initial_credit_deposit, 10, 10);
        CreditBank bank = new CreditBank();
        bank.setId(getDeviceUUID());
        bank.setTotalBalance(initialTotal);
        bank.setOverallTotalBalance(initialTotal);

        try {
            getCreditBankInfoEndpoint().insertCreditBank(bank).execute();
            Log.i(TAG, bank.toPrettyString());
        } catch (IOException e) {
            //do nothing...
        }
        //this is a new install, initialize local credit details as well
        //get a handle to the credit service/bank account
        CreditServices creditServices = ApplicationServices.instance().getCreditService();
        creditServices.initializedByManagement(initialTotal);
    }

	/**
	 * Updated the device info in the back-end
	 * @param deviceInfo
	 * @throws IOException
	 */
	public void updateDeviceInfo(DeviceInfo deviceInfo) throws IOException{
		getDeviceInfoEndpoint().addDeviceInfo(deviceInfo).execute();
		Log.i(TAG, deviceInfo.toPrettyString());
	}

}
