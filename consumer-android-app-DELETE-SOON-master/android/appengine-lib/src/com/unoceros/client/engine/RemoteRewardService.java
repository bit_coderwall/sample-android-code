package com.unoceros.client.engine;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.unoceros.client.engine.offerinfoendpoint.Offerinfoendpoint;
import com.unoceros.client.engine.offerinfoendpoint.model.CollectionResponseOfferInfo;
import com.unoceros.client.engine.offerinfoendpoint.model.OfferInfo;
import com.unoceros.client.engine.rewardcodeinfoendpoint.Rewardcodeinfoendpoint;
import com.unoceros.client.engine.rewardcodeinfoendpoint.model.TangoCardInfo;
import com.unoceros.client.engine.sync.SyncForegroundReceiver;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.db.ClaimedRewardsDAO;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;

import org.json.JSONObject;

/**
 * Created by Amit
 */
public class RemoteRewardService extends IntentService {
    private static final String TAG = "RemoteRewardService";

    public RemoteRewardService() {
        super("RemoteRewardService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        Bundle extras = intent.getExtras();

        if(AppUtility.ACTION_GET_REWARD_REDEMPTION_CODE.equalsIgnoreCase(action)) {
            Campaign campaign = extras.getParcelable("campaign");
            String transactionId = extras.getString("transactionId");
            
            //create local intent
            Intent localIntent = new Intent("rewards-code-redemption");
            localIntent.putExtras(extras);

            try {
                //get end point service
                ApiServiceHandler endpointService = ApiServiceHandler.instance(this.getApplication());

                //try obtaining this reward
                String deviceId = endpointService.getDeviceUUID();
                String rewardType = campaign.getTitle();
                double creditValue = AppUtility.calculateCredits(this, campaign);
                double rewardAmount = AppUtility.calculateRewardAmount(this, campaign); //1;
                Log.d(TAG, "Redeeming rewardType: " + rewardType + ", amount: " + rewardAmount + ", for deviceId: " + deviceId);
                Rewardcodeinfoendpoint.GetRewardByStatus status = endpointService.getRewardCodeInfoEndpoint()
                        .getRewardByStatus(deviceId, rewardType, rewardAmount)
                        .setCreditValue(creditValue)
                        .setClientAppVersion(AppUtility.getVersionName(getApplication()))
                        .setTransactionId(transactionId);
                TangoCardInfo info = status.execute();
                boolean success = false;
                if (info != null) {
                    String error = info.getError();
                    //Log.d(TAG, "Reward Error Log: " + error);
                    if (TextUtils.isEmpty(error)) {
                        //no error string - assume success
                        success = true;
                    } else {
                        //check error json string to see if it contains an error key-value pair
                        JSONObject json = new JSONObject(error);
                        if (json != null && !json.isNull("success") && json.getBoolean("success")) {
                            //no specific error key-value pair - assume success
                            success = true;
                        }
                    }
                }

                //set success to true
                localIntent.putExtra("success", success);
                if (success)
                    Log.d(TAG, "Reward redemption successfully executed");
                else
                    Log.d(TAG, "Reward redemption failed");
            } catch (Exception e) {

                //set success to false
                localIntent.putExtra("success", false);
                Log.e(TAG, "Reward redemption failed: ", e);
            }

            //broadcast streaming intent
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        } else if(AppUtility.ACTION_INSERT_OFFER_BY_DATE.equalsIgnoreCase(action)){
            //get the rest of the data from the bundle
            Campaign campaign = extras.getParcelable("campaign");
            String date = extras.getString("date");
            String uuid = extras.getString("uuid");

            //create local intent
            Intent localIntent = new Intent("rewards-inserted");
            localIntent.putExtras(extras);

            try {
                //get end point service
                ApiServiceHandler endpointService = ApiServiceHandler.instance(this.getApplication());
                double creditValue = AppUtility.calculateCredits(this, campaign);
                double dollarValue = AppUtility.calculateDollar(this, creditValue);

                //try inserting this offer
                String deviceId = endpointService.getDeviceUUID();
                Offerinfoendpoint.InsertByDate insertQuery = endpointService.getOfferInfoEndpoint()
                        .insertByDate(uuid, deviceId, campaign.getId(), date);
                OfferInfo info = insertQuery
                        .setTitle(campaign.getTitle())
                        .setDescription(campaign.getDescription())
                        .setImageUrl(campaign.getThumbnailUrl())
                        .setPrevUrl(campaign.getPrevUrl())
                        .setOfferUrl(campaign.getUrl())
                        .setDollarValue(dollarValue)
                        .setCreditValue(creditValue)
                        .execute();

                Log.d(TAG, "Offer inserted by date into remote db: "+info);
            }catch (Exception e){
                Log.e(TAG, "Error occurred inserting claimed offer: "+e.getMessage());
            }

            //broadcast streaming intent
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        } else if(AppUtility.ACTION_SYNC_REMOTE_OFFER_WITH_DEVICE.equalsIgnoreCase(action)){
            try {
                //get end point service
                ApiServiceHandler endpointService = ApiServiceHandler.instance(this.getApplication());

                //try retrieving all claimed offers for this device
                String deviceId = endpointService.getDeviceUUID();
                Offerinfoendpoint.GetOffersByDeviceId getQuery = endpointService.getOfferInfoEndpoint()
                        .getOffersByDeviceId(deviceId)
                        .setLimit(30); //sync upto the past 15 days

                CollectionResponseOfferInfo claimedOffers = getQuery.execute();
                if(claimedOffers != null && claimedOffers.getItems() != null && claimedOffers.getItems().size() > 0){
                    ClaimedRewardsDAO mPersistentDao = new ClaimedRewardsDAO(this);
                    mPersistentDao.open();
                    for(OfferInfo offerInfo: claimedOffers.getItems()){
                        //for each offer in remote, check if it exists in local
                        String date = offerInfo.getInsertDate();
                        String uuid = offerInfo.getUuid();
                        boolean hasOffer = mPersistentDao.isOfferExistByUUID(uuid);
                        if(!hasOffer){
                            //compose new campaign model and persist to local storage
                            Campaign campaign = new Campaign();
                            campaign.setId(offerInfo.getOfferId());
                            campaign.setTitle(offerInfo.getTitle());
                            campaign.setDescription(offerInfo.getDescription());
                            campaign.setPrevUrl(offerInfo.getPrevUrl());
                            campaign.setUrl(offerInfo.getOfferUrl());
                            campaign.setThumbnailUrl(offerInfo.getImageUrl());
                            campaign.setScore(offerInfo.getCreditValue() + "");
                            campaign.setUuid(offerInfo.getUuid());
                            
                            Log.d(TAG, "campaign: "+campaign);
                            //insert to db
                            mPersistentDao.insertByDate(date, campaign, true);
                        }
                    }
                    mPersistentDao.close();
                }

                Log.d(TAG, "Remote offer syncing with device");
            }catch (Exception e){
                Log.e(TAG, "Error occurred syncing remote offer with local device storage: "+e.getMessage());
            }
        }

        //complete broadcast
        SyncForegroundReceiver.completeWakefulIntent(intent);
    }
}
