package com.unoceros.client.engine.sync;

import android.app.IntentService;
import android.content.Intent;

import com.unoceros.client.engine.ApiServiceHandler;
import com.unoceros.client.engine.creditbankendpoint.Creditbankendpoint;
import com.unoceros.client.engine.creditbankendpoint.model.CreditBank;
import com.unoceros.client.engine.deviceinfoendpoint.model.DeviceInfo;
import com.unoceros.consumer.util.Log;

/**
 * Created by Amit
 */
public class SyncCreditWithdrawHandler extends IntentService {

    private static final String TAG = "SyncCreditWithdrawHandler";

    public SyncCreditWithdrawHandler(){
        super("SyncCreditWithdrawHandler");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        //End point service
        ApiServiceHandler endpointService = ApiServiceHandler.instance(this.getApplication());

        try {
            //get the device info for this device from the back-end
            DeviceInfo deviceInfo = endpointService.getDeviceInfoEndpoint()
                    .getDeviceInfo(endpointService.getDeviceUUID()).execute();

            if(deviceInfo != null){
                //post to endpoints
                String deviceId = deviceInfo.getDeviceInfoID();
                double amount = intent.getDoubleExtra("amount", 0);
                String transactionId = intent.getStringExtra("transactionId");
                Creditbankendpoint endpoint = endpointService.getCreditBankInfoEndpoint();
                CreditBank bank = endpoint.withdraw(deviceId, amount)
                        .setTransactionId(transactionId)
                        .execute();

                Log.d(TAG, "onHandleIntent:: Total remote balance is: "+bank.getTotalBalance());
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        SyncForegroundReceiver.completeWakefulIntent(intent);
    }
}