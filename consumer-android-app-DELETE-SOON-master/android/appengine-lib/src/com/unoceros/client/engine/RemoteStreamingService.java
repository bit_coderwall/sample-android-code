package com.unoceros.client.engine;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.unoceros.client.engine.creditbankendpoint.Creditbankendpoint;
import com.unoceros.client.engine.creditbankendpoint.model.CollectionResponseDepositTier;
import com.unoceros.client.engine.creditbankendpoint.model.CreditBank;
import com.unoceros.client.engine.creditbankendpoint.model.DepositTier;
import com.unoceros.client.engine.sync.SyncForegroundReceiver;
import com.unoceros.consumer.model.DepositStream;
import com.unoceros.consumer.services.ApplicationServices;
import com.unoceros.consumer.services.CreditServices;
import com.unoceros.consumer.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Amit
 */
public class RemoteStreamingService extends IntentService {

    private static final String TAG = "RemoteStreamingService";

    public RemoteStreamingService() {
        super("RemoteStreamingService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        int limit = extras.getInt("limit", 5);
        int offset = extras.getInt("offset", 0);
        String order = extras.getString("order");

        //end point service
        ApiServiceHandler endpointService = ApiServiceHandler.instance(this.getApplication());
        Intent localIntent = new Intent("activity-streaming");
        String errorMsg = null;

        try {
            Creditbankendpoint.GetDepositTier remoteStream = endpointService.getCreditBankInfoEndpoint()
                    .getDepositTier(endpointService.getDeviceUUID());
            CollectionResponseDepositTier streamList = remoteStream.setDeviceId(endpointService.getDeviceUUID())
                    .setLimit(limit)
                    .setOffset(offset)
                    .setOrder(order).execute();

            ArrayList<DepositStream> parcelableStream = new ArrayList<DepositStream>();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); //2014-11-22T19:20:26.463Z
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
            TimeZone tz = TimeZone.getTimeZone("UTC");
            sdf.setTimeZone(tz);
            if (streamList != null && streamList.getItems() != null && streamList.getItems().size() > 0) {
                for (DepositTier tier : streamList.getItems()) {
                    Date date = sdf.parse(tier.getDate().toString());
                    DepositStream stream = new DepositStream();
                    String dateStr = sdf2.format(date);
                    ///Log.d(TAG, "deposit tier date: "+dateStr+", utc: "+date); //sdf.parse(tier.getDate().toString())); //+", date: "+year+"-"+month+"-"+day);
                    stream.setDate(dateStr);
                    stream.setAmount(tier.getAmount());
                    stream.setRate(tier.getRate());
                    parcelableStream.add(stream);
                }
            } else {
                DepositStream stream = new DepositStream();
                stream.setDate(sdf.format(new Date()));
                stream.setAmount(0.0);
                stream.setRate(0.0);
                parcelableStream.add(stream);
            }

            localIntent.putParcelableArrayListExtra("parcelableStream", parcelableStream);
        }catch (GoogleJsonResponseException e){
            errorMsg = "Error Syncing Activity Stream"; //e.getMessage();
            GoogleJsonError details = e.getDetails();
            if(details != null)
                errorMsg = "Error Syncing Activity Stream"; //details.getMessage();
        }catch (Exception e) {
            Log.e(TAG, "Error Syncing Activity Stream", e);
            errorMsg = "Error Syncing Activity Stream"; //e.getMessage();
        }

        if(TextUtils.isEmpty(errorMsg)) {
            try {
                CreditBank bank = endpointService.getCreditBankInfoEndpoint()
                        .getCreditBank(endpointService.getDeviceUUID()).execute();

                if (bank != null) {
                    //unbox these guys
                    double overallBalance = bank.getOverallTotalBalance() != null ? bank.getOverallTotalBalance() : 0.0;
                    double totalBalance = bank.getTotalBalance() != null ? bank.getTotalBalance() : 0.0;
                    double todayBalance = bank.getTodayBalance() != null ? bank.getTodayBalance() : 0.0;
                    double todayAverage = bank.getAverage() != null ? bank.getAverage() : 0.0;

                    //update local credit services
                    CreditServices creditServices = ApplicationServices.instance().getCreditService();
                    creditServices.setDailyAverage(todayAverage);
                    creditServices.setNetworkAverage(0.0);
                    creditServices.invokedByRemoteAdmin(overallBalance, totalBalance, todayBalance);
                }
            } catch (Exception e) {
                Log.e(TAG, "Error Syncing Creditbank Service", e);
            }

            localIntent.putExtra("success", true);
        }else{
            localIntent.putExtra("success", false);
            localIntent.putExtra("message", errorMsg);
        }

        //broadcast streaming intent
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

        //complete broadcast
        SyncForegroundReceiver.completeWakefulIntent(intent);
    }
}