package com.unoceros.client.engine.sync;

import java.io.IOException;
import java.util.Date;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.api.client.util.DateTime;
import com.google.gson.Gson;
import com.unoceros.client.engine.ApiServiceHandler;
import com.unoceros.client.engine.creditbankendpoint.Creditbankendpoint;
import com.unoceros.client.engine.creditbankendpoint.model.CreditBank;
import com.unoceros.client.engine.deviceinfoendpoint.model.DeviceInfo;
import com.unoceros.consumer.data.DailyComputeData;
import com.unoceros.consumer.data.DailyComputeData.ComputeStats;
import com.unoceros.consumer.services.ApplicationServices;
import com.unoceros.consumer.services.CreditServices;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.util.SyncUtility;
import com.unoceros.consumer.utils.EngineUtility;
import com.unoceros.engine.JobManager;
import com.unoceros.kit.JobPayload;

public class SyncHelper {

	private static final String TAG = "SyncHelper";
    private ApiServiceHandler endpointServices;
    private SharedPreferences pref;

	private static final double MILESTONE_CREDIT_EARNED = 200;
	
	public SyncHelper(Application application){
		endpointServices = ApiServiceHandler.instance(application);

        pref = application.getApplicationContext().getSharedPreferences(AppUtility.SYNC_PREF, Context.MODE_PRIVATE);
    }
	
	public double onHardRefresh(Context context){
		boolean isComputing = EngineUtility.isComputing(context);
		boolean canCompute = SyncUtility.canCompute(context);
		long endComputeTime = 0;
        return computeEarnedCredits(context, null, isComputing, canCompute, endComputeTime);
	}
	
	public void onSyncService(Context context, DeviceInfo deviceInfo, JobPayload payload, boolean isComputing, boolean canCompute){
		Log.i(TAG, "onSyncService::");
		Editor editor = pref.edit();
		long startComputeTime = pref.getLong("startComputeTime", 0);
		long endComputeTime = 0;
		
		Log.d(TAG, "onSyncService:: startComputeTime: "+startComputeTime+", format: "+SyncUtility.getDateFromMilliseconds(startComputeTime));
		Log.d(TAG, "onSyncService:: endComputeTime: "+endComputeTime+", format: "+SyncUtility.getDateFromMilliseconds(endComputeTime));
		
		//sync to remote, earned credits, compute stats etc
		syncRemoteStorage(context, deviceInfo, payload, isComputing, canCompute, startComputeTime, endComputeTime);
		
		//sync times
		long syncTime = pref.getLong("syncTime", 0); //last sync time
		long currentTime = System.currentTimeMillis(); //current sync time
		long startOfDay = SyncUtility.getStartOfDay().getTime();

        //check day bounds--todays balance is reset on first encounter of every start of day
        Log.d(TAG, "onSyncService:: SyncTime: "+syncTime+", format: "+SyncUtility.getDateFromMilliseconds(syncTime));
        Log.d(TAG, "onSyncService:: StartOfDay: "+startOfDay+", format: "+SyncUtility.getDateFromMilliseconds(startOfDay));
        Log.d(TAG, "onSyncService:: CurrentTime: "+currentTime+", format: "+SyncUtility.getDateFromMilliseconds(currentTime));

        if(syncTime == 0){
			editor.putLong("syncTime", currentTime);
            editor.remove("todaysBalance");
			Log.d(TAG, "onSyncService:: First Sync on installation: "+currentTime+", format: "+SyncUtility.getDateFromMilliseconds(currentTime));
		}else if(syncTime < startOfDay && currentTime > startOfDay){
			Log.d(TAG, "onSyncService:: Last Sync Day: "+syncTime+", format: "+SyncUtility.getDateFromMilliseconds(syncTime));
			Log.d(TAG, "onSyncService:: New Sync Day: "+currentTime+", format: "+SyncUtility.getDateFromMilliseconds(currentTime));
			//first encounter of new day
			editor.putLong("syncTime", currentTime);
			//clear previous day credits earning
			editor.remove("todaysBalance");
            editor.remove("milestoneAlerted");
		}else{
            editor.putLong("syncTime", currentTime);
        }
		editor.apply();
	}
	
	public void onEndOfCompute(Context context, JobPayload payload, long duration, boolean isComputing, boolean canCompute){
		Log.i(TAG, "onEndOfCompute::");
		Editor editor = pref.edit();
		long currentTime = System.currentTimeMillis();
		
		long endComputeTime = currentTime;
		long startComputeTime = pref.getLong("startComputeTime", 0);
		duration = (endComputeTime - startComputeTime > duration || startComputeTime <= 0)?duration:(endComputeTime - startComputeTime);
		
		editor.putLong("startCanComputeTime", currentTime);
		editor.putLong("endComputeTime", endComputeTime);
		editor.putLong("computeDuration", duration);
		editor.apply();

		Log.d(TAG, "onEndOfCompute:: startComputeTime: "+startComputeTime+", format: "+SyncUtility.getDateFromMilliseconds(startComputeTime));
		Log.d(TAG, "onEndOfCompute:: endComputeTime: "+endComputeTime+", format: "+SyncUtility.getDateFromMilliseconds(endComputeTime));
		Log.d(TAG, "onEndOfCompute:: computeDuration: "+duration);
		Log.d(TAG, "onEndOfCompute:: startCanComputeTime: "+currentTime+", format: "+SyncUtility.getDateFromMilliseconds(currentTime));

		//sync upon done or paused computing
		syncRemoteStorage(context, null, payload, isComputing, canCompute, startComputeTime, endComputeTime);
	
		//clear start and end compute 
		editor.remove("startComputeTime");
		editor.remove("endComputeTime");
		editor.remove("computeDuration");
		editor.apply();
	}
	
	public void onStartOfCompute(Context context, boolean isComputing, boolean canCompute){
		Log.i(TAG, "onStartOfCompute::");
		long currentTime = System.currentTimeMillis();
		Editor editor = pref.edit();
		editor.putLong("startComputeTime", currentTime);
		editor.remove("endComputeTime");
		editor.remove("computeDuration");
		editor.apply();
		
		Log.d(TAG, "onStartOfCompute:: startComputeTime: "+currentTime+", format: "+SyncUtility.getDateFromMilliseconds(currentTime));
		
		//force canCompute rate for earning credits
		computeEarnedCredits(context, null, isComputing, canCompute, 0);
		
		//clear start of can compute since we are actually doing computing, rate will be different
		editor.remove("startCanComputeTime");
		editor.apply();
	}


	private synchronized void syncRemoteStorage(Context context, DeviceInfo deviceInfo, JobPayload payload,
                                                boolean isComputing, boolean canCompute,
			                                    long startComputeTime, long endComputeTime){
		
		Log.d(TAG, "syncRemoteStorage:: isComputing: "+isComputing+", canCompute: "+canCompute+
				", startComputeTime: "+startComputeTime+", endComputeTime: "+endComputeTime);
		//defaults earned credits for this sync event to zero
		double earnedCredits;

		//check if any job is currently running (regardless of whether it is paused on not)
		if(isComputing){
			//job is currently started and running within a day start and end marker (12:00am to 11:59pm)
			if(canCompute){
				//isComputing = true and canCompute = true
				//check if computing task has overlapped the start of day (edge case)
                boolean isOverlapped = isComputeOverlappedStart(startComputeTime);
				if(isOverlapped){
					//if this is the case, then task end time should be end of last day,
					//that is, 1 second prior to start of day, 11:59pm, 23:59 military time
					//and new task start time is the start of day (12:00am, 00:00 military time)
					normalizeComputingDuration(context, deviceInfo, payload, startComputeTime);
				}else{
                    //a compute task was just successfully completed for a particular game
                    //isComputing = true and canCompute = true
                    //store duration if any
                    if(endComputeTime > 0 && payload != null){
                        String date = SyncUtility.getFormatedDate(SyncUtility.getStartOfDay()); //2014-01-30
                        long duration = endComputeTime - startComputeTime;
                        storeComputingStats(context, deviceInfo, payload, date, startComputeTime, duration);
                    }
                }
			}else{
				//assume it was paused due to the fact that the device does not meet canCompute rules
				//isComputing = true and canCompute = false
				//store duration if any
                if(endComputeTime > 0 && payload != null){
                    String date = SyncUtility.getFormatedDate(SyncUtility.getStartOfDay()); //2014-01-30
					long duration = endComputeTime - startComputeTime;
					storeComputingStats(context, deviceInfo, payload, date, startComputeTime, duration);
				}
			}
			
			//keep tabs on computing earning power
			earnedCredits = computeEarnedCredits(context, deviceInfo, isComputing, canCompute, endComputeTime);
		}else{
			//job is not running, keep tabs on credit earning power
			//a device can still earn credit if it is available for computing task
			if(canCompute){
				//get elapse time from last checked point (could be the last time the server was sync
				//or the last time the compute job was finished)
				//isComputing = false and canCompute = true
				//store duration if any
				if(endComputeTime > 0 && payload != null){
					String date = SyncUtility.getFormatedDate(SyncUtility.getStartOfDay()); //2014-01-30
					long duration = endComputeTime - startComputeTime;
					storeComputingStats(context, deviceInfo, payload, date, startComputeTime, duration);
				}
			}else{
				//no credits earning power here, user device does not meet the business rule
				//for earning credit, perhaps users needs to plug-in their devices.
				//we could have an interesting notification here
				//isComputing = false and canCompute = false
				//store duration if any
                if(endComputeTime > 0 && payload != null){
                    String date = SyncUtility.getFormatedDate(SyncUtility.getStartOfDay()); //2014-01-30
					long duration = endComputeTime - startComputeTime;
					storeComputingStats(context, deviceInfo, payload, date, startComputeTime, duration);
				}
			}

			//regardless lets attempt to retrieve any previously earned credits
			earnedCredits = computeEarnedCredits(context, deviceInfo, isComputing, canCompute, endComputeTime);
		}

		//deposit to remote credit bank
		if(deviceInfo != null && earnedCredits > 0){
			//STORE STATS TO REMOTE ENDPOINTS
            Date depositDate = new Date(); //getStartOfDay();
            DateTime dateTime =  new DateTime(depositDate);
			String deviceId = this.endpointServices.getDeviceUUID(); //deviceInfo.getDeviceInfoID();
			EngineUtility.storeComputeStatsToRemote(this.endpointServices, pref);

			//STORE CREDITS TO REMOTE ENDPOINTS
			earnedCredits = Math.round(earnedCredits * 100.0) / 100.0;
            earnedCredits = SyncHelper.normalizedCredits(context, isComputing, earnedCredits);
			Log.d(TAG, "syncRemoteStorage:: depositing earned credits to remote: "+earnedCredits);
			try {
                double tierRate = AppUtility.getCreditRate(context, isComputing);
				//post to endpoints
				Creditbankendpoint endpoint = this.endpointServices.getCreditBankInfoEndpoint();
                CreditBank bank = endpoint.depositByDate(deviceId, dateTime, earnedCredits, tierRate).execute();
                
                Log.d(TAG, "syncRemoteStorage:: Daily average is: " + bank.getAverage());

                //lets wait for little bit for the previous endpoint execution to take effect
                //perhaps couple seconds should do
                Thread.sleep(4000);

                //unbox these guys
                double overallBalance = bank.getOverallTotalBalance()!=null?bank.getOverallTotalBalance():0.0;
                double totalBalance = bank.getTotalBalance()!=null?bank.getTotalBalance():0.0;
                double todayBalance = bank.getTodayBalance()!=null?bank.getTodayBalance():0.0;
                double todayAverage = bank.getAverage()!=null?bank.getAverage():0.0;

                //can choose to notify the user of an important milestone
                String message = earnedCredits+" additional credits applied towards today's total!";
                boolean isForceful = false;
                boolean milestoneAlerted = pref.getBoolean("milestoneAlerted", false);
                if(todayBalance >= MILESTONE_CREDIT_EARNED && !milestoneAlerted) {
                    isForceful = true;
                    message = "Congrats, you've earned "+todayBalance+" credits today!";
                    pref.edit().putBoolean("milestoneAlerted", true).apply();
                    //NotificationHelper.notifyMilestoreCredits(context, message, isForceful);
                }else {
                    //NotificationHelper.notifyCreditsEarned(context, message, isForceful);
                }

                //update local credit services
                CreditServices creditServices = ApplicationServices.instance().getCreditService();
                creditServices.deposit(earnedCredits);
                creditServices.setDailyAverage(todayAverage);
                creditServices.setNetworkAverage(0.0);
                creditServices.invokedByRemoteAdmin(overallBalance, totalBalance, todayBalance);

                pref.edit().remove("earnedCredits").apply();
			} catch (IOException e) {
				Log.e("Error depositing to remote: ", e.getMessage(), e);
                //persist earning to local store--in cases of network failure etc
				pref.edit().putFloat("earnedCredits", (float) earnedCredits).apply();
			} catch (InterruptedException e) {
                Log.e("Interrupted when depositing to remote: ", e.getMessage(), e);
                //persist earning to local store--in cases of network failure etc
                pref.edit().putFloat("earnedCredits", (float) earnedCredits).apply();
            }
        }else{
			//persist earning to local store--in cases of network failure etc
			pref.edit().putFloat("earnedCredits", (float)earnedCredits).apply();
			Log.d(TAG, "syncRemoteStorage:: depositing earned credits to local store: "+earnedCredits);
		}
	}
	
	private double computeEarnedCredits(Context context, DeviceInfo deviceInfo, boolean isComputing, boolean canCompute, long endComputeTime) {
		double earnedCredits = pref.getFloat("earnedCredits", 0f);
		double todaysBalance = pref.getFloat("todaysBalance", 0f);
		
		long currentTime = System.currentTimeMillis();
		//long endComputeTime = pref.getLong("endComputeTime", 0);

		Log.d(TAG, "computeEarnedCredits:: earnedCredits: "+earnedCredits);
		Log.d(TAG, "computeEarnedCredits:: todaysBalance: "+todaysBalance);
		Log.d(TAG, "computeEarnedCredits:: isComputing: "+isComputing+", canCompute: "+canCompute);
		Log.d(TAG, "computeEarnedCredits:: endComputeTime: "+endComputeTime+", format: "+SyncUtility.getDateFromMilliseconds(endComputeTime));
		if(endComputeTime > 0 || isComputing){
			//check for instances where startCanComputeTime might equally be same as startComputeTime
			long startCanComputeTime  = pref.getLong("startCanComputeTime", 0);
			Log.d(TAG, "computeEarnedCredits:: startCanComputeTime: "+startCanComputeTime+", format: "+SyncUtility.getDateFromMilliseconds(startCanComputeTime));
			if(canCompute == true){
				//check if we have a previously stored startCanComputeTime value
				if(startCanComputeTime <= 0){
					//assume no previously can compute start time, check if canCompute state in case where the job is paused
					if(EngineUtility.jobStateIsStopped() && deviceInfo != null){
						//only for sync job
						Log.d(TAG, "computeEarnedCredits:: job paused/stopped/interrupted at time: "+currentTime);
						pref.edit().putLong("startCanComputeTime", currentTime).apply();
					}
					return earnedCredits;
				}else{
					if(EngineUtility.jobStateIsStopped() && deviceInfo != null){
						//determine earned credits and return only for sync jobs
                        double tierRate = AppUtility.getCreditRate(context, isComputing);
						earnedCredits = earnedCredits + (((tierRate * (currentTime - startCanComputeTime)))/3600000);
						pref.edit().remove("startComputeEarningTime").apply();
						
						Log.d(TAG, "computeEarnedCredits:: job paused/stopped/interrupted at time: "+currentTime);
						pref.edit().putLong("startCanComputeTime", currentTime).apply();
						return earnedCredits;
					}else{
						//fall thru to calculate earned credits, and reset to zero
						//pref.edit().remove("startCanComputeTime").commit();
						pref.edit().putLong("startCanComputeTime", currentTime).apply();
					}
				}
			}
			//we are currently doing some computing task
			//use the last time we got here to get the duration
			long startComputeTime  = pref.getLong("startComputeTime", 0);
			long startComputeEarningTime  = pref.getLong("startComputeEarningTime", 0);
			if(startComputeEarningTime <= 0)
				startComputeEarningTime = startComputeTime;
			
			Log.d(TAG, "computeEarnedCredits:: currentTime: "+currentTime+", startComputeEarningTime: "+startComputeEarningTime);
			Log.d(TAG, "computeEarnedCredits:: duration: "+(currentTime - startComputeEarningTime));
            double tierRate = AppUtility.getCreditRate(context, isComputing);
			//earnedCredits = earnedCredits + ((tierRate * (currentTime - startComputeEarningTime))/3600000);
            earnedCredits = earnedCredits
                    + ((tierRate * (SyncHelper.validateTimeLapse(context, currentTime, startComputeEarningTime)))/3600000);
			//set to current time
			pref.edit().putLong("startComputeEarningTime", currentTime).apply();
		}else{
			long startCanComputeTime  = pref.getLong("startCanComputeTime", 0);
			Log.d(TAG, "computeEarnedCredits:: startCanComputeTime: "+startCanComputeTime+", format: "+SyncUtility.getDateFromMilliseconds(startCanComputeTime));
			if(canCompute == false){
				//check if we have a previously stored startCanComputeTime value
				if(startCanComputeTime <= 0){
					//assume no previously can compute start time, lets wait until canCompute state changes
					return earnedCredits;
				}else{
					//assume previously store compute start time that elapsed has not be reported yet
					//fall thru to calculate earned credits, and reset to zero
					pref.edit().remove("startCanComputeTime").apply();
				}
			}else{
				//maybe the start of can compute state, need to store as currentTime iff this was the first encounter of canCompute
				if(startCanComputeTime <= 0){
					//start of canCompute state, set to currentTime
					pref.edit().putLong("startCanComputeTime", currentTime).apply();
					return earnedCredits;
				}else{
					//this was not the first instance of canCompute state, set to current time and fall thru to calculate earn credits
					pref.edit().putLong("startCanComputeTime", currentTime).apply();
				}
			}

			//add it up using rate
            double tierRate = AppUtility.getCreditRate(context, isComputing);
			//earnedCredits = earnedCredits + ((tierRate * (currentTime - startCanComputeTime))/3600000);
            earnedCredits = earnedCredits
                    + ((tierRate * (SyncHelper.validateTimeLapse(context, currentTime, startCanComputeTime)))/3600000);
            pref.edit().remove("startComputeEarningTime").apply();
		}
		
		//round to 2 decimals
		todaysBalance =  Math.round((todaysBalance + earnedCredits) * 100.0) / 100.0;
		earnedCredits = Math.round(earnedCredits * 100.0) / 100.0;
		
		//persist todays total to local store
		pref.edit().putFloat("todaysBalance", (float)todaysBalance).apply();
		
		Log.d(TAG, "Earned Credits: "+earnedCredits);
		Log.d(TAG, "Today's Credits: "+todaysBalance);
		
		return earnedCredits;
	}
	
	private boolean isComputeOverlappedStart(long startComputeTime) {
        boolean isOverlapped = false;
        if(startComputeTime <= 0)
			return isOverlapped;
		
		long startDayMark = SyncUtility.getStartOfDay().getTime();
		long endDayMark = SyncUtility.getEndOfDay().getTime();
		isOverlapped = !(startDayMark <= startComputeTime &&  startComputeTime <= endDayMark);
        return isOverlapped;
	}

	private void normalizeComputingDuration(Context context, DeviceInfo deviceInfo, JobPayload payload, long startComputeTime) {
		Log.d(TAG, "normalizeComputingDuration:: startComputeTime: "+startComputeTime+", format: "+SyncUtility.getDateFromMilliseconds(startComputeTime));
		
		long startDayMark = SyncUtility.getStartOfDay().getTime();
		long duration = startDayMark - startComputeTime;
		
		//persist collected compute data to internal storage
		String date = SyncUtility.getFormatedDate(SyncUtility.getYesterdayDate()); //2014-01-30
		storeComputingStats(context, deviceInfo, payload, date, startComputeTime, duration);
	}
	
	private void storeComputingStats(Context context, DeviceInfo deviceInfo, JobPayload payload, String date,
			long startMark, long duration) {

        //illegal start mark
        if(startMark <= 0)
            return;

        //compute duration cannot be greater than 15 minutes at a time
        if(duration >= EngineUtility.SYNC_INTERVAL_MILLISECONDS)
            duration = EngineUtility.SYNC_INTERVAL_MILLISECONDS;

        //store daily computing data to internal storage
        String deviceId = endpointServices.getDeviceUUID();
        String key = String.valueOf((deviceId+date).hashCode());
        String jobId = payload != null? payload.getJobId(): "none";
        String projectId = payload != null? payload.getProjectId(): "none";
        String customerId = payload != null? payload.getCustomerId(): "none";
        String pluginId = payload != null? payload.getPluginId(): "none";
        String jobStatus = JobManager.instance().getState().toString();

		Log.d(TAG, "storeComputingStats:: date: "+date+", startMark: "+startMark+", " +
                "duration: "+duration+", payload: "+payload);

		//compose daily compute data using combination of date string and device id as key
		DailyComputeData data = new DailyComputeData();
		data.setKey(key);
		data.setDate(date);
		
		//compose stats
		ComputeStats stats = data.new ComputeStats();
		stats.setDuration(duration);
		stats.setStartMark(startMark);
		stats.setJobId(jobId);
        stats.setProjectId(projectId);
        stats.setCustomerId(customerId);
        stats.setPluginId(pluginId);
        stats.setStatus(jobStatus);
        stats.setStartOfDay(SyncUtility.getStartOfDay().getTime());
        stats.setEndOfDay(SyncUtility.getEndOfDay().getTime());
        stats.setDate(new DateTime(SyncUtility.getStartOfDay()));

		//check device storage for old data
		Gson gson = new Gson();
		//check if there is an existing compute data in storage for this key
		String gson_str = pref.getString(key, null);
		if(gson_str == null){		
			data.addStats(stats);
			gson_str = gson.toJson(data, DailyComputeData.class);
		}else{
			//just add to stats to stored instance of daily compute data
			data = gson.fromJson(gson_str, DailyComputeData.class);
			data.addStats(stats);
			gson_str = gson.toJson(data, DailyComputeData.class);
		}
		
		//store to internal device storage if deviceInfo is null otherwise remote server
		if(deviceInfo == null){
			//store to internal storage if deviceInfo is null
			pref.edit().putString(key, gson_str).apply();
			Log.d(TAG, "storeComputingStats:: storing to internal storage - Gson: " + gson_str);
		}

        //store remotely
        if(gson_str != null)
            EngineUtility.storeComputeStatsToRemote(this.endpointServices, pref);
	}

    private static double normalizedCredits(Context context, boolean isComputing, double credits){
        double tierRate = AppUtility.getCreditRate(context, isComputing); //credits/hour

        double synHours = (EngineUtility.SYNC_INTERVAL_MILLISECONDS / (double)(1000*60*60)); //3600000
        double maxCredits = (tierRate * synHours);

        //double synMinutes = ((CloudAppUtility.SYNC_INTERVAL_MILLISECONDS / (double)(1000*60)));
        //double maxCredits = (tierRate * synMinutes) / 60;

        if(credits > maxCredits)
            credits = maxCredits;

        return credits;
    }

    private static long validateTimeLapse(Context context, long currentStatusTime, long lastStatusTime){
        long duration = currentStatusTime-lastStatusTime;

        if(duration >= EngineUtility.SYNC_INTERVAL_MILLISECONDS)
            duration = EngineUtility.SYNC_INTERVAL_MILLISECONDS;

        return duration;
    }
}
