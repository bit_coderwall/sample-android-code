package com.unoceros.consumer.app;

/**
 * Created by Amit
 */
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.unoceros.client.engine.ApiServiceHandler;
import com.unoceros.client.engine.Globals;
import com.unoceros.client.engine.deviceinfoendpoint.model.DeviceInfo;
import com.unoceros.consumer.IntroScreenActivity;
import com.unoceros.consumer.InviteOnlyActivity;
import com.unoceros.consumer.MainLandingActivity;
import com.unoceros.consumer.NoNetworkActivity;
import com.unoceros.consumer.SignUpSlickblueActivity;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.utils.EngineUtility;

public class AppDelegate extends ActionBarActivity {

    private static final String TAG = "AppDelegate";
    private  SharedPreferences mPrefs;
    private GoogleCloudMessaging gcmInstance;
    private String regId;
    private ApiServiceHandler endpointService;
    private boolean _globalRegError;
    private boolean mIntroScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_delegate);

        /*
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            Log.e("AppDelegate", pi.signatures[0].toCharsString());
        }catch (Exception e){
            e.printStackTrace();
        }*/

        final ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setCustomView(com.unoceros.consumer.R.layout.actionbar_generic);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);

            //remove home icon
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        _globalRegError = false;

        //check intro screen
        mPrefs = getSharedPreferences(AppUtility.INSTALLATION_PREF_FILE, Context.MODE_PRIVATE);
        mIntroScreen = mPrefs.getBoolean("introscreen", true);

        //track app start
        TrackingManager.getInstance(AppController.getInstance()).trackAppStart(this);

        //End point service
        endpointService = ApiServiceHandler.instance(this.getApplication());
    }

    private void launchNextActivity(){
        if(AppState.isInitialized() && AppState.instance().isSignedIn() &&
                AppState.instance().userAgreedToTOSAndPP()) {
            final TextView message = ((TextView) findViewById(R.id.app_delegate_loading_text));
            message.setBackgroundColor(getResources().getColor(R.color.transparent));
            message.setText("Loading...");
            new AsyncTask<Void, Integer, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... params) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        Log.e("AppDelegate", e.getMessage(), e);
                    }
                    boolean isInvited = false;
                    try {
                        DeviceInfo deviceInfo = endpointService.getDeviceInfoEndpoint()
                                .getDeviceInfo(endpointService.getDeviceUUID()).execute();

                        if (deviceInfo != null) {
                            isInvited = deviceInfo.getIsInvited();
                            Log.d(TAG, deviceInfo.toPrettyString());
                        }
                    }catch (Exception e){
                        Log.e(TAG, e.getMessage());
                    }
                    return isInvited;
                }
    
                @Override
                protected void onPostExecute(Boolean isInvited) {
                    if(isInvited) {
                        startActivity(new Intent(AppDelegate.this, MainLandingActivity.class));
                        finish();
                    }else{
                        //launch notifier where user can retry registration
                        launchErrorNotifier("Oops, please make a clean install from google play store android app.");
                    }
                }
            }.execute(null, null, null);
        
        }else {
            new AsyncTask<Void, Integer, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... params) {
                    boolean isInvited = false;
                    try {
                        DeviceInfo deviceInfo = endpointService.getDeviceInfoEndpoint()
                                .getDeviceInfo(endpointService.getDeviceUUID()).execute();

                        if (deviceInfo != null) {
                            isInvited = deviceInfo.getIsInvited();
                            Log.d(TAG, deviceInfo.toPrettyString());
                        }
                    }catch (Exception e){
                        Log.e(TAG, e.getMessage());
                    }
                    return isInvited;
                }

                @Override
                protected void onPostExecute(Boolean isInvited) {
                    isInvited = true; //force true to disable in-app invite system;
                    if(isInvited) {
                        //proceed to login screen
                        startActivity(new Intent(AppDelegate.this, SignUpSlickblueActivity.class));
                        finish();
                    }else{
                        //display invitation screen
                        startActivity(new Intent(AppDelegate.this, InviteOnlyActivity.class));
                        finish();
                    }
                }
            }.execute(null, null, null);
        }
    }

    private void launchErrorNotifier(String msg){
        //Error occurred, show retry button
        final TextView message = ((TextView) findViewById(R.id.app_delegate_loading_text));
        message.setText(msg);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 50); // llp.setMargins(left, top, right, bottom);
        params.gravity = Gravity.BOTTOM;
        message.setLayoutParams(params);

        final Button retryBtn = ((Button) findViewById(R.id.app_delegate_retry));
        retryBtn.setVisibility(View.VISIBLE);
        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                message.setText("Please wait...");
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, 0); // llp.setMargins(left, top, right, bottom);
                params.gravity = Gravity.BOTTOM;
                message.setLayoutParams(params);
                retryBtn.setVisibility(View.GONE);

                //registerGCM
                registerGCM(true);
            }
        });
    }

    private void registerGCM(boolean retrying){
        //sanity check
        if(!AppUtility.isValidApplicationNode(this))
            return;

        // Check device for Play Services APK. If check succeeds, proceed with
        // GCM registration.
        boolean isRegisteringInBackground = false;
        boolean isDeviceSupport = true;
        if (EngineUtility.checkPlayServices(this)){
            // Retrieve registration id from local storage
            regId = EngineUtility.getRegistrationId(this);
            if (TextUtils.isEmpty(regId)){
                isRegisteringInBackground = true;
                if(!AppUtility.haveNetworkConnection(getApplicationContext())){
                    //check network connectivity
                    AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
                }else {
                    //there is internet connection, register with GCM
                    registerInBackground(null, false);
                }
            }else{
                Log.i(Globals.TAG, "Gcm RegId: " + regId);
                //retrying, looks like device was registered via GCM, but fail somewhere downstream
                if(retrying) {
                    isRegisteringInBackground = true;
                    registerInBackground(regId, retrying);
                }
            }
        }else{
            //could not signed in
            String msg = "Device is not supported.  No valid Google Play Store found...";
            Log.i(Globals.TAG, msg);
            isDeviceSupport = false;
            launchErrorNotifier(msg);
        }

        //if not being registering in the background, then launch next activity
        if(!isRegisteringInBackground && isDeviceSupport) {
            launchNextActivity();
        }
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground(final String registrationId, final boolean retrying){
        _globalRegError = false;
        new AsyncTask<Void, Integer, ServiceStatus>(){
            @Override
            protected ServiceStatus doInBackground(Void... params){
                ServiceStatus status = null;
                try{
                    int count = 0;
                    if(retrying && registrationId != null){
                        //already has a gcm regId
                        publishProgress(50);
                    }else {
                        //get new gcm regId
                        gcmInstance = GoogleCloudMessaging.getInstance(AppDelegate.this);
                        regId = gcmInstance.register(Globals.GCM_SENDER_ID);

                        for (int i = count; i < 20; i++) {
                            publishProgress(i);
                        }

                        // Persist the regID - no need to register again.
                        EngineUtility.storeRegistrationId(AppDelegate.this, regId);

                        count = 20;
                        for (int i = count; i < 50; i++) {
                            publishProgress(i);
                        }
                    }

                    String msg = "Device registered, registration ID=" + regId;

                    // Send the registration ID to the server over
                    // HTTP, so it can use GCM/HTTP or CCS to send messages to the app.
                    endpointService.sendRegistrationIdToBackend(AppDelegate.this, regId);
                    count = 50;
                    for (int i = count; i < 98; i++) {
                        publishProgress(i);
                    }
                    status = new ServiceStatus(false, msg);
                }catch (Throwable ex){
                    String msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                    publishProgress(99);
                    status = new ServiceStatus(true, msg);
                } finally {
                    publishProgress(100);
                }
                return status;
            }

            @Override
            protected void onProgressUpdate(Integer...progress){
                if(!_globalRegError) {
                    String loadingText = getString(R.string.registering_loading);
                    ((TextView) findViewById(R.id.app_delegate_loading_text)).setText(loadingText + progress[0]);
                }
            }

            @Override
            protected void onPostExecute(ServiceStatus status){
                if(status != null) {
                    Log.i(Globals.TAG, status.message);
                    Context context = getApplicationContext();
                    if (AppUtility.signedWithDebugKey(context) || AppUtility.isDebuggable(context)) {
                        Toast.makeText(AppDelegate.this, status.message, Toast.LENGTH_LONG).show();
                    }

                    if(status.isError) {
                        launchErrorNotifier(status.message+", please check your wireless connection.");
                    }else{
                        launchNextActivity();
                    }
                }
            }
        }.execute(null, null, null);
    }

    @Override
    public void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    public void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }

    public void onResume(){
        super.onResume();

        if(mIntroScreen){
            Intent intro = new Intent(this, IntroScreenActivity.class);
            startActivityForResult(intro, IntroScreenActivity.INTRO_REQUEST);
        }else {
            //Check if this device can be added as a valid node to Unoceros network
            if (AppUtility.isValidApplicationNode(this)) {
                LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationReceiver, new IntentFilter("registration"));
                checkNetworkConnection();
            } else {
                String errorMessage = getString(R.string.device_not_supported);
                ((TextView) findViewById(R.id.app_delegate_loading_text)).setText(errorMessage);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationReceiver);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check which request we're responding to
        if (requestCode == IntroScreenActivity.INTRO_REQUEST) {
            if(resultCode  == Activity.RESULT_OK) {
                mIntroScreen = false;
                mPrefs.edit().putBoolean("introscreen", mIntroScreen).apply();
            }
        }
    }

    private void checkNetworkConnection() {
        if (!AppUtility.haveNetworkConnection(this)) {
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        }else{
            //attempt to register device to Google Cloud Messaging Services
            registerGCM(false);
        }
    }

    private class ServiceStatus{
        public boolean isError;
        public String message;
        public ServiceStatus(boolean isError, String message){
            this.isError = isError;
            this.message = message;
        }
    }

    private BroadcastReceiver mRegistrationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //String regId = intent.getStringExtra("regId");
            boolean isError = intent.getBooleanExtra("isError", true);
            if(isError){
                _globalRegError = true;
                String msg = intent.getStringExtra("message");
                launchErrorNotifier(msg);
            }else {
                launchNextActivity();
            }
        }
    };
}
