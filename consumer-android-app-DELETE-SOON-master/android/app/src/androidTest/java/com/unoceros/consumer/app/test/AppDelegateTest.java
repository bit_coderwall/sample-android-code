package com.unoceros.consumer.app.test;

import android.test.InstrumentationTestCase;

import org.joda.time.DateTime;

/**
 * Created by Ejiro
 */
public class AppDelegateTest extends InstrumentationTestCase {

    public void testGetEstimatedDateOfNextRewardInternal() {
        DateTime dt = new DateTime(2013, 9, 1, 0, 0);
        DateTime nextReward = new DateTime(2013, 9, 1, 0, 0); //AppState.getEstimatedDateOfNextRewardInternal(dt, dt, 0);
        assertEquals(nextReward, dt);
    }
}
