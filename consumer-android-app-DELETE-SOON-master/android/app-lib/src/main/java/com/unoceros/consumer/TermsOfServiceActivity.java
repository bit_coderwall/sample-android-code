package com.unoceros.consumer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.services.AdProfileServices;
import com.unoceros.consumer.util.AppUtility;


public class TermsOfServiceActivity extends ActionBarActivity {

    public final static int WALK_THRU_REQUEST = 2222;
    private static final String TAG = "TermsOfServiceActivity";
    private SharedPreferences mPref;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_terms_of_service);

        //enable up navigation
        final ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setCustomView(R.layout.actionbar_generic);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);

            //remove home icon
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        mPref = this.getSharedPreferences(AppUtility.SYNC_PREF, Context.MODE_PRIVATE);

		//((TextView) findViewById(R.id.welcome_unoceros_textview)).setMovementMethod(LinkMovementMethod.getInstance());
		//((TextView) findViewById(R.id.welcome_unoceros_textview)).setText(Html.fromHtml(getResources().getString(R.string.welcome_to)));

        findViewById(R.id.accept_tos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                termsOfServiceAccepted(TermsOfServiceActivity.this, true);
            }
        });

        /*
        findViewById(R.id.walkthrough_no_agree_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //tracking TOS
                TrackingManager.getInstance(AppController.getInstance()).
                        trackEvent(TermsOfServiceActivity.this, "TOS Not-Accepted");

                setResult(RESULT_CANCELED);
                TermsOfServiceActivity.this.finish();
            }
        }); */
	}

    public static void termsOfServiceAccepted(Activity activity, boolean finishActivity){
        if(activity != null) {
            AppState.instance().acceptTOSAndPP();
            activity.setResult(RESULT_OK);

            //request new affiliate id if null
            SharedPreferences mPref = activity.getSharedPreferences(AppUtility.SYNC_PREF, Context.MODE_PRIVATE);
            String affiliateId = mPref.getString("affiliateId", null);
            if (TextUtils.isEmpty(affiliateId)) {
                String companyName = AppController.getInstance().getUserInfo().getUserName();
                AdProfileServices profile = new AdProfileServices();
                profile.updateProfile(activity, companyName, "", "", "", "", "11111", "", false);
            }

            //broadcast sync setup service request
            Intent syncIntent = new Intent(AppUtility.SYNC_ACTION_SETUP);
            // You can also include some extra data.
            syncIntent.putExtra("message", "User Accepted Terms of Service!");
            syncIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            activity.sendBroadcast(syncIntent);
            
            //tracking TOS
            TrackingManager.getInstance(AppController.getInstance()).
                    trackEvent(activity, "TOS Accepted");

            if(finishActivity)
                //finally close
                activity.finish();
        }
    }
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
		return true;
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
            default:{
                //
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkNetworkConnection() {
        if (!AppUtility.haveNetworkConnection(this)) {
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        }
    }


    public void onResume(){
        super.onResume();

        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "TermsOfService.onResume", AppState.instance().getStatusJson());

        checkNetworkConnection();
    }

    public void onPause(){
        super.onPause();

        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "TermsOfService.onPause", AppState.instance().getStatusJson());
    }

    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }
}
