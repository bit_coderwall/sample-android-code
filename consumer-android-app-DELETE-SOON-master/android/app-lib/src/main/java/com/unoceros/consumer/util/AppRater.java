package com.unoceros.consumer.util;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


import com.unoceros.consumer.R;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;

import java.util.List;

public class AppRater {
    private static String APP_TITLE = "UNOCEROS APP"; //"YOUR-APP-NAME";
    private static String APP_PNAME = "com.unoceros.compute"; //"YOUR-PACKAGE-NAME";
    
    private static int DAYS_UNTIL_PROMPT = 3;
    private static int LAUNCHES_UNTIL_PROMPT = 7;
    
    public static Dialog app_launched(Activity context, String appTitle, String appPackageName) throws NullPointerException{
    	if(appTitle == null || appTitle.isEmpty() || 
    			appPackageName == null || appPackageName.isEmpty()){ 
    		throw new NullPointerException("app title or app package name cannot be null");
    	}
    	
    	APP_TITLE = appTitle;
    	APP_PNAME = appPackageName;
    	try{
    		DAYS_UNTIL_PROMPT = Integer.parseInt(context.getString(R.string.apprater_days_until_prompt));
    		LAUNCHES_UNTIL_PROMPT = Integer.parseInt(context.getString(R.string.apprater_launches_until_prompt)); 
    	}catch(Exception e){
    		DAYS_UNTIL_PROMPT = 3;
    		LAUNCHES_UNTIL_PROMPT = 7;
    	}
    	
        SharedPreferences prefs = context.getSharedPreferences("apprater", 0);
        if (prefs.getBoolean("dontshowagain", false)) { return null; }
        
        SharedPreferences.Editor editor = prefs.edit();
        
        // Increment launch counter
        long launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("launch_count", launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }
        
        // Wait at least n days before opening
        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
            if (System.currentTimeMillis() >= date_firstLaunch + 
                    (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
                return showRateDialog(context, editor);
            }
        }
        
        editor.commit();
        return null;
    }   
    
    public static void showRateDialog(final Activity context, final SharedPreferences.Editor editor,
    		String appTitle, String appPackageName) throws NullPointerException{
    	if(appTitle == null || appTitle.isEmpty() || 
    			appPackageName == null || appPackageName.isEmpty()){
    		throw new NullPointerException("app title or app package name cannot be null");
    	}
    	
    	APP_TITLE = appTitle;
    	APP_PNAME = appPackageName;
    	try{
    		DAYS_UNTIL_PROMPT = Integer.parseInt(context.getString(R.string.apprater_days_until_prompt));
    		LAUNCHES_UNTIL_PROMPT = Integer.parseInt(context.getString(R.string.apprater_launches_until_prompt)); 
    	}catch(Exception e){
    		DAYS_UNTIL_PROMPT = 3;
    		LAUNCHES_UNTIL_PROMPT = 7;
    	}
    	
    	showRateDialog(context, editor);
    }
    
	public static Dialog showRateDialog(final Activity context, final SharedPreferences.Editor editor) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		View v = context.getLayoutInflater().inflate(R.layout.apprater_dialog, null);
		builder.setView(v);
		builder.setTitle("Rate " + APP_TITLE);
		
		final AlertDialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		
		//message text
		TextView messageText = (TextView)v.findViewById(R.id.apprater_dialog_message);
		String message = context.getResources().getString(R.string.apprater_message);
		message = String.format(message, APP_TITLE);
		messageText.setText(message);
		
		Button rateAppBtn = (Button) v.findViewById(R.id.apprater_dialog_rate_app_btn);
		rateAppBtn.setText("Rate " + APP_TITLE);
		rateAppBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	if (editor != null) {
                    editor.putBoolean("dontshowagain", true);
                    editor.commit();
                }
            	try{
            		TrackingManager.getTracker().trackEvent(context, "appRater_rated_app");
            		//try opening the marketplace app
            		Intent intent = AppRater.showMarketAppstore(context, APP_PNAME);
            		if(intent != null)
            			context.startActivity(intent);
            	}catch(Exception e){
            	}finally{
            		dialog.dismiss();
            	}
            }
        });   

		Button remindMeBtn = (Button) v.findViewById(R.id.apprater_dialog_remind_me_later_btn);
		remindMeBtn.setText("Remind me later");
		remindMeBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
        		try {
					TrackingManager.getTracker().trackEvent(context, "appRater_remind_me");
				} catch (Exception e) {
				}finally{
					dialog.dismiss();
				}
            }
        });
		
		Button noThanksBtn = (Button) v.findViewById(R.id.apprater_dialog_no_thanks_btn);
		noThanksBtn.setText("No, thanks");
		noThanksBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    editor.putBoolean("dontshowagain", true);
                    editor.commit();
                }
                try {
					TrackingManager.getTracker().trackEvent(context, "appRater_no_thanks");
				} catch (Exception e) {
				}finally{
					dialog.dismiss();
				}
            }
        });
		
		dialog.show();
		return dialog;
	}

	
	/**
     * Returns intent that  opens app in Google Play or Amazon Appstore
     * @param activity
     * @param packageName
     * @return null if no market available, otherwise intent
     */
    public static Intent showMarketAppstore(Activity activity, String packageName){
    	//google market
        Intent i = new Intent(Intent.ACTION_VIEW);
        String url = activity.getString(R.string.apprater_market_google_url) + packageName; //google market
        i.setData(Uri.parse(url));
        if (AppController.getInstance().isForAmazonMarket() == false){
            return i;
        }

        //amazon appstore
        url = activity.getString(R.string.apprater_market_amazon_url) + packageName; //amazon appstore
        i.setData(Uri.parse(url)); 
        if (AppController.getInstance().isForAmazonMarket() == true){
            return i;
        }
        
        //default to station app page
        url = activity.getString(R.string.apprater_unoceros_url);//apps page
        i.setData(Uri.parse(url));
        
        return i;
    }

    public static boolean isIntentAvailable(Activity context, Intent intent) {
        final PackageManager packageManager = context.getPackageManager();      
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent,PackageManager.MATCH_DEFAULT_ONLY);
        boolean isAvail = list.size() > 0;
        
        return isAvail;
    }
}