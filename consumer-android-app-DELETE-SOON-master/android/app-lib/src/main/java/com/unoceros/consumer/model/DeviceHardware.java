package com.unoceros.consumer.model;

import android.app.Application;
import android.net.Uri;
import android.os.Build;

import com.unoceros.consumer.R;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.DeviceUuidFactory;
import com.unoceros.consumer.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import java.util.UUID;

public class DeviceHardware {

	private static final String TAG = "DeviceHardware";
	
	private String platformType;
	private String platformVersion;
	private String osVersion;
	private String os;
	private String appName;
	private String appVersion;
    private String deviceInfo;

    /**not 100% unique, this are use for attribution*/
    private String telephonyId;
    private String androidId;
    private String googleAid;

    /**unique android id*/
    private volatile static UUID uuid;

    private Application mContext;
	
	public DeviceHardware(Application context) {
		mContext = context;

        //get uuid prior to setting up this hardward model
        uuid = DeviceUuidFactory.instance(context).getDeviceUuid();

        //set up the hardware model
        Build b = new Build();
		setOs("Android");
		setOsVersion(Build.VERSION.RELEASE);
		setPlatformVersion(Build.MODEL);
		setPlatformType(Build.MANUFACTURER);
		setBuild(b);

        //log
		Log.d(TAG, Build.class.toString());
	}
	
	private void setBuild(Build b){
		String VERSION = Build.VERSION.RELEASE;
		String DEVICE = Build.DEVICE;
		String DISPLAY = Build.DISPLAY;
		String MANUFACTURER = Build.MANUFACTURER;
		String MODEL = Build.MODEL;
		String PRODUCT = Build.PRODUCT;
		String TYPE = Build.TYPE;
		String BRAND = Build.BRAND;

        deviceInfo = "DEVICEID: " + getUniqueDeviceId() +
                "\nVERSION: " + VERSION +
                //"\nDEVICE: " + DEVICE +
                //"\nDISPLAY: " + DISPLAY +
                "\nMANUFACTURER: " + MANUFACTURER +
                "\nMODEL: " + MODEL +
                //"\nPRODUCT: " + PRODUCT +
                //"\nTYPE: " + TYPE +
                "\nBRAND: " + BRAND;

		Log.d(TAG, "DEVICEID: " + getUniqueDeviceId() +
                "\nVERSION: " + VERSION +
                "\nDEVICE: " + DEVICE +
                "\nDISPLAY: " + DISPLAY +
                "\nMANUFACTURER: " + MANUFACTURER +
                "\nMODEL: " + MODEL +
                "\nPRODUCT: " + PRODUCT +
                "\nTYPE: " + TYPE +
                "\nBRAND: " + BRAND);
		
	}
	public String getPlatformType() {
		return platformType;
	}
	public void setPlatformType(String platformType) {
		if(platformType != null)
			this.platformType = Uri.encode(platformType);
	}
	public String getPlatformVersion() {
		return platformVersion;
	}
	public void setPlatformVersion(String platformVersion) {
		if(platformVersion != null)
			try {
				this.platformVersion = URLEncoder.encode(platformVersion, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				
			}
	}
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		if(osVersion != null)
			this.osVersion = Uri.encode(osVersion);
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		if(os != null)
			this.os = Uri.encode(os);
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		if(appName != null)
			this.appName = Uri.encode(appName);
	}
	public String getAppVersion() {
		if(appVersion == null || appVersion.isEmpty()){
			appVersion = AppUtility.getVersionName(mContext);
		}
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		if(appName != null)
			this.appVersion = Uri.encode(appVersion);
	}

    public String toString(){

        return deviceInfo+" \nAPP VERSION: "+mContext.getString(R.string.app_version);
    }

    public UUID getUniqueDeviceId(){
        return uuid!=null?uuid:UUID.randomUUID();
    }

    public static String getOSVersion(){
        return Build.VERSION.RELEASE;
    }

    public String getTelephonyId(){
        return telephonyId;
    }
    public void setTelephonyId(String telephonyId) {
        this.telephonyId = telephonyId;
    }

    public String getGoogleAid() {
        return googleAid;
    }

    public void setGoogleAid(String googleAid) {
        this.googleAid = googleAid;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }
}
