package com.unoceros.consumer;


import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.model.PushMessage;
import com.unoceros.consumer.util.AppRater;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.util.NotificationHelper;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 */
public class SplashUI extends ActionBarActivity
        implements SensorEventListener {

    /**
     * Sensor stuff
     */
    private SensorManager _sensorManager;
    private Sensor _tempSensor;

    private static final String TAG = "LaunchScreen";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!AppState.isInitialized()) {
            AppState.setContext(AppController.getInstance());
        }

        if(getSupportActionBar() != null)
            getSupportActionBar().hide();

        _sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        if (_sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE) != null) {
            _tempSensor = _sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
            _sensorManager.registerListener(this, _tempSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }

        setContentView(R.layout.activity_splash_ui);

        // create tracking pixel for HasOffers
        WebView pixelTracker = (WebView) findViewById(R.id.pixeltracker);
        if (pixelTracker != null) {
            if (AppState.IS_QA_BUILD) Log.v(TAG, "tracking pixel");
            pixelTracker.loadData("<!-- Offer Conversion: Unoceros (Android) --><iframe src=\"https://getaoffer.go2jump.org/SL2l\" scrolling=\"no\" frameborder=\"0\" width=\"1\" height=\"1\"></iframe><!-- // End Offer Conversion -->",
                    "text/html", "UTF-8");
        } else if (AppState.IS_QA_BUILD) Log.v(TAG, "Couldn't track pixel");

        //Modified Intent to point at slickblue activity
        // Change the behavior of the sign in button as needed.
        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!AppState.instance().isSignedIn() ||
                        !AppState.instance().userAgreedToTOSAndPP()) {
                    Intent myIntent = new Intent(SplashUI.this, SignUpSlickblueActivity.class);
                    SplashUI.this.startActivity(myIntent);
                } else if (AppState.instance().isSignedIn() &&
                        AppState.instance().userAgreedToTOSAndPP()) {
                    Intent myIntent = new Intent(SplashUI.this, MainLandingActivity.class);
                    Bundle extra = new Bundle();
                    extra.putBoolean("LaunchedFromSplash", true);
                    myIntent.putExtras(extra);
                    SplashUI.this.startActivity(myIntent);
                }
            }
        });

        //launch App Rater dialog
        AppRater.app_launched(this, this.getString(R.string.app_name), getPackageName());

        //check if launch from notification
        boolean stickyStart = getIntent().getBooleanExtra(NotificationHelper.STICKY_NOTIFICATION_START, false);
        if(stickyStart) {
            //track app start
            TrackingManager.getInstance(AppController.getInstance()).trackAppStart(this);

            Intent messageIntent = new Intent();
            Bundle messageBundle = getIntent().getBundleExtra("bundle");
            if(messageBundle != null) {
                messageBundle.putBoolean("fromPush", true);
                messageIntent.setClass(this, MessageCenterActivity.class);
            }else{
                messageBundle = new Bundle();
                messageIntent.setClass(this, SplashUI.class);
            }
            messageIntent.putExtras(messageBundle);
            startActivity(messageIntent);
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setAppStatus();
        _sensorManager.registerListener(this, _tempSensor, SensorManager.SENSOR_DELAY_NORMAL);

        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "SplashUI.onResume", AppState.instance().getStatusJson());
    }

    @Override
    protected void onPause() {
        super.onPause();
        _sensorManager.unregisterListener(this);

        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "SplashUI.onPause", AppState.instance().getStatusJson());
    }

    @Override
    protected void onDestroy() {
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "SplashUI.onDestroy", AppState.instance().getStatusJson());
        TrackingManager.getInstance(this.getApplication()).
                trackDestroyActivity(this);
        super.onDestroy();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        setAppStatus();
    }

    private void setAppStatus() {
        //check if user email is not a valid pattern, show login screen
        if(!AppUtility.isValidateEmail(AppController.getInstance().getUserInfo().getUserEmail())) {
            Toast.makeText(this, "Your email address is not valid.", Toast.LENGTH_SHORT).show();
            AppState.instance().signOut();
        }

        if (AppState.instance().isSignedIn() && AppState.instance().userAgreedToTOSAndPP()) {
            final Button signInButton = (Button) findViewById(R.id.login);
            signInButton.setText(R.string.continue_to_status);

        }else{
            final Button signInButton = (Button) findViewById(R.id.login);
            signInButton.setText(R.string.login);
        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        AppState.instance().setTemperature(event.values[0] * 9 / 5 + 32);
    }
}
