package com.unoceros.consumer;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;


public class AboutPreferencesActivity extends PreferenceActivity
        implements OnSharedPreferenceChangeListener {

    private AboutPreferencesActivity mContext;
    private SharedPreferences mPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.about_preference);
        mContext = this;

        //set up title/subtitle bar
        actionBarSetup();

        // get reference to shared pref
        mPref = PreferenceManager.getDefaultSharedPreferences(mContext.getApplicationContext());

        Preference tof = findPreference("terms_and_conditions");
        tof.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                String url = mContext.getString(R.string.terms_and_conditions_link);
                openWebView(preference, url);
                return false;
            }
        });

        Preference ppo = findPreference("privacy_policy");
        ppo.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                String url = mContext.getString(R.string.privacy_policy_link);
                openWebView(preference, url);
                return false;
            }
        });

        Preference tpl = findPreference("open_source_license");
        tpl.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                try {
                    String url = "file:///android_asset/html/licenses.html";
                    openWebView(preference, url);
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), "Error occurred loading licensing page: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                return false;
            }
        });

        Preference debug = findPreference("debug_info");
        if(AppUtility.signedWithDebugKey(this) || AppUtility.isDebuggable(this)) {
            debug.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Dialog auth_dialog = new Dialog(AboutPreferencesActivity.this,
                            android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    auth_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    auth_dialog.setContentView(R.layout.fragment_webview);
                    WebView web = (WebView) auth_dialog.findViewById(R.id.fragment_webView);
                    //web.getSettings().setJavaScriptEnabled(true);
                    web.loadData(Log.debug_logs.toString(), "text/html", "UTF-8");
                    auth_dialog.show();
                    return false;
                }
            });
        }else{
            PreferenceScreen screen = getPreferenceScreen();
            screen.removePreference(debug);
        }
    }


    /**
     * Sets the Action Bar for new Android versions.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        ActionBar ab = getActionBar();
        if(ab!= null){
            ab.setDisplayHomeAsUpEnabled(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                ab.setTitle(this.getString(R.string.activity_about_preference));
                ab.setSubtitle(R.string.app_version);
            }
        }
    }

    public void openWebView(Preference preference, String url) {
        Uri link = Uri.parse(url);
        Bundle webBundle = new Bundle();
        Intent intent = new Intent();
        webBundle.putParcelable("uri", link);
        webBundle.putString(ModalWebViewActivity.EXTRA_TITLE, preference.getTitle().toString());
        intent.setClass(mContext, ModalWebViewActivity.class);
        intent.putExtras(webBundle);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.getActionBar() != null)
            this.getActionBar().setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // app icon in action bar clicked; go home
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    // When the activity is started
    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    }
}
