package com.unoceros.consumer.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.Toast;

import com.unoceros.consumer.OfferDetailActivity;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Const;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.R;
import com.unoceros.consumer.adapter.CampaignListAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Amit
 *
 * @See https://developer.android.com/samples/SwipeRefreshListFragment/index.html
 */
public class CampaignListFragment extends ListFragment{

    public static final String TAG = "CampaignListFragment";
    public static final String IMAGE_RESOURCE_ID = "iconResourceID";
    public static final String ITEM_NAME = "itemName";

    private List<Campaign> campaignItems;
    private CampaignListAdapter listAdapter;
    private AlertDialog mInactiveAlertDialog;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private OnFragmentInteractionListener mListener;

    // newInstance constructor for creating fragment with arguments
    public static CampaignListFragment newInstance(int page, String title) {
        CampaignListFragment campaign = new CampaignListFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        campaign.setArguments(args);
        return campaign;
    }

    // newInstance constructor for creating fragment with arguments
    public static CampaignListFragment newInstance() {
        CampaignListFragment campaign = new CampaignListFragment();
        Bundle args = new Bundle();
        campaign.setArguments(args);
        return campaign;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // retain this fragment
        setRetainInstance(true);

        // Notify the system to allow an options menu for this fragment.
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        // Create the list fragment's content view by calling the super method
        final View listFragmentView = super.onCreateView(inflater, container, savedInstanceState);

        // Now create a SwipeRefreshLayout to wrap the fragment's content view
        mSwipeRefreshLayout = new ListFragmentSwipeRefreshLayout(container.getContext());

        // Add the list fragment's content view to the SwipeRefreshLayout, making sure that it fills
        // the SwipeRefreshLayout
        mSwipeRefreshLayout.addView(listFragmentView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        // Make sure that the SwipeRefreshLayout will fill the fragment
        mSwipeRefreshLayout.setLayoutParams(
                new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));

        // Now return the SwipeRefreshLayout as this fragment's content view
        return mSwipeRefreshLayout;
    }

    /**
     * Set the {@link android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener} to listen for
     * initiated refreshes.
     *
     * @see android.support.v4.widget.SwipeRefreshLayout#setOnRefreshListener(android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener)
     */
    public void setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener listener) {
        mSwipeRefreshLayout.setOnRefreshListener(listener);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setBackgroundColor(Color.parseColor(AppUtility.LIST_BG_HEX_COLOR_TRANSPARENT));

        //only do this if state is not saved
        if(savedInstanceState == null) {
            setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Log.i(TAG, "onRefresh called from SwipeRefreshLayout");
                    initiateRefresh(false);
                }
            });

            Log.i(TAG, "onActivityCreated");

            // No data initially
            setEmptyText("Loading Rewards Catalog...");

            //campaigns
            campaignItems = new ArrayList<>();

            //create an empty adapter
            listAdapter = new CampaignListAdapter(this.getActivity(), campaignItems, true);
            setListAdapter(listAdapter);

            //remove scrollbars
            ListView listView = getListView();
            listView.setVerticalScrollBarEnabled(false);
            listView.setHorizontalScrollBarEnabled(false);

            //ListView listView = this.getListView();
            //Animation animation = AnimationUtils.loadAnimation(this.getActivity().getApplicationContext(), R.anim.up_from_bottom); //
            //(position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
            //listView.startAnimation(animation);

            //LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this.getActivity(), R.anim.list_animation_layout);
            //listView.setLayoutAnimation(controller);


            // Change the colors displayed by the SwipeRefreshLayout by providing it with // color resource ids
            setColorScheme(android.R.color.holo_red_dark,
                    android.R.color.holo_green_light,
                    android.R.color.holo_blue_light,
                    android.R.color.holo_orange_light);

        }
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
    }


    @Override
    public void onResume(){
        super.onResume();

        try {
            //upload from cache
            initiateRefresh(true);
        }catch (Exception e){
            //some terrible happened
            Log.e(TAG, e.getMessage(), e);
        }
    }

    public void onPause(){
        super.onPause();
    }

    /*
    private void invalidateDrawerCounter(){
        //notify containing parent of item change
        ArrayList<Campaign> campaigns = OfferDetailFragment.retrieveFromDataStore(getActivity(), Const.DATASTORE_WISHLIST_NAME);
        if(campaigns != null) {
            Bundle bundle = new Bundle();
            bundle.putInt("position", getArguments().getInt("cart_position"));
            bundle.putInt("counter", campaigns.size());
            mListener.onFragmentInteraction(bundle);
        }
    }*/

    /**
     * By abstracting the refresh process to a single method, the app allows both the
     * SwipeGestureLayout onRefresh() method and the Refresh action item to refresh the content.
     */
    private void initiateRefresh(final boolean fromCache) {
        Log.i(TAG, "initiateRefresh");
        final Activity activity = getActivity();

        ArrayList<Campaign> campaigns = CampaignListFragment.readFromInternalStorage(activity, "OFFERS");
        if(campaigns != null && campaigns.size() > 0){
            //use cached version
            parseCacheList(campaigns);

            // Stop the refreshing indicator
            setRefreshing(false);
        }else {
            String Api_Domain = activity.getString(R.string.hasoffer_offer_domain_api);
            String Api_Offer_Path = activity.getString(R.string.hasoffer_all_offer_api_param);
            //int pageNum = 1;
            //String URL_FEED = Api_Domain + "?" + String.format(Api_Offer_Path, pageNum);
            String URL_FEED = Api_Domain + "?" + Api_Offer_Path;
            String Api_Offer_Path_Ids = activity.getString(R.string.hasoffer_all_offer_api_ids);
            String URL_FEED_IDs = Api_Domain + "?" + Api_Offer_Path_Ids;

            //sleep for a second...simulates loading
            if(!fromCache) {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            new OfferDownloadingTask().execute(URL_FEED, URL_FEED_IDs);
        }
    }

    private void parseCacheList(ArrayList<Campaign> campaigns){
        //clear the data store
        listAdapter.clearData();
        // notify data changes to list adapter
        listAdapter.notifyDataSetChanged();

        for(Campaign campaign: campaigns){
            listAdapter.addData(campaign);
        }
        // notify data changes to list adapter
        listAdapter.notifyDataSetChanged();

    }

    /**
     * Parsing json response and passing the offer data to feed view list adapter
     * */
    private void parseJsonFeed(JSONObject jsonOffer, JSONObject jsonAllIds) {
        try {
            JSONArray ids = jsonAllIds.getJSONObject("response").getJSONArray("data");
            JSONObject data = jsonOffer.getJSONObject("response").getJSONObject("data");
            JSONObject offers = data.isNull("data")?data:data.getJSONObject("data");

            //clear the data store
            listAdapter.clearData();
            // notify data changes to list adapter
            listAdapter.notifyDataSetChanged();

            //we had to do this because the json returned for the Offer data are not in a list
            //kind of weird why hasOffer did it like this
            //for(int j = 0; j < 20; j++)
            ArrayList<Campaign> campaigns = new ArrayList<Campaign>();
            ArrayList<Campaign> campaignsInactive = new ArrayList<Campaign>();
            ArrayList<Campaign> campaignsActive = new ArrayList<Campaign>();

            for(int i = 0; i < ids.length(); i++){
                //for each id get the associated offer
                if(ids.get(i) == null)
                    continue; //hmm?? skip either way

                try {
                    String offerId = ids.get(i).toString();
                    JSONObject feedObj = offers.isNull(offerId)?null:offers.getJSONObject(offerId);
                   // if (feedObj == null)
                     //   continue; //skip no offer found

                    JSONObject offerObj = feedObj.getJSONObject("Offer");
                    JSONObject thumbObj = feedObj.isNull("Thumbnail") ? null : feedObj.getJSONObject("Thumbnail");

                    String id = offerObj.isNull("id") ? null : offerObj.getString("id");
                    String title = offerObj.isNull("name") ? null : offerObj.getString("name");
                    String desc = offerObj.isNull("description") ? null : offerObj.getString("description");
                    String status = offerObj.isNull("status") ? null : offerObj.getString("status");
                    String ratings = offerObj.isNull("rating") ? null : offerObj.getString("rating");
                    String score = offerObj.isNull("ref_id") ? null : offerObj.getString("ref_id");
                    String offer_url = offerObj.isNull("offer_url") ? null : offerObj.getString("offer_url");
                    String prev_url = offerObj.isNull("preview_url") ? null : offerObj.getString("preview_url");
                    String expiration_date = offerObj.isNull("expiration_date") ? null : offerObj.getString("expiration_date"); //yyyy-mm-dd

                    String image = thumbObj.isNull("url") ? null : thumbObj.getString("url");

                    Campaign campaign = new Campaign();
                    campaign.setId(id);
                    campaign.setTitle(title);
                    campaign.setDescription(desc);
                    campaign.setThumbnailUrl(image);
                    campaign.setStatus(status);
                    campaign.setRating(ratings);
                    campaign.setScore(score);
                    campaign.setUrl(offer_url);
                    campaign.setPrevUrl(prev_url);
                    campaign.setExpireDateStr(expiration_date);

                    // Creating two arrays to merge active and Inactive offers
                    if(score == null || score.isEmpty() || score.equals("0")){
                        campaignsInactive.add(campaign);
                    }else{
                        campaignsActive.add(campaign);
                    }


                }catch (Exception e){
                    Log.e(TAG, e.getMessage(), e);
                }
            }
            campaigns =  arrangeCampaign(campaignsActive, campaignsInactive);
            CampaignListFragment.writeToInternalStorage(getActivity(), "OFFERS", campaigns);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e); //e.printStackTrace();

            ArrayList<Campaign> campaigns = CampaignListFragment.readFromInternalStorage(getActivity(), "OFFERS");
            if(campaigns != null && campaigns.size() > 0) {
                //use cached version
                parseCacheList(campaigns);

                // Stop the refreshing indicator
                setRefreshing(false);
            }
        } finally{
            // notify data changes to list adapter
            listAdapter.notifyDataSetChanged();
        }
    }

    private  ArrayList<Campaign> arrangeCampaign(ArrayList<Campaign> campaignsActive, ArrayList<Campaign> campaignsInactive) {
        int inactiveSize = campaignsInactive.size();
        int activeSize = campaignsActive.size();
        int totalSize = activeSize+inactiveSize;
        boolean order = true;


        ArrayList<Campaign> campaign = new ArrayList<Campaign>();
        for(int i=0, j=0;i<totalSize;){
            if(order==true && activeSize==0){
                order=false;
            }
            if( order==true && activeSize!=0) {
                activeSize--;
                order = false;
                campaign.add(campaignsActive.get(i));
                listAdapter.addData(campaignsActive.get(i));
                i++;
            }
            if(order==false && inactiveSize!=0){
                inactiveSize--;
                order=true;
                campaign.add(campaignsInactive.get(j));
                listAdapter.addData(campaignsInactive.get(j));
                j++;
            }
            if(inactiveSize==0 && activeSize==0)
                break;
        }
        return campaign;
    }


    private class OfferDownloadingTask extends AsyncTask<String, Integer, ArrayList<JSONObject>> {
        @Override
        protected void onProgressUpdate(Integer... values) {
        }
        @Override
        protected void onPostExecute(ArrayList<JSONObject> jsons) {
            //parsing json data
            parseJsonFeed(jsons.get(0), jsons.get(1));

            // Stop the refreshing indicator
            setRefreshing(false);
        }
        @Override
        protected ArrayList<JSONObject> doInBackground(String... urls) {
            String offers_api = urls[0];
            String ids_api = urls[1];
            // getting JSON string from URL
            ArrayList<JSONObject> jsonlist = null;
            try {
                jsonlist = new ArrayList<JSONObject>();

                JSONObject offerJson = AppUtility.getJSONFromUrl(offers_api);
                jsonlist.add(offerJson);

                JSONObject idsJson = AppUtility.getJSONFromUrl(ids_api);
                jsonlist.add(idsJson);
            }catch (Exception e){
                Log.e(TAG, e.getMessage(), e);
            }

            return jsonlist;
        }
    }

    /**
     * Set whether the {@link android.support.v4.widget.SwipeRefreshLayout} should be displaying
     * that it is refreshing or not.
     */
    public void setRefreshing(boolean refreshing) {
        mSwipeRefreshLayout.setRefreshing(refreshing);
    }

    /**
     * Set the color scheme for the {@link android.support.v4.widget.SwipeRefreshLayout}.
     */
    public void setColorScheme(int colorRes1, int colorRes2, int colorRes3, int colorRes4) {
        mSwipeRefreshLayout.setColorScheme(colorRes1, colorRes2, colorRes3, colorRes4);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Campaign campaign = (Campaign)getListView().getItemAtPosition(position);

        Log.d(TAG, "Clicked on offer with reference id: "+campaign.getScore());
         if(campaign.getScore() != null && !campaign.getScore().equals("0")
                && !campaign.getScore().equals("")) {
            Intent details = new Intent(getActivity(), OfferDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelable("campaign", campaign);
            bundle.putInt("position", position);
            bundle.putLong("id", id);
            details.putExtras(bundle);
            getActivity().startActivity(details);
        }else{
             Activity activity = getActivity();
             SharedPreferences mPref = activity.getSharedPreferences(AppUtility.SYNC_PREF, Context.MODE_PRIVATE);
             WebView webView = new WebView(activity);
             webView.setVisibility(View.INVISIBLE);
             final String _affiliateId = mPref.getString("affiliateId", null);
             OfferDetailFragment.postbackImpression(activity, campaign, webView, _affiliateId);
              mInactiveAlertDialog = AppUtility.alertInactiveDialog(activity, campaign, _affiliateId);
        }
    }

    /**
     * Inner class for refresh functionality
     */
    private class ListFragmentSwipeRefreshLayout extends SwipeRefreshLayout {

        public ListFragmentSwipeRefreshLayout(Context context) {
            super(context);
        }

        /**
         * As mentioned above, we need to override this method to properly signal when a
         * 'swipe-to-refresh' is possible.
         *
         * @return true if the {@link android.widget.ListView} is visible and can scroll up.
         */
        @Override
        public boolean canChildScrollUp() {
            final ListView listView = getListView();
            if (listView.getVisibility() == View.VISIBLE) {
                return canListViewScrollUp(listView);
            } else {
                return false;
            }
        }
    }

    /**
     * Utility method to check whether a {@link ListView} can scroll up from it's current position.
     * Handles platform version differences, providing backwards compatible functionality where
     * needed.
     */
    private static boolean canListViewScrollUp(ListView listView) {
        if (android.os.Build.VERSION.SDK_INT >= 14) {
            // For ICS and above we can call canScrollVertically() to determine this
            return ViewCompat.canScrollVertically(listView, -1);
        } else {
            // Pre-ICS we need to manually check the first visible item and the child view's top
            // value
            return listView.getChildCount() > 0 &&
                    (listView.getFirstVisiblePosition() > 0
                            || listView.getChildAt(0).getTop() < listView.getPaddingTop());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if(mInactiveAlertDialog != null && mInactiveAlertDialog.isShowing())
         mInactiveAlertDialog.dismiss();
    }

    @Override
    public void setUserVisibleHint(final boolean visible) {
        super.setUserVisibleHint(visible);

        if (visible) {
            if(mListener != null){
                mListener.onFragmentVisible(true);
            }
        }
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            if(mListener != null){
                mListener.onFragmentVisible(true);
            }
        }
    }

    /**
     * SERIALIZATION HELPERS
     */
    private static synchronized ArrayList<Campaign> readFromInternalStorage(Context context, String fileName) {
        try {
            File file = new File(context.getFilesDir(), fileName);
            if (!file.exists() || !file.canRead()) {
                Log.d(TAG, "No saved Offer list");
                return null;
            }
            long  diff = System.currentTimeMillis() - file.lastModified();
            long expireTime = 3600000; //an hour of expiration
            if(AppUtility.signedWithDebugKey(context) || AppUtility.isDebuggable(context)){
                  expireTime = 300000; //5 minute for debugging
            }
            if(diff > expireTime) {
                Log.d(TAG, "Saved Offer list is expired");
                return null;
            }

            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream is = new ObjectInputStream(fis);
            ArrayList<Campaign> campaigns = (ArrayList<Campaign>) is.readObject();
            is.close();
            Log.d(TAG, "Offers list read from disk: " + campaigns);
            return campaigns;
        } catch (Exception ioe) {
            Log.e(TAG, "File read error, creating a new one.", ioe);
        }

        // should never return null, programmer error.
        return null;
    }

    public static synchronized boolean writeToInternalStorage(Context context, String fileName, ArrayList<Campaign> campaigns) {
        try {
            File file = new File(context.getFilesDir(), fileName);
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(campaigns);
            os.close();
        } catch (Exception ioe) {
            Log.e(TAG, "Persisting listed Offers to disk failed!");
            ioe.printStackTrace();
            return false;
        }

        return true;
    }
}
