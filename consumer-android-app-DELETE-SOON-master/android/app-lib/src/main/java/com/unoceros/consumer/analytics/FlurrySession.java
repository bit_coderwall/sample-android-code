package com.unoceros.consumer.analytics;
import android.content.Context;

import com.flurry.android.FlurryAgent;
import com.unoceros.consumer.util.Log;

/**
 * Created by amit
 */
public class FlurrySession {
    public static String FLURRY_KEY = "9XSFC6WDZ8H6ZJWZGPDC";

    public static void startSession(Context context) {
        FlurryAgent.setContinueSessionMillis(30 * 1000);
        FlurryAgent.onStartSession(context, FLURRY_KEY);
        FlurryAgent.setLogEvents(true);
        Log.d("Creating Session for ",":" + context.toString());
    }

    public static void stopSession(Context context) {
        FlurryAgent.onEndSession(context);
        Log.d("Closing Session for ",":" + context.toString());

    }
}
