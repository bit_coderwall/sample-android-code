package com.unoceros.consumer.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.unoceros.consumer.R;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.PushMessage;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.views.FeedImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Amit
 */

public class PushMessageListAdapter extends BaseAdapter {

    private Context context; // activity;
    private LayoutInflater inflater;
    private List<PushMessage> messageList;

    private boolean isClickable = true;

    public PushMessageListAdapter(Activity activity, List<PushMessage> messageList, boolean isClickable) {
        this.context = activity.getApplicationContext();
        this.messageList = messageList;
        if(messageList == null)
            this.messageList = new ArrayList<>();

        this.isClickable = isClickable;
    }

    @Override
    public int getCount() {
        return messageList.size();
    }

    @Override
    public Object getItem(int location) {
        return messageList.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void addData(PushMessage message){
        if(message != null && message.getMessageId() != null)
            messageList.add(message);
    }

    public void removeData(int position){
        if(position < messageList.size())
            messageList.remove(position);
    }

    public void clearData() {
        // clear the data
        messageList.clear();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        //if (convertView == null) {
        //    convertView = LayoutInflater.from(context).inflate(R.layout.row, parent, false);

        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null){
                convertView = inflater.inflate(R.layout.message_center_headlines, null);

            holder = new ViewHolder();

            TextView title = (TextView) convertView.findViewById(R.id.message_headline);
            TextView desc = (TextView) convertView.findViewById(R.id.message_details);
            TextView thumb_text = (TextView) convertView.findViewById(R.id.thumbnail_text);
            TextView label = (TextView) convertView.findViewById(R.id.message_label);
            
            holder.title = title;
            holder.desc = desc;
            holder.thumb_text = thumb_text;
            holder.label = label;
            
            holder.dirty = true;

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        
        // getting movie data for the row
        PushMessage message = messageList.get(position);
        if(message != null) {
            if(message.getMessageId() != null){

                // title
                holder.title.setText(message.getHeadline());
                holder.title.setVisibility(View.VISIBLE);
                
                // description
                if(message.getDetails() != null) {
                    holder.desc.setText(message.getDetails());
                    holder.desc.setVisibility(View.VISIBLE);
                }

                // thumb text and color
                holder.thumb_text.setText(message.getHeadline().toUpperCase().charAt(0)+"");
                holder.thumb_text.setBackgroundColor(message.getColor()); //R.color.bluegraynoceros);
                holder.thumb_text.setVisibility(View.VISIBLE);
                
                if(message.getIsNew()){
                    holder.label.setText("NEW");
                    holder.label.setVisibility(View.VISIBLE);
                }
            }else{
                //hide the details
                holder.title.setVisibility(View.INVISIBLE);
                holder.desc.setVisibility(View.GONE);
                holder.thumb_text.setVisibility(View.INVISIBLE);
                holder.label.setVisibility(View.GONE);
            }

            if (!isClickable) {
                convertView.setEnabled(false);
                convertView.setClickable(false);
            }
        }else{
            //should never happen, but just in case
            convertView.setVisibility(View.GONE);
            removeData(position);
        }
        return convertView;
    }

    static class ViewHolder {
        TextView title;
        TextView desc;
        TextView thumb_text;
        TextView label;
        boolean dirty;
    }
    
    public static int getRandomColor(String title){
        int redMax = 256;
        if(title != null && title.length() > 3)
            redMax = title.charAt(0)+title.charAt(1)+title.charAt(2)+(256/2);
                    
        int R = (int)(Math.random()*redMax);
        int G = (int)(Math.random()*256);
        int B = (int)(Math.random()*256);
        int color = Color.rgb(R, G, B); //random color, but can be bright or dull

        /*
        //to get rainbow, pastel colors
        Random random = new Random();
        final float hue = random.nextFloat();
        final float saturation = 0.9f;//1.0 for brilliant, 0.0 for dull
        final float luminance = 1.0f; //1.0 for brighter, 0.0 for black
        color = Color.getHSBColor(hue, saturation, luminance);
        */
        
        return color;
    }
}
