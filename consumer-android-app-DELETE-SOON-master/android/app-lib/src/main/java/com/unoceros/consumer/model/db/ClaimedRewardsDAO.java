package com.unoceros.consumer.model.db;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Amit
 */
public class ClaimedRewardsDAO {
    private static final String TAG = "ClaimedRewardsDAO";

    // SQL Database
    private SQLiteDatabase database;
    private DbClaimedRewards dbHelper;

    private String[] dbColumns = {DbClaimedRewards.ID,DbClaimedRewards.REWARDS_ID, DbClaimedRewards.REWARDS_UUID,
            DbClaimedRewards.REWARDS_CLAIMED_DATE,DbClaimedRewards.REWARD_GSON_DATA};

    public ClaimedRewardsDAO(Context context){
        //mContext = context.getApplicationContext();

        //create an instance of the db helper
        dbHelper = new DbClaimedRewards(context, DbClaimedRewards.DATABASE_NAME, null, DbClaimedRewards.DATABASE_VERSION);
    }

    public void open(){
        database = dbHelper.getWritableDatabase();
    }

    public void close(){
        database.close();
        dbHelper.close();
    }

    public UUID insertByDate(String date, Campaign campaign, boolean isNew){
        UUID rewardUUID = UUID.randomUUID();
        if(campaign == null)
            return rewardUUID;
        
        if(campaign.getUuid() != null && campaign.getUuid().length() > 0)
            rewardUUID = UUID.fromString(campaign.getUuid());
        else
            campaign.setUuid(rewardUUID.toString());
        Gson gson = new Gson();
        String gsonData = gson.toJson(campaign);
        ContentValues values = new ContentValues();
        values.put(DbClaimedRewards.REWARDS_UUID, campaign.getUuid()); //use as key in endpoint
        values.put(DbClaimedRewards.REWARDS_ID, campaign.getId());
        values.put(DbClaimedRewards.REWARDS_CLAIMED_DATE, date);
        values.put(DbClaimedRewards.REWARD_GSON_DATA, gsonData);

        Log.d(TAG, "inserting rewards by date: "+date);
        Log.d(TAG, "gson data: "+gsonData);

        if(isNew){
            //insert
            database.insert(DbClaimedRewards.DATABASE_TABLE, null, values);
        }else{
            //update
            try{
                if(campaign.getUuid() != null && campaign.getUuid().length() > 0)
                    database.update(DbClaimedRewards.DATABASE_TABLE, values,
                            DbClaimedRewards.REWARDS_UUID+"=?", new String[]{campaign.getUuid()});
                else
                    database.update(DbClaimedRewards.DATABASE_TABLE, values,
                            DbClaimedRewards.REWARDS_ID+"=?", new String[]{campaign.getId()});
            }catch (Exception e){
                Log.e(TAG, e.getMessage(), e);
            }
        }

        return rewardUUID;
    }

    public List<Campaign> retrieveByDate(String date){
        List<Campaign> results = new ArrayList<Campaign>();
        //Log.d(TAG, "retrieving rewards by date: "+date);

        //sanity check
        if(date == null)
            return null;

        try {
            Cursor cursor = database.query(DbClaimedRewards.DATABASE_TABLE, dbColumns,
                    DbClaimedRewards.REWARDS_CLAIMED_DATE + "=?", new String[]{date}, null, null, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                //Log.d(TAG, "Number of items in cursor: " + cursor.getCount());
                Campaign campaign = cursorToReward(cursor);
                if(campaign != null)
                    results.add(campaign);
                cursor.moveToNext();
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        return results;
    }

    public boolean isOfferExistByUUID(String uuid){
        boolean hasOffer = false;
        //sanity check
        if(TextUtils.isEmpty(uuid))
            return false;

        try {
            Cursor cursor = database.query(DbClaimedRewards.DATABASE_TABLE, dbColumns,
                    DbClaimedRewards.REWARDS_UUID + "=?", new String[]{uuid}, null, null, null);

            cursor.moveToFirst();
            if(cursor.getCount() > 0){
                hasOffer = true;
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        return hasOffer;
    }

    public List<Campaign> getAll(){
        List<Campaign> results = new ArrayList<Campaign>();

        try {
            Cursor cursor = database.query(DbClaimedRewards.DATABASE_TABLE, dbColumns,
                    null, null, null, null, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                //Log.d(TAG, "Number of items in cursor: " + cursor.getCount());
                Campaign campaign = cursorToReward(cursor);
                results.add(campaign);
                cursor.moveToNext();
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        return results;
    }

    private Campaign cursorToReward(Cursor cursor){
        Campaign campaign = null;
        try {
            if (cursor != null && cursor.getCount() > 0) {
                int rewardsUUIDIndex = cursor.getColumnIndex(DbClaimedRewards.REWARDS_UUID);
                int rewardsGsonIndex = cursor.getColumnIndex(DbClaimedRewards.REWARD_GSON_DATA);
                String uuid = cursor.getString(rewardsUUIDIndex);
                //Log.d(TAG, "rewards uuid index: "+rewardsUUIDIndex+", UUID: "+uuid);

                Gson gson = new Gson();
                String gson_data = cursor.getString(rewardsGsonIndex);
                //Log.d(TAG, "rewards gson index: "+rewardsGsonIndex+", Gson: "+gson_data);
                
                campaign = gson.fromJson(gson_data, Campaign.class);
                campaign.setUuid(uuid);
            }
        }catch (Exception e){
            Log.e(TAG, e.getMessage(), e);
        }
        return campaign;
    }
}
