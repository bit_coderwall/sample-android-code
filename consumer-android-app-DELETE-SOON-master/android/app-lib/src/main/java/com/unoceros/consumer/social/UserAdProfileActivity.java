package com.unoceros.consumer.social;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.flurry.android.FlurryAgent;
import com.unoceros.consumer.NoNetworkActivity;
import com.unoceros.consumer.R;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.services.AdProfileServices;
import com.unoceros.consumer.util.AppUtility;


/**
 * Created by Amit
 */
public class UserAdProfileActivity extends Activity{

    public static final int REQUEST_CODE = 119;
    private static final String TAG = "UserProfileActivity";

    private UserInfo _mUserInfo;

    private TextView _mAddresView;
    private TextView _mCityView;
    private TextView _mRegionView;
    private TextView _mCountryView;
    private TextView _mZipCodeView;
    private TextView _mPhoneView;

    private String _trackingId;
    private SharedPreferences mPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consumer_profile);

        //enable up navigation
        if(getActionBar() != null)
            getActionBar().setDisplayHomeAsUpEnabled(true);

        //share pref
        final Activity activity = this;
        mPref = activity.getSharedPreferences(AppUtility.SYNC_PREF, Context.MODE_PRIVATE);
        _trackingId = mPref.getString("affiliateId", null);

        //get the global user info
        _mUserInfo = AppController.getInstance().getUserInfo();

        //user profile image
        NetworkImageView profileImageView = (NetworkImageView) findViewById(R.id.affiliate_profile_image);
        profileImageView.setDefaultImageResId(R.drawable.user);
        profileImageView.setImageUrl(_mUserInfo.getUserImageUrl(), AppController.getInstance().getImageLoader());
        profileImageView.setVisibility(View.VISIBLE);

        //user profile name
        ((TextView)findViewById(R.id.affiliate_profile_name)).setText(_mUserInfo.getUserName());

        //update btn
        Button updateBtn = (Button)activity.findViewById(R.id.affiliate_update_btn);
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.setResult(RESULT_OK);
                updateProfile();
            }
        });

        Button skipBtn = (Button)activity.findViewById(R.id.affiliate_skip_btn);
        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.setResult(RESULT_CANCELED);
                finish();
            }
        });

        _mAddresView = (TextView)activity.findViewById(R.id.affiliate_address);
        _mCityView = (TextView)activity.findViewById(R.id.affiliate_city);
        _mRegionView = (TextView)activity.findViewById(R.id.affiliate_state);
        _mCountryView = (TextView)activity.findViewById(R.id.affiliate_country);
        _mZipCodeView = (TextView)activity.findViewById(R.id.affiliate_zipcode);
        _mPhoneView = (TextView)activity.findViewById(R.id.affiliate_phone);
    }

    private void checkNetworkConnection() {
        if (!AppUtility.haveNetworkConnection(this)) {
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        }
    }


    public void onResume(){
        super.onResume();


        checkNetworkConnection();
    }

    public void onPause(){
        super.onPause();

    }

    @Override
    public void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    public void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void updateProfile(){
        if (!AppUtility.haveNetworkConnection(this)) {
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        }else if(TextUtils.isEmpty(_trackingId)) {
            String companyName = _mUserInfo.getUserName();
            String address = _mAddresView.getText().toString();
            String cityName = _mCityView.getText().toString();
            String region = _mRegionView.getText().toString();
            String countryName = _mCountryView.getText().toString();
            String zipCode = _mZipCodeView.getText().toString();
            String phoneNumber = _mPhoneView.getText().toString();

            //zip pattern for validation
            String zipCodePattern = "\\d{5}(-\\d{4})?";

            if(TextUtils.isEmpty(zipCode) || !zipCode.matches(zipCodePattern)){
                _mZipCodeView.setError("Sorry, zipcode is not valid");
            }else {
                AdProfileServices profile =  new AdProfileServices();
                profile.updateProfile(this, companyName, address, cityName, region, countryName, zipCode, phoneNumber, true);
            }
        }else{
            finish();
        }
    }

}
