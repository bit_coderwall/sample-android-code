package com.unoceros.consumer.social;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import com.android.volley.toolbox.NetworkImageView;
import com.flurry.android.FlurryAgent;
import com.unoceros.consumer.ModalWebViewActivity;
import com.unoceros.consumer.NoNetworkActivity;
import com.unoceros.consumer.R;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Const;
import com.unoceros.consumer.util.Log;

public class TwitterSignInActivity extends BaseSignInActivity{
    private static final String TAG = "TwitterSignInActivity";
    public final static int TWITTER_SIGNIN_REQUEST = 4895;

    private Twitter twitter;
    private RequestToken requestToken;
    private SharedPreferences mSharedPreferences;

    private Dialog auth_dialog;
    private WebView web;
    private ViewGroup loginform;

    private String oauth_verifier;
    private AccessToken accessToken;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_progress);

        mSharedPreferences = getSharedPreferences(AppUtility.INSTALLATION_PREF_FILE, MODE_PRIVATE);

        try {
            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.setOAuthConsumerKey(Const.CONSUMER_KEY);
            configurationBuilder.setOAuthConsumerSecret(Const.CONSUMER_SECRET);
            Configuration configuration = configurationBuilder.build();

            //twitter = TwitterFactory.getSingleton(); //new TwitterFactory().getInstance();
            twitter = new TwitterFactory(configuration).getInstance();
            //twitter.setOAuthConsumer(Const.CONSUMER_KEY, Const.CONSUMER_SECRET);

            auth_dialog = new Dialog(this); //, android.R.style.Theme_Black_NoTitleBar);
            auth_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            auth_dialog.setContentView(R.layout.dialog_webview_auth);
            final Window window = auth_dialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            web = (WebView)auth_dialog.findViewById(R.id.dialog_webview);
            loginform = (ViewGroup)auth_dialog.findViewById(R.id.login_form);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void checkNetworkConnection() {
        if (!AppUtility.haveNetworkConnection(this)) {
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        }
    }

    protected void onResume() {
        super.onResume();

        checkNetworkConnection();

        if(isFinishing()){
            if(auth_dialog != null && auth_dialog.isShowing()) {
                auth_dialog.dismiss();
            }
            auth_dialog = null;
        }
    }

    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
        if (isConnected()) {
            String oauthAccessToken = mSharedPreferences.getString(Const.PREF_KEY_TOKEN, "");
            String oAuthAccessTokenSecret = mSharedPreferences.getString(Const.PREF_KEY_SECRET, "");

            ConfigurationBuilder confbuilder = new ConfigurationBuilder();
            Configuration conf = confbuilder
                    .setOAuthConsumerKey(Const.CONSUMER_KEY)
                    .setOAuthConsumerSecret(Const.CONSUMER_SECRET)
                    .setOAuthAccessToken(oauthAccessToken)
                    .setOAuthAccessTokenSecret(oAuthAccessTokenSecret)
                    .build();

            //connected to twitter, get user info
            new AccessTokenGet().execute(true);
        }else{
            askOAuth();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check which request we're responding to
        if (requestCode == TWITTER_SIGNIN_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d(TAG, "Twitter signed in accepted");
                setResult(RESULT_CANCELED);
                finish();
            }else{
                //user hit the back button
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    }

    /**
     * check if the account is authorized
     *
     * @return
     */
    private boolean isConnected() {
        return mSharedPreferences.getString(Const.PREF_KEY_TOKEN, null) != null;
    }

    private void askOAuth() {
        new AsyncTask<Void, Void, String>(){
            @Override
            protected String doInBackground(Void... params) {
                String oauth_url = null;
                try {
                    requestToken = twitter.getOAuthRequestToken(Const.CALLBACK_URL);
                    oauth_url = requestToken.getAuthenticationURL();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return oauth_url;
            }
            protected void onPostExecute(String oauth_url) {
                if(oauth_url != null) {
                    ((TextView)findViewById(R.id.login_status_message)).setText("Connecting...");
                    openDialog(oauth_url);
                }else{
                    ((TextView)findViewById(R.id.login_status_message)).setText("Network Error or Invalid Credentials");
                }
            }
        }.execute();
    }

    public void openWebView(String title, String url) {
        Uri link = Uri.parse(url);
        Bundle webBundle = new Bundle();
        Intent intent = new Intent();
        webBundle.putParcelable("uri", link);
        webBundle.putString(ModalWebViewActivity.EXTRA_TITLE, title);
        intent.setClass(this, ModalWebViewActivity.class);
        intent.putExtras(webBundle);
        startActivity(intent);
        startActivityForResult(intent, TWITTER_SIGNIN_REQUEST);
    }

    @Override
    protected void onStop() {
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);

        if(auth_dialog!= null)
            auth_dialog.dismiss();
    }

    private void openDialog(final String oauth_url){
        web.getSettings().setJavaScriptEnabled(true);
        web.setWebViewClient(new WebViewClient() {
            boolean authComplete = false;

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                //Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
                web.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                try {
                    auth_dialog.show();
                    if (url.contains("oauth_verifier") && authComplete == false) {
                        authComplete = true;
                        Log.e("Url", url);
                        Uri uri = Uri.parse(url);
                        oauth_verifier = uri.getQueryParameter("oauth_verifier");
                        auth_dialog.dismiss();
                        new AccessTokenGet().execute(false);
                    } else if (url.contains("denied")) {
                        auth_dialog.dismiss();
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                } catch (Exception e) {
                    //fail gracefully
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        });
        web.loadUrl(oauth_url);

        auth_dialog.setCancelable(true);
        auth_dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
            }
        });
        auth_dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    private class AccessTokenGet extends AsyncTask<Boolean, String, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ((TextView)findViewById(R.id.login_status_message)).setText("Fetching Data ...");
            findViewById(R.id.login_progress_bar).setVisibility(View.VISIBLE);
        }
        @Override
        protected Boolean doInBackground(Boolean... isConnected) {
            try {
                if(isConnected[0]){
                    String token = mSharedPreferences.getString(Const.PREF_KEY_TOKEN, null);
                    String tokenSecret = mSharedPreferences.getString(Const.PREF_KEY_SECRET, null);
                    accessToken = new AccessToken(token, tokenSecret);
                    twitter.setOAuthAccessToken(accessToken);
                }else {
                    accessToken = twitter.getOAuthAccessToken(requestToken, oauth_verifier);
                }

                //save access token info to local storage
                SharedPreferences.Editor edit = mSharedPreferences.edit();
                edit.putString(Const.PREF_KEY_TOKEN, accessToken.getToken());
                edit.putString(Const.PREF_KEY_SECRET, accessToken.getTokenSecret());
                edit.apply();

                //twitter user model
                User user = twitter.showUser(accessToken.getUserId());

                //unoceros user info model
                UserInfo userInfo = AppController.getInstance().getUserInfo();
                userInfo.setUserId(user.getId()+"");
                userInfo.setUserName(user.getName());
                userInfo.setUserImageUrl(user.getOriginalProfileImageURL());
                userInfo.setTwitterUserId(user.getId()+"");

                String email = user.getScreenName();
                if(AppUtility.isValidateEmail(email))
                    userInfo.setUserEmail(email);

            } catch (TwitterException e) {
                Log.e(TAG, "Error login into twitter: "+e.getMessage(), e);
            }
            return true;
        }
        @Override
        protected void onPostExecute(Boolean response) {
            if(response) {
                final UserInfo userInfo = AppController.getInstance().getUserInfo();

                //persist user info
                AppState.instance().setUsername(userInfo.getUserName());
                AppState.instance().setEmailAddress(userInfo.getUserEmail());
                AppState.instance().setAgeRange(0);

                //findViewById(R.id.login_progress_bar).setVisibility(View.GONE);
                NetworkImageView profileImageView = (NetworkImageView) findViewById(R.id.login_profile_image);
                profileImageView.setDefaultImageResId(R.drawable.user);
                profileImageView.setImageUrl(userInfo.getUserImageUrl(), AppController.getInstance().getImageLoader());
                //profileImageView.setVisibility(View.VISIBLE);
                //((TextView)findViewById(R.id.login_status_message)).setText("Welcome " + userInfo.getUserName() + "! One moment please!");

                if (!AppUtility.isValidateEmail(userInfo.getUserEmail())) {
                    findViewById(R.id.login_progress_bar).setVisibility(View.GONE);
                    findViewById(R.id.login_status_message).setVisibility(View.GONE);

                    //show login form since email not valid
                    loginform.setVisibility(View.VISIBLE);
                    web.setVisibility(View.INVISIBLE);
                    Button updateBtn = (Button)loginform.findViewById(R.id.sign_in_button);
                    updateBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            boolean cancel = attemptUpdate(loginform, userInfo);
                            if(!cancel){
                                if (!AppState.instance().isSignedIn())
                                    AppState.instance().signIn();

                                auth_dialog.dismiss();
                                setResult(RESULT_OK);
                                affiliateProfileUpdate(userInfo);

                                TrackingManager.getInstance(AppController.getInstance()).
                                        trackEvent(TwitterSignInActivity.this, "Twitter Sign-up");
                            }
                        }
                    });
                    auth_dialog.setCancelable(true);
                    auth_dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            //show the progress bar and msg
                            findViewById(R.id.login_progress_bar).setVisibility(View.VISIBLE);
                            findViewById(R.id.login_status_message).setVisibility(View.VISIBLE);
                        }
                    });
                    auth_dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    });
                    auth_dialog.show();
                } else {
                    if (!AppState.instance().isSignedIn())
                        AppState.instance().signIn();

                    setResult(RESULT_OK);
                    affiliateProfileUpdate(userInfo);
                    TrackingManager.getInstance(AppController.getInstance()).
                            trackEvent(TwitterSignInActivity.this, "Twitter Sign-up");
                    /*
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                Thread.sleep(3500);
                            }catch (Exception e){}
                            //finish();
                        }
                    }).start();*/
                }
            }else{
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    }

    private boolean attemptUpdate(ViewGroup loginform, UserInfo userInfo){
        boolean cancel = false;
        View focusView = null;

        EditText emailView = (EditText)loginform.findViewById(R.id.sign_up_login_email);
        EditText emailConfirmView = (EditText)loginform.findViewById(R.id.sign_up_login_email_confirm);

        // Reset errors.
        emailView.setError(null);
        emailConfirmView.setError(null);

        // Store values at the time of the login attempt.
        String email = emailView.getText().toString();
        String emailConfirm = emailConfirmView.getText().toString();

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            emailView.setError(getString(R.string.error_field_required));
            focusView = emailView;
            cancel = true;
            //} else if (!mEmail.contains("@")) {
        } else if (!AppUtility.isValidateEmail(email)) {
            emailView.setError(getString(R.string.error_invalid_email));
            focusView = emailView;
            cancel = true;
        }

        // Check that the email addresses match
        if (!email.equalsIgnoreCase(emailConfirm)) {
            emailConfirmView.setError(getString(R.string.error_email_not_matching));
            focusView = emailConfirmView;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            if (focusView != null)
                focusView.requestFocus();
        } else {
            //update email
            userInfo.setUserEmail(email);
            AppState.instance().setEmailAddress(email);
        }

        return cancel;
    }

    /**
     * Remove Token, Secret from preferences
     */
    public void signOut() {
        mSharedPreferences = getSharedPreferences(AppUtility.INSTALLATION_PREF_FILE, MODE_PRIVATE);
        Editor editor = mSharedPreferences.edit();
        editor.remove(Const.PREF_KEY_TOKEN);
        editor.remove(Const.PREF_KEY_SECRET);

        editor.apply();
    }


    public void revokeAccess(){
        mSharedPreferences = getSharedPreferences(AppUtility.INSTALLATION_PREF_FILE, MODE_PRIVATE);
        Editor editor = mSharedPreferences.edit();
        editor.remove(Const.PREF_KEY_TOKEN);
        editor.remove(Const.PREF_KEY_SECRET);

        editor.apply();
    }
}
