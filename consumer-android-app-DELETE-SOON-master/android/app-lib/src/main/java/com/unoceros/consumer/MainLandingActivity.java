package com.unoceros.consumer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.google.gson.Gson;
import com.unoceros.consumer.adapter.DrawerMenuAdapter;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.fragment.OnFragmentInteractionListener;
import com.unoceros.consumer.fragment.ProgressFragment2;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.DrawerMenuItem;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Amit
 */
public class MainLandingActivity extends ActionBarActivity
                    implements OnFragmentInteractionListener{

    private static final String TAG = "MainLandingActivity";

    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);

    }
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    private View mDrawerManuView;
    private ListView mDrawerList;
    private CharSequence mTitle;
    private CharSequence mDrawerTitle;

    private int mClickPosition = 0;
    private boolean mIsItemClicked = false;

    //private View mContentFrame;
    //private View mContentLoading;

    private List<DrawerMenuItem> mMenuDataList;
    private DrawerMenuAdapter mDrawerListAdapter;
    private ActionBar mActionBar;

    private boolean mLaunchedFromSplash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main_landing);

        //populate the menu items
        mMenuDataList = new ArrayList<>();
        populateMenuList(mMenuDataList);

        mTitle = mDrawerTitle = getTitle();

        //intent extras
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        mLaunchedFromSplash = false;
        if(extras != null)
            mLaunchedFromSplash = extras.getBoolean("LaunchedFromSplash", false);

        //get user info
        UserInfo userInfo = AppController.getInstance().getUserInfo();
        final SharedPreferences prefs = this.getSharedPreferences(AppUtility.INSTALLATION_PREF_FILE, MODE_PRIVATE);
        String userName = userInfo.getUserName();
        String userImage = userInfo.getUserImageUrl();
        String userEmail = userInfo.getUserEmail();
        if(userName == null || userName.equals(""))
            userName = prefs.getString("username", null);
        if(userEmail == null || userEmail.equals(""))
            userEmail = prefs.getString("useremail", null);
        if(userImage == null || userImage.equals(""))
            userImage = prefs.getString("userimage", null);
        NetworkImageView profileImageView = (NetworkImageView) findViewById(R.id.main_landing_drawer_user_icon);
        profileImageView.setDefaultImageResId(R.drawable.user);
        profileImageView.setImageUrl(userImage, AppController.getInstance().getImageLoader());

        ((TextView)findViewById(R.id.main_landing_drawer_user_name)).setText(userName);
        //((TextView)findViewById(R.id.main_landing_drawer_user_email)).setText(userEmail);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerManuView = findViewById(R.id.main_landing_drawer_view);
        mDrawerList = (ListView) findViewById(R.id.main_landing_drawer_list);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,  GravityCompat.START);

        // set a darker background when drawer is open
        mDrawerLayout.setScrimColor(getResources().getColor(R.color.deepblunoceros_trans));

        // Set the adapter for the list view
        //mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mDrawerItems));
        mDrawerListAdapter = new DrawerMenuAdapter(this, R.layout.drawer_menu_item, mMenuDataList);
        mDrawerList.setAdapter(mDrawerListAdapter);

        //mContentFrame = findViewById(R.id.main_landing_content_frame);
        //mContentLoading = findViewById(R.id.main_landing_loading);

        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!mMenuDataList.get(position).isHeader()) {
                    mClickPosition = position;
                    mIsItemClicked = true;
                    //mContentLoading.setVisibility(View.VISIBLE);
                    //mContentFrame.setVisibility(View.GONE);
                    mDrawerLayout.closeDrawer(mDrawerManuView);

                    //reset counter
                    mMenuDataList.get(position).setCounter(0);
                }else{
                    mClickPosition = -1;
                }
            }
        });

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                //R.drawable.ic_navigation_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                setTitle(mTitle);

                if(mIsItemClicked)
                    navigateTo(mClickPosition);

                mIsItemClicked = false;
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu
                syncState();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setTitle(mDrawerTitle);

                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu
                syncState();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        //mDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_navigation_drawer);

        mActionBar = getSupportActionBar();
        if(mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setHomeButtonEnabled(true);
        }


        //listen for back stack change
        this.getSupportFragmentManager().addOnBackStackChangedListener(
            new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                //Update UI here
                String title = getString(R.string.app_name);
                try {
                    int index = getSupportFragmentManager().getBackStackEntryCount() - 1;
                    if(index >= 0)
                        title = getSupportFragmentManager().getBackStackEntryAt(index).getName();
                }catch (Exception e) {
                    Log.e(TAG, e.getMessage(), e);
                }finally{
                    setTitle(title);
                }
            }
        });

        if (savedInstanceState == null) {
            navigateTo(0);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check which request we're responding to
        if (requestCode == UserPreferenceActivity.RESULT_SETTINGS) {
            //it is a preference change
            if(resultCode == RESULT_OK) {
                //the user logged out
                finish();
            }else {
                int previousPosition = 0;
                navigateTo(previousPosition); //TODO navigate to previous
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //check internet
        checkNetworkConnection();

        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "MainLandingActivity.onResume", AppState.instance().getStatusJson());
    }

    @Override
    protected void onPause() {
        super.onPause();
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "MainLandingActivity.onPause", AppState.instance().getStatusJson());
    }

    @Override
    protected void onDestroy() {
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "MainLandingActivity.onDestroy", AppState.instance().getStatusJson());
        TrackingManager.getInstance(this.getApplication()).
                trackDestroyActivity(this);

        super.onDestroy();
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        //boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        //menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        //Inflate your menu.
        //getMenuInflater().inflate(R.menu.main_landing, menu);

        // Set menu items
        //menu.findItem(R.id.menu_item_sync);
        //menu.findItem(R.id.menu_item_settings);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }else{
            
            int i = item.getItemId();
            /*
            if (i == R.id.menu_item_sync) {
                //sync progress fragment
                commitProgressFragment(0, true);
            } else if (i == R.id.menu_item_settings) {
                //open settings
                setTitle(this.getString(R.string.app_name));
                Intent pref = new Intent(this, UserPreferenceActivity.class);
                startActivityForResult(pref, UserPreferenceActivity.RESULT_SETTINGS);
            }
            */
            if (i == R.id.menu_item_settings) {
                //open settings
                setTitle(this.getString(R.string.app_name));
                Intent pref = new Intent(this, UserPreferenceActivity.class);
                startActivityForResult(pref, UserPreferenceActivity.RESULT_SETTINGS);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);

        /*
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
        */
    }

    private void checkNetworkConnection() {
        if (!AppUtility.haveNetworkConnection(this)) {
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        }
    }

    private void populateMenuList(List<DrawerMenuItem> list){
        list.add(new DrawerMenuItem("My Credits", R.drawable.icon_coin_white3)); //R.drawable.ic_action_about));
        list.add(new DrawerMenuItem(getString(R.string.title_activity_offer_list), R.drawable.icon_offers2)); //R.drawable.ic_action_settings));
        //list.add(new DrawerMenuItem("Network", android.R.drawable.ic_menu_help)); //R.drawable.ic_action_settings));

        /*
        ArrayList<Campaign> campaigns = OfferDetailFragment.retrieveFromDataStore(this, Const.DATASTORE_WISHLIST_NAME);
        DrawerMenuItem wishlist = new DrawerMenuItem("Shopping Cart", R.drawable.icon_cart);
        if(campaigns != null && campaigns.size() > 0){
            wishlist.setCounter(campaigns.size());
        }
        list.add(wishlist); //R.drawable.ic_action_settings));
        */
        list.add(new DrawerMenuItem("Claimed Rewards", R.drawable.icon_present)); //R.drawable.ic_action_settings));

        list.add(new DrawerMenuItem("Earning History", R.drawable.icon_history)); //R.drawable.ic_action_settings));

        //list.add(new DrawerMenuItem("Social News", true)); // adding a header to the list
        list.add(new DrawerMenuItem("Twitter", R.drawable.icon_twitter)); //R.drawable.ic_action_about));
        list.add(new DrawerMenuItem("Facebook", R.drawable.icon_facebook)); //R.drawable.ic_action_about));

        list.add(new DrawerMenuItem("How It Works", R.drawable.icon_how)); //R.drawable.ic_action_about));
        list.add(new DrawerMenuItem("FAQ", R.drawable.icon_faq)); //R.drawable.ic_action_about));
        list.add(new DrawerMenuItem("Message Center", R.drawable.icon_message2)); //R.drawable.ic_action_about));
        
        //list.add(new DrawerMenuItem("Other Options", true)); // adding a header to the list
        //list.add(new DrawerMenuItem("Contacts", android.R.drawable.ic_menu_help)); //R.drawable.ic_action_help));
        list.add(new DrawerMenuItem("Settings", R.drawable.icon_settings2)); //R.drawable.ic_action_about));
    }

    /** Swaps fragments in the main content view */
    private void navigateTo(int position) {

        //check internet
        checkNetworkConnection();

        // Create a new fragment and specify the planet to show based on
        // position
        Fragment fragment = null;
        //Bundle args = new Bundle();
        switch(position) {
            case 0:
                //Status
                fragment = commitProgressFragment(position, false);
                break;
            case 1:
                //Special Offers/MarketPlace
                Intent marketPlaceIntent = new Intent(this, OfferListActivity.class);
                Bundle marketPlaceBundle = new Bundle();
                marketPlaceBundle.putString("title", mMenuDataList.get(position).getItemName());
                marketPlaceBundle.putInt("position", position);
                marketPlaceBundle.putBoolean("isCampaignList", true);
                marketPlaceBundle.putString("itemName", mMenuDataList.get(position).getItemName());
                marketPlaceBundle.putInt("imageResourceId", mMenuDataList.get(position).getImgResID());
                marketPlaceIntent.putExtras(marketPlaceBundle);
                startActivity(marketPlaceIntent);
                break;
            case 2:
                //My Claimed Offers
                Intent claimedIntent = new Intent(this, OfferListActivity.class);
                Bundle claimedBundle = new Bundle();
                claimedBundle.putString("title", mMenuDataList.get(position).getItemName());
                claimedBundle.putInt("position", position);
                claimedBundle.putBoolean("isFromHistory", true);
                claimedIntent.putExtras(claimedBundle);
                startActivity(claimedIntent);
                break;
            case 3:
                //My Device Activity History
                Intent historyIntent = new Intent(this, HistoryActivityStream2.class);
                startActivity(historyIntent);
                break;
            case 4:
                //Twitter
                Uri twitterLink =  Uri.parse(this.getString(R.string.twitter_link));
                Bundle twitterBundle = new Bundle();
                Intent twitterIntent = new Intent();
                twitterBundle.putParcelable("uri", twitterLink);
                twitterBundle.putString(ModalWebViewActivity.EXTRA_TITLE, mMenuDataList.get(position).getItemName());
                twitterIntent.setClass(this, ModalWebViewActivity.class);
                twitterIntent.putExtras(twitterBundle);
                startActivity(twitterIntent);
                break;
            case 5:
                //Facebook
                Uri facebookLink =  Uri.parse(this.getString(R.string.facebook_link));
                Bundle facebookBundle = new Bundle();
                Intent facebookIntent = new Intent();
                facebookBundle.putParcelable("uri", facebookLink);
                facebookBundle.putString(ModalWebViewActivity.EXTRA_TITLE, mMenuDataList.get(position).getItemName());
                facebookIntent.setClass(this, ModalWebViewActivity.class);
                facebookIntent.putExtras(facebookBundle);
                startActivity(facebookIntent);
                break;
            case 6:
                //How it Works
                /*
                Uri howItWorksLink =  Uri.parse(this.getString(R.string.how_it_works_link));
                Bundle howItWorksBundle = new Bundle();
                Intent howItWorksIntent = new Intent();
                howItWorksBundle.putParcelable("uri", howItWorksLink);
                howItWorksBundle.putString(ModalWebViewActivity.EXTRA_TITLE, mMenuDataList.get(position).getItemName());
                howItWorksIntent.setClass(this, ModalWebViewActivity.class);
                howItWorksIntent.putExtras(howItWorksBundle);
                startActivity(howItWorksIntent);
                */
                Intent intro = new Intent(this, HowItWorksActivity.class);
                startActivity(intro);
                break;
            case 7:
                //Frequently Ask Questions
                Uri faqLink =  Uri.parse(this.getString(R.string.frequently_ask_questions_link));
                Bundle faqBundle = new Bundle();
                Intent faqIntent = new Intent();
                faqBundle.putParcelable("uri", faqLink);
                faqBundle.putString(ModalWebViewActivity.EXTRA_TITLE, mMenuDataList.get(position).getItemName());
                faqIntent.setClass(this, ModalWebViewActivity.class);
                faqIntent.putExtras(faqBundle);
                startActivity(faqIntent);
                break;
            case 8:
                //Message Center
                Bundle messageBundle = new Bundle();
                messageBundle.putBoolean("fromPush", false);
                Intent messageIntent = new Intent();
                messageIntent.setClass(this, MessageCenterActivity.class);
                messageIntent.putExtras(messageBundle);
                startActivity(messageIntent);
                break;
            case 9:
                //Settings
                setTitle(this.getString(R.string.app_name));
                Intent pref = new Intent(this, UserPreferenceActivity.class);
                startActivityForResult(pref, UserPreferenceActivity.RESULT_SETTINGS);
                break;
        }

        if(fragment != null) {
            //set the title in the action bar
            setTitle(mMenuDataList.get(position).getItemName());
            //mDrawerList.setItemChecked(position, true);
        }

        if(position >=0) {
            //mDrawerLayout.closeDrawer(mDrawerList);
            mDrawerLayout.closeDrawer(mDrawerManuView);
        }

    }

    private Fragment commitProgressFragment(int position, boolean forceSync){
        Fragment fragment = ProgressFragment2.newInstance(forceSync);
        fragment.getArguments().putString(ProgressFragment2.ITEM_NAME, mMenuDataList.get(position).getItemName());
        fragment.getArguments().putInt(ProgressFragment2.IMAGE_RESOURCE_ID, mMenuDataList.get(position).getImgResID());
        //fragment.getArguments().putInt("cart_position", 2); //the index for wishlist/cart
        getSupportFragmentManager()
                .beginTransaction()
                        //.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        //.setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(R.id.main_landing_content_frame,
                        fragment,
                        ProgressFragment2.TAG)
                //.addToBackStack(mMenuDataList.get(position).getItemName())
                .commit();
        return fragment;
    }
    @Override
    public void setTitle(CharSequence title) {
        if(title != null) {
            mTitle = title.toString();
            if(mActionBar != null)
                mActionBar.setTitle(mTitle);
        }
    }

    @Override
    public void onFragmentInteraction(Bundle bundle) {
        if(bundle != null){
            //check if counter changed
            int position = bundle.getInt("position");
            int counter = bundle.getInt("counter");
            if(counter >= 0 && (position > 0 && position < mMenuDataList.size())){
                mMenuDataList.get(position).setCounter(counter);
                mDrawerListAdapter.notifyDataSetChanged();
            }else{
                mMenuDataList.get(position).setCounter(counter);
                mDrawerListAdapter.notifyDataSetChanged();
            }

        }
    }


    @Override
    public void onFragmentVisible(boolean visible) {
        Log.i(TAG, "fragment visible: "+visible);
    }

    public void onBackPressed(){
        // Close the drawer
        if(mDrawerLayout != null && mDrawerManuView != null
            && mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                //drawer is open
              mDrawerLayout.closeDrawer(mDrawerManuView);
        }else{
            // Catch back action and pops from backstack
            // (if you called previously to addToBackStack() in your transaction)
            if (getSupportFragmentManager().getBackStackEntryCount() > 1){ //1 because all fragments are added to the backstack
                getSupportFragmentManager().popBackStack();
            } else {
                if(!mLaunchedFromSplash) {
                    //Intent mainIntent = new Intent(this, ContinueStatusSlickblueActivity.class);
                    //startActivity(mainIntent);
                    //finish();
                }else{
                    //finish();
                }
                //finish();
                //default
                super.onBackPressed();
            }
        }
    }
}
