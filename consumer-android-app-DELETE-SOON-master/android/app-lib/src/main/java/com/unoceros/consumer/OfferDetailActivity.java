package com.unoceros.consumer;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.fragment.OfferDetailFragment;
import com.unoceros.consumer.fragment.OnFragmentInteractionListener;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.DeviceHardware;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.services.ApplicationServices;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;

/**
 * Created by Amit
 */
public class OfferDetailActivity extends ActionBarActivity
        implements OnFragmentInteractionListener{

    private static final String TAG = "OfferDetailActivity";
    private Campaign campaign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.generic_merge_layout);

        Bundle bundle = getIntent().getExtras();
        campaign = bundle.getParcelable("campaign");
        int position = bundle.getInt("position");
        long id = bundle.getLong("id");
        boolean isFromHistory = bundle.getBoolean("isFromHistory");

        //updated sub title
        actionBarSetup(campaign.getTitle());

        OfferDetailFragment fragment = OfferDetailFragment.newInstance(position,id);
        fragment.getArguments().putParcelable("offer", campaign);
        fragment.getArguments().putBoolean("isFromHistory", isFromHistory); //buying or saving it
        getSupportFragmentManager()
                .beginTransaction()
                        //.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        //.setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                .replace(android.R.id.content,
                        fragment,
                        OfferDetailActivity.TAG)
                .commit();
    }

    private void actionBarSetup(String subTitle) {
        ActionBar ab = getSupportActionBar();
        if(ab!= null){
            ab.setDisplayHomeAsUpEnabled(true);
            //ab.setSubtitle(subTitle);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            // app icon in action bar clicked; go to previous activity or home
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void checkNetworkConnection() {
        if (!AppUtility.haveNetworkConnection(this)) {
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //check network
        checkNetworkConnection();

        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "OfferDetail.onResume", AppState.instance().getStatusJson());
    }

    @Override
    protected void onPause() {
        super.onPause();
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "OfferDetail.onPause", AppState.instance().getStatusJson());
    }

    @Override
    protected void onDestroy() {
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "OfferDetail.onDestroy", AppState.instance().getStatusJson());
        TrackingManager.getInstance(this.getApplication()).
                trackDestroyActivity(this);

        super.onDestroy();

    }

    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }


    @Override
    public void onFragmentInteraction(Bundle b) {

    }

    @Override
    public void onFragmentVisible(boolean visible) {

    }

    public void attributeInstall(View v){
        ApplicationServices services = ApplicationServices.getInstance(this.getApplication());
        DeviceHardware hardware = services.getDeviceHardware();
        UserInfo user = services.getUserInfo();

        String params;
        params = "&device_id="+hardware.getTelephonyId();
        params = params+"&android_id="+hardware.getAndroidId();
        params = params+"&google_aid="+hardware.getGoogleAid();
        params = params+"&user_email="+user.getUserEmail()+"&user_id="+user.getUserId()+"&username="+user.getUserName()
                +"&fb_user_id="+user.getFacebookUserId()+"&twitter_user_id="+user.getTwitterUserId()+"&google_user_id="+user.getGoogleUserId();

        String measurementUrl = campaign.getUrl()+params;
        WebView appstore = new WebView(this);
        appstore.loadUrl(measurementUrl);
        Log.d(TAG, "Attributing install to: "+measurementUrl);


        //tracking installs
        TrackingManager.getInstance(AppController.getInstance()).
                trackEvent(OfferDetailActivity.this, "Attribute Install");
    }
}
