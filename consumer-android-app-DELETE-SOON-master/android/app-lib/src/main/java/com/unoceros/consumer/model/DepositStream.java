package com.unoceros.consumer.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Amit
 */
public class DepositStream implements Parcelable, Serializable {

    private String date;
    private Double rate;
    private Double amount;

    public DepositStream() {}

    public DepositStream(Parcel in){
        readFromParcel(in);
    }

    public Double getRate() {
        return rate;
    }
    public void setRate(Double rate) {
        this.rate = rate;
    }
    public Double getAmount() {
        return amount;
    }
    public void setAmount(Double depositAmount) {
        this.amount = depositAmount;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getDate());
        dest.writeDouble(getAmount());
        dest.writeDouble(getRate());
    }

    public void readFromParcel(Parcel in) {
        setDate(in.readString());
        setAmount(in.readDouble());
        setRate(in.readDouble());
    }

    public transient static final Parcelable.Creator<DepositStream> CREATOR = new Parcelable.Creator<DepositStream>() {
        public DepositStream createFromParcel(Parcel in) {
            return new DepositStream(in);
        }

        public DepositStream[] newArray(int size) {
            return new DepositStream[size];
        }
    };
}
