package com.unoceros.consumer.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.content.Context;
import android.provider.Settings;

import com.crashlytics.android.Crashlytics;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
/**
 * @author sdaswani
 *         Encapsulates the state of this app for global reference.
 */
public class AppState implements Serializable {

    static final long serialVersionUID = 23;
    public static boolean IS_QA_BUILD = false;

    /**
     * CONSTANTS
     */
    private static final String TAG = "AppState";
    private static final String FILENAME = "app.state";
    private static final String SIGN_UP_CHECKPOINT = "USER_SIGNED_UP";
    private static final String SIGN_OUT_CHECKPOINT = "USER_SIGNED_OUT";
    private static final String TOS_CHECKPOINT = "USER_ACCEPTED_TOS";

    private static final String START_COMPUTE_CHECKPOINT = "COMPUTATION_STARTED";
    private static final String PAUSE_COMPUTE_CHECKPOINT = "COMPUTATION_PAUSED";
    private static final String END_COMPUTE_CHECKPOINT = "COMPUTATION_END";

    /**
     * SINGLETON
     */
    private static Context _context;
    private static Application _application;
    private static AppState _instance;

    /**
     * Private empty constructor
     */
    private AppState() {}

    /**
     * Returns an instance of this app state
     * @return
     */
    public static synchronized AppState instance() {
        if (_instance == null) {
            //sanity check...though it should already been initialize in the AppController
            if(!isInitialized())
                setContext(AppController.getInstance());

            //get the instance from internal storage if available
            _instance = readFromInternalStorage();

            //update user info
            if(_instance != null) {
                UserInfo userInfo = AppController.getInstance().getUserInfo();
                userInfo.setUserName(_instance._username);
                userInfo.setUserEmail(_instance._emailAddress);
                userInfo.setAge(_instance._ageRange);
            }
        }
        return _instance;
    }

    /**
     * Returns true if this state has being initialize, it uses the context to determine init
     * status, @See setContext must be called first, otherwise this always returns false
     * @return
     */
    public static boolean isInitialized() {
        return (_context != null);
    }

    /**
     * Static member use to set the application context of state object, should be called first
     * within the context of this application
     * @param application
     */
    public static void setContext(Application application) {
        _application = application;
        _context = application.getApplicationContext();

        try {
            if (AppUtility.signedWithDebugKey(_context) || AppUtility.isDebuggable(_context)) {
                //do debug stuff
                IS_QA_BUILD = true;
                //Crashlytics.start(_context);
            } else {
                //do release stuff
                Crashlytics.start(_context);
            }
        }catch (Exception e){
            Log.e(TAG, "Error initializing Crashlytics", e);
        }
    }

    /**
     * SERIALIZATION HELPERS
     */
    private static synchronized AppState readFromInternalStorage() {
        if (_context == null) {
            if (AppState.IS_QA_BUILD) Log.e(TAG, "No Application, can't read from disk!");
            return new AppState();
        }

        try {
            File file = new File(_context.getFilesDir(), FILENAME);
            if (!file.exists() || !file.canRead()) {
                if (AppState.IS_QA_BUILD) Log.d(TAG, "No saved state, creating a new one.");
                return new AppState();
            }

            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream is = new ObjectInputStream(fis);
            AppState state = (AppState) is.readObject();
            is.close();
            if (AppState.IS_QA_BUILD) Log.d(TAG, "State read from disk: " + state);
            return state;
        } catch (IOException ioe) {
            if (AppState.IS_QA_BUILD) Log.d(TAG, "File read error, creating a new one.");
            return new AppState();
        } catch (ClassNotFoundException cnfe) {
            // should never happen, programmer error
        }

        // should never return null, programmer error.
        return null;
    }

    public static synchronized boolean writeToInternalStorage(AppState state) {
        if (_context == null) {
            if (AppState.IS_QA_BUILD) Log.e(TAG, "No Application, can't commit to disk!");
            return false;
        }

        try {
            File file = new File(_context.getFilesDir(), FILENAME);
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(state);
            os.close();
        } catch (IOException ioe) {
            if (AppState.IS_QA_BUILD) Log.e(TAG, "Commit to disk failed!");
            ioe.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * SERIALIZED STATE MACHINE ACCESSORS
     */
    private boolean _signedIn = false;
    private boolean _tosAndPPAgreedTo = false;
    public synchronized boolean isSignedIn() {
        return _signedIn;
    }
    public synchronized boolean userAgreedToTOSAndPP() {
        return _tosAndPPAgreedTo;
    }

    /**
     * UNSERIALIZED STATE MACHINE ACCESSORS
     */
    private transient float _temperature = 0;
    public void setTemperature(float fahrenheit) {
        _temperature = fahrenheit;
    }

    /**
     * STATE MACHINE TRANSITIONS
     */
    public synchronized void signOut() throws RuntimeException {
        if (AppState.IS_QA_BUILD) Log.d(TAG, "Logging out.");
        _signedIn = false;
        writeToInternalStorage(this);

        TrackingManager.getInstance(_application).
                trackEvent(_application, SIGN_OUT_CHECKPOINT, getStatusJson());
    }

    public synchronized void signIn() throws RuntimeException {
        if (_signedIn) {
            if (AppState.IS_QA_BUILD) Log.e(TAG, "Trying to log in again!");
            throw new RuntimeException("State Machine Error: trying to sign in again!");
        }
        if (AppState.IS_QA_BUILD) Log.d(TAG, "Logging in.");
        _signedIn = true;
        writeToInternalStorage(this);

        TrackingManager.getInstance(_application).
                trackEvent(_application, SIGN_UP_CHECKPOINT, getStatusJson());
    }

    public synchronized void acceptTOSAndPP() throws RuntimeException {
        if (!_signedIn) {
            if (AppState.IS_QA_BUILD) Log.e(TAG, "Trying to accept terms before signing in!");
            throw new RuntimeException("State Machine Error: trying to accept terms before signing in!");
        }
        if (_tosAndPPAgreedTo) {
            if (AppState.IS_QA_BUILD) Log.e(TAG, "Trying to accept terms again!");
            //throw new RuntimeException("State Machine Error: trying to accept terms again!");
            return;
        }
        if (AppState.IS_QA_BUILD) Log.d(TAG, "Accepting TOS and PP.");
        _tosAndPPAgreedTo = true;
        writeToInternalStorage(this);

        TrackingManager.getInstance(_application).
                trackEvent(_application, TOS_CHECKPOINT, getStatusJson());
    }

    /*
    public synchronized void pauseComputation() throws RuntimeException {

        TrackingManager.getInstance(_application).
                trackEvent(_application, PAUSE_COMPUTE_CHECKPOINT, getStatusJson());
    }

    public synchronized void computeStart() throws RuntimeException {

        TrackingManager.getInstance(_application).
                trackEvent(_application, START_COMPUTE_CHECKPOINT, getStatusJson());

    }

    public synchronized void computeEnd() throws RuntimeException {
        TrackingManager.getInstance(_application).
                trackEvent(_application, END_COMPUTE_CHECKPOINT, getStatusJson());

        writeToInternalStorage(this);

    }
    */

    /**
     * ANALYTIC CALLS
     */
    /*
    public void trackDeviceReboot() {
        //_mixpanel.track("Device Rebooted", getStatusJson());
        TrackingManager.getInstance(_application).
                trackEvent(_application, "Device Rebooted", getStatusJson());
    }

    public void trackAppPackageReplaced() {
        //_mixpanel.track("App Package Replaced", getStatusJson());
        TrackingManager.getInstance(_application).
                trackEvent(_application, "App Package Replaced", getStatusJson());
    }

    public void trackFrontUIResumed() {
        //_mixpanel.track("DemoUI.onResume", getStatusJson());
        TrackingManager.getInstance(_application).
                trackEvent(_application, "DemoUI.onResume", getStatusJson());
    }

    public void trackFrontUIPaused() {
        //_mixpanel.track("DemoUI.onPause", getStatusJson());
        TrackingManager.getInstance(_application).
                trackEvent(_application, "DemoUI.onPause", getStatusJson());
    }


    /*
    public void trackFrontUIDestroyed() {
        //_mixpanel.track("DemoUI.onDestroy", getStatusJson());
        TrackingManager.getInstance(_application).
                trackEvent(_application, "DemoUI.onDestroy", getStatusJson());
    }

    public void trackProgressUIDestroyed() {
        //_mixpanel.track("ProgressUI.onDestroy", getStatusJson());
        TrackingManager.getInstance(_context).
                trackEvent(_context, "ProgressUI.onDestroy", getStatusJson());
    }*/

/*
    public void trackRewardChoiceChanged() {
        //_mixpanel.track("Charity Changed", getStatusJson());
        TrackingManager.getInstance(_application).
                trackEvent(_application, "Charity Changed", getStatusJson());
    }

    public void trackRewardChoiceForcedToPandora() {
        //_mixpanel.track("Charity Forced (CharityWater)", getStatusJson());
        TrackingManager.getInstance(_application).
                trackEvent(_context, "Charity Forced (CharityWater)", getStatusJson());
    }

    public void trackSuccessfulRedemptions(String code) {
        JSONObject statusJson = getStatusJson();
        try {
            statusJson.put("Invite Code", code);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //_mixpanel.track("Code Redeem Success", statusJson);
        TrackingManager.getInstance(_application).
                trackEvent(_application, "Code Redeem Success", getStatusJson());
    }

    public void trackFailedRedemptions(String code) {
        JSONObject statusJson = getStatusJson();
        try {
            statusJson.put("Invite Code", code);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //_mixpanel.track("Code Redeem Failure", statusJson);
        TrackingManager.getInstance(_application).
                trackEvent(_application, "Code Redeem Failure", getStatusJson());
    }
*/
    /**
     * USER DATA
     */
    private String _username = null;
    private String _emailAddress = null;
    private int _ageRange = 0;

    public void setUsername(String name) {
        _username = name;
    }
    public void setEmailAddress(String email) {
        _emailAddress = email;
    }
    public void setAgeRange(int range) {
        _ageRange = range;
    }


    /**
     * MISCELLANEOUS
     */
    /*
    public static DateTime getEstimatedDateOfNextRewardInternal(DateTime lastReward, DateTime currentDate,
                                                                float completionPercentage) {
        float daysToNextReward = 15; // assume 15 days out
        try {
            int hoursSinceLastReward = Hours.hoursBetween(lastReward, currentDate).getHours();
            // only use the completion percentage if enough work done.
            if ((hoursSinceLastReward > 23) && (completionPercentage > 1.0)) {
                float hourlyCompletionPercentage = completionPercentage / hoursSinceLastReward;
                float estimateHoursToReward = 100.0f / hourlyCompletionPercentage;
                daysToNextReward = estimateHoursToReward / 24;
            }
        }catch (Exception e){
            Log.e(TAG, e.getMessage(), e);
        }
        return lastReward.plusDays((int) Math.ceil(daysToNextReward));
    }

    public void showProgressNotification() {
        ProgressNotification.notifyPercentage(_context, "" + getRewardSummaryCompletionPercentage());
    }

    public void showPausedNotification() {
        ProgressNotification.notifyPaused(_context);
    }

    public Notification getProgessNotification() {
        return ProgressNotification.getPercentageNotification(_context,
                "" + getRewardSummaryCompletionPercentage());
    }

    public Notification getPausedNotification() {
        return ProgressNotification.getPausedNotification(_context);
    }

    */
    public String toString() {
        JSONObject jsonRep = getStatusJson();
        if (jsonRep != null)
            return jsonRep.toString();

        return "";
    }

    public JSONObject getStatusJson() {
        JSONObject jsonRep = new JSONObject();
        try {
            jsonRep.put("UserSignedIn", _signedIn);
            jsonRep.put("AcceptedToS", _tosAndPPAgreedTo);
            jsonRep.put("Name", _username);
            jsonRep.put("E-mail", _emailAddress);
            jsonRep.put("AgeRange", _ageRange);
            //jsonRep.put("ComputationTimeInSeconds", getComputationTimeInSeconds());
            jsonRep.put("Temperature", _temperature);
            //jsonRep.put("DailyCompletionPercentage", getDailyCompletionPercentage());
            jsonRep.put("Version", AppUtility.getVersionName(AppController.getInstance()));
        } catch (JSONException e) {
            if (AppState.IS_QA_BUILD) Log.e(TAG, "Terrible - couldn't create json object.");
        }
        return jsonRep;
    }

}
