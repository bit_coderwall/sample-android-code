package com.unoceros.consumer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.fragment.CampaignListFragment;
import com.unoceros.consumer.fragment.OnFragmentInteractionListener;
import com.unoceros.consumer.fragment.UserRewardListFragment;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Const;

/**
 * Created by Amit
 */
public class OfferListActivity extends ActionBarActivity
        implements OnFragmentInteractionListener{

    private static final String TAG = "OfferListActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.generic_merge_layout);

        Bundle bundle = getIntent().getExtras();
        String title = bundle.getString("title");
        int position = bundle.getInt("position");
        boolean isFromHistory = bundle.getBoolean("isFromHistory");
        boolean isCampaignList = bundle.getBoolean("isCampaignList");

        //updated sub title
        actionBarSetup(title);

        if(isFromHistory) {
            UserRewardListFragment fragment = UserRewardListFragment.newInstance("History", Const.DATASTORE_HISTORY_NAME, false);
            fragment.getArguments().putString("emptyError", "Claimed Rewards Could Not Be Loaded...");
            fragment.getArguments().putString("emptyMessage", "No Claimed Rewards Found...");
            fragment.getArguments().putString("emptyDefault", "Loading Claimed Rewards...");
            fragment.getArguments().putInt("position", position);
            fragment.getArguments().putBoolean("deletable", false);
            getSupportFragmentManager()
                    .beginTransaction()
                            //.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                            //.setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                    .replace(android.R.id.content,
                            fragment,
                            UserRewardListFragment.TAG)
                    .commit();
        }else if(isCampaignList){
            String itemName = bundle.getString("itemName");
            int imageResourceId = bundle.getInt("imageResourceId");
            CampaignListFragment fragment = CampaignListFragment.newInstance();
            fragment.getArguments().putString(CampaignListFragment.ITEM_NAME, itemName);
            fragment.getArguments().putInt(CampaignListFragment.IMAGE_RESOURCE_ID, imageResourceId);
            getSupportFragmentManager()
                    .beginTransaction()
                            //.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                            //.setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .replace(android.R.id.content,
                            fragment,
                            CampaignListFragment.TAG)
                    .commit();
        }
    }

    private void actionBarSetup(String title) {
        ActionBar ab = getSupportActionBar();
        if(ab!= null){
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            // app icon in action bar clicked; go to previous activity or home
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.credits, menu);

        //save an instance of the menu
        prepareOptionsMenu(menu);

        //show the up-icon
        if(getActionBar() != null)
            this.getActionBar().setDisplayHomeAsUpEnabled(true);

        return true;
    }

    private void prepareOptionsMenu(Menu menu) {
        try{
            if(menu != null){
                LinearLayout customLayout = (LinearLayout) menu.findItem(R.id.main_menu_credits).getActionView();
                final View addView = this.getLayoutInflater().inflate(R.layout.actionbar_credits, null);
                TextView creditBalance  = (TextView) addView.findViewById(R.id.header_credits_balance);
                //ImageView weatherImage  = (ImageView) addView.findViewById(R.id.header_credits_coin_img);

                customLayout.removeAllViews();
                customLayout.addView(addView);
                customLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle bundle = new Bundle();
                        Intent intent = new Intent();
                        intent.setClass(OfferListActivity.this, MainLandingActivity.class);
                        intent.putExtras(bundle);

                        startActivity(intent); //launch a new activity
                    }
                });
            }
        }catch(Exception e){}
    }
    */

    private void checkNetworkConnection() {
        if (!AppUtility.haveNetworkConnection(this)) {
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //check network
        checkNetworkConnection();

        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "OfferListActivity.onResume", AppState.instance().getStatusJson());
    }

    @Override
    protected void onPause() {
        super.onPause();
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "OfferListActivity.onPause", AppState.instance().getStatusJson());
    }

    @Override
    protected void onDestroy() {
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "OfferListActivity.onDestroy", AppState.instance().getStatusJson());
        TrackingManager.getInstance(this.getApplication()).
                trackDestroyActivity(this);

        super.onDestroy();

    }

    @Override
    public void onFragmentInteraction(Bundle b) {

    }

    @Override
    public void onFragmentVisible(boolean visible) {

    }
    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }
}
