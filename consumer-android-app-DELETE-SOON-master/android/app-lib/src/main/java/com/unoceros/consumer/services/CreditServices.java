package com.unoceros.consumer.services;

import android.content.Context;
import android.content.SharedPreferences;

import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;

/**
 * Created by Amit
 */
public class CreditServices {

    private static final String TAG = "CreditServices";
    private double overallBalance;
    private double totalBalance;
    private double todayBalance;
    private double dailyAverage;
    private double networkAverage;

    private SharedPreferences prefs;
    public CreditServices(Context context) {
        prefs = context.getSharedPreferences(AppUtility.DATA_STORE_PREF, Context.MODE_PRIVATE);

        //reset credit service with default data
        retrieveFromDataStore();
    }

    private void retrieveFromDataStore(){
        overallBalance = prefs.getFloat("overallBalance", -1.0f);
        totalBalance = prefs.getFloat("totalBalance", -1.0f);
        todayBalance = prefs.getFloat("todayBalance", -1.0f);
        dailyAverage = prefs.getFloat("dailyAverage", 0.0f);
        networkAverage = prefs.getFloat("networkAverage", 0.0f);
    }

    private void persistToDataStore(){
        //persist defaults values to internal store -- webservices
        prefs.edit().putFloat("overallBalance", (float) overallBalance).apply();
        prefs.edit().putFloat("totalBalance", (float) totalBalance).apply();
        prefs.edit().putFloat("todayBalance", (float) todayBalance).apply();
        prefs.edit().putFloat("dailyAverage", (float) dailyAverage).apply();
        prefs.edit().putFloat("networkAverage", (float) networkAverage).apply();
    }

    public double deposit(double amount){
        overallBalance = overallBalance + amount;
        totalBalance = totalBalance + amount;
        todayBalance = todayBalance + amount;
        Log.d(TAG, "Depositing "+amount+" credits");

        //persist defaults values to internal store -- webservices
        persistToDataStore();
        return totalBalance;
    }

    public double withdraw(double amount) throws IllegalAccessException {
        if(amount < totalBalance){
            totalBalance = totalBalance - amount;
            Log.d(TAG, "Withdrawing "+amount+" credits");

            //persist defaults values to internal store -- webservices
            persistToDataStore();
            return totalBalance;
        }else {
            throw new IllegalAccessException("Insufficient funds");
        }
    }

    public void setDailyAverage(double average){
        dailyAverage = average;
        //persist defaults values to internal store -- webservices
        persistToDataStore();
    }
    public double getDailyAverage(){
        return Math.round(dailyAverage * 100.0) / 100.0;
    }
    public void setNetworkAverage(double average){
        networkAverage = average;
        persistToDataStore();
    }
    public double getNetworkAverage(){
        return Math.round(networkAverage * 100.0) / 100.0;
    }
    public double getTotalBalance(){
        return Math.round(totalBalance * 100.0) / 100.0; //round to 2 decimal places
    }
    public double getTodayDeposit(){
        return Math.round(todayBalance * 100.0) / 100.0; //round to 2 decimal places
    }
    public double getOverallBalance(){
        return Math.round(overallBalance * 100.0) / 100.0; //rount to 2 decimal places
    }

    /**Remote/Admin Transactions*/
    public void invokedByRemoteAdmin(double overallBalance, double totalBalance, double todayBalance){
        //fix discrepancies between local internal storage and remote service
        this.overallBalance = overallBalance;
        this.totalBalance = totalBalance;
        this.todayBalance = todayBalance;
        persistToDataStore();
    }

    public void invokedOverrideByAdmin(double overallBalance, double totalBalance, double todayBalance,
                                       double todayAverage, double networkAverage){
        //retrieve from secured and persistent store prior to initialization
        retrieveFromDataStore();

        if(this.totalBalance <= 0){
            this.overallBalance = overallBalance;
            this.totalBalance = totalBalance;
            this.todayBalance = todayBalance;
            this.dailyAverage = todayAverage;
            this.networkAverage = networkAverage;
            persistToDataStore();
        }
    }

    public void initializedByManagement(double initialBalance){
        //validate balances
        //assume initialize on app start
        this.overallBalance = initialBalance;
        this.totalBalance = initialBalance;
        this.todayBalance = 0.0;
        Log.d(TAG, "Welcome, you've being awarded " + this.totalBalance + " credits to start with");

        //persist defaults values back to internal store -- webservices
        persistToDataStore();
    }
}
