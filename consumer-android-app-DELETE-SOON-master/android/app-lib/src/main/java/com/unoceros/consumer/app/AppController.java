package com.unoceros.consumer.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.google.gson.Gson;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.model.DeviceHardware;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.services.ApplicationServices;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;

/**
 * Created by Amit
 */
public class AppController extends Application {

    //global
    protected static final String TAG = AppController.class.getSimpleName();
    protected static Application mInstance;

    protected UserInfo mUserInfo;

    @Override
    public void onCreate() {
        super.onCreate();

        Context appContext = this.getApplicationContext();

        if(AppUtility.signedWithDebugKey(appContext) || AppUtility.isDebuggable(appContext)){
            //do debug stuff
            Log.logLevel = android.util.Log.DEBUG;
            VolleyLog.DEBUG = true;
        }else{
            //do release stuff
            Log.logLevel = android.util.Log.WARN;
            VolleyLog.DEBUG = false;
        }

        //App services
        ApplicationServices.getInstance(this);

        //Instantiate the user with unique id
        DeviceHardware hardware = ApplicationServices.getInstance(this).getDeviceHardware();
        mUserInfo = ApplicationServices.getInstance(this).getUserInfo();
        mUserInfo.setUserId(hardware.getUniqueDeviceId().toString());

        /**
         MOVED ALL THESE BELOW USERINFO, THERE MAYBE SOME DEVICE HARDWARE AND USER INFO DEPENDENCIES
         WHEN INITIALIZING TRACKING
         */
        //Analytics
        TrackingManager.getInstance(this);

        //App state
        AppState.setContext(this);

        //set local instance
        mInstance = this;
    }

    public static synchronized AppController getInstance() {
        return (AppController)mInstance;
    }

    public ImageLoader getImageLoader(){
        return ApplicationServices.getInstance(this)
                .getNetworkRequest().getImageLoader();
    }

    public RequestQueue getRequestQueue(){
        return ApplicationServices.getInstance(this)
                .getNetworkRequest().getRequestQueue();
    }

    public <T> void addToRequestQueue(Request<T> req) {
        ApplicationServices.getInstance(this)
                .getNetworkRequest().addToRequestQueue(req);
    }

    /**
     * Returns the user info model, calling this method ensure that the returned object is the most
     * up to date info regarding this user, since it is synced with meta in the internal storage.
     *
     * @return
     */
    public UserInfo getUserInfo(){
        final SharedPreferences prefs = this.getSharedPreferences(AppUtility.INSTALLATION_PREF_FILE, MODE_PRIVATE);
        Gson gson = new Gson();
        UserInfo persisted_user_info = null;
        try {
            String gson_data = prefs.getString("userInfo", null);
            if (gson_data != null)
                persisted_user_info = gson.fromJson(gson_data, UserInfo.class);
        }catch (Exception e){
            Log.e(TAG, "Error unpacking user info from persistent storage");
        }

        if (persisted_user_info == null) {
            String gsonData = gson.toJson(mUserInfo);
            prefs.edit().putString("userInfo", gsonData).apply();
        }else{
            //update user information before returning
            mUserInfo.syncUp(persisted_user_info);
        }
        return mUserInfo;
    }

    public boolean isForAmazonMarket(){
        return false;
    }
}
