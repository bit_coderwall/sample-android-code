package com.unoceros.consumer.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.unoceros.consumer.fragment.WalkthroughFragmentA;
import com.unoceros.consumer.fragment.WalkthroughFragmentB;
import com.unoceros.consumer.fragment.WalkthroughFragmentC;

/**
 * Created by Amit
 */
public class WalkThruPagerAdapter extends FragmentPagerAdapter {

    int mCount = 0;
    Bundle mArgs = new Bundle();
    public WalkThruPagerAdapter(FragmentManager fm, int count, Bundle args) {
        super(fm);
        mCount = count;
        mArgs = args;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        switch (i){
            case 0:
                fragment = WalkthroughFragmentA.newInstance(mArgs);
                break;
            case 1:
                fragment = WalkthroughFragmentB.newInstance(mArgs);
                break;
            case 2:
                fragment = WalkthroughFragmentC.newInstance(mArgs);
                break;
            default:
                fragment = WalkthroughFragmentA.newInstance(mArgs);
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return mCount;
    }
}
