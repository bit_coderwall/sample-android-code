package com.unoceros.consumer.model.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Amit
 */
public class DbClaimedRewards extends SQLiteOpenHelper{

    private static final String TAG = "DbClaimedRewards";

    //Fields
    public static final String ID = "_id";
    public static final String REWARDS_UUID = "rewardsUUID";
    public static final String REWARDS_ID = "rewardsId";
    public static final String REWARDS_CLAIMED_DATE = "dateClaimed";
    public static final String REWARD_GSON_DATA = "rewardGsonData";

    //Database Info
    public static final String DATABASE_NAME = "claimedRewards.db";
    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_TABLE = "rewardsHistory";

    //SQL creation statement
    private static final String DATABASE_CREATE = "CREATE TABLE "
            + DATABASE_TABLE + "(" + ID + " integer primary key autoincrement, "
            + REWARDS_UUID+ " text not null, "
            + REWARDS_ID+ " text not null, "
            + REWARDS_CLAIMED_DATE+ " text not null, "
            + REWARD_GSON_DATA + " text not null)";

    public DbClaimedRewards(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");

        //simple deletes all existing data and re-create
        database.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
        onCreate(database);
    }
}
