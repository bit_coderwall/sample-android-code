package com.unoceros.consumer.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amit
 */
public class Log {

    public static int logLevel = android.util.Log.WARN;
    public static List<String> debug_logs = new ArrayList<String>();

    public static void e(String tag, String msg){
        android.util.Log.e(tag, msg);
        if(Log.logLevel == android.util.Log.DEBUG){
            debug_logs.add(msg+"\n");
        }
    }

    public static void e(String tag, String msg, Throwable tr){
        android.util.Log.e(tag, msg, tr);
    }

    public static void d(String tag, String msg){
        if(Log.logLevel == android.util.Log.DEBUG) {
            android.util.Log.d(tag, msg);
            debug_logs.add(msg+"\n");
        }
    }

    public static void i(String tag, String msg){
        if(Log.logLevel == android.util.Log.DEBUG) {
            android.util.Log.i(tag, msg);
            debug_logs.add(msg+"\n");
        }
    }

    public static void v(String tag, String msg){
        if(Log.logLevel == android.util.Log.DEBUG) {
            android.util.Log.v(tag, msg);
            debug_logs.add(msg+"\n");
        }
    }
}
