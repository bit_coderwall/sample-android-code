package com.unoceros.consumer.social;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;

import com.unoceros.consumer.NoNetworkActivity;
import com.unoceros.consumer.R;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;

import org.json.JSONObject;

import java.net.URI;
import java.net.URL;

/**
 * Created by Amit
 */
abstract public class BaseSignInActivity extends ActionBarActivity {

    protected static final String TAG = "BaseSignInActivity";
    private SharedPreferences mPref;

    abstract public void signOut();
    abstract public void revokeAccess();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_progress);

        mPref = this.getSharedPreferences(AppUtility.SYNC_PREF, Context.MODE_PRIVATE);

        final ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setCustomView(R.layout.actionbar_generic);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);

            //remove home icon
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == UserAdProfileActivity.REQUEST_CODE){
            setResult(RESULT_OK);
            finish();
        }else if(requestCode == NoNetworkActivity.REQUEST_CODE){
            if(resultCode == NoNetworkActivity.NO_CONNECTION_BACK){
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    }

    protected void affiliateProfileUpdate(UserInfo userInfo){
        //at this point user is definitely signed in
        if (!AppState.instance().isSignedIn())
            AppState.instance().signIn();

        /*
        String partnerId = mPref.getString("affiliateId", null);
        if(TextUtils.isEmpty(partnerId)) {
            //Intent i = new Intent(this, UserAdProfileActivity.class);
            //startActivityForResult(i, UserAdProfileActivity.REQUEST_CODE);
        }else{
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try{
                        Thread.sleep(3500);
                    }catch (Exception e){}
                    finish();
                }
            }).start();
        }
        */
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(3500);
                }catch (Exception e){}
                Log.d(TAG, "closing sign in screen.");
                finish();
            }
        }).start();
    }

}
