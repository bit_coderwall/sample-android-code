package com.unoceros.consumer;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.fragment.OnFragmentInteractionListener;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.social.EmailSignInActivity;
import com.unoceros.consumer.social.FacebookSignInActivity;
import com.unoceros.consumer.social.GoogleSignInActivity;
import com.unoceros.consumer.social.TwitterSignInActivity;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.util.SyncUtility;


/**
 * Created by Amit
 */
public class SocialSignUpActivity extends ActionBarActivity
        implements OnFragmentInteractionListener{

    private final static String TAG = "SocialSignUpActivity";
    private Activity activity;
    private ActionBar mActionBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup_social1);

        activity = this;

        if (!AppState.isInitialized()) {
            AppState.setContext(AppController.getInstance());
        }

        mActionBar = getSupportActionBar();
        if(mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(false);
            mActionBar.setHomeButtonEnabled(false);
            mActionBar.hide();
        }

        findViewById(R.id.signup_twitter_left_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent twitter = new Intent(activity, TwitterSignInActivity.class);
                startActivityForResult(twitter, TwitterSignInActivity.TWITTER_SIGNIN_REQUEST);
            }
        });

        findViewById(R.id.signup_unoceros_center_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailSignUp = new Intent(activity, EmailSignInActivity.class);
                startActivityForResult(emailSignUp, EmailSignInActivity.EMAIL_SIGNIN_REQUEST);
            }
        });

        findViewById(R.id.signup_gplus_right_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent google = new Intent(activity, GoogleSignInActivity.class);
                startActivityForResult(google, GoogleSignInActivity.GOOGLE_SIGNIN_REQUEST);
            }
        });

        findViewById(R.id.signup_fb_top_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent facebook = new Intent(activity, FacebookSignInActivity.class);
                startActivityForResult(facebook, FacebookSignInActivity.FACEBOOK_SIGNIN_REQUEST);
            }
        });

        if(AppState.instance().isSignedIn()) {
            //show welcome screen??
            Log.d(TAG, "login state: user logged in successfully!!");
            if(AppState.instance().userAgreedToTOSAndPP()) {
                //sync remote offer to local store
                SyncUtility.syncOfferWithDevice(this);

                //splash screen
                Intent mainIntent = new Intent(this, SplashUI.class);
                startActivity(mainIntent);
                finish();
            }else{
                Intent walkThruIntent = new Intent(this, TermsOfServiceActivity.class);
                startActivityForResult(walkThruIntent, TermsOfServiceActivity.WALK_THRU_REQUEST);
            }
        }else{
            //show signup buttons
            Log.d(TAG, "login state: user is not logged in!!");
            //show login fragment
            findViewById(R.id.signup_options_layout_new).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check which request we're responding to
        if (requestCode == TermsOfServiceActivity.WALK_THRU_REQUEST) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                //terms accepted
                validateWalkThruConfirmation();

                //broadcast sync setup service request
                Intent syncIntent = new Intent(AppUtility.SYNC_ACTION_SETUP);
                // You can also include some extra data.
                syncIntent.putExtra("message", "User Accepted Terms of Service!");
                syncIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                sendBroadcast(syncIntent);
            } else {
                //terms denied
                Toast.makeText(activity, "Well that is unfortunate...", Toast.LENGTH_LONG).show();
            }
        }else if(requestCode == GoogleSignInActivity.GOOGLE_SIGNIN_REQUEST){
            if (resultCode == Activity.RESULT_OK) {
                //signed in via google
                persistUserInfo();
                validateWalkThruConfirmation();
            } else {
                //could not signed in
                Toast.makeText(activity, "Could not sign in using google...", Toast.LENGTH_LONG).show();
            }
        }else if(requestCode == EmailSignInActivity.EMAIL_SIGNIN_REQUEST){
            if (resultCode == Activity.RESULT_OK) {
                //signed in using email
                persistUserInfo();
                validateWalkThruConfirmation();
            } else {
                //could not signed in
                Toast.makeText(activity, "Could not sign in using email...", Toast.LENGTH_LONG).show();
            }
        }else if(requestCode == TwitterSignInActivity.TWITTER_SIGNIN_REQUEST){
            if (resultCode == Activity.RESULT_OK) {
                //signed in using email
                persistUserInfo();
                validateWalkThruConfirmation();
            } else {
                //could not signed in
                Toast.makeText(activity, "Could not sign in using twitter...", Toast.LENGTH_LONG).show();
            }
        }else if(requestCode == FacebookSignInActivity.FACEBOOK_SIGNIN_REQUEST){
            if (resultCode == Activity.RESULT_OK) {
                //signed in using email
                persistUserInfo();
                validateWalkThruConfirmation();
            } else {
                //could not signed in
                Toast.makeText(activity, "Could not sign in using facebook...", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }

    public void onResume(){
        super.onResume();

        //tracking resume
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "SocialSignUpActivity.onResume", AppState.instance().getStatusJson());
    }

    public void onPause(){
        super.onPause();

        //tracking pause
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "SocialSignUpActivity.onPause", AppState.instance().getStatusJson());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(activity);
                if (NavUtils.shouldUpRecreateTask(activity, upIntent)) {
                    // This activity is NOT part of this app's task, so create a new task
                    // when navigating up, with a synthesized back stack.
                    TaskStackBuilder.create(activity)
                            // Add all of this activity's parents to the back stack
                            .addNextIntentWithParentStack(upIntent)
                                    // Navigate up to the closest parent
                            .startActivities();
                } else {
                    NavUtils.navigateUpFromSameTask(activity);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void validateWalkThruConfirmation(){
        if (AppState.instance().isSignedIn() && !AppState.instance().userAgreedToTOSAndPP()) {
            Intent walkThruIntent = new Intent(this, TermsOfServiceActivity.class);
            startActivityForResult(walkThruIntent, TermsOfServiceActivity.WALK_THRU_REQUEST);
        }else if (AppState.instance().isSignedIn() && AppState.instance().userAgreedToTOSAndPP()) {
            //sync remote offer to local store
            SyncUtility.syncOfferWithDevice(this);

            Intent mainIntent = new Intent(this, MainLandingActivity.class);
            startActivity(mainIntent);
            finish();
        }
    }

    private void persistUserInfo(){
        UserInfo userInfo = AppController.getInstance().getUserInfo();
        final SharedPreferences prefs = this.getSharedPreferences(AppUtility.INSTALLATION_PREF_FILE, MODE_PRIVATE);
        prefs.edit().putString("username", userInfo.getUserName()).apply();
        prefs.edit().putString("useremail", userInfo.getUserEmail()).apply();
        prefs.edit().putString("userimage", userInfo.getUserImageUrl()).apply();

        Gson gson = new Gson();
        String gsonData = gson.toJson(userInfo);
        prefs.edit().putString("userInfo", gsonData).apply();
        TrackingManager.getInstance(AppController.getInstance()).sendTrackedInformation(userInfo);
        TrackingManager.getInstance(AppController.getInstance()).trackComputeAvailability(userInfo);
    }

    @Override
    public void onFragmentInteraction(Bundle bundle) {
        Log.i(TAG, bundle.toString());
    }

    @Override
    public void onFragmentVisible(boolean visible) {
        Log.i(TAG, "fragment visible: "+visible);
    }
}
