package com.unoceros.consumer.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Amit
 */
public class Campaign implements Parcelable, Serializable {

    private String id;
    private String title, description, thumbnailUrl;
    private String score, status;
    private String rating;
    private String prev_url;
    private String offer_url;
    private String expireDateStr;
    private String uuid;
    
    public Campaign() {}

    public Campaign(Parcel in){
        readFromParcel(in);
    }
    public Campaign(String id, String name, String desc, String thumbnailUrl, String score, String status, String rating) {
        this.id = id;
        this.title = name;
        this.description = desc;
        this.thumbnailUrl = thumbnailUrl;
        this.score = score;
        this.rating = rating;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getUrl() {
        return offer_url;
    }

    public void setUrl(String url) {
        this.offer_url = url;
    }

    public String getPrevUrl() {
        return prev_url;
    }

    public void setPrevUrl(String url) {
        this.prev_url = url;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getExpireDateStr(){return expireDateStr;}

    public void setExpireDateStr(String date){this.expireDateStr = date;}

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    @Override
    public String toString(){
        return this.uuid;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(getTitle());
        dest.writeString(getDescription());
        dest.writeString(getThumbnailUrl());
        dest.writeString(getStatus());
        dest.writeString(getScore());
        dest.writeString(getUrl());
        dest.writeString(getPrevUrl());
        dest.writeString(getRating());
        dest.writeString(getExpireDateStr());
        dest.writeString(getUuid());
    }

    public void readFromParcel(Parcel in) {
        setId(in.readString());
        setTitle(in.readString());
        setDescription(in.readString());
        setThumbnailUrl(in.readString());
        setStatus(in.readString());
        setScore(in.readString());
        setUrl(in.readString());
        setPrevUrl(in.readString());
        setRating(in.readString());
        setExpireDateStr(in.readString());
        setUuid(in.readString());
    }

    public static final Parcelable.Creator<Campaign> CREATOR = new Parcelable.Creator<Campaign>() {
        public Campaign createFromParcel(Parcel in) {
            return new Campaign(in);
        }

        public Campaign[] newArray(int size) {
            return new Campaign[size];
        }
    };

    @Override
    public int hashCode(){
        int code = (getUuid()!=null && getUuid().length()>0)?getUuid().hashCode():getId().hashCode();
        return code;
    }

    @Override
    public boolean equals(Object o){
        if (o instanceof Campaign){
            boolean eq = getId().equalsIgnoreCase(((Campaign) o).getId());
            if(getUuid() != null && getUuid().length() > 0)
                eq = getUuid().equalsIgnoreCase(((Campaign) o).getUuid());
            return eq;
        }
        return false;
    }
}