package com.unoceros.consumer.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.unoceros.consumer.NoNetworkActivity;
import com.unoceros.consumer.R;
import com.unoceros.consumer.SocialSignUpActivity;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.fragment.OfferDetailFragment;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.DepositStream;
import com.unoceros.consumer.services.ApplicationServices;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.security.auth.x500.X500Principal;

/**
 * Created by Amit
 */
public class AppUtility {

    private static final String TAG = "AppUtility";
    private static final X500Principal DEBUG_DN = new X500Principal("CN=Android Debug,O=Android,C=US");


    /**
     * Preference File Names
     */
    public static final String INSTALLATION_PREF_FILE = "INSTALLATION";
    public static final String DATA_STORE_PREF = "DATASTORE";
    public static final String SYNC_PREF = "SYNC";

    /**
     * Intent Actions
     */
    public static final String SYNC_ACTION_SETUP = "com.unoceros.consumer.compute.SYNC_ACTION_SETUP";
    public static final String SYNC_ACTION_SERVICE = "com.unoceros.consumer.compute.SYNC_ACTION_SERVICE";
    public static final String SYNC_ACTION_PARTNER_CREATED = "com.unoceros.consumer.compute.SYNC_ACTION_PARTNER_CREATED";
    public static final String SYNC_ACTION_CREDIT_WITHDRAW_MADE = "com.unoceros.consumer.compute.SYNC_ACTION_CREDIT_WITHDRAW_MADE";
    public static final String ACTION_GET_ACTIVITY_STREAMING = "com.unoceros.consumer.compute.ACTION_GET_ACTIVITY_STREAMING";
    public static final String ACTION_GET_REWARD_REDEMPTION_CODE = "com.unoceros.consumer.compute.ACTION_GET_REWARD_REDEMPTION_CODE";
    public static final String ACTION_INSERT_OFFER_BY_DATE = "com.unoceros.consumer.compute.ACTION_INSERT_OFFER_BY_DATE";
    public static final String ACTION_SYNC_REMOTE_OFFER_WITH_DEVICE = "com.unoceros.consumer.compute.ACTION_SYNC_REMOTE_OFFER_WITH_DEVICE";


    // Sets an ID for the notification
    public static final int NOTIFICATION_ID = 0;
    public static final String LIST_BG_HEX_COLOR_TRANSPARENT = "#E5FFFFFF";

    /**
     * API 18 and above* 
     * @return
     */
    public static boolean hasJellyBeanMR2() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    }
    
    public static boolean isDebuggable(Context ctx) {
        boolean debuggable = false;

        PackageManager pm = ctx.getPackageManager();
        try {
            ApplicationInfo appinfo = pm.getApplicationInfo(ctx.getPackageName(), 0);
            debuggable = (0 != (appinfo.flags &= ApplicationInfo.FLAG_DEBUGGABLE));
        } catch (Exception e) {
	        /*debuggable variable will remain false*/
        }

        return debuggable;
    }

    public static String getVersionName(Application context) {
        String v = context.getResources().getString(R.string.app_version);
        ;
        try {
            v = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // Huh? Really?
        }
        return v;
    }

    public static boolean signedWithDebugKey(Context context) {
        boolean debuggable = false;

        try {
            PackageInfo pinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            Signature signatures[] = pinfo.signatures;

            for (int i = 0; i < signatures.length; i++) {
                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                ByteArrayInputStream stream = new ByteArrayInputStream(signatures[i].toByteArray());
                X509Certificate cert = (X509Certificate) cf.generateCertificate(stream);
                debuggable = cert.getSubjectX500Principal().equals(DEBUG_DN);
                if (debuggable)
                    break;
            }

        } catch (Exception e) {
            //debuggable variable will remain false
        }
        return debuggable;
    }

    public static void checkNetworkConnection(Activity context, int requestCode) {
        if (!AppUtility.haveNetworkConnection(context)) {
            Intent intent = new Intent();
            intent.setClass(context, NoNetworkActivity.class);
            context.startActivityForResult(intent, requestCode);
        }
    }

    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI") || ni.getTypeName().contains("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE") || ni.getTypeName().contains("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;

            if (haveConnectedMobile == true || haveConnectedMobile == true)
                break;
        }

        return haveConnectedWifi || haveConnectedMobile;
    }

    public static boolean haveWifiConnection(Context context) {
        boolean haveConnectedWifi = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected()) {
                    haveConnectedWifi = true;
                    break;
                }
        }
        return haveConnectedWifi;
    }

    public static boolean haveMobileConnection(Context context) {
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected()) {
                    haveConnectedMobile = true;
                    break;
                }
        }
        return haveConnectedMobile;
    }

    /**
     * Validate email with regular expression
     *
     * @param email
     *            email for validation
     * @return true valid email, false invalid email
     */
    public static boolean isValidateEmail(final String email) {
        if(TextUtils.isEmpty(email))
            return false;

        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Returns true if device can be a valid node in Unoceros network.  This method tries determining
     * if the device is an emulator using different device metadata.  It is a valid node by default
     * if the app is signed with a debug certificate.
     *
     * @param context
     * @return
     */
    public static boolean isValidApplicationNode(Context context){
        //check if signed release version
        if (!(AppUtility.signedWithDebugKey(context) || AppUtility.isDebuggable(context))) {
            //check if emulator
            try {
                boolean isEmulator = //
                        Build.FINGERPRINT.startsWith("generic")//
                                || Build.FINGERPRINT.startsWith("unknown")//
                                || Build.MODEL.contains("google_sdk")//
                                || Build.MODEL.contains("Emulator")//
                                || Build.MODEL.contains("Android SDK built for x86")
                                || Build.MANUFACTURER.contains("Genymotion");
                if (isEmulator)
                    return false;

                isEmulator = Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic");
                if (isEmulator)
                    return false;

                isEmulator = "google_sdk".equals(Build.PRODUCT);
                if (isEmulator)
                    return false;
            } catch (Exception e){
                //not a valid node
                Log.e(TAG, "Error occurred trying to determine if device is a valid node");
                return false;
            }
        }
        return true;
    }

    public static String getUniqueUserID(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(AppUtility.INSTALLATION_PREF_FILE, Context.MODE_PRIVATE);

        String id = prefs.getString(Const.PREFS_DEVICE_ID, null);
        if (id == null) {
            id = ApplicationServices.getInstance(AppController.getInstance()).getDeviceHardware().getUniqueDeviceId().toString();
            prefs.edit().putString(Const.PREFS_DEVICE_ID, id).apply();
        }

        return id;
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e(TAG, "Directory not created");
        }
        return file;
    }

    public static synchronized boolean isStoredStreamExpired(Context context, String fileName) {
        File file = new File(context.getFilesDir(), fileName);
        if (!file.exists() || !file.canRead()) {
            Log.d(TAG, "No saved Activity stream");
            return true;
        }

        long  diff = System.currentTimeMillis() - file.lastModified();
        long expireTime = 3600000; //an hour of expiration
        if(diff > expireTime) {
            Log.d(TAG, "Saved Activity stream is expired");
            return true;
        }

        return false;
    }

    public static synchronized ArrayList<DepositStream> readFromInternalStorage(Context context, String fileName) {
        try {
            File file = new File(context.getFilesDir(), fileName);
            if (!file.exists() || !file.canRead()) {
                Log.d(TAG, "No saved Activity stream");
                return null;
            }

            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream is = new ObjectInputStream(fis);
            ArrayList<DepositStream> parcelableStream = (ArrayList<DepositStream>) is.readObject();
            is.close();
            return parcelableStream;
        } catch (Exception ioe) {
            Log.e(TAG, "File read error, creating a new one.", ioe);
        }

        // should never return null, programmer error.
        return null;
    }

    public static synchronized boolean writeToInternalStorage(Context context, String fileName, ArrayList<DepositStream> parcelableStream) {
        try {
            File file = new File(context.getFilesDir(), fileName);
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(parcelableStream);
            os.close();
        } catch (Exception ioe) {
            Log.e(TAG, "Persisting activity stream to disk failed!");
            ioe.printStackTrace();
            return false;
        }

        return true;
    }

    public static String getFinalUrl(String link) {
        if (Looper.myLooper() == Looper.getMainLooper())
            return link;

        String newlink = link;
        try {
            URL url = new URL(link);
            Log.d(TAG, "HTTP start url: "+ url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(1500);
            conn.setReadTimeout(2500);
            conn.setRequestMethod("HEAD");
            int code = conn.getResponseCode();
            Log.d(TAG, "HTTP response code: " + code);
            Log.d(TAG, "HTTP response url: " + conn.getURL());
            if (!url.getHost().equals(conn.getURL().getHost())
                    || code == HttpURLConnection.HTTP_MOVED_TEMP
                    || code == HttpURLConnection.HTTP_MOVED_PERM) {
                //redirected
                newlink = conn.getHeaderField("Location");
                Log.d(TAG, "Redirect Link: "+newlink);
            }
            conn.disconnect();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return newlink;
    }

    public static JSONObject getJSONFromUrl(String url) {
        InputStream is = null;
        JSONObject jObj = null;
        String json = null;

        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        try {
            jObj = new JSONObject(json);
        } catch (Exception e) {
            Log.e(TAG, "Error parsing data " + e.toString());
        }

        // return JSON String
        return jObj;

    }

    /*public static double calculateUnicoin(Context context, Campaign campaign) {
        double diff = context.getResources().getFraction(R.fraction.unoceros_offers_sequence_diff, 10, 10);
        double base = context.getResources().getFraction(R.fraction.unoceros_initial_credit_deposit, 10, 10);
        double rank = Double.valueOf(campaign.getScore());
        double offerUnicoins = ((diff * rank) + base);
        return offerUnicoins;
    }*/

    public static double calculateCredits(Context context, Campaign campaign) {
        try {
            String value = campaign.getScore();
            if(value == null || value.equals("0"))
                value = "0";

            double credits = Double.valueOf(value);
            return credits;
        }catch(Exception e){
            return Double.MAX_VALUE;
        }
    }

    /*
    public static int calculateDollar(Context context, Campaign campaign){
        double dollarValue = 0;
        try{
            String value = campaign.getScore();
            if(value == null || value.equals("0"))
                value = "0";
            double rate = context.getResources().getFraction(R.fraction.unoceros_offers_credit_dollar_rate, 10, 10);
            dollarValue = Double.parseDouble(value) * rate; //0.02;
            dollarValue = Math.round(dollarValue * 100.0) / 100.0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return ((int)Math.round(dollarValue));
    }*/

    public static int calculateDollar(Context context, Double credits){
        double dollarValue = 0;
        try{
            double rate = context.getResources().getFraction(R.fraction.unoceros_offers_credit_dollar_rate, 10, 10);
            if(credits > 0)
                dollarValue = credits / rate; //0.02;
            dollarValue = Math.round(dollarValue * 100.0) / 100.0;
        }catch (Exception e){
            e.printStackTrace();
        }
        return ((int)Math.round(dollarValue));
    }

    public static String encodeURIComponent(String component) {
        String result = null;
        try {
            result = URLEncoder.encode(component, "UTF-8")
                    .replaceAll("\\%28", "(")
                    .replaceAll("\\%29", ")")
                    .replaceAll("\\+", "%20")
                    .replaceAll("\\%27", "'")
                    .replaceAll("\\%21", "!")
                    .replaceAll("\\%7E", "~");
        } catch (UnsupportedEncodingException e) {
            result = component;
        }
        return result;
    }

    public static double getCreditRate(Context context, boolean isComputing){
        double creditRate = context.getResources().getInteger(R.integer.credit_rate_cancompute);

        return creditRate;
    }

    /**
     * Calculates and returns the rewards amount for a specific offer.  This reward amount is passed
     * as an argument to the backend endpoint when an offer is claimed.
     *
     * For offers that are not variable (incremental static price structure eg 1, 5, 10 etc), the
     * rewardAmount defaults to 1, for offers that are variable (continues price structure eg 0.1 to 1000),
     * the rewardAmount equates the dollar amount in cent.
     *
     * @param context
     * @param campaign
     * @return
     */
    public static int calculateRewardAmount(Context context, Campaign campaign){
        int rewardAmount = 1;
        //if offer is variable
        if(campaign != null && campaign.getTitle() != null
                && campaign.getTitle().toLowerCase().contains("v-std")){
            //int dollar = AppUtility.calculateDollar(context, campaign);

            double creditValue = AppUtility.calculateCredits(context, campaign);
            int dollarValue = AppUtility.calculateDollar(context, creditValue);

            //convert dollar value to reward amount for variable rewards
            rewardAmount = dollarValue * 100;
            Log.d(TAG, "RewardAmount: "+rewardAmount);
        }

        return rewardAmount;
    }

    /**
     * Returns a JSON respresentation of the user last know location, as lat and long.  No gps
     * permission access is require
     *
     * @param context
     * @return
     * @throws Exception
     */
    public static JSONObject getLastKnowLocation(Context context) throws Exception{
        JSONObject gps = new JSONObject();
        try {
            GoogleApiClient apiClient = new GoogleApiClient.Builder(context)
                    .addApi(LocationServices.API)
                    .build();
            Location location = LocationServices.FusedLocationApi.getLastLocation(apiClient);
            gps.put("lat", location.getLatitude());
            gps.put("long", location.getLongitude());
        }catch (Exception e) {
            Log.d(TAG, "Fused Location Error: ");
            LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            List<String> providers = manager.getProviders(true);
            Location location = null;
            for (final String provider : providers) {
                location = manager.getLastKnownLocation(provider);
                if (location != null) break;
            }
            if (location != null) {
                gps.put("lat", location.getLatitude());
                gps.put("long", location.getLongitude());
            } else {
                gps.put("lat", "");
                gps.put("long", "");
            }
        }
        return gps;
    }

    /**
     * Returns size in bytes* 
     * @param external
     * @return
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    @SuppressWarnings("deprecation")
    public static long totalDiskMemory(boolean external){
        StatFs statFs = new StatFs(Environment.getRootDirectory().getAbsolutePath());
        if(external)
            statFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        long total = 0;
        if(hasJellyBeanMR2())
            total = (statFs.getBlockCountLong() * statFs.getBlockSizeLong());
        else
            total = (statFs.getBlockCount() * statFs.getBlockSize());
        return total;
    }

    /**
     * Returns size in bytes* 
     * @param external
     * @return
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    @SuppressWarnings("deprecation")
    public static long freeDiskMemory(boolean external){
        StatFs statFs = new StatFs(Environment.getRootDirectory().getAbsolutePath());
        if(external)
            statFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        long free = 0;
        if(hasJellyBeanMR2())
            free = (statFs.getAvailableBlocksLong() * statFs.getBlockSizeLong());
        else
            free = (statFs.getAvailableBlocks() * statFs.getBlockSize());
        
        return free;
    }

    /**
     * Returns size in bytes* 
     * @param external
     * @return
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    @SuppressWarnings("deprecation")
    public static long busyDiskMemory(boolean external){
        StatFs statFs = new StatFs(Environment.getRootDirectory().getAbsolutePath());
        if(external)
            statFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        long total = 0;
        long free = 0;
        if(hasJellyBeanMR2()) {
            total = (statFs.getBlockCountLong() * statFs.getBlockSizeLong());
            free = (statFs.getAvailableBlocksLong() * statFs.getBlockSizeLong());
        }else {
            total = (statFs.getBlockCount() * statFs.getBlockSize());
            free = (statFs.getAvailableBlocks() * statFs.getBlockSize());
        }
        long busy = total - free;
        return busy;
    }

    public static boolean hasExternalStoragePrivateFile(Context context, String type, String fileName) {
        // Get path for the file on external storage.  If external
        // storage is not currently mounted this will fail.
        File path = context.getExternalFilesDir(type);
        File file = new File(path, fileName);
        if (file != null)
            return file.exists();

        return false;
    }


    public static AlertDialog alertInactiveDialog(final Activity activity, final Campaign campaign, final String affiliateId){
        String alertMessage = activity.getString(R.string.alert_inactive_rewards);
        // Create alert dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(alertMessage)
            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //do nothing
                }
            })
            .setPositiveButton("Vote!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Toast.makeText(activity, "Thank You!", Toast.LENGTH_SHORT).show();
                    WebView webView = new WebView(activity);
                    webView.setVisibility(View.INVISIBLE);
                    OfferDetailFragment.postbackConversion(activity, campaign, affiliateId, webView, null, null, null);
                }
            });

        // Create the AlertDialog object and return it
        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        return dialog;
    }
}
