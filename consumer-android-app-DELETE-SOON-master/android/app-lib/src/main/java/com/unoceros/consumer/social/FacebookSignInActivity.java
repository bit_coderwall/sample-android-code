package com.unoceros.consumer.social;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.flurry.android.FlurryAgent;
import com.unoceros.consumer.NoNetworkActivity;
import com.unoceros.consumer.R;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Amit
 */
public class FacebookSignInActivity extends BaseSignInActivity{

    private final static String TAG = "FacebookSignInActivity";

    public final static int FACEBOOK_SIGNIN_REQUEST = 4888;


    private Dialog auth_dialog;
    private WebView web;
    private ViewGroup loginform;

    private boolean mIsConnected;
    private Activity activity;
    private UiLifecycleHelper facebookUIHelper;
    private Session.StatusCallback facebookSessionCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            if (exception != null) {
                Log.d("Facebook", exception.getMessage());
            }
            Log.d(TAG, "Session State: " + session.getState());
            // you can make request to the /me API or do other stuff like post, etc. here
            onFacebookSessionStateChange(session, state, exception);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_progress);

        activity = this;

        facebookUIHelper = new UiLifecycleHelper(this, facebookSessionCallback);
        facebookUIHelper.onCreate(savedInstanceState);

        //findViewById(R.id.login_progress_bar).setVisibility(View.GONE);

        //form for validating user email
        auth_dialog = new Dialog(this); //, android.R.style.Theme_Black_NoTitleBar);
        auth_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        auth_dialog.setContentView(R.layout.dialog_webview_auth);
        final Window window = auth_dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        web = (WebView)auth_dialog.findViewById(R.id.dialog_webview);
        loginform = (ViewGroup)auth_dialog.findViewById(R.id.login_form);

        //initialize facebook
        initFacebookLoginClient();
    }

    private void checkNetworkConnection(){
        if(!AppUtility.haveNetworkConnection(this)){
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        }else{

            // For scenarios where the main activity is launched and user
            // session is not null, the session state change notification
            // may not be triggered. Trigger it if it's open/closed.
            Session session = Session.getActiveSession();
            if (session != null) {
                if(session.isOpened() || session.isClosed() )
                    onFacebookSessionStateChange(session, session.getState(), null);

                Log.i(TAG, "session: "+session+", open: "+session.isOpened()+", close: "+session.isClosed());
            }
            facebookUIHelper.onResume();
        }
    }

    public void onStart() {
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    public void onStop() {
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
        facebookUIHelper.onStop();
    }

    public void onResume(){
        super.onResume();

        checkNetworkConnection();
    }

    public void onPause(){
        super.onPause();
        facebookUIHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        facebookUIHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        facebookUIHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        }catch (Exception e){
            Log.e(TAG, e.getMessage(), e);
        }
    }

    private void initFacebookLoginClient(){
        // start Facebook Login

        // set the permissions in the third parameter of the call
        List<String> permissionList = Arrays.asList("email", "public_profile", "user_friends"); //, "user_birthday", "user_hometown", "user_location");
        openFacebookActiveSession(this, true, permissionList, facebookSessionCallback);
    }

    private void onFacebookSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (session != null && session.isOpened()) {
            // if the session is already open,
            // try to show the selection fragment
            // Make an API call to get user data and define a
            // new callback to handle the response.
            Request request = Request.newMeRequest(session,
                    new Request.GraphUserCallback() {
                        @Override
                        public void onCompleted(GraphUser user, Response response) {
                            // If the response is successful
                            if(!mIsConnected) {
                                mIsConnected = true;
                                facebookUserIsConnected(session, user, response);
                            }
                        }
                    });
            request.executeAsync();
        } else {
            if (SessionState.OPENING == state) {
                Log.e(TAG, "facebook session opening...");
            } else{
                Log.e(TAG, (session != null) ? session.toString() : state.toString());
                Log.e(TAG, "closing facebook session", exception);
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    }

    private static Session openFacebookActiveSession(Activity activity, boolean allowLoginUI,
                                                     List<String> permissions, Session.StatusCallback callback) {
        Session.OpenRequest openRequest = new Session.OpenRequest(activity)
                .setPermissions(permissions).setCallback(callback);
        Session session = new Session.Builder(activity).build();
        if (SessionState.CREATED_TOKEN_LOADED.equals(session.getState()) || allowLoginUI) {
            Session.setActiveSession(session);
            session.openForRead(openRequest);
            return session;
        }
        return null;
    }

    private void facebookUserIsConnected(Session session, GraphUser user, Response response){
        if (session == Session.getActiveSession()) {
            if (user != null) {
                //Toast.makeText(activity, "Facebook User is connected!", Toast.LENGTH_LONG).show();
                // Set the id for the ProfilePictureView
                // view that in turn displays the profile picture.
                // Find the user's profile picture custom view
                /*ProfilePictureView profilePictureView = (ProfilePictureView)findViewById(R.id.signup_facebook_image);
                profilePictureView.setCropped(true);
                profilePictureView.setProfileId(user.getId());
                // Set the Textview's text to the user's name.
                TextView welcome = (TextView) findViewById(R.id.signup_welcome_message);
                welcome.setText("Hello " + user.getUsername() + "!");
                profilePictureView.setVisibility(View.VISIBLE);
                */

                new AsyncTask<GraphUser, String, String>(){
                    GraphUser user;
                    public String doInBackground(GraphUser...users){
                        //update view
                        user = users[0];
                        String personPhoto = AppUtility.getFinalUrl("http://graph.facebook.com/"+user.getId()
                                +"/picture?type=large&redirect=true&width=400&height=400");

                        return personPhoto;
                    }
                    public void onPostExecute(String personPhoto){
                        String personEmail = user.getProperty("email")!=null?user.getProperty("email").toString():user.getId();
                        String personAge = user.getBirthday();
                        //((TextView)findViewById(R.id.login_status_message)).setText("Welcome " + user.getFirstName() + "! One moment please!");
                        //findViewById(R.id.login_progress_bar).setVisibility(View.GONE);

                        NetworkImageView profileImageView = (NetworkImageView) activity.findViewById(R.id.login_profile_image);
                        profileImageView.setDefaultImageResId(R.drawable.user);
                        profileImageView.setImageUrl(personPhoto, AppController.getInstance().getImageLoader());
                        //profileImageView.setVisibility(View.VISIBLE);

                        //persist user info
                        AppState.instance().setUsername(user.getFirstName()+" "+user.getLastName());
                        AppState.instance().setEmailAddress(personEmail);
                        AppState.instance().setAgeRange(0);

                        final UserInfo userInfo = AppController.getInstance().getUserInfo();
                        userInfo.setUserId(user.getId());
                        userInfo.setUserName(user.getFirstName()+" "+user.getLastName());
                        userInfo.setUserImageUrl(personPhoto);
                        userInfo.setFacebookUserId(user.getId());
                        userInfo.setUserEmail(personEmail);

                        if (!AppUtility.isValidateEmail(userInfo.getUserEmail())) {
                            findViewById(R.id.login_progress_bar).setVisibility(View.GONE);
                            findViewById(R.id.login_status_message).setVisibility(View.GONE);

                            //show login form since email not valid
                            loginform.setVisibility(View.VISIBLE);
                            web.setVisibility(View.INVISIBLE);
                            Button updateBtn = (Button)loginform.findViewById(R.id.sign_in_button);
                            updateBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    boolean cancel = attemptUpdate(loginform, userInfo);
                                    if(!cancel){
                                        if (!AppState.instance().isSignedIn())
                                            AppState.instance().signIn();

                                        auth_dialog.dismiss();
                                        setResult(RESULT_OK);
                                        affiliateProfileUpdate(userInfo);

                                        TrackingManager.getInstance(AppController.getInstance()).
                                                trackEvent(FacebookSignInActivity.this, "Facebook Sign-up");
                                    }
                                }
                            });
                            auth_dialog.setCancelable(true);
                            auth_dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    //show the progress bar and msg
                                    findViewById(R.id.login_progress_bar).setVisibility(View.VISIBLE);
                                    findViewById(R.id.login_status_message).setVisibility(View.VISIBLE);
                                }
                            });
                            auth_dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialogInterface) {
                                    setResult(RESULT_CANCELED);
                                    finish();
                                }
                            });
                            auth_dialog.show();
                        }else {
                            if (!AppState.instance().isSignedIn())
                                AppState.instance().signIn();

                            setResult(RESULT_OK);
                            affiliateProfileUpdate(userInfo);
                            TrackingManager.getInstance(AppController.getInstance()).
                                    trackEvent(FacebookSignInActivity.this, "Facebook Sign-up");
                            /*
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try{
                                        Thread.sleep(3500);
                                    }catch (Exception e){}
                                    //finish();
                                }
                            }).start();*/
                        }
                    }
                }.execute(user);
            }
        }
        if (response.getError() != null) {
            Log.e(TAG, response.getError().toString(), response.getError().getException());
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    private boolean attemptUpdate(ViewGroup loginform, UserInfo userInfo){
        boolean cancel = false;
        View focusView = null;

        EditText emailView = (EditText)loginform.findViewById(R.id.sign_up_login_email);
        EditText emailConfirmView = (EditText)loginform.findViewById(R.id.sign_up_login_email_confirm);

        // Reset errors.
        emailView.setError(null);
        emailConfirmView.setError(null);

        // Store values at the time of the login attempt.
        String email = emailView.getText().toString();
        String emailConfirm = emailConfirmView.getText().toString();

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            emailView.setError(getString(R.string.error_field_required));
            focusView = emailView;
            cancel = true;
            //} else if (!mEmail.contains("@")) {
        } else if (!AppUtility.isValidateEmail(email)) {
            emailView.setError(getString(R.string.error_invalid_email));
            focusView = emailView;
            cancel = true;
        }

        // Check that the email addresses match
        if (!email.equalsIgnoreCase(emailConfirm)) {
            emailConfirmView.setError(getString(R.string.error_email_not_matching));
            focusView = emailConfirmView;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            if (focusView != null)
                focusView.requestFocus();
        } else {
            //update email
            userInfo.setUserEmail(email);
            AppState.instance().setEmailAddress(email);
        }

        return cancel;
    }

    @Override
    public void signOut() {

    }

    @Override
    public void revokeAccess() {

    }
}
