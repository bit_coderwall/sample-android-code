package com.unoceros.consumer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;

public class NoNetworkActivity extends ActionBarActivity{

    private static final String TAG = "NoNetworkActivity";
    private NoNetworkActivity mContext;


    //request code
    public static final int REQUEST_CODE = 999;

    //result code
	public static final int CONNECTION_BACK = 0;
	public static final int NO_CONNECTION_BACK = 1;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		mContext = this;
		
		setContentView(R.layout.activity_no_network);
		Button refreshBtn = (Button) findViewById(R.id.try_again_btn);
		refreshBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean connected = AppUtility.haveNetworkConnection(mContext);
				Log.d(TAG, "Connected: " + connected);
				if(connected){
					//go back to previous activity
					Log.d(TAG," Go back to previous activity.");
					setResult(CONNECTION_BACK);
					finish();
				}
			}
		});

        final ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setCustomView(R.layout.actionbar_generic);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);

            //remove home icon
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

	@Override
	public void onResume(){
		super.onResume();

        if(isFinishing()){
            //someone hit the back button
            finish();
        }
	}

	@Override
	public void onPause(){
		super.onPause();
		boolean connected = AppUtility.haveNetworkConnection(mContext);
		if(connected){
            //connection is back, go back to previous activity
			setResult(CONNECTION_BACK);
            finish();
		}else{
            //still no connection
			setResult(NO_CONNECTION_BACK);
		}
	}


    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }

	@Override
	public void onDestroy(){
		super.onDestroy();

	}
}


