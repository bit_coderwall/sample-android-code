package com.unoceros.consumer.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.unoceros.consumer.ContinueStatusSlickblueActivity;
import com.unoceros.consumer.MainLandingActivity;
import com.unoceros.consumer.MessageCenterActivity;
import com.unoceros.consumer.R;
import com.unoceros.consumer.SocialSignUpActivity;
import com.unoceros.consumer.SplashUI;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.model.PushMessage;

import java.util.UUID;

/**
 * Helper class for showing and canceling progress notifications.
 */
public class NotificationHelper {

    public static final String STICKY_NOTIFICATION_START = "STICKY_START";

    public static void notifyPushMessage(Context context, Bundle messageBundle) {
        PushMessage msg = null;
        if(messageBundle != null) {
            msg = messageBundle.getParcelable("pushMessage");
        }
        if(msg != null) {
            Notification note = getNotification(context,
                    R.drawable.ic_launcher,
                    "Unoceros [Message]",
                    msg.getHeadline(), //ticker
                    msg.getHeadline(), //text
                    R.drawable.ic_notification_active,
                    false,
                    messageBundle,
                    null);

            notify(context, note);
        }
    }

    public static void notifyNoInternet(Context context, String msg, boolean isForceful) {
        Resources res = context.getResources();
        Notification note =  getNotification(context,
                R.drawable.ic_notification_inactive,
                "Unoceros [No Network]",
                msg,
                msg,
                R.drawable.ic_notification_inactive,
                false,
                null,
                null);


        notify(context, note);
    }

    public static void notifyPaused(final Context context) {
        Resources res = context.getResources();
        String text = res.getString(R.string.paused_notification_placeholder_text_template);
        Notification note =  getNotification(context,
                R.drawable.ic_notification_inactive,
                "Unoceros [Paused]",
                "Unoceros Credit Earning Paused",
                text,
                R.drawable.ic_notification_inactive,
                false,
                null,
                null);


        notify(context, note);
    }

    /**
     * Put the message into a notification and post it.
     * This is just one simple example of what you might choose to do with
     * a milestone message.
     * @param context
     * @param msg
     * @param isForceful
     */
    public static void notifyMilestoreCredits(Context context, String msg, boolean isForceful) {
        Resources res = context.getResources();
        Notification note =  getNotification(context,
                R.drawable.ic_launcher,
                "Unoceros [Milestone]",
                msg,
                msg,
                R.drawable.ic_notification_active,
                false,
                null,
                null);

        notify(context, note);
    }

    public static void notifyCreditsEarned(Context context, String msg, boolean isForceful) {
        Resources res = context.getResources();
        Notification note =  getNotification(context,
                R.drawable.ic_launcher,
                "Unoceros [Credits Earned]",
                msg,
                msg,
                R.drawable.ic_notification_active,
                false,
                null,
                null);

        notify(context, note);
    }
    private static Notification getNotification(final Context context,
                                                final int bigIcon,
                                                final String title,
                                                final String ticker,
                                                final String text,
                                                final int smallIcon,
                                                boolean isForceful,
                                                Bundle bundle,
                                                Class intentClass){

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
        boolean shouldNotified = isForceful;
        long now = System.currentTimeMillis();
        if(!isForceful){
            long lastNotified = sharedPrefs.getLong("lastNotified", 0);
            if(lastNotified > 0){
                //check if we notified the user 30 minutes ago
                if (now - lastNotified >= 30*60*1000) {
                    shouldNotified = true;
                }
            }else{
                //has note being notified before--first time
                shouldNotified = true;
            }

        }
        if(AppUtility.signedWithDebugKey(context) || AppUtility.isDebuggable(context)){
            shouldNotified = true;
        }
        boolean userNotifyPrefs = sharedPrefs.getBoolean("prefNotification", true);
        if(shouldNotified && userNotifyPrefs) {
            if(intentClass == null)
                intentClass = ContinueStatusSlickblueActivity.class;
            Intent intent = new Intent(context, intentClass);
            intent.putExtra(NotificationHelper.STICKY_NOTIFICATION_START, true);
            intent.putExtra("bundle", bundle);
                    
            //.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            // Adds the back stack
            stackBuilder.addParentStack(ContinueStatusSlickblueActivity.class);
            // Adds the Intent to the top of the stack
            stackBuilder.addNextIntent(intent);
            // Gets a PendingIntent containing the entire back stack
            //PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent,
            //        PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent contentIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            //large icon
            Bitmap bm = BitmapFactory.decodeResource(context.getResources(), bigIcon);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(smallIcon)
                    .setLargeIcon(bm)
                    .setContentTitle(title)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                    .setContentText(text)
                    .setTicker(ticker)
                    .setAutoCancel(true)
                            //.setVibrate(null)
                            //.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                    .setContentIntent(contentIntent);

            //get the notification object
            Notification note = mBuilder.build();

            //check preference if to add sound
            boolean isVibrationON = sharedPrefs.getBoolean("prefVibrate", true);
            if(isVibrationON)
                note.defaults |= Notification.DEFAULT_VIBRATE;
            else
                note.defaults &= ~Notification.DEFAULT_VIBRATE;

            //check preference if to add sound
            boolean isSoundOn = sharedPrefs.getBoolean("prefSound", true);
            if(isSoundOn)
                note.defaults |= Notification.DEFAULT_SOUND;
            else
                note.defaults &= ~Notification.DEFAULT_SOUND;

            //set last notified time to now
            sharedPrefs.edit().putLong("lastNotified", now).apply();

            return note;
        }

        return null;
    }

    private static void notify(Context context, Notification notification) {
        if(notification != null) {
            // Gets an instance of the NotificationManager service
            NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            nm.notify(AppUtility.NOTIFICATION_ID, notification);
        }
    }
}
