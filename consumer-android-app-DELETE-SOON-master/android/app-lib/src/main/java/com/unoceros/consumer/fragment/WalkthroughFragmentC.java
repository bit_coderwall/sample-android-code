package com.unoceros.consumer.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.unoceros.consumer.R;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.services.AdProfileServices;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WalkthroughFragmentA#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WalkthroughFragmentC extends Fragment {
    private Button mRegButton;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment WalkthroughFragmentC.
     */
    public static WalkthroughFragmentC newInstance(Bundle args) {
        WalkthroughFragmentC fragment = new WalkthroughFragmentC();
        fragment.setArguments(args);
        return fragment;
    }

    public WalkthroughFragmentC() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_walkthrough_c, container, false);

        Bundle bundle = getArguments();
        boolean showRegButton = true;
        if(bundle != null)
            showRegButton = bundle.getBoolean("showRegButton");

        mRegButton = (Button) rootView.findViewById(R.id.reg_button);
        //ImageButton leftBtn = (ImageButton) rootView.findViewById(R.id.button_left);
        if(!showRegButton) {
            mRegButton.setVisibility(View.INVISIBLE);
            //leftBtn.setVisibility(View.INVISIBLE);
        }else
            mRegButton.setVisibility(View.VISIBLE);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Activity activity = getActivity();

        mRegButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.setResult(Activity.RESULT_OK);

                //tracking Registration Confirmation
                TrackingManager.getInstance(AppController.getInstance()).
                        trackEvent(activity, "Registration Confirmed");

                //finally close
                activity.finish();
            }
        });
    }

}
