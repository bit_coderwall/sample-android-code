package com.unoceros.consumer.services;

import android.app.Application;

import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.model.DeviceHardware;
import com.unoceros.consumer.model.UserInfo;

public class ApplicationServices {

	private static ApplicationServices instance;

    //device info
	private static DeviceHardware deviceHardware;

    //network/feed/image loading services
    private static NetworkRequest networkRequest;

    //credit/back account
    private static CreditServices creditService;

    //user info
    private static UserInfo userInfo;


	private ApplicationServices(Application context){
        //populate userInfo
        userInfo = new UserInfo();

        //create the different services
		deviceHardware = new DeviceHardware(context);
        networkRequest = new NetworkRequest(context);
        creditService = new CreditServices(context);
	}
	
	public static ApplicationServices getInstance(Application context){
		if(instance == null)
			instance = new ApplicationServices(context);
		
		return instance;
	}

    public static ApplicationServices instance(){
        if(instance == null)
            instance = new ApplicationServices(AppController.getInstance());
        return instance;
    }

    /**
     * Device hardware and app specific info
     * @param appName
     * @param appVersion
     * @return
     */
	public DeviceHardware setDeviceHardware(String appName, String appVersion){
		if(appName != null && !appName.isEmpty())
			deviceHardware.setAppName(appName);
		if(appVersion != null && !appVersion.isEmpty())
			deviceHardware.setAppVersion(appVersion);
		return deviceHardware;
	}
	public DeviceHardware getDeviceHardware(){
		return deviceHardware;
	}

    /**
     * Feed loading and Image loading proxy
     * @return
     */
    public NetworkRequest getNetworkRequest(){
        return networkRequest;
    }

    /**
     * Returns a instance of user info without syncing with internal storage.  To get a more updated
     * version of the userInfo, it is best to call {link AppController.getInstance().getUserInfo()}
     *
     * @return
     */
    public UserInfo getUserInfo(){
        return userInfo;
    }


    public CreditServices getCreditService(){
        return creditService;
    }
}
