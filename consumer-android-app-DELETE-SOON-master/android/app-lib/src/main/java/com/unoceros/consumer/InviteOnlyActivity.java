package com.unoceros.consumer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;

import com.unoceros.consumer.services.ApplicationServices;

/**
 * Created by Amit
 */
public class InviteOnlyActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_only);

        Button btn = (Button) findViewById(R.id.request_invitation_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestInviteViaEmail(InviteOnlyActivity.this);
            }
        });
    }

    private void requestInviteViaEmail(Activity context){
        String deviceInfo = ApplicationServices.getInstance(context.getApplication())
                .getDeviceHardware().toString();
        String body =
                "---------------------------------\n"+
                        context.getString(R.string.contact_support_email_body)+"\n"
                        +deviceInfo+"\n"+
                        "---------------------------------";

        StringBuffer buffer = new StringBuffer();
        buffer.append("mailto:");
        buffer.append( context.getString(R.string.contact_support_email_address));
        buffer.append("?subject=");
        buffer.append(context.getString(R.string.invite_request_email_subject));
        buffer.append("&body=");
        buffer.append(body);
        String uriString = buffer.toString().replace(" ", "%20");
        startActivity(Intent.createChooser(new Intent(Intent.ACTION_SENDTO, Uri.parse(uriString)),
                "Invite Request"));
    }
}
