package com.unoceros.consumer;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.model.DepositStream;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.views.DeviceActivityStreamItem;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Amit
 */
public class HistoryActivityStream extends ActionBarActivity{
    private static final String TAG = "HistoryActivityStream";
    private boolean mIsPagination;
    private Button activityStreamLoadMoreBtn;
    private ProgressBar activityStreamProgress;

    private int mLimit;
    private int mOffset;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_history_stream);

        //updated sub title
        actionBarSetup("Daily");

        //Progress Bar
        activityStreamProgress = (ProgressBar) findViewById(R.id.rewards_activity_stream_progress_bar);

        //Button load...
        activityStreamLoadMoreBtn = (Button) findViewById(R.id.rewards_activity_more_stream_button);
        activityStreamLoadMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityStreamProgress.setVisibility(View.VISIBLE);

                //loading more - pagination
                mIsPagination = true;
                mOffset = mLimit + mOffset;
                loadStreamBy(mLimit, mOffset);
            }
        });

        //Now populate the content
        mIsPagination = false;
        mLimit = 15; //limits are express in days
        mOffset = 0;
        loadStreamBy(mLimit, mOffset);
    }

    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }

    private void actionBarSetup(String subTitle) {
        ActionBar ab = getSupportActionBar();
        if(ab!= null){
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setSubtitle(subTitle);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            // app icon in action bar clicked; go to previous activity or home
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("limit", mLimit);
        outState.putInt("offset", mOffset);
        outState.putBoolean("pagination", mIsPagination);
        super.onSaveInstanceState(outState);
    }

    private void loadStreamBy(int limit, int offset){
        Intent streamIntent = new Intent(AppUtility.ACTION_GET_ACTIVITY_STREAMING);
        // You can also include some extra data.
        streamIntent.putExtra("limit", limit);
        streamIntent.putExtra("offset", offset);
        streamIntent.putExtra("order", "desc");
        streamIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(streamIntent);
    }

    private void checkNetworkConnection() {
        if (!AppUtility.haveNetworkConnection(this)) {
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //check network
        checkNetworkConnection();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mActivityStreamingReceiver, new IntentFilter("activity-streaming"));

        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "HistoryActivityStream.onResume", AppState.instance().getStatusJson());
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mActivityStreamingReceiver);

        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "HistoryActivityStream.onPause", AppState.instance().getStatusJson());
    }

    @Override
    protected void onDestroy() {
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "HistoryActivityStream.onDestroy", AppState.instance().getStatusJson());
        TrackingManager.getInstance(this.getApplication()).
                trackDestroyActivity(this);

        super.onDestroy();

    }

    private void populateActivityStream(Activity activity, ArrayList<DepositStream> parcelableStream, boolean isPagination) {
        LinearLayout activityStreamContainer = (LinearLayout) activity.findViewById(R.id.rewards_activity_stream_container);

        if(!isPagination) {
            activityStreamContainer.removeAllViews();

            activityStreamProgress.setVisibility(View.VISIBLE);
            activityStreamContainer.setVisibility(View.GONE);
        }else{
            //loading more
            activityStreamProgress.setVisibility(View.VISIBLE);
            activityStreamContainer.setVisibility(View.VISIBLE);
        }

        activityStreamLoadMoreBtn.setEnabled(false);
        activityStreamLoadMoreBtn.setClickable(false);

        if(parcelableStream != null && parcelableStream.size() > 0) {
            //preserve insertion order
            LinkedHashMap<String, Double> creditMap = new LinkedHashMap<String, Double>();
            LinkedHashMap<String, Double> hourMap = new LinkedHashMap<String, Double>();

            for (DepositStream stream : parcelableStream) {
                String date = stream.getDate();
                double credits = stream.getAmount();
                double hrs = (stream.getAmount() * 60) / stream.getRate();

                Double dailyCredits = creditMap.get(date);
                Double dailyHours = hourMap.get(date);
                if (dailyCredits == null || dailyCredits == 0) {
                    creditMap.put(date, credits);
                    hourMap.put(date, hrs);
                } else {
                    dailyCredits = credits + dailyCredits;
                    dailyHours = hrs + dailyHours;
                    creditMap.put(date, dailyCredits);
                    hourMap.put(date, dailyHours);
                }
            }

            for (String date : creditMap.keySet()) {
                Double credits = creditMap.get(date);
                credits = Math.round(credits * 100.0) / 100.0; //round to 2 decimal places

                Double hrs = hourMap.get(date);
                hrs = Math.round(hrs / 60 * 100.0) / 100.0; //round to 2 decimal places

                DeviceActivityStreamItem activityStreamData = new DeviceActivityStreamItem(activity);
                activityStreamData.configData(date, credits + "", hrs + "");

                //add each activity stream data to the container
                activityStreamContainer.addView(activityStreamData);
            }
            //enable load more button
            activityStreamLoadMoreBtn.setEnabled(true);
            activityStreamLoadMoreBtn.setClickable(true);
            activityStreamLoadMoreBtn.setVisibility(View.VISIBLE);

            //no more activity
            ((TextView)activity.findViewById(R.id.rewards_activity_no_more_text)).setVisibility(View.GONE);
        }else{
            //disable load more button
            activityStreamLoadMoreBtn.setEnabled(false);
            activityStreamLoadMoreBtn.setClickable(false);
            activityStreamLoadMoreBtn.setVisibility(View.GONE);

            //no more activity
            ((TextView)activity.findViewById(R.id.rewards_activity_no_more_text)).setVisibility(View.VISIBLE);
        }

        if(!isPagination) {
            activityStreamProgress.setVisibility(View.GONE);
            activityStreamContainer.setVisibility(View.VISIBLE);
        }else{
            //loading more
            activityStreamProgress.setVisibility(View.GONE);
            activityStreamContainer.setVisibility(View.VISIBLE);
        }
    }

    private BroadcastReceiver mActivityStreamingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ArrayList<DepositStream> parcelableStream = intent.getParcelableArrayListExtra("parcelableStream");
            populateActivityStream(HistoryActivityStream.this, parcelableStream, mIsPagination);
        }
    };
}
