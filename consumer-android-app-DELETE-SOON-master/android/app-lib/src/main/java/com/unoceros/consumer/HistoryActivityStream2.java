package com.unoceros.consumer;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.unoceros.consumer.adapter.ActivityStreamingAdapter;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.model.DepositStream;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Amit
 */
public class HistoryActivityStream2 extends ActionBarActivity{
    private static final String TAG = "HistoryActivityStream";
    private static final String PERSIST_FILENAME = "stream2.tmp";

    private boolean mIsPagination;
    private ListView mListView;
    private TextView mEmptyView;
    private int mLimit;
    private int mOffset;

    private ArrayList<DepositStream> mParcelableStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_history_stream2);

        //the list view holding the streaming items
        mListView=((ListView)findViewById(R.id.rewards_activity_stream_list));
        mEmptyView = (TextView)findViewById(R.id.rewards_activity_stream_empty);
        mEmptyView.setText("Loading Activity Stream...");
        mListView.setEmptyView(mEmptyView);

        //updated sub title
        actionBarSetup("Daily");
        Activity activity = this;

        //use stored version temporary
        boolean useCache = false;
        mParcelableStream = AppUtility.readFromInternalStorage(activity, HistoryActivityStream2.PERSIST_FILENAME);
        if(mParcelableStream != null) {
            useCache = true;
        }

        if(savedInstanceState != null && !useCache) {
            mParcelableStream = savedInstanceState.getParcelableArrayList("parcelableStream");
            if(mParcelableStream != null){
                useCache = true;
            }
        }

        if(!useCache) {
            //Now populate the content
            mIsPagination = false;
            mLimit = 15; //limits are express in days
            mOffset = 0;
            loadStreamBy(mLimit, mOffset);
        }else{
            //populate view with saved states
            populateActivityStream(activity, mParcelableStream, mIsPagination);
            boolean isExpired = AppUtility.isStoredStreamExpired(activity, HistoryActivityStream2.PERSIST_FILENAME);
            if(isExpired){
                mIsPagination = false;
                mLimit = 15; //limits are express in days
                mOffset = 0;
                loadStreamBy(mLimit, mOffset);
            }

        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);

    }

    private void actionBarSetup(String subTitle) {
        ActionBar ab = getSupportActionBar();
        if(ab!= null){
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setSubtitle(subTitle);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            // app icon in action bar clicked; go to previous activity or home
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("limit", mLimit);
        outState.putInt("offset", mOffset);
        outState.putBoolean("pagination", mIsPagination);
        outState.putParcelableArrayList("parcelableStream", mParcelableStream);
        super.onSaveInstanceState(outState);
    }

    private void loadStreamBy(int limit, int offset){
        Intent streamIntent = new Intent(AppUtility.ACTION_GET_ACTIVITY_STREAMING);
        // You can also include some extra data.
        streamIntent.putExtra("limit", limit);
        streamIntent.putExtra("offset", offset);
        streamIntent.putExtra("order", "desc");
        streamIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(streamIntent);
    }

    private void checkNetworkConnection() {
        if (!AppUtility.haveNetworkConnection(this)) {
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //check network
        checkNetworkConnection();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mActivityStreamingReceiver, new IntentFilter("activity-streaming"));

        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "HistoryActivityStream.onResume", AppState.instance().getStatusJson());
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mActivityStreamingReceiver);

        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "HistoryActivityStream.onPause", AppState.instance().getStatusJson());
    }

    @Override
    protected void onDestroy() {
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "HistoryActivityStream.onDestroy", AppState.instance().getStatusJson());
        TrackingManager.getInstance(this.getApplication()).
                trackDestroyActivity(this);

        super.onDestroy();
    }

    private void populateActivityStream(Activity activity, ArrayList<DepositStream> parcelableStream, boolean isPagination) {

        if(parcelableStream != null && parcelableStream.size() > 0){
            //create an empty adapter
            ActivityStreamingAdapter listAdapter = new ActivityStreamingAdapter(activity, R.layout.slickblue_list_history_credits_and_offers_timeline, parcelableStream, parcelableStream.size());

            //remove scrollbars
            mListView.setVerticalScrollBarEnabled(false);
            mListView.setHorizontalScrollBarEnabled(false);
            mListView.setAdapter(listAdapter);
        }else{
            try {
                //create an empty adapter
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String dateStr = sdf.format(new Date());

                //create default stream object
                DepositStream streamItem = new DepositStream();
                streamItem.setRate(0.0);
                streamItem.setAmount(0.0);
                streamItem.setDate(dateStr);
                parcelableStream = new ArrayList<DepositStream>();
                parcelableStream.add(streamItem);
                ActivityStreamingAdapter listAdapter = new ActivityStreamingAdapter(activity, R.layout.slickblue_list_history_credits_and_offers_timeline, parcelableStream, 0);

                //remove scrollbars
                mListView.setVerticalScrollBarEnabled(false);
                mListView.setHorizontalScrollBarEnabled(false);
                mListView.setAdapter(listAdapter);
            }catch (Exception e){
                Log.e(TAG, e.getMessage(), e);
            }
        }
    }

    private BroadcastReceiver mActivityStreamingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean success = intent.getBooleanExtra("success", false);
            if(success) {
                ArrayList<DepositStream> parcelableStream = intent.getParcelableArrayListExtra("parcelableStream");
                populateActivityStream(HistoryActivityStream2.this, parcelableStream, mIsPagination);
                mParcelableStream = parcelableStream;

                //store activity stream temporary
                AppUtility.writeToInternalStorage(context, HistoryActivityStream2.PERSIST_FILENAME, parcelableStream);
            }else{
                String errorMsg = intent.getStringExtra("message");
                errorMsg = "Sorry, error occurred while updating your activities. "+errorMsg;
                Toast.makeText(context, errorMsg, Toast.LENGTH_LONG).show();

                ArrayList<DepositStream> parcelableStream = AppUtility.readFromInternalStorage(context, HistoryActivityStream2.PERSIST_FILENAME);
                populateActivityStream(HistoryActivityStream2.this, parcelableStream, mIsPagination);
            }
        }
    };
}
