package com.unoceros.consumer.services;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.unoceros.consumer.R;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;

import org.json.JSONObject;

import java.net.URI;
import java.net.URL;

/**
 * Created by Amit
 */
public class AdProfileServices {

    private static final String TAG = "AdProfileServices";

    public void updateProfile(Activity activity, String companyName, String address, String cityName, String region,
                              String countryName, String zipCode, String phoneNumber, boolean doFinish){
        //zip pattern for validation
        String zipCodePattern = "\\d{5}(-\\d{4})?";

        if(TextUtils.isEmpty(zipCode) || !zipCode.matches(zipCodePattern)){
            Log.d("ProfileUpdater", "Sorry, zipcode is not valid");
        }else {
            String partnerKey = "Affiliate"; //"Advertiser";
            String api_domain = activity.getString(R.string.hasoffer_affiliate_domain_api);
            String api_param = String.format(activity.getString(R.string.hasoffer_create_partner_param), companyName,
                    address, cityName, region, countryName, zipCode, phoneNumber);

            String api = api_domain + "?" + api_param;
            new ProfileUpdatingTask(activity, partnerKey, doFinish).execute(api);
        }
    }

    /**
     *
     */
    private class ProfileUpdatingTask extends AsyncTask<String, Integer, String> {
        private String partnerKey;
        private boolean doFinish;
        private Activity context;
        public ProfileUpdatingTask(Activity context, String key, boolean doFinish){
            this.doFinish = doFinish;
            this.partnerKey = key;
            this.context = context;
        }
        @Override
        protected String doInBackground(String... urls) {
            String affiliateId = null;
            try{
                //get the affiliate data
                URL url = new URL(urls[0]);
                URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                url = uri.toURL();
                JSONObject affiliateJson = AppUtility.getJSONFromUrl(url.toString());

                //parse json data to get the Affiliate id
                JSONObject data = affiliateJson.getJSONObject("response").getJSONObject("data").getJSONObject(partnerKey);
                affiliateId = data.isNull("id") ? "" : data.getString("id");
            }catch (Exception e){
                //not good
                Log.e(TAG, e.getMessage(), e);
            }

            return affiliateId;
        }

        @Override
        protected void onPostExecute(String affiliateId) {
            super.onPostExecute(affiliateId);

            //save to local store
            SharedPreferences pref = context.getSharedPreferences(AppUtility.SYNC_PREF, Context.MODE_PRIVATE);
            pref.edit().putString("affiliateId", affiliateId).apply();

            //try updating remote endpoints via broadcast request
            if(!TextUtils.isEmpty(affiliateId)) {
                Intent affiliateIntent = new Intent(AppUtility.SYNC_ACTION_PARTNER_CREATED);
                // You can also include some extra data.
                affiliateIntent.putExtra("affiliateId", affiliateId);
                affiliateIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                context.sendBroadcast(affiliateIntent);
            }

            //close activity
            if(doFinish)
                context.finish();
        }
    }
}
