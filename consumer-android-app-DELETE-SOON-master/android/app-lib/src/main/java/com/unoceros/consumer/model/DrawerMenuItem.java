package com.unoceros.consumer.model;

/**
 * Created by Amit
 */
public class DrawerMenuItem {
    private String itemName;
    private int imgResID;
    private String title;
    private boolean isSpinner;
    private boolean isHeader;
    private int counter;

    public DrawerMenuItem(String itemName, int imgResID) {
        super();
        this.itemName = itemName;
        this.imgResID = imgResID;
    }

    public DrawerMenuItem(String title, boolean isHeader) {
        super();
        this.title = title;
        this.isHeader = isHeader;
    }

    public DrawerMenuItem(boolean isSpinner) {
        super();
        this.isSpinner = isSpinner;
    }

    public String getItemName() {
        return this.itemName;
    }
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
    public int getImgResID() {
        return imgResID;
    }
    public void setImgResID(int imgResID) {
        this.imgResID = imgResID;
    }
    public String getTitle(){return title;}
    public void setTitle(String title){this.title = title;}
    public int getCounter(){return counter;}
    public void setCounter(int counter){this.counter = counter;}
    public boolean isSpinner(){return isSpinner;}
    public void setSpinner(boolean spinner){this.isSpinner = spinner;}
    public boolean isHeader(){return isHeader;}
    public void setHeader(boolean header){this.isHeader = header;}

}
