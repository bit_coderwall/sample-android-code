package com.unoceros.consumer;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import com.unoceros.consumer.adapter.WalkThruPagerAdapter;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.libs.ZoomOutPageTransformer;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * Created by Amit
 */
public class HowItWorksActivity extends ActionBarActivity {
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager_intro_screen);
        actionBarSetup();

        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
        int count = 3;
        Bundle args = new Bundle();
        args.putBoolean("showRegButton", false);
        args.putBoolean("isHowItWorks", true);
        WalkThruPagerAdapter adapter = new WalkThruPagerAdapter(getSupportFragmentManager(), count, args);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(adapter);

        //Bind the title indicator to the adapter
        CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.indicator);
        indicator.setViewPager(mViewPager);
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {}

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {}

            @Override
            public void onPageScrollStateChanged(int arg0) {}
        });

        //set transformer
        mViewPager.setPageTransformer(true, new ZoomOutPageTransformer());

    }

    private void actionBarSetup() {
        ActionBar ab = getSupportActionBar();
        if(ab!= null){
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            // app icon in action bar clicked; go to previous activity or home
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }

    public void onResume(){
        super.onResume();

        //tracking resume
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "HowItWorks.onResume", AppState.instance().getStatusJson());
    }

    public void onPause(){
        super.onPause();

        //tracking pause
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "HowItWorks.onPause", AppState.instance().getStatusJson());
    }
}
