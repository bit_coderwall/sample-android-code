package com.unoceros.consumer.util;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.unoceros.consumer.R;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

//import com.unoceros.engine.JobManager;
//import com.unoceros.engine.service.AbstractTaskScheduler;

/**
 * Created by Amit
 */
public class SyncUtility {

    private static final String TAG = "SyncUtility";

    public static String getDateFromMilliseconds(long millis) {
        Date date = new Date(millis);
        String newstring = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault()).format(date);
        return newstring;
    }

    public static Date getYesterdayDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return calendar.getTime();
    }

    public static Date getEndOfDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    public static Date getStartOfDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static long toEndOfDay() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        long howMany = (c.getTimeInMillis() - System.currentTimeMillis());
        return howMany;
    }

    public static String getFormatedDate(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
    }

    /**
     * True if Device is in a state that a job can do computational task
     *
     * @param context
     * @return
     */
    public static boolean canCompute(Context context) {
        float level = batteryPrefLevel(context);
        return SyncUtility.isCharging(context) || SyncUtility.batteryLevel(context) >= level;
    }

    /**
     * Returns user preferred battery level settings
     * @param context
     * @return
     */
    public static float batteryPrefLevel(Context context){
        int level = context.getResources().getInteger(R.integer.battery_level_default);
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
        String batteryLevel = sharedPrefs.getString("prefBatteryLevel", level+""); //default to 75%

        if (!TextUtils.isEmpty(batteryLevel)) {
            //change to user selected preference
            level = Integer.parseInt(batteryLevel);
        }

        Log.d(TAG, "Battery level preference is : " + batteryLevel + "%");
        return level;
    }

    /**
     * Returns current device battery level
     * @param context
     * @return
     */
    public static float batteryLevel(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.getApplicationContext().registerReceiver(null, ifilter);
        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

        float batteryPct = level / (float) scale;
        return batteryPct * 100;
    }

    public static boolean isCharging(Context context) {
        return isChargingValues(context)[0];
    }

    private static boolean[] isChargingValues(Context context) {
        boolean[] retBools = new boolean[3];
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.getApplicationContext().registerReceiver(null, ifilter);

        int status = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1) : -1;
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;
        retBools[0] = isCharging;

        // How are we charging?
        int chargePlug = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1) : -1;
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
        retBools[1] = usbCharge;
        retBools[2] = acCharge;

        return retBools;
    }

    public static boolean isConnectedToWiFi(Context context){
        return AppUtility.haveWifiConnection(context);
    }

    /**
     * Request remotely stored offers by synced with device,
     * does nothing if user has successfully signed in
     *
     * Sync only occurs if the last time the device was synced was more than 24hrs ago
     * @param context
     */
    public static void syncOfferWithDevice(Context context) {
        if(!AppState.instance().isSignedIn())
            return;

        final SharedPreferences prefs = context.getSharedPreferences(AppUtility.INSTALLATION_PREF_FILE, Context.MODE_PRIVATE);
        long lastSyncDateMill = prefs.getLong("lastSyncDateMill", 0);

        Log.d(TAG, "syncing offer with device requested, last synced: "+lastSyncDateMill);
        if(lastSyncDateMill <= 0){
            //sync, since this might be the first time the device is actually syncing since install
            Intent redeemIntent = new Intent(AppUtility.ACTION_SYNC_REMOTE_OFFER_WITH_DEVICE);
            redeemIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            context.sendBroadcast(redeemIntent);

            //update prefs
            prefs.edit().putLong("lastSyncDateMill", System.currentTimeMillis()).apply();
        }else{
            long  diff = System.currentTimeMillis() - lastSyncDateMill;
            long expireTime = 86400000; //24 hour expiration
            if(diff > expireTime) {
                //sync if more than 24hr since last synced
                Intent redeemIntent = new Intent(AppUtility.ACTION_SYNC_REMOTE_OFFER_WITH_DEVICE);
                redeemIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                context.sendBroadcast(redeemIntent);

                //update prefs
                prefs.edit().putLong("lastSyncDateMill", System.currentTimeMillis()).apply();
            }
        }
    }
}