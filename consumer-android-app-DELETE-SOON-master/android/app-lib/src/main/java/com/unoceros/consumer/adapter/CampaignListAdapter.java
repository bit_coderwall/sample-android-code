package com.unoceros.consumer.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.model.Campaign;

import java.util.ArrayList;
import java.util.List;

import com.unoceros.consumer.R;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.views.FeedImageView;

/**
 * Created by Amit
 */

public class CampaignListAdapter extends BaseAdapter {

    private Context context; // activity;
    private LayoutInflater inflater;
    private List<Campaign> campaignItems;
    private ImageLoader imageLoader;
    private int lastPosition = -1;
    private boolean isClickable = true;

    public CampaignListAdapter(Activity activity, List<Campaign> campaignItems, boolean isClickable) {
        this.context = activity.getApplicationContext();
        this.campaignItems = campaignItems;
        this.imageLoader = AppController.getInstance().getImageLoader();
        if(campaignItems == null)
            this.campaignItems = new ArrayList<Campaign>();

        this.isClickable = isClickable;
    }

    @Override
    public int getCount() {
        return campaignItems.size();
    }

    @Override
    public Object getItem(int location) {
        return campaignItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void addData(Campaign campaign){
        if(campaign != null && campaign.getId() != null)
            campaignItems.add(campaign);
    }

    public void removeData(int position){
        if(position < campaignItems.size())
            campaignItems.remove(position);
    }

    public void clearData() {
        // clear the data
        campaignItems.clear();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        //if (convertView == null) {
        //    convertView = LayoutInflater.from(context).inflate(R.layout.row, parent, false);

        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null){
                convertView = inflater.inflate(R.layout.passport_layout, null);

            if (imageLoader == null)
                imageLoader = AppController.getInstance().getImageLoader();

            holder = new ViewHolder();

            FeedImageView thumbNail = (FeedImageView) convertView.findViewById(R.id.thumbnail);
            TextView title = (TextView) convertView.findViewById(R.id.title);
            TextView desc = (TextView) convertView.findViewById(R.id.description);
            TextView rating = (TextView) convertView.findViewById(R.id.ratings);
            //TextView status = (TextView) convertView.findViewById(R.id.status);
            //TextView score = (TextView) convertView.findViewById(R.id.score);
            //TextView label = (TextView) convertView.findViewById(R.id.labels);
            ProgressBar spinner = (ProgressBar)convertView.findViewById(R.id.progressBar);
            ViewGroup creditLayout = (ViewGroup)convertView.findViewById(R.id.credit_layout);

            holder.thumb = thumbNail;
            holder.title = title;
            holder.desc = desc;
            holder.rating = rating;
            //holder.status = status;
            //holder.score = score;
            //holder.label = label;
            holder.spinner = spinner;
            holder.creditLayout = creditLayout;
            holder.dirty = true;

            convertView.setTag(holder);

            //if(position > lastPosition) {
                Animation animation = AnimationUtils.loadAnimation(context,
                        (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
                convertView.startAnimation(animation);
            //}
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        // getting movie data for the row
        Campaign campaign = campaignItems.get(position);
        if(campaign != null) {
            // thumbnail image
            //holder.thumb.setDefaultImageResId();
            //holder.thumb.setErrorImageResId();

            // Feed image with aspect ratio adjustment
            if (campaign.getThumbnailUrl() != null) {
                final ImageView imageView = holder.thumb;
                final ProgressBar spinner = holder.spinner;

                if (holder.dirty) {
                    spinner.setVisibility(View.GONE);
                    imageView.setVisibility(View.VISIBLE); //must be invisible otherwise the layout width/height is 0 prior to loading in the image
                } else {
                    //likely using cache bitmap, no need to spinner
                    spinner.setVisibility(View.GONE);
                    imageView.setVisibility(View.VISIBLE);
                }
                holder.thumb.setResponseObserver(new FeedImageView.ResponseObserver() {
                    @Override
                    public void onError() {
                        //error image
                        spinner.setVisibility(View.GONE);
                        imageView.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onSuccess() {
                        imageView.setVisibility(View.VISIBLE);
                        spinner.setVisibility(View.GONE);
                    }
                });
                holder.thumb.setImageUrl(campaign.getThumbnailUrl(), imageLoader);
                holder.thumb.requestLayout();
            } else {
                holder.thumb.setVisibility(View.VISIBLE);
                holder.spinner.setVisibility(View.GONE);
            }

            if(campaign.getScore() != null && !campaign.getScore().equals("0")
                    && !campaign.getScore().equals("")) {
                // description
                holder.desc.setText(campaign.getDescription());

                // rating
                double creditValue = AppUtility.calculateCredits(context, campaign);
                holder.rating.setText("" + creditValue);
                //holder.rating.setText(campaign.getRating());

                // title
                //int dollarValue = AppUtility.calculateDollar(context, campaign);
                int dollarValue = AppUtility.calculateDollar(context, creditValue);
                holder.title.setText("$" + dollarValue);
                //holder.title.setText(campaign.getTitle());

                holder.creditLayout.setVisibility(View.VISIBLE);
                holder.desc.setVisibility(View.VISIBLE);
                holder.rating.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.VISIBLE);
            }else{
                // description
                holder.desc.setText(campaign.getDescription());
                
                //hide the details - assume this is offer that is not probably set and perhaps coming soon
                holder.creditLayout.setVisibility(View.VISIBLE);
                holder.desc.setVisibility(View.VISIBLE);
                holder.rating.setVisibility(View.GONE);
                holder.title.setVisibility(View.GONE);
            }

            /*
            // rating
            holder.rating.setText("Ratings: " + ((campaign.getRating() == null) ? "000" : String.valueOf(campaign.getRating())));

            // scores
            holder.score.setText("Score: "+String.valueOf(campaign.getScore()));
            holder.score.setText("Credits: " + AppUtility.calculateUnicoin(context, campaign));

            // Checking for null feed status
            if (campaign.getStatus() != null) {
                holder.status.setText("Status: " + String.valueOf(campaign.getStatus()));
                holder.status.setVisibility(View.VISIBLE);
            } else {
                // status is null, remove from the view
                holder.status.setVisibility(View.GONE);
            }
            */

            if (!isClickable) {
                convertView.setEnabled(false);
                convertView.setClickable(false);
            }
        }else{
            //should never happen, but just in case
            convertView.setVisibility(View.GONE);
            removeData(position);
        }
        return convertView;
    }

    static class ViewHolder {
        //NetworkImageView thumb;
        FeedImageView thumb;
        TextView title;
        TextView desc;
        TextView rating;
        //TextView score;
        //TextView status;
        //TextView label;
        ProgressBar spinner;
        ViewGroup creditLayout;
        boolean dirty;
    }
}
