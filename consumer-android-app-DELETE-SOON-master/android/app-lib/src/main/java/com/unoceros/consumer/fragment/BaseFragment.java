package com.unoceros.consumer.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;

/**
 * Created by Amit
 */
public class BaseFragment extends Fragment {
    protected static final String TAG = "BlankFragment";

    protected OnFragmentInteractionListener mListener;

    public static final String IMAGE_RESOURCE_ID = "iconResourceID";
    public static final String ITEM_NAME = "itemName";

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        // Notify the system to allow an options menu for this fragment.
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void setUserVisibleHint(final boolean visible) {
        super.setUserVisibleHint(visible);

        if (visible) {
            if(mListener != null){
                mListener.onFragmentVisible(true);
            }
        }
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);

        if (visible) {
            if(mListener != null){
                mListener.onFragmentVisible(true);
            }
        }
    }

    public boolean getIsVisible(){
        if (getParentFragment() != null && getParentFragment() instanceof BaseFragment)
        {
            return isVisible() && ((BaseFragment) getParentFragment()).getIsVisible();
        }
        else
        {
            return isVisible();
        }
    }
}
