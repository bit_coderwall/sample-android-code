package com.unoceros.consumer.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.unoceros.consumer.OfferDetailActivity;
import com.unoceros.consumer.adapter.CampaignListAdapter;
import com.unoceros.consumer.libs.SwipeDismissListViewTouchListener;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.db.ClaimedRewardsDAO;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Const;
import com.unoceros.consumer.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amit
 */
public class UserRewardListFragment extends ListFragment{

    public static final String TAG = "UserRewardListFragment";

    private boolean mIsClickable;
    private boolean mIsDeletable;
    private String mDatastore;
    private List<Campaign> campaignItems;
    private CampaignListAdapter listAdapter;

    private OnFragmentInteractionListener mListener;

    private ClaimedRewardsDAO mPersistentDao;

    // newInstance constructor for creating fragment with arguments
    public static UserRewardListFragment newInstance(String title, String datastore, boolean isListItemClickable) {
        UserRewardListFragment campaign = new UserRewardListFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("datastore", datastore);
        args.putBoolean("clickable", isListItemClickable);
        campaign.setArguments(args);
        return campaign;
    }

    public UserRewardListFragment(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Notify the system to allow an options menu for this fragment.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Create the list fragment's content view by calling the super method
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setBackgroundColor(Color.parseColor(AppUtility.LIST_BG_HEX_COLOR_TRANSPARENT));

        // No data initially
        setEmptyText(getArguments().getString("emptyDefault", "Loading ..."));
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        final Activity activity = getActivity();

        mDatastore = getArguments().getString("datastore", null);
        mIsClickable = getArguments().getBoolean("clickable", true);
        mIsDeletable = getArguments().getBoolean("deletable", false);

        //create persistent datastore
        mPersistentDao = new ClaimedRewardsDAO(activity);

        //campaigns
        campaignItems = new ArrayList<>();

        //create an empty adapter
        listAdapter = new CampaignListAdapter(this.getActivity(), campaignItems, mIsClickable);
        setListAdapter(listAdapter);

        //remove scrollbars
        ListView listView = getListView();
        listView.setVerticalScrollBarEnabled(false);
        listView.setHorizontalScrollBarEnabled(false);

        if(mIsDeletable){
            enableSwipeToDelete(listView, listAdapter);
        }
    }

    public void onResume(){
        super.onResume();

        mPersistentDao.open();

        //retrieve content from persistent storage
        if(mDatastore != null) {
            //now populate the list with content from this datastore
            populatedAdapter(mDatastore);
        }else{
            setEmptyText(getArguments().getString("emptyError", "No Items Found, Check Back Again ..."));
        }

        //update drawer layout for counter
        //invalidateDrawerCounter();

    }

    public void onPause(){
        super.onPause();

        //update drawer layout for counter
        //invalidateDrawerCounter();

        mPersistentDao.close();
    }

    /*
    private void invalidateDrawerCounter(){
        //notify containing parent of item change
        ArrayList<Campaign> campaigns = OfferDetailFragment.retrieveFromDataStore(getActivity(), Const.DATASTORE_WISHLIST_NAME);
        if(campaigns != null) {
            Bundle bundle = new Bundle();
            bundle.putInt("position", getArguments().getInt("cart_position"));
            bundle.putInt("counter", campaigns.size());
            mListener.onFragmentInteraction(bundle);
        }
    }*/

    private void enableSwipeToDelete(ListView listView, final CampaignListAdapter adapter){
        // Create a ListView-specific touch listener. ListViews are given special treatment because
        // by default they handle touches for their list items... i.e. they're in charge of drawing
        // the pressed state (the list selector), handling list item clicks, etc.
        SwipeDismissListViewTouchListener touchListener =
                new SwipeDismissListViewTouchListener(
                        listView,
                        new SwipeDismissListViewTouchListener.DismissCallbacks() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }

                            @Override
                            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    Campaign deletablaCampaign = (Campaign)adapter.getItem(position);
                                    adapter.removeData(position);
                                    ArrayList<Campaign> campaigns = null; //OfferDetailFragment.removeFromDataStore(getActivity(), deletablaCampaign, mDatastore);
                                    if(campaigns != null){
                                        Bundle bundle = new Bundle();
                                        bundle.putInt("counter", campaigns.size());
                                        bundle.putInt("position", getArguments().getInt("position"));
                                        mListener.onFragmentInteraction(bundle);
                                    }
                                }

                                setEmptyText(getArguments().getString("emptyMessage", "No Items Found ..."));
                                adapter.notifyDataSetChanged();
                            }
                        });
        listView.setOnTouchListener(touchListener);
        // Setting this scroll listener is required to ensure that during ListView scrolling,
        // we don't look for swipes.
        listView.setOnScrollListener(touchListener.makeScrollListener());
    }

    private void populatedAdapter(String datastore){
        //clear the data store
        listAdapter.clearData();
        // notify data changes to list adapter
        listAdapter.notifyDataSetChanged();

        List<Campaign> campaigns = mPersistentDao.getAll(); //OfferDetailFragment.retrieveFromDataStore(getActivity(), datastore);
        if(campaigns == null)
            return; //sanity check

        for(Campaign campaign : campaigns){
            listAdapter.addData(campaign);
        }

        if(campaigns.size() <= 0)
            setEmptyText(getArguments().getString("emptyMessage", "No Items Found ..."));

        // notify data changes to list adapter
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Campaign campaign = (Campaign)getListView().getItemAtPosition(position);

        Intent details = new Intent(getActivity(), OfferDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("campaign", campaign);
        bundle.putInt("position", position);
        bundle.putLong("id", id);
        bundle.putBoolean("isFromHistory", true);
        details.putExtras(bundle);
        getActivity().startActivity(details);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
