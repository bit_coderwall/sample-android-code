package com.unoceros.consumer;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import android.widget.ZoomButtonsController;

import com.flurry.android.FlurryAgent;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.util.AppUtility;

@SuppressLint("SetJavaScriptEnabled")
public class ModalWebViewActivity extends ActionBarActivity{

     @Nullable
    @Override
    public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
        return super.onWindowStartingActionMode(callback);
    }

    public static final String EXTRA_TITLE = "webviewTitle";
	private static final String TAG = "ModalWebViewActivity";

	private ModalWebViewActivity mContext;
	private ViewGroup mHolder;
	private WebView webView;

	private Uri mLink;
	private String mTitle;

	private ViewGroup mLoadingIcon;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Let's display the progress in the activity title bar, like the
		// browser app does.
		supportRequestWindowFeature(Window.FEATURE_PROGRESS);

		setContentView(R.layout.modal_webview);
		mContext = this;

		final ActionBar actionBar = getSupportActionBar();

		//get the URL via intent
		Intent intent = getIntent();
		Bundle bundle = savedInstanceState;
		if (bundle == null) {
			bundle = intent.getExtras();
		}

		//try again
		if(mLink == null)
			mLink = bundle.getParcelable("uri");

		//get the title if provided
		mTitle = bundle.getString(EXTRA_TITLE);
        boolean resetZoom = bundle.getBoolean("resetZoom", true);
		
		//add title
		if(actionBar != null){
			try{
				if(mTitle != null){
                    actionBar.setTitle(mTitle);
                    actionBar.setDisplayHomeAsUpEnabled(true);
				}else{
					//hide this modal window
                    actionBar.hide();
				}
			}catch(Exception e){
				Log.e(TAG, "ERROR setting modal webview title");
			}
		}

		//set layout
		mHolder = (ViewGroup)this.findViewById(R.id.webview_holder); 
		webView = (WebView) this.findViewById(R.id.webview);
		mLoadingIcon = (ViewGroup)this.findViewById(R.id.loading_icon);
		mLoadingIcon.setVisibility(View.VISIBLE);
		
		//add listeners
		webView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				// Activities and WebViews measure progress with different scales.
				// The progress meter will automatically disappear when we reach 100%
				//mContext.setProgress(progress * 1000);
				try{
					//Reported Bug: CalledFromWrongThreadException: Only the original
					//thread that created a view hierarchy can touch its views
					setProgress(progress * 1000);
					
					if(progress >= 100){
						//Toast.makeText(mContext, "loading completed!", Toast.LENGTH_SHORT).show();
						mLoadingIcon.setVisibility(View.GONE);
					}
				}catch(Exception e){}

				Log.d(TAG, "Progress: "+progress);
			}
		});
		webView.setWebViewClient(new WebViewClient() {
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				Toast.makeText(mContext, "Oops! " + description, Toast.LENGTH_SHORT).show();
			}
		});

		try{
			//enable js prior to loading the page
			webView.getSettings().setJavaScriptEnabled(true);
			
			//for zoom purposes
			if(resetZoom){
				webView.getSettings().setLoadWithOverviewMode(true);
				webView.getSettings().setUseWideViewPort(true);
				webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
				webView.setScrollbarFadingEnabled(true);
				webView.setInitialScale(100);
			}
			
			//for pinch to zoom support
			webView.getSettings().setSupportZoom(true);
			webView.getSettings().setBuiltInZoomControls(true);
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
				// Use the API 11+ calls to disable the controls
				// Use a separate class to obtain 1.6 compatibility
				new Runnable() {
					public void run() {
						webView.getSettings().setDisplayZoomControls(false);
					}
				}.run();
			} else {
				final ZoomButtonsController zoom_controll =
						(ZoomButtonsController) webView.getClass().getMethod("getZoomButtonsController").invoke(webView, (Object)null);
				zoom_controll.getContainer().setVisibility(View.GONE);
			}
		}catch(Exception e){
			//do nothing, continue on
		}finally{
			//now load the url
			new loadURLTask().execute();
		}
	}

	/**
	 * To avoid NetworkingOnMainThreadException
	 * @author eakporob
	 *
	 */
	private class loadURLTask extends AsyncTask<Void,Void,Void>{
		protected Void doInBackground(Void... params) {
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			//now load the page
			try{
				if(mLink != null)
					webView.loadUrl(mLink.toString());
			}catch(Exception e){
				Log.d(TAG, "Error loading Url: "+mLink, e);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//Inflate your menu.
		getMenuInflater().inflate(R.menu.modal_webview_action_menu, menu);

		// Set done menu
		menu.findItem(R.id.menu_item_done);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_item_done) {
			//close this view
			finish();
			return true;
		}else if(item.getItemId() == android.R.id.home){
			// app icon in action bar clicked; go to previous activity or home
            finish();
            return true;
		} else {

			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable("uri", mLink);
		outState.putString(EXTRA_TITLE, mTitle);
	}

	@Override
	public void onBackPressed()
	{
		if(webView.canGoBack())
			webView.goBack();
		else
			super.onBackPressed();
	}

	@Override
	public void onStart(){
		super.onStart();
		//track view
		TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
	}

	@Override
	public void onStop(){
		super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
        FlurryAgent.onEndSession(this);
	}

    private void checkNetworkConnection() {
        if (!AppUtility.haveNetworkConnection(this)) {
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //check network
        checkNetworkConnection();
	}

	@Override
	public void onDestroy(){
		super.onDestroy();
		mHolder.removeView(webView);
		if(webView != null){
			webView.removeAllViews();
			webView.destroy();
			webView = null;
		}
	}
}
