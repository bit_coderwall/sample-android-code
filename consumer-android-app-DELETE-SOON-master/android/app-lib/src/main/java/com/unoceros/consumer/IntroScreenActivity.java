package com.unoceros.consumer;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

import com.unoceros.consumer.adapter.WalkThruPagerAdapter;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.libs.DepthPageTransformer;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * Created by Amit
 */
public class IntroScreenActivity extends ActionBarActivity {
    public static final int INTRO_REQUEST = 911;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager_intro_screen);

        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
        int count = 3;
        Bundle args = new Bundle();
        args.putBoolean("showRegButton", true);
        WalkThruPagerAdapter adapter = new WalkThruPagerAdapter(getSupportFragmentManager(), count, args);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(adapter);

        //Bind the title indicator to the adapter
        CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.indicator);
        indicator.setViewPager(mViewPager);
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private int previousPosition;
            @Override
            public void onPageSelected(int position) {
                //ColorDrawable[] colors = {new ColorDrawable(backgroundColors[previousPosition]), new ColorDrawable(backgroundColors[position])};
                //TransitionDrawable trans = new TransitionDrawable(colors);
                //viewPager.setBackgroundDrawable(trans);
                //trans.startTransition(200);
                previousPosition = position;
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {}

            @Override
            public void onPageScrollStateChanged(int arg0) {}
        });

        //set transformer
        mViewPager.setPageTransformer(true, new DepthPageTransformer());
    }


    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }

    public void onResume(){
        super.onResume();

        //tracking resume
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "IntroScreen.onResume", AppState.instance().getStatusJson());
    }

    public void onPause(){
        super.onPause();

        //tracking pause
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "IntroScreen.onPause", AppState.instance().getStatusJson());
    }

    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
        }
    }

    private void crossfade(final View startView, final View endView) {
        // Retrieve and cache the system's default "short" animation time.
        int mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        // Set the end view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        endView.setAlpha(0f);
        endView.setVisibility(View.VISIBLE);

        // Animate the end view to 100% opacity, and clear any animation
        // listener set on the view.
        endView.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);

        // Animate the startView view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        startView.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        startView.setVisibility(View.GONE);
                    }
                });
    }
}
