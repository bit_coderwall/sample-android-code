package com.unoceros.consumer.fragment;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.unoceros.consumer.NoNetworkActivity;
import com.unoceros.consumer.OfferDetailActivity;
import com.unoceros.consumer.R;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.db.ClaimedRewardsDAO;
import com.unoceros.consumer.services.ApplicationServices;
import com.unoceros.consumer.services.CreditServices;
import com.unoceros.consumer.social.UserAdProfileActivity;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.views.FeedImageView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OfferDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class OfferDetailFragment extends BaseFragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "position";
    private static final String ARG_PARAM2 = "id";

    private WebView _webViewPreview = null;
    private WebView _webViewTracking = null; //click impression tracking
    private WebView _webViewPixel = null; //conversion pixel tracking

    private View _sendRedemptionCode = null;
    private ProgressBar _redeemProgress = null;
    private Button _redeemCodeBtn = null;  //button for requesting code - deprecated??
    private Button _claimRewardButton = null; //button for claiming reward
    private TextView _redeemMessage = null;

    private String _affiliateId = null;
    private ClaimedRewardsDAO mPersistentDao;

    private enum ButtonState {
        DEFAULT, CLAIMED, CLAIMING, NOT_ENOUGH_CREDITS, ERROR
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param position Parameter 1.
     * @param id       Parameter 2.
     * @return A new instance of fragment OfferDetailFragment.
     */
    public static OfferDetailFragment newInstance(int position, long id) {
        OfferDetailFragment fragment = new OfferDetailFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, position);
        args.putLong(ARG_PARAM2, id);
        fragment.setArguments(args);
        return fragment;
    }

    public OfferDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_offer_detail, container, false);
        _webViewPreview = (WebView) rootView.findViewById(R.id.offer_detail_webview_preview_link);
        _webViewTracking = (WebView) rootView.findViewById(R.id.offer_detail_webview_tracking_link);
        _webViewPixel = (WebView) rootView.findViewById(R.id.offer_detail_webview_tracking_pixel);
        _sendRedemptionCode = rootView.findViewById(R.id.offer_detail_redeem_note);
        _sendRedemptionCode.setVisibility(View.GONE);

        _redeemProgress = (ProgressBar) rootView.findViewById(R.id.offer_detail_redeem_progressBar);
        _redeemCodeBtn = (Button) rootView.findViewById(R.id.offer_detail_redeem_code_button);
        _redeemProgress.setVisibility(View.VISIBLE);
        _redeemCodeBtn.setVisibility(View.GONE);
        _redeemMessage = (TextView) rootView.findViewById(R.id.offer_detail_redeem_info);

        return rootView;
    }

    @SuppressLint("JavascriptInterface")
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Activity activity = getActivity();

        mPersistentDao = new ClaimedRewardsDAO(activity);

        SharedPreferences mPref = getActivity().getSharedPreferences(AppUtility.SYNC_PREF, Context.MODE_PRIVATE);
        _affiliateId = mPref.getString("affiliateId", null);

        final Campaign campaign = getArguments().getParcelable("offer");
        final Boolean isFromHistory = getArguments().getBoolean("isFromHistory");
        if (campaign == null)
            return; //should never happen

        //reset title bar
        ActionBar mActionBar = activity.getActionBar();
        if (mActionBar != null)
            mActionBar.setTitle("Offer Details");

        //get a handle to the credit service/bank account
        CreditServices creditServices = ApplicationServices.getInstance(activity.getApplication()).getCreditService();

        //do the unicoin math
        final double userAvailableCredits = creditServices.getTotalBalance(); // 2500 + 1750;
        //final double offerTotalCredits = AppUtility.calculateUnicoin(activity, campaign);
        final double offerTotalCredits = AppUtility.calculateCredits(activity, campaign);

        if (!TextUtils.isEmpty(_affiliateId)) {
            //profile is affiliated, it can be paid by product
            _redeemCodeBtn.setText(getString(R.string.offer_redeeming_code_button_send));
        } else {
            //profile is not yet affiliated, cannot be paid by product
            _redeemCodeBtn.setText(getString(R.string.offer_redeeming_code_button_profile));
        }

        //more credits needed alert
        final TextView moreCreditsNeededText = (TextView) activity.findViewById(R.id.offer_detail_claiming_requirement);
        double neededCredits = Math.round((offerTotalCredits - userAvailableCredits) * 100.0) / 100.0; //credit
        int rate = getResources().getInteger(R.integer.credit_rate_cancompute);
        double neededComputeTime = Math.round(neededCredits / rate * 100.0) / 100.0; //time
        moreCreditsNeededText.setText(
                getString(R.string.alert_not_enough_credits_to_redeem_reward, neededCredits, neededComputeTime));
        moreCreditsNeededText.setVisibility(View.GONE);

        //claim offer button
        _claimRewardButton = (Button) activity.findViewById(R.id.offer_detail_getit_btn);
        if (!isFromHistory) {
            //from market place
            setClaimButtonState(ButtonState.DEFAULT);

            boolean canMultipleBeClaimed = true;
            //check if can be claimed multiple times
            if (!canMultipleBeClaimed) {
                //check offer if already redeemed in history
                List<Campaign> gotOffers = mPersistentDao.getAll(); //retrieveFromDataStore(activity, Const.DATASTORE_HISTORY_NAME);
                if (gotOffers.contains(campaign)) {
                    //offer already claimed, set state
                    setClaimButtonState(ButtonState.CLAIMED);

                    //show redemption send button
                    _sendRedemptionCode.setVisibility(View.VISIBLE);
                    _redeemProgress.setVisibility(View.GONE);
                   /*
                    _redeemCodeBtn.setVisibility(View.VISIBLE);
                    _redeemCodeBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!TextUtils.isEmpty(_affiliateId)) {
                                sendCodeViaEmail(campaign, null);
                            } else {
                                Intent i = new Intent(activity, UserAdProfileActivity.class);
                                startActivityForResult(i, UserAdProfileActivity.REQUEST_CODE);
                            }
                        }
                    });*/
                }

                //check if user needs more credits to obtain
                if (offerTotalCredits > userAvailableCredits) {
                    //can not redeem offer, needs more compute time
                    setClaimButtonState(ButtonState.NOT_ENOUGH_CREDITS);
                    moreCreditsNeededText.setVisibility(View.VISIBLE);
                }
            } else {
                //can be claimed multiple times
                //check if user needs more credits to obtain
                if (offerTotalCredits > userAvailableCredits) {
                    //can not redeem offer, needs more compute time
                    setClaimButtonState(ButtonState.NOT_ENOUGH_CREDITS);
                    moreCreditsNeededText.setVisibility(View.VISIBLE);
                }
            }
        } else {
            //from history

            //claimed...button state
            //set button to claimed state
            setClaimButtonState(ButtonState.CLAIMED);

            //check offer if already placed in history
            List<Campaign> savedOffers = mPersistentDao.getAll();
            if (savedOffers.contains(campaign)) {
                //set button to claimed state
                setClaimButtonState(ButtonState.CLAIMED);

                //show redemption send button
                _sendRedemptionCode.setVisibility(View.VISIBLE);
                _redeemProgress.setVisibility(View.GONE);
                /*
                _redeemCodeBtn.setVisibility(View.VISIBLE);
                _redeemCodeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!TextUtils.isEmpty(_affiliateId)) {
                            sendCodeViaEmail(campaign, null);
                        } else {
                            Intent i = new Intent(activity, UserAdProfileActivity.class);
                            startActivityForResult(i, UserAdProfileActivity.REQUEST_CODE);
                        }
                    }
                });*/
            }
        }

        //populate top detail view
        ((TextView) activity.findViewById(R.id.offer_detail_description)).setText(campaign.getDescription());
        ((TextView) activity.findViewById(R.id.offer_detail_credits)).setText("Credits: " + offerTotalCredits);

        /* Was used for the original detail layout, unused now
        ((TextView) activity.findViewById(R.id.offer_detail_ratings))
            .setText("Ratings: " + ((campaign.getRating() == null) ? "0.0" : String.valueOf(campaign.getRating())));
        ((TextView) activity.findViewById(R.id.offer_detail_expires)).setText("Exp: " + campaign.getExpireDateStr());
        */
        final ProgressBar spinner = (ProgressBar) activity.findViewById(R.id.offer_detail_imageview_progressBar);
        final FeedImageView thumbImage = (FeedImageView) activity.findViewById(R.id.offer_detail_thumbnail);
        if (campaign.getThumbnailUrl() != null) {
            spinner.setVisibility(View.VISIBLE);
            thumbImage.setVisibility(View.GONE);
            thumbImage.setResponseObserver(new FeedImageView.ResponseObserver() {
                @Override
                public void onError() {
                    spinner.setVisibility(View.GONE);
                    thumbImage.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onSuccess() {
                    thumbImage.setVisibility(View.VISIBLE);
                    spinner.setVisibility(View.GONE);
                }
            });
            thumbImage.setImageUrl(campaign.getThumbnailUrl(), AppController.getInstance().getImageLoader());
        } else {
            thumbImage.setVisibility(View.VISIBLE);
            spinner.setVisibility(View.GONE);
        }

        _claimRewardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!AppUtility.haveNetworkConnection(getActivity())) {
                    //check network connectivity
                    AppUtility.checkNetworkConnection(getActivity(), NoNetworkActivity.REQUEST_CODE);
                } else {
                    alertRedeemDialog(activity, campaign, userAvailableCredits);

                    Bundle b = new Bundle();
                    b.putParcelable("offer", campaign);
                    b.putBoolean("isOfferRedeemed", true);
                    onButtonPressed(b);
                }
            }
        });

        /**postback click impression via hidden web view*/
        postbackImpression(activity, campaign, _webViewTracking, _affiliateId);

        //show details of offer
        String offer_url = campaign.getPrevUrl();
        if (offer_url != null) {
            //prepare to load advertisers campaign page
            final ProgressBar progressBar = (ProgressBar) activity.findViewById(R.id.offer_detail_webview_progressBar);
            progressBar.setMax(100);
            progressBar.setProgress(0);

            /**setup preview web view*/
            _webViewPreview.getSettings().setJavaScriptEnabled(true);
            _webViewPreview.addJavascriptInterface(this, "android");
            _webViewPreview.setWebViewClient(new WebViewClient() {
                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    //Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
                }
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Log.d(TAG, "shouldOverrideUrlLoading url: "+url);
                    return true;
                }
                @Override
                public boolean shouldOverrideKeyEvent (WebView view, KeyEvent event){
                    Log.d(TAG, "shouldOverrideKeyEvent event: "+event);
                    return true;
                }
            });

            _webViewPreview.setWebChromeClient(new WebChromeClient() {
                @Override
                public void onProgressChanged(WebView view, int newProgress) {
                    progressBar.setProgress(newProgress);

                    if (newProgress > 50) {
                        thumbImage.setVisibility(View.VISIBLE);
                        spinner.setVisibility(View.GONE);
                    }
                    super.onProgressChanged(view, newProgress);
                }
            });

            final GestureDetector gestureDetector = new GestureDetector(activity, new AdGestureDetector());
            _webViewPreview.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return gestureDetector.onTouchEvent(event);
                }
            });
            _webViewPreview.loadUrl(offer_url);

        } else {
            //show recommendations???
            _webViewPreview.setVisibility(View.GONE);
        }
    }

    private class AdGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDoubleTapEvent(MotionEvent e) {
            Log.d(TAG, "DoubleTapEvent");
            //DO CLICK
            ((OfferDetailActivity)getActivity()).attributeInstall(null);
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e){
            Log.d(TAG, "SingleTapConfirmed");
            //DO CLICK
            ((OfferDetailActivity)getActivity()).attributeInstall(null);
            return true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == UserAdProfileActivity.REQUEST_CODE) {
            //make sure affiliate id is persisted in preference
            //reset redemption btn
            SharedPreferences mPref = getActivity().getSharedPreferences(AppUtility.SYNC_PREF, Context.MODE_PRIVATE);
            _affiliateId = mPref.getString("affiliateId", "2"); //2 is a dummy unoceros affiliate id
            _redeemCodeBtn.setText(getString(R.string.offer_redeeming_code_button_send));
        }
    }

    public void onResume() {
        super.onResume();

        //register local service
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mRewardRedemptionReceiver, new IntentFilter("rewards-code-redemption"));

        //LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
        //        mRewardRedemptionReceiver, new IntentFilter("rewards-inserted"));

        checkNetworkConnection();
        mPersistentDao.open();
    }

    public void onPause(){
        super.onPause();

        //unregister local service
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mRewardRedemptionReceiver);

        mPersistentDao.close();
    }

    private void setClaimButtonState(ButtonState state){
        switch (state){
            case DEFAULT:
                //default state, can claim an offer
                _claimRewardButton.setEnabled(true);
                _claimRewardButton.setClickable(true);
                _claimRewardButton.setText(R.string.detail_offer_getit_button);
                //_claimRewardButton.setBackgroundResource(R.drawable.button_download_offer_detail);
                _claimRewardButton.setBackgroundResource(R.drawable.button_gradient2);
                _claimRewardButton.setVisibility(View.VISIBLE);
                break;
            case CLAIMED:
                //offer has already been claimed
                _claimRewardButton.setEnabled(false);
                _claimRewardButton.setClickable(false);
                _claimRewardButton.setText(R.string.detail_offer_Redeemed_button);
                //_claimRewardButton.setBackgroundResource(R.drawable.ic_action_accept_white);
                _claimRewardButton.setBackgroundResource(R.drawable.button_gradient3);
                _claimRewardButton.setVisibility(View.VISIBLE);
                break;
            case CLAIMING:
                //process of claiming an offer...
                _claimRewardButton.setEnabled(false);
                _claimRewardButton.setClickable(false);
                _claimRewardButton.setText(R.string.detail_offer_Redeeming_button);
                //_claimRewardButton.setBackgroundResource(R.drawable.ic_action_accept_white);
                _claimRewardButton.setBackgroundResource(R.drawable.button_gradient2);
                _claimRewardButton.setVisibility(View.VISIBLE);
                break;
            case NOT_ENOUGH_CREDITS:
                //can't claim an offer, insufficient funds
                _claimRewardButton.setEnabled(false);
                _claimRewardButton.setClickable(false);
                _claimRewardButton.setText("Yikes");
                //getInButton.setBackgroundResource(R.drawable.ic_action_accept_white);
                _claimRewardButton.setBackgroundResource(R.drawable.button_gradient3);
                _claimRewardButton.setVisibility(View.GONE);
                break;
            case ERROR:
                //oops, error occurred why attempting to claim an offer
                _claimRewardButton.setEnabled(false);
                _claimRewardButton.setClickable(false);
                _claimRewardButton.setText("Oops");
                _claimRewardButton.setBackgroundResource(R.drawable.button_gradient3);
                _claimRewardButton.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void checkNetworkConnection() {
        if (!AppUtility.haveNetworkConnection(getActivity())) {
            //check network connectivity
            AppUtility.checkNetworkConnection(getActivity(), NoNetworkActivity.REQUEST_CODE);
        } else {
            if (_redeemMessage != null && _redeemCodeBtn != null) {
                if (!TextUtils.isEmpty(_affiliateId)) {
                    //profile is affiliated, it can be paid by product
                    _redeemCodeBtn.setText(getString(R.string.offer_redeeming_code_button_send));
                } else {
                    //profile is not yet affiliated, cannot be paid by product
                    _redeemCodeBtn.setText(getString(R.string.offer_redeeming_code_button_profile));
                }
            }
        }
    }

    public static void postbackImpression(final Activity context, final Campaign campaign,
                                    WebView _webViewTracking, String _affiliateId) {
        String click_url = context.getString(R.string.hasoffer_track_impression_link, campaign.getId(), _affiliateId);
        _webViewTracking.setVisibility(View.GONE);
        _webViewTracking.setWebViewClient(new WebViewClient() {
        });
        _webViewTracking.setWebChromeClient(new WebChromeClient() {
        });
        _webViewTracking.loadUrl(click_url);

        //analytics tracking of clicking event for offers
        TrackingManager.getInstance(AppController.getInstance()).trackViewCampaign(context, TrackingManager.OFFER_CLICKED, campaign);
    }

    private void withdrawByTransactionId(final Activity context, final Campaign campaign, 
                 String transactionId, boolean successful){
        try {
            //double amount = AppUtility.calculateUnicoin(getActivity(), campaign);
            double amount = AppUtility.calculateCredits(getActivity(), campaign);
            
            //local withdraw
            if(successful) {
                CreditServices creditServices = ApplicationServices.getInstance(getActivity().getApplication()).getCreditService();
                creditServices.withdraw(amount);

                //remote withdraw
                Intent withdrawIntent = new Intent(AppUtility.SYNC_ACTION_CREDIT_WITHDRAW_MADE);
                // You can also include some extra data.
                withdrawIntent.putExtra("amount", amount);
                withdrawIntent.putExtra("transactionId", transactionId);
                withdrawIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                context.sendBroadcast(withdrawIntent);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }    
    }
    
    public static void postbackConversion(final Activity context, final Campaign campaign, final String _affiliateId,
                                    final WebView _webViewPixel, final TextView redeemMessage, 
                                    final Button redeemCodeBtn, final ProgressBar redeemProgress) {
        
        String conversion_url = context.getString(R.string.hasoffer_track_conversion_link, campaign.getId(), _affiliateId);
        
        if(redeemMessage != null) {
            redeemMessage.setText(context.getString(R.string.offer_redeeming_message_progress));
            if (!TextUtils.isEmpty(_affiliateId) && redeemCodeBtn != null) {
                //profile is affiliated, it can be paid by product
                redeemCodeBtn.setText(context.getString(R.string.offer_redeeming_code_button_send));
            } else if(redeemCodeBtn != null){
                //profile is not yet affiliated, cannot be paid by product
                redeemCodeBtn.setText(context.getString(R.string.offer_redeeming_code_button_profile));
            }
        }
        
        //track conversion link
        _webViewPixel.setVisibility(View.GONE);
        _webViewPixel.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(context, description, Toast.LENGTH_SHORT).show();
            }

            public void onPageFinished(WebView view, final String url) {
                //Toast.makeText(context, "onPageFinish: "+url, Toast.LENGTH_SHORT).show();
                //hide progress bar and show button
                if(redeemMessage != null && redeemProgress != null) {
                    redeemProgress.setVisibility(View.GONE);
                    redeemMessage.setText(context.getString(R.string.offer_redeeming_message_send));
                    /*
                    _redeemCodeBtn.setVisibility(View.VISIBLE);
                    _redeemCodeBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!TextUtils.isEmpty(_affiliateId)) {
                                sendCodeViaEmail(campaign, url);
                            } else {
                                Intent i = new Intent(context, UserAdProfileActivity.class);
                                startActivityForResult(i, UserAdProfileActivity.REQUEST_CODE);
                            }
                        }
                    });
                    */
                }
            }
        });
        _webViewPixel.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }
        });
        _webViewPixel.loadUrl(conversion_url);

        //analytics tracking of purchasing event for offers
        TrackingManager.getInstance(AppController.getInstance()).trackViewCampaign(context, TrackingManager.OFFER_PURCHASED, campaign);
    }


    // TODO: Rename method, update argument and hook method into UI event
    private void onButtonPressed(Bundle b) {
        if (mListener != null) {
            //mListener.onFragmentInteraction(b);
        }
    }

    private void sendCodeViaEmail(Campaign campaign, String trackingUrl) {
        String trackingLink = trackingUrl != null ? "Tracking Url: " + trackingUrl.replace("&", "%26") + "<br>" : "";
        Log.d(TAG, trackingLink);

        //Email Redemption Code
        String userEmail = AppController.getInstance().getUserInfo().getUserEmail();
        Spanned body = Html.fromHtml("---------------------------------<br>" +
                "<h2>Claimed Offer from Unoceros:</h2><br>" +
                campaign.getTitle() + ": Click the link below to redeem you offer:<br><br>" +
                campaign.getPrevUrl() + "<br><br>" +
                "---------------------------------<br>");

        String subject = "Unoceros: Redeem Your Reward";
        StringBuffer buffer = new StringBuffer();
        buffer.append("mailto:");
        buffer.append(userEmail);
        buffer.append("?subject=");
        buffer.append(subject);
        buffer.append("&body=");
        buffer.append(body);
        String uriString = buffer.toString().replace(" ", "%20");
        startActivity(Intent.createChooser(new Intent(Intent.ACTION_SENDTO, Uri.parse(uriString)),
                "Send Via:"));

    }

    private AlertDialog alertRedeemDialog(final Activity activity, final Campaign campaign, double availableCredit){
        //double amount = AppUtility.calculateUnicoin(getActivity(), campaign);
        double amount = AppUtility.calculateCredits(getActivity(), campaign);
        String email = AppController.getInstance().getUserInfo().getUserEmail();

        String alertMessage = activity.getString(R.string.alert_rewards_redemption_dialog_message, amount, email);
        // Create alert dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(alertMessage)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //redeeming...button state
                        setClaimButtonState(ButtonState.CLAIMING);

                        //show progress bar
                        _redeemMessage.setText(getString(R.string.offer_redeeming_message_progress));
                        _sendRedemptionCode.setVisibility(View.VISIBLE);
                        _redeemProgress.setVisibility(View.VISIBLE);
                        _redeemCodeBtn.setVisibility(View.GONE);

                        //try initiating a redemption code request with the back end services
                        initiateRedemptionCodeRequest(activity, campaign);
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //do nothing
                    }
                });

        // Create the AlertDialog object and return it
        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        return dialog;
    }

    private void initiateRedemptionCodeRequest(Activity context, Campaign campaign){
        //send broadcast to remote rewards service
        Intent redeemIntent = new Intent(AppUtility.ACTION_GET_REWARD_REDEMPTION_CODE);
        Bundle bundle = new Bundle();
        bundle.putString("transactionId", UUID.randomUUID().toString());
        bundle.putParcelable("campaign", campaign);
        redeemIntent.putExtras(bundle);       
        redeemIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        context.sendBroadcast(redeemIntent);
    }

    private void insertByDateToRemote(Activity context, UUID uuid, String date, 
                                      Campaign campaign, String transactionId){
        //send broadcast to remote rewards service
        Intent redeemIntent = new Intent(AppUtility.ACTION_INSERT_OFFER_BY_DATE);
        Bundle bundle = new Bundle();
        bundle.putString("uuid", uuid.toString());
        bundle.putString("date", date);
        bundle.putParcelable("campaign", campaign);
        bundle.putString("transactionId", transactionId);
        redeemIntent.putExtras(bundle);
        redeemIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        context.sendBroadcast(redeemIntent);
    }

    private BroadcastReceiver mRewardRedemptionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        boolean successful = intent.getBooleanExtra("success", false);
        String transactionId = intent.getStringExtra("transactionId");
        Campaign campaign = extras.getParcelable("campaign");
        Activity activity = getActivity();
        if(successful){
            //if successful, make a postback
            //persist to the history datastore
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sdf.format(new Date());
            UUID uuid = mPersistentDao.insertByDate(date, campaign, true);
            campaign.setUuid(uuid.toString());

            //persist to the remote history backstore
            insertByDateToRemote(activity, uuid, date, campaign, transactionId);

            //withdraw amount from creditbank by transaction id
            withdrawByTransactionId(activity, campaign, transactionId, true);

            /**postback conversion pixel via hidden web view*/
            postbackConversion(activity, campaign, _affiliateId,
                    _webViewPixel, _redeemMessage, _redeemCodeBtn, _redeemProgress);

            //redeemed
            setClaimButtonState(ButtonState.CLAIMED);
        }else{
            //server error
            String errorMsg = getString(R.string.offer_redeeming_message_error);
            _redeemMessage.setText(errorMsg);
            _redeemProgress.setVisibility(View.GONE);
            Toast.makeText(context, errorMsg, Toast.LENGTH_LONG).show();

            //error
            setClaimButtonState(ButtonState.ERROR);
        }
        }
    };
}