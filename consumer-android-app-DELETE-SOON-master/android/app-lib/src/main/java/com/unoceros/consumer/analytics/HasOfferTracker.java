package com.unoceros.consumer.analytics;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.mobileapptracker.MATEventItem;
import com.mobileapptracker.MobileAppTracker;
import com.unoceros.consumer.R;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.DeviceHardware;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.services.ApplicationServices;
import com.unoceros.consumer.util.AppUtility;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Amit
 */
public class HasOfferTracker implements ITracker {

    //instance
    private static HasOfferTracker instance;

    private UserInfo userInfo;
    private MobileAppTracker mobileAppTracker;

    private HasOfferTracker(final Application context) {

        // Initialize MAT
        String advertiserKey = context.getString(R.string.mat_advertiser_id);
        String conversionKey = context.getString(R.string.mat_conversion_key);
        MobileAppTracker.init(context, advertiserKey, conversionKey);
        mobileAppTracker = MobileAppTracker.getInstance();

        final DeviceHardware hardware = ApplicationServices.getInstance(context).getDeviceHardware();
        // Collect Google Play Advertising ID; REQUIRED for attribution of Android apps distributed via Google Play
        new Thread(new Runnable() {
            @Override public void run() {
                // See sample code at http://developer.android.com/google/play-services/id.html
                try {
                    AdvertisingIdClient.Info adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context);
                    mobileAppTracker.setGoogleAdvertisingId(adInfo.getId(), adInfo.isLimitAdTrackingEnabled());
                    hardware.setGoogleAid(adInfo.getId());
                } catch (Exception e) {
                    // Unrecoverable error connecting to Google Play services (e.g.,
                    // the old version of the service doesn't support getting AdvertisingId).
                    String androidId = Settings.Secure.getString(context.getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    mobileAppTracker.setAndroidId(androidId);
                    hardware.setAndroidId(androidId);
                }
            }
        }).start();

        //android id
        String androidId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        mobileAppTracker.setAndroidId(androidId);
        hardware.setAndroidId(androidId);

        //device id
        String telephonyId = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
        mobileAppTracker.setDeviceId(telephonyId);
        hardware.setTelephonyId(telephonyId);

        //mac address
        // WifiManager objects may be null
        try {
            WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            mobileAppTracker.setMacAddress(wm.getConnectionInfo().getMacAddress());
        } catch (NullPointerException e) {
        }

        if(AppUtility.signedWithDebugKey(context) || AppUtility.isDebuggable(context)){
            //do debug stuff
            mobileAppTracker.setDebugMode(true);
            mobileAppTracker.setAllowDuplicates(true);
        }else{
            //do release stuff
            mobileAppTracker.setDebugMode(false);
            mobileAppTracker.setAllowDuplicates(false);
        }

        //user information
        userInfo = ApplicationServices.getInstance(context).getUserInfo();
        mobileAppTracker.setUserId(userInfo.getUserId());
    }

    public static HasOfferTracker getInstance(Application applicationContext) {
        if (instance == null) {
            instance = new HasOfferTracker(applicationContext);
        }

        return instance;
    }

    public void trackAppStart(Activity activity) {
        // Get source of open for app re-engagement
        mobileAppTracker.setReferralSources(activity);
        // MAT will not function unless the measureSession call is included
        mobileAppTracker.measureSession();
    }

    /**
     * Should be called from the onResume method of your activity
     *
     * @param activity
     */
    public void trackResumeActivity(Activity activity) {
    }

    public void trackStartActivity(Activity activity) {
    }

    public void trackStopActivity(Activity activity) {
    }

    public void trackDestroyActivity(Activity activity){
    }

    public void trackAppState(Activity activity, String states) {
    }

    public void trackPageView(Activity activity, String viewName) {
    }

    public void trackEvent(Context context, String eventName, JSONObject json) {
        //call the overloaded method
        trackEvent(context, eventName);
    }
    public void trackEvent(Activity activity, String eventName, JSONObject json) {
        //call the overloaded method
        trackEvent(activity.getApplicationContext(), eventName);
    }

    public void trackEvent(Activity activity, String eventName) {
        //call the overloaded method
        trackEvent(activity.getApplicationContext(), eventName);
    }

    public void trackEvent(Context context, String eventName) {
        UserInfo userInfo = AppController.getInstance().getUserInfo();
        setUserInfo(userInfo);
        mobileAppTracker.measureAction(eventName);
    }

    public void trackEventWithConnectionSpeed(Activity activity, String eventName) {
        String eventNameStr = eventName;
        if(AppUtility.haveWifiConnection(activity)){
            //wifi connection
            eventNameStr = "AppStartWifiConnection";
        }else if(AppUtility.haveMobileConnection(activity)){
            //carrier connection
            eventNameStr = "AppStartCarrierConnection";
        }else{
            //no connection
            eventNameStr = "AppStartNoConnection";
        }
        UserInfo userInfo = AppController.getInstance().getUserInfo();
        setUserInfo(userInfo);
        mobileAppTracker.measureAction(eventNameStr);
    }

    public void trackViewCampaign(Activity activity, String eventName, Campaign campaign) {
        double credits = AppUtility.calculateCredits(activity, campaign);
        String itemName = campaign.getTitle();
        int quantity = 1;
        double unitPrice = AppUtility.calculateDollar(activity, credits);
        double revenue = 0.0;
        MATEventItem eventItem = new MATEventItem(
                itemName,
                quantity,
                unitPrice,
                revenue,
                campaign.getId(),
                campaign.getDescription(),
                String.valueOf(campaign.getRating()),
                campaign.getStatus(),
                campaign.getScore());
        ArrayList<MATEventItem> items = new ArrayList<MATEventItem>();
        items.add(eventItem);

        UserInfo userInfo = AppController.getInstance().getUserInfo();
        setUserInfo(userInfo);
        String currency = "USD";
        mobileAppTracker.measureAction(eventName, items, revenue, currency, userInfo.getUserId());
    }

    @Override
    public void sendTrackedInformation(UserInfo userInfo) {
        setUserInfo(userInfo);
        mobileAppTracker.measureAction("update_user_info");
    }

    private void setUserInfo(UserInfo userInfo){
        mobileAppTracker.setUserId(userInfo.getUserId());
        mobileAppTracker.setUserName(userInfo.getUserName());
        mobileAppTracker.setUserEmail(userInfo.getUserEmail());
        mobileAppTracker.setAge(userInfo.getAge());

        mobileAppTracker.setGender(userInfo.getGender());
        mobileAppTracker.setFacebookUserId(userInfo.getFacebookUserId());
        mobileAppTracker.setGoogleUserId(userInfo.getGoogleUserId());
        mobileAppTracker.setTwitterUserId(userInfo.getTwitterUserId());

        mobileAppTracker.setLatitude(userInfo.getLatitude());
        mobileAppTracker.setLongitude(userInfo.getLongitude());
    }
    @Override
    public void trackComputeAvailability(UserInfo userInfo){

    }
}
