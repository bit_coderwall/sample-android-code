package com.unoceros.consumer.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.unoceros.consumer.R;
import com.unoceros.consumer.model.DrawerMenuItem;

import java.util.List;

/**
 * Created by Amit
 */
public class DrawerMenuAdapter extends ArrayAdapter<DrawerMenuItem> {

    private Context context;
    private List<DrawerMenuItem> drawerItemList;
    private int layoutResID;

    public DrawerMenuAdapter(Context context, int layoutResourceID, List<DrawerMenuItem> listItems) {
        super(context, layoutResourceID, listItems);
        this.context = context;
        this.drawerItemList = listItems;
        this.layoutResID = layoutResourceID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DrawerItemHolder drawerHolder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder();

            view = inflater.inflate(layoutResID, parent, false);
            drawerHolder.itemName = (TextView) view.findViewById(R.id.drawer_menu_item_name);
            drawerHolder.icon = (ImageView) view.findViewById(R.id.drawer_menu_item_icon);
            drawerHolder.spinner = (Spinner) view.findViewById(R.id.drawer_menu_spinner);
            drawerHolder.title = (TextView) view.findViewById(R.id.drawer_menu_header_text);
            drawerHolder.counter = (TextView) view.findViewById(R.id.drawer_menu_item_counter);
            drawerHolder.headerLayout = (LinearLayout) view.findViewById(R.id.drawer_menu_header_layout);
            drawerHolder.itemLayout = (LinearLayout) view.findViewById(R.id.drawer_menu_item);
            drawerHolder.spinnerLayout = (LinearLayout) view.findViewById(R.id.drawer_menu_spinner_layout);

            view.setTag(drawerHolder);
        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();

        }

        DrawerMenuItem dItem = this.drawerItemList.get(position);
        if (dItem.isSpinner()) {
            /*
            drawerHolder.headerLayout.setVisibility(LinearLayout.INVISIBLE);
            drawerHolder.itemLayout.setVisibility(LinearLayout.INVISIBLE);
            drawerHolder.spinnerLayout.setVisibility(LinearLayout.VISIBLE);
            List<SpinnerItem> userList = new ArrayList<SpinnerItem>();
            userList.add(new SpinnerItem(R.drawable.user1, "Ahamed Ishak","ishakgmail.com"));
            userList.add(new SpinnerItem(R.drawable.user2, "Brain Jekob", "brain.jgmail.com"));
            CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(context,
                    R.layout.custom_spinner_item, userList);

            drawerHolder.spinner.setAdapter(adapter);
            drawerHolder.spinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0,View arg1, int arg2, long arg3) {
                        Toast.makeText(context, "User Changed",Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                    }
                });
            */
        } else if (dItem.getTitle() != null  && dItem.isHeader()) {
            //Header Item, Technically not clickable
            drawerHolder.headerLayout.setVisibility(View.VISIBLE);
            drawerHolder.itemLayout.setVisibility(View.INVISIBLE);
            drawerHolder.spinnerLayout.setVisibility(View.INVISIBLE);
            drawerHolder.title.setText(dItem.getTitle());
        } else {
            //Clickable menu items
            drawerHolder.headerLayout.setVisibility(View.INVISIBLE);
            drawerHolder.spinnerLayout.setVisibility(View.INVISIBLE);
            drawerHolder.itemLayout.setVisibility(View.VISIBLE);
            drawerHolder.icon.setImageDrawable(view.getResources().getDrawable(dItem.getImgResID()));
            drawerHolder.itemName.setText(dItem.getItemName());
            if(dItem.getCounter() > 0) {
                drawerHolder.counter.setVisibility(View.VISIBLE);
                drawerHolder.counter.setText(dItem.getCounter()+"");
            }else{
                drawerHolder.counter.setVisibility(View.INVISIBLE);
                drawerHolder.counter.setText(dItem.getCounter()+"");
            }
        }
        return view;
    }

    private static class DrawerItemHolder {
        TextView itemName, title, counter;
        ImageView icon;
        LinearLayout headerLayout, itemLayout, spinnerLayout;
        Spinner spinner;
    }
}