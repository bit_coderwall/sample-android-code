package com.unoceros.consumer.model;

/**
 * Created by Amit
 */
public class UserInfo {

    private String userId;
    private String userEmail;
    private String userName;
    private String userImageUrl;
    private String facebookUserId;
    private String twitterUserId;
    private String googleUserId;
    private int gender;
    private int age;
    private double latitude;
    private double longitude;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        if(this.userId == null || this.userId.equals(""))
            this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;
    }

    public String getFacebookUserId() {
        return facebookUserId;
    }

    public void setFacebookUserId(String facebookUserId) {
        this.facebookUserId = facebookUserId;
    }

    public String getTwitterUserId() {
        return twitterUserId;
    }

    public void setTwitterUserId(String twitterUserId) {
        this.twitterUserId = twitterUserId;
    }

    public String getGoogleUserId() {
        return googleUserId;
    }

    public void setGoogleUserId(String googleUserId) {
        this.googleUserId = googleUserId;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void syncUp(UserInfo userInfo){
        if(userInfo != null) {
            if(getUserId()==null)setUserId(userInfo.getUserId());
            if(getUserEmail()==null)setUserEmail(userInfo.getUserEmail());
            if(getUserName()==null)setUserName(userInfo.getUserName());
            if(getUserImageUrl()==null)setUserImageUrl(userInfo.getUserImageUrl());
            if(getFacebookUserId()==null)setFacebookUserId(userInfo.getFacebookUserId());
            if(getTwitterUserId()==null)setTwitterUserId(userInfo.getTwitterUserId());
            if(getGoogleUserId()==null)setGoogleUserId(userInfo.getGoogleUserId());

            if(getAge()==0)setAge(userInfo.getAge());
            if(getGender()==0)setGender(userInfo.getGender());
            if(getLatitude()==0)setLatitude(userInfo.getLatitude());
            if(getLongitude()==0)setLongitude(userInfo.getLongitude());
        }
    }
}
