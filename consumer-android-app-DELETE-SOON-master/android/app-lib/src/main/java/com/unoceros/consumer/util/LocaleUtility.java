package com.unoceros.consumer.util;


import com.google.gson.internal.bind.ArrayTypeAdapter;

import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by ajish on 3/1/15.
 */

public class LocaleUtility {

    public static String whereAmI(){
        return Locale.getDefault().getCountry();
    }

    public static int matchOfferToCountry( ArrayList<String> countryIdsFromHasOffers ){

        //make a best possible guess at getting
        // the country index within a list with q country ids
        // quickly from the HasOffers json.
        int ret = 0;

        //TODO: determine a default better than zero

        for( int i = 0; i < countryIdsFromHasOffers.size(); i++) {
            String parsedCountryCode = countryIdsFromHasOffers.get(i).split("-")[0];
            parsedCountryCode = parsedCountryCode.substring(Math.max(parsedCountryCode.length() - 2, 0));
            if (parsedCountryCode.toUpperCase().equals("ZN")) {
                parsedCountryCode = "ZN";
            }
            if (whereAmI().equals(parsedCountryCode)) {
                ret = i;
            }
        }

        return ret;
    }

}
