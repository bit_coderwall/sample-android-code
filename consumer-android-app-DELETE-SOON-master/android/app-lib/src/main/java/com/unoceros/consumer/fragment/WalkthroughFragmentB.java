package com.unoceros.consumer.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.unoceros.consumer.R;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WalkthroughFragmentA#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WalkthroughFragmentB extends Fragment {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment WalkthroughFragmentB.
     */
    public static WalkthroughFragmentB newInstance(Bundle args) {
        WalkthroughFragmentB fragment = new WalkthroughFragmentB();
        fragment.setArguments(args);
        return fragment;
    }

    public WalkthroughFragmentB() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_walkthrough_b, container, false);
        Bundle bundle = getArguments();
        boolean isHowItWorks = true;
        if(bundle != null)
            isHowItWorks = bundle.getBoolean("isHowItWorks");

        TextView text = (TextView) rootView.findViewById(R.id.text);
        //ImageButton leftBtn = (ImageButton) rootView.findViewById(R.id.button_left);
        //ImageButton rightBtn = (ImageButton) rootView.findViewById(R.id.button_right);
        if(isHowItWorks) {
            text.setVisibility(View.VISIBLE);
            //leftBtn.setVisibility(View.INVISIBLE);
            //rightBtn.setVisibility(View.INVISIBLE);
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Activity activity = getActivity();
    }
}
