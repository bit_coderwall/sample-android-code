package com.unoceros.consumer.analytics;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.flurry.android.FlurryAgent;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.DeviceHardware;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.services.ApplicationServices;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.util.SyncUtility;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;


/**
 * Created by amit
 */
public abstract class FlurryTracker implements ITracker {

    //instance
    private static FlurryTracker instance;

    public static String AppStartWifiConnection = "WIFI";
    public static String AppStartCarrierConnection = "MOBILE_CARRIER";
    public static String AppStartNoConnection = "NO_WIFI_CARRIER";
    public final static String UNOCEROS_NETWORK_RUNNING_ON = "UNOCEROS_NETWORK_RUNNING_ON";
    public final static String UNOCEROS_NODE_ID = "UNOCEROS_NODE_ID";

    public String appStartWith;
    public DeviceHardware deviceHardware;


    protected FlurryTracker(Application context) {
        deviceHardware = ApplicationServices.getInstance(context).getDeviceHardware();

        // set Version
        FlurryAgent.setVersionName(deviceHardware.getAppVersion());

        // set Flurry SDK version
        FlurryAgent.getAgentVersion();

        // Allow Flurry to record location information in reports.
        FlurryAgent.setReportLocation(true);

        appStartWith = reportDeviceInfo(context);
    }

    private String reportDeviceInfo(Application context) {
        String eventNameStr = null;
        try {
            if (AppUtility.haveWifiConnection(context)) {
                //wifi connection
                eventNameStr = AppStartWifiConnection;
            } else if (AppUtility.haveMobileConnection(context)) {
                //carrier connection
                eventNameStr = AppStartCarrierConnection;
            } else {
                //no connection
                eventNameStr = AppStartNoConnection;
            }
            Log.d("AppStartWith", eventNameStr);
            createHash(UNOCEROS_NETWORK_RUNNING_ON, null, null, eventNameStr);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return eventNameStr;
    }

    public static FlurryTracker getInstance(Application applicationContext) {
        if (instance == null) {
            instance = new FlurryTracker(applicationContext) {
                @Override
                public void trackAppStart(Activity activity) {
                    createHash(activity.getClass().getSimpleName(), activity, null, null);
                }

                @Override
                public void trackStartActivity(Activity activity) {
                    FlurrySession.startSession(activity);
                    createHash(activity.getClass().getSimpleName(), activity, null, null);
                }

                @Override
                public void trackStopActivity(Activity activity) {
                    FlurrySession.stopSession(activity);
                    createHash(activity.getClass().getSimpleName(), activity, null, null);
                }

                @Override
                public void trackResumeActivity(Activity activity) {
                    createHash(activity.getClass().getSimpleName(), activity, null, null);
                }

                @Override
                public void trackDestroyActivity(Activity activity) {
                    createHash(activity.getClass().getSimpleName(), activity, null, null);
                }

                @Override
                public void trackAppState(Activity activity, String states) {
                    createHash(activity.getClass().getSimpleName(), null, null, states);
                }

                @Override
                public void trackPageView(Activity activity, String viewName) {
                    createHash(viewName, activity, null, null);
                    FlurryAgent.onPageView();
                }

                @Override
                public void trackEvent(Context context, String eventName, JSONObject json) {
                    createHash(eventName, null, context, null);
                }

                @Override
                public void trackEvent(Activity activity, String eventName, JSONObject json) {
                    createHash(eventName, activity, null, null);
                }

                @Override
                public void trackEvent(Activity activity, String eventName) {
                    createHash(eventName, activity, null, null);
                }

                @Override
                public void trackEvent(Context context, String eventName) {
                    createHash(eventName, null, context, null);
                }

                @Override
                public void trackEventWithConnectionSpeed(Activity activity, String eventName) {

                }

                @Override
                public void trackViewCampaign(Activity activity, String eventName, Campaign campaign) {
                    double credits = AppUtility.calculateCredits(activity, campaign);

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("TITLE", campaign.getTitle());
                    params.put("DESCRIPTION", campaign.getDescription());
                    params.put("ID", campaign.getId());
                    params.put("CREDIT_VALUE", credits+"");
                    params.put("DOLLAR_VALUE", AppUtility.calculateDollar(activity, credits)+"");
                    params.put("OFFER_URL", campaign.getUrl());
                    params.put("PREVIEW_URL", campaign.getPrevUrl());
                    params.put("THUMB_URL", campaign.getThumbnailUrl());
                    FlurryAgent.logEvent(eventName.toUpperCase(), params);
                }

                @Override
                public void sendTrackedInformation(UserInfo userInfo) {
                    FlurryAgent.setAge(userInfo.getAge());
                    FlurryAgent.setUserId(userInfo.getUserId());
                }


                @Override
                public void trackComputeAvailability(UserInfo userInfo){
                    Map<String, String> params = new HashMap<String, String>();
                    if (null != appStartWith) {
                        params.put(UNOCEROS_NETWORK_RUNNING_ON, appStartWith);
                    }

                    Context context = AppController.getInstance().getApplicationContext();
                    boolean canCompute = SyncUtility.canCompute(context);
                    //boolean isComputing = SyncUtility.isComputing(context);
                    //boolean isActive = (canCompute && !isComputing);
                    //params.put("UNOCEROS_COMPUTATION_ENGINE_STATUS", isActive?"YES":"NO");
                    params.put("MEETS_COMPUTE_THRESHOLD", canCompute?"YES":"NO");
                    //params.put("IS_COMPUTING", isComputing?"YES":"NO");
                    params.put("BATTERY_LEVEL", SyncUtility.batteryLevel(context)+"%");
                    params.put("IS_CHARGING", SyncUtility.isCharging(context)?"YES":"NO");
                    params.put("APP_VERSION", AppUtility.getVersionName(AppController.getInstance()));

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
                    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                    params.put("TIME_ZONE", TimeZone.getDefault().getID());
                    params.put("TIME_UTC", sdf.format(new Date()));

                    params.put("PRIMARY_KEY", UUID.randomUUID().toString());
                    params.put(UNOCEROS_NODE_ID, deviceHardware.getUniqueDeviceId().toString());
                    FlurryAgent.logEvent("UNOCEROS_NODE_INFORMATION", params);
                }
            };
        }

        return instance;
    }

    public void createHash(String eventName, Activity activity, Context context, String eventDescription) {
        try {
            Map<String, String> params = new HashMap<String, String>();
            if (null != activity) {
                params.put(eventName, activity.toString());
            } else if (null != context) {
                params.put(eventName, context.toString());
            } else if (null != eventDescription) {
                params.put(eventName, eventDescription);
            }
            FlurryAgent.logEvent(eventName, params);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
