package com.unoceros.consumer.fragment.message;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.unoceros.consumer.ModalWebViewActivity;
import com.unoceros.consumer.R;
import com.unoceros.consumer.model.PushMessage;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Amit
 */
public class DetailsFragment extends Fragment {

    private static final String TAG = "DetailsFragment";
    public static final String ARG_POSITION = "position";
    public static final String ARG_MESSAGE = "messages";
    
    private int mCurrentPosition = -1;
    private PushMessage mCurrentMessage = null;
    
    public DetailsFragment() {
    }

    public static DetailsFragment newInstance(int position, PushMessage message){
        DetailsFragment details = new DetailsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        args.putParcelable(ARG_MESSAGE, message);
        details.setArguments(args);
        return details;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // If activity recreated (such as from screen rotate), restore
        // the previous article selection set by onSaveInstanceState().
        // This is primarily necessary when in the two-pane layout.
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
            mCurrentMessage = savedInstanceState.getParcelable(ARG_MESSAGE);
        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.message_center_details, container, false);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Activity activity = getActivity();

        //set headline
        TextView headline = (TextView) activity.findViewById(R.id.message_headline);
        headline.setText("");

        //set dateline
        TextView dateline = (TextView) activity.findViewById(R.id.message_date);
        dateline.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            // app icon in action bar clicked; go to previous activity or home
            //when is two-pane view, do nothing
            if (getFragmentManager().findFragmentById(R.id.article_fragment) == null) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    public void onStart() {
        super.onStart();

        // During startup, check if there are arguments passed to the fragment.
        // onStart is a good place to do this because the layout has already been
        // applied to the fragment at this point so we can safely call the method
        // below that sets the article text.
        Bundle args = getArguments();
        if (args != null) {
            // Set article based on argument passed in
            updateArticleView(args.getInt(ARG_POSITION), (PushMessage)args.getParcelable(ARG_MESSAGE));
        } else if (mCurrentPosition != -1) {
            // Set article based on saved instance state defined during onCreateView
            updateArticleView(mCurrentPosition, mCurrentMessage);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the current article selection in case we need to recreate the fragment
        outState.putInt(ARG_POSITION, mCurrentPosition);
    }
    
    public void updateArticleView(int position, PushMessage messages) {
        //set headline
        TextView headline = (TextView) getActivity().findViewById(R.id.message_headline);
        headline.setText(messages.getHeadline());

        //set dateline
        Date messageDate = new Date(messages.getTimestamp());
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd hh:mm a");
        TextView dateline = (TextView) getActivity().findViewById(R.id.message_date);
        dateline.setText(sdf.format(messageDate));
        
        //set details
        WebView details = (WebView) getActivity().findViewById(R.id.message_details);
        setDescription(getActivity(), details, messages);
        
        mCurrentPosition = position;
        mCurrentMessage = messages;
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void resetFontSize(Activity context, WebView webView){
        String defaultSize = context.getResources().getString(R.string.default_font_size);
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        String fontsize = sharedPrefs.getString("updates_fontsize", defaultSize);
        int size = Integer.valueOf(fontsize);

        WebSettings settings = webView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        settings.setDefaultFontSize(size);
        settings.setJavaScriptEnabled(true);
    }

    private void setDescription(final Activity context, final WebView webView, final PushMessage message){
        resetFontSize(context, webView);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCod,String description, String failingUrl) {
                webView.clearView();
                webView.setVisibility(View.GONE);
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                Log.d(TAG, "shouldOverrideUrlLoading::Url: " + url);
                try{
                    Uri link = Uri.parse(url);

                    if(link.getHost() == null){
                        //fall thru so it can be opened in a modal browser
                    }else if(link.getHost().contains("unoceros.com")){
                        //handle by intent filters in manifest
                        return false;
                    }

                    Bundle webBundle = new Bundle();
                    Intent intent = new Intent();
                    webBundle.putParcelable("uri", link);
                    webBundle.putString(ModalWebViewActivity.EXTRA_TITLE, message.getHeadline());
                    intent.setClass(context, ModalWebViewActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtras(webBundle);
                    context.startActivity(intent);
                }catch(Exception e){}
                return true;
            }
            @Override
            public void onLoadResource(WebView view, String url){
                //Log.d(TAG, "onLoadResource::Url: "+url);
            }
        });

        //use webview, but escape the text content first
        String baseUrl = context.getString(R.string.weblink);
        webView.setBackgroundColor(context.getResources().getColor(R.color.whitenoceros));
        if(message.getDetails() == null)
            message.setDetails("Oops, No Details!!");
        webView.loadDataWithBaseURL(baseUrl, Html.fromHtml(message.getDetails()).toString(), "text/html", "UTF-8", null);
        webView.setVisibility(View.VISIBLE);
        
    }
}
