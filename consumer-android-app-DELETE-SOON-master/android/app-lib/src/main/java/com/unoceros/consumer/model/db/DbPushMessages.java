package com.unoceros.consumer.model.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Amit
 */
public class DbPushMessages extends SQLiteOpenHelper{

    private static final String TAG = "DbPushMessages";

    //Fields
    public static final String ID = "_id";
    public static final String MESSAGE_UUID = "messageUUID";
    public static final String MESSAGE_HEADLINE = "messageHeadline";
    public static final String MESSAGE_DETAILS = "messageDetails";
    public static final String MESSAGE_TIMESTAMP = "messageTimestamp";
    public static final String MESSAGE_DATE = "messageDate";
    public static final String MESSAGE_GSON_DATA = "messageGsonData";

    //Database Info
    public static final String DATABASE_NAME = "pushMessages.db";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_TABLE = "pushMessages";

    //SQL creation statement
    private static final String DATABASE_CREATE = "CREATE TABLE "
            + DATABASE_TABLE + "(" + ID + " integer primary key autoincrement, "
            + MESSAGE_UUID+ " text not null, "
            + MESSAGE_HEADLINE+ " text not null, "
            + MESSAGE_DETAILS+ " text not null, "
            + MESSAGE_TIMESTAMP+ " INTEGER,"
            + MESSAGE_DATE+ " text not null, "
            + MESSAGE_GSON_DATA + " text not null)";

    public DbPushMessages(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");

        //simple deletes all existing data and re-create
        database.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
        onCreate(database);
    }
}
