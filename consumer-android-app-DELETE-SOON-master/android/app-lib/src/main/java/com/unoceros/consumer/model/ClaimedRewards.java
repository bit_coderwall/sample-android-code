package com.unoceros.consumer.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Amit
 */
public class ClaimedRewards implements Serializable {

    private SimpleDateFormat mdateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private LinkedHashMap<String, List<Campaign>> history;

    public ClaimedRewards(){
        history = new LinkedHashMap<String, List<Campaign>>();
    }

    public void addReward(Date date, Campaign campaign){
        String dateStr = mdateFormat.format(date);
        List<Campaign> campaigns = history.get(dateStr);
        if(campaigns == null){
            campaigns = new ArrayList<Campaign>();
            campaigns.add(campaign);
            history.put(dateStr, campaigns);
        }else {
            campaigns.add(campaign);
        }
    }

    public List<Campaign> getHistory(Date date){
        String dateStr = mdateFormat.format(date);
        return history.get(dateStr);
    }

    public LinkedHashMap<String, List<Campaign>> getAllHistory(){
        return history;
    }
}
