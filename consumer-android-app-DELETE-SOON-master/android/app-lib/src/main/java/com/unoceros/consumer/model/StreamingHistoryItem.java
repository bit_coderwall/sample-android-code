package com.unoceros.consumer.model;

import java.util.Date;
import java.util.List;

/**
 * Created by Amit
 */
public class StreamingHistoryItem {

    private Date date;
    private String month;
    private String day;
    private String credit;
    private String hour;
    private List<Campaign> history;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public List<Campaign> getHistory() {
        return history;
    }

    public void setHistory(List<Campaign> history) {
        this.history = history;
    }

}
