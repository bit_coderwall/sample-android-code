package com.unoceros.consumer;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.fragment.message.DetailsFragment;
import com.unoceros.consumer.fragment.message.HeadlinesFragment;
import com.unoceros.consumer.model.PushMessage;
import com.unoceros.consumer.util.AppUtility;

/**
 * Created by Amit
 */
public class MessageCenterActivity extends ActionBarActivity
        implements HeadlinesFragment.OnHeadlineSelectedListener{
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.fragment_message_center);

        actionBarSetup("Messages");

        Bundle bundle = getIntent().getExtras();
        boolean fromPush = bundle.getBoolean("fromPush");
        PushMessage message = bundle.getParcelable("pushMessage");
        
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            HeadlinesFragment firstFragment = HeadlinesFragment.newInstance(fromPush, bundle);

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(android.R.id.content,
                            firstFragment,
                            HeadlinesFragment.TAG)
                    .commit();
        }
    }

    private void actionBarSetup(String title) {
        ActionBar ab = getSupportActionBar();
        if(ab!= null){
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle(title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            // app icon in action bar clicked; go to previous activity or home
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void checkNetworkConnection() {
        if (!AppUtility.haveNetworkConnection(this)) {
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //check network
        checkNetworkConnection();

        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "MessageCenterActivity.onResume", AppState.instance().getStatusJson());
    }

    @Override
    protected void onPause() {
        super.onPause();
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "MessageCenterActivity.onPause", AppState.instance().getStatusJson());
    }

    @Override
    protected void onDestroy() {
        TrackingManager.getInstance(this.getApplication()).
                trackEvent(this, "MessageCenterActivity.onDestroy", AppState.instance().getStatusJson());
        TrackingManager.getInstance(this.getApplication()).
                trackDestroyActivity(this);

        super.onDestroy();
    }

    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }
    
    public void onMessageSelected(int position, PushMessage messages) {
        // The user selected the headline of an article from the HeadlinesFragment
        // Do something here to display that article

        DetailsFragment articleFrag = (DetailsFragment)
                getSupportFragmentManager().findFragmentById(R.id.article_fragment);

        if (articleFrag != null) {
            // If article frag is available, we're in two-pane layout...

            // Call a method in the DetailFragment to update its content
            articleFrag.updateArticleView(position, messages);
        } else {
            // Otherwise, we're in the one-pane layout and must swap frags...

            // Create fragment and give it an argument for the selected article
            Bundle args = new Bundle();
            args.putInt(DetailsFragment.ARG_POSITION, position);
            DetailsFragment newFragment = DetailsFragment.newInstance(position, messages);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(android.R.id.content, 
                    newFragment, 
                    HeadlinesFragment.TAG);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        }
    }
}
