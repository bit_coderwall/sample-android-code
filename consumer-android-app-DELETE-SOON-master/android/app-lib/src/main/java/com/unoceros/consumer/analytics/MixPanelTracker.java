package com.unoceros.consumer.analytics;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.unoceros.consumer.R;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.util.AppUtility;

import org.json.JSONObject;

/**
 * Created by Amit
 */
public class MixPanelTracker implements ITracker {

    //instance
    private static MixPanelTracker instance;
    private static MixpanelAPI _mixpanel;

    private MixPanelTracker(Application context) {

        String token = context.getString(R.string.mix_panel_token_PROD);
        if (AppUtility.signedWithDebugKey(context) || AppUtility.isDebuggable(context)) {
            token = context.getString(R.string.mix_panel_token_QA);
        }
        _mixpanel = MixpanelAPI.getInstance(context, token);
    }

    public static MixPanelTracker getInstance(Application applicationContext) {
        if (instance == null) {
            instance = new MixPanelTracker(applicationContext);
        }

        return instance;
    }

    public void trackAppStart(Activity activity){
        _mixpanel.track(activity.getLocalClassName()+" appStart", AppState.instance().getStatusJson());
    }
    public void trackStartActivity(Activity activity){
        _mixpanel.track(activity.getLocalClassName()+" onStart", AppState.instance().getStatusJson());
    }
    public void trackResumeActivity(Activity activity){
        _mixpanel.track(activity.getLocalClassName()+" onResume", AppState.instance().getStatusJson());
    }
    public void trackStopActivity(Activity activity){
        _mixpanel.track(activity.getLocalClassName()+" onStop", AppState.instance().getStatusJson());
    }
    public void trackDestroyActivity(Activity activity){
        _mixpanel.flush();
    }
    public void trackAppState(Activity activity, String states){
        _mixpanel.track(states, AppState.instance().getStatusJson());
    }

    public void trackPageView(Activity activity, String viewName){
        _mixpanel.track(viewName, AppState.instance().getStatusJson());
    }

    public void trackEvent(Context context, String eventName, JSONObject json) {
        _mixpanel.track(eventName, json);
    }

    public void trackEvent(Activity activity, String eventName, JSONObject json) {
        _mixpanel.track(eventName, json);
    }

    public void trackEvent(Activity activity, String eventName){
        trackEvent(activity.getApplicationContext(), eventName);
    }

    public void trackEvent(Context context, String eventName){
        _mixpanel.track(eventName, AppState.instance().getStatusJson());
    }
    
     @Override
    public void sendTrackedInformation(UserInfo userInfo) {
    }

    @Override
    public void trackComputeAvailability(UserInfo userInfo){
    }

    public void trackEventWithConnectionSpeed(Activity activity, String eventName){
        String eventNameStr = eventName;
        if(AppUtility.haveWifiConnection(activity)){
            //wifi connection
            eventNameStr = "AppStartWifiConnection";
        }else if(AppUtility.haveMobileConnection(activity)){
            //carrier connection
            eventNameStr = "AppStartCarrierConnection";
        }else{
            //no connection
            eventNameStr = "AppStartNoConnection";
        }
        _mixpanel.track(eventNameStr, AppState.instance().getStatusJson());
    }

    public void trackViewCampaign(Activity activity, String eventName, Campaign campaign){}
}
