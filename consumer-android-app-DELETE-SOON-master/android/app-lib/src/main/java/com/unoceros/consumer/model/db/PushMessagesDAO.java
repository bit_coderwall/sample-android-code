package com.unoceros.consumer.model.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.PushMessage;
import com.unoceros.consumer.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Amit
 */
public class PushMessagesDAO {
    private static final String TAG = "PushMessagesDAO";

    // SQL Database
    private SQLiteDatabase database;
    private DbPushMessages dbHelper;

    private String[] dbColumns = {
            DbPushMessages.ID,
            DbPushMessages.MESSAGE_UUID,
            DbPushMessages.MESSAGE_HEADLINE, 
            DbPushMessages.MESSAGE_DETAILS,
            DbPushMessages.MESSAGE_TIMESTAMP,
            DbPushMessages.MESSAGE_DATE, 
            DbPushMessages.MESSAGE_GSON_DATA};

    public PushMessagesDAO(Context context){
        //mContext = context.getApplicationContext();

        //create an instance of the db helper
        dbHelper = new DbPushMessages(context, DbPushMessages.DATABASE_NAME, null, DbPushMessages.DATABASE_VERSION);
    }

    public void open(){
        database = dbHelper.getWritableDatabase();
    }

    public void close(){
        database.close();
        dbHelper.close();
    }

    public PushMessage retrieveByID(String uuid){
        PushMessage message = null;
        try{
            //retrieve first
            Cursor cursor = database.query(DbPushMessages.DATABASE_TABLE, dbColumns,
                    DbPushMessages.MESSAGE_UUID + "=?", new String[]{uuid}, null, null, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                //Log.d(TAG, "Number of items in cursor: " + cursor.getCount());
                message = cursorToMessage(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }catch (Exception e){
            Log.e(TAG, e.getMessage());
        }
        return message;
    }
    public int deleteByID(String uuid){
        int id = 0;
        try{
            //now delete
            id = database.delete(DbPushMessages.DATABASE_TABLE,
                    DbPushMessages.MESSAGE_UUID+"=?", new String[]{uuid});
        }catch (Exception e){
            Log.e(TAG, e.getMessage(), e);
        }
        
        return id;
    }
    
    public String insertByDate(String date, PushMessage message, boolean isNew){
        Gson gson = new Gson();
        String gsonData = gson.toJson(message);
        ContentValues values = new ContentValues();
        values.put(DbPushMessages.MESSAGE_UUID, message.getMessageId()); //user as key in endpoint
        values.put(DbPushMessages.MESSAGE_HEADLINE, message.getHeadline());
        values.put(DbPushMessages.MESSAGE_DETAILS, message.getDetails());
        values.put(DbPushMessages.MESSAGE_TIMESTAMP, message.getTimestamp());
        values.put(DbPushMessages.MESSAGE_DATE, date);
        values.put(DbPushMessages.MESSAGE_GSON_DATA, gsonData);

        Log.d(TAG, "inserting messages by date: "+date);
        Log.d(TAG, "gson data: "+gsonData);

        if(isNew){
            //insert
            database.insert(DbPushMessages.DATABASE_TABLE, null, values);
        }else{
            //update
            try{
                database.update(DbPushMessages.DATABASE_TABLE, values,
                        DbPushMessages.MESSAGE_UUID+"=?", new String[]{message.getMessageId()});
            }catch (Exception e){
                Log.e(TAG, e.getMessage(), e);
            }
        }

        return message.getMessageId();
    }

    public List<PushMessage> retrieveByDate(String date){
        List<PushMessage> results = new ArrayList<>();
        //Log.d(TAG, "retrieving messages by date: "+date);

        //sanity check
        if(date == null)
            return null;

        try {
            Cursor cursor = database.query(DbPushMessages.DATABASE_TABLE, dbColumns,
                    DbPushMessages.MESSAGE_DATE + "=?", new String[]{date}, null, null, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                //Log.d(TAG, "Number of items in cursor: " + cursor.getCount());
                PushMessage message = cursorToMessage(cursor);
                results.add(message);
                cursor.moveToNext();
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        return results;
    }

    public List<PushMessage> getAll(){
        List<PushMessage> results = new ArrayList<>();

        try {
            Cursor cursor = database.query(DbPushMessages.DATABASE_TABLE, dbColumns,
                    null, null, null, null, DbPushMessages.ID+" DESC");
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                //Log.d(TAG, "Number of items in cursor: " + cursor.getCount());
                PushMessage message = cursorToMessage(cursor);
                results.add(message);
                cursor.moveToNext();
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        return results;
    }

    private PushMessage cursorToMessage(Cursor cursor){
        PushMessage message = null;
        try {
            if (cursor != null && cursor.getCount() > 0) {
                int rewardsGsonIndex = cursor.getColumnIndex(DbPushMessages.MESSAGE_GSON_DATA);
                Gson gson = new Gson();
                String gson_data = cursor.getString(rewardsGsonIndex);
                //Log.d(TAG, "retrieved gson data: "+gson_data);
                message = gson.fromJson(gson_data, PushMessage.class);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return message;
    }
}
