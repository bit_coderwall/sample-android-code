package com.unoceros.consumer.analytics;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.unoceros.consumer.R;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.DeviceHardware;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.services.ApplicationServices;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.util.SyncUtility;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

/**
 * Created by Amit
 */
public class GoogleTracker implements ITracker {

    //instance
    private static GoogleTracker instance;

    public static final String GOOGLE_ANALYTICS_PROPERTY_ID = "UA-57343459-1";

    public static String AppStartWifiConnection = "WIFI";
    public static String AppStartCarrierConnection = "MOBILE_CARRIER";
    public static String AppStartNoConnection = "NO_WIFI_CARRIER";
    public final static String UNOCEROS_NETWORK_RUNNING_ON = "UNOCEROS_NETWORK_RUNNING_ON";
    public final static String UNOCEROS_NODE_ID = "UNOCEROS_NODE_ID";
    public String appStartWith;

    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     *
     * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
     * storing them all in Application object helps ensure that they are created only once per
     * application instance.
     */
    private enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }
    private Tracker mTracker;

    private GoogleTracker(final Application context){
        DeviceHardware deviceHardware = ApplicationServices.getInstance(context).getDeviceHardware();

        //initialize tracker
        //HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
        analytics.setDryRun(false);
        analytics.setAppOptOut(false);
        analytics.enableAutoActivityReports(context);

        // Set the log level to verbose.
        if(AppUtility.signedWithDebugKey(context.getApplicationContext())
                || AppUtility.isDebuggable(context.getApplicationContext())) {
            //do debug stuff
            analytics.getLogger()
                    .setLogLevel(Logger.LogLevel.VERBOSE);
        }else{
            //do release stuff
            analytics.getLogger()
                    .setLogLevel(Logger.LogLevel.WARNING);
        }

        mTracker = analytics.newTracker(GOOGLE_ANALYTICS_PROPERTY_ID); //R.xml.app_tracker);
        mTracker.setAnonymizeIp(false);
        mTracker.setUseSecure(true);

        //mTrackers.put(TrackerName.APP_TRACKER, mTracker);

        //enable advertising feature
        mTracker.enableAdvertisingIdCollection(true);

        //app related stuff
        mTracker.setAppName(deviceHardware.getAppName());
        mTracker.setAppVersion(deviceHardware.getAppVersion());

        //user id setup
        String uuid = deviceHardware.getUniqueDeviceId().toString();
        mTracker.set("&uid", uuid); // Set the user ID using signed-in user_id.
        mTracker.setClientId(uuid);

        appStartWith = reportDeviceInfo(context);
    }

    private String reportDeviceInfo(Application context) {
        String eventNameStr = null;
        try {
            if (AppUtility.haveWifiConnection(context)) {
                //wifi connection
                eventNameStr = AppStartWifiConnection;
            } else if (AppUtility.haveMobileConnection(context)) {
                //carrier connection
                eventNameStr = AppStartCarrierConnection;
            } else {
                //no connection
                eventNameStr = AppStartNoConnection;
            }
            Log.d("AppStartWith", eventNameStr);

            HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
                    .setCategory(UNOCEROS_NETWORK_RUNNING_ON)
                    .setAction("Internet Check")
                    .setLabel(eventNameStr)
                    .setValue(1);
            mTracker.send(builder.build());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return eventNameStr;
    }


    public static GoogleTracker getInstance(Application applicationContext) {
        if (instance == null) {
            instance = new GoogleTracker(applicationContext);
        }

        return instance;
    }

    @Override
    public void trackAppStart(Activity activity) {
        String screenName = activity.getClass().getSimpleName();
        // Set screen name.
        mTracker.setScreenName(screenName);

        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void trackStartActivity(Activity activity) {
        String screenName = activity.getClass().getSimpleName();
        // Set screen name.
        mTracker.setScreenName(screenName);

        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void trackResumeActivity(Activity activity) {

    }

    @Override
    public void trackStopActivity(Activity activity) {

    }

    @Override
    public void trackDestroyActivity(Activity activity) {

    }

    @Override
    public void trackAppState(Activity activity, String states) {
        String screenName = activity.getClass().getSimpleName();
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
                .setCategory(screenName)
                .setAction(states)
                .setLabel("application state")
                .setValue(1);
        mTracker.send(builder.build());
    }

    @Override
    public void trackPageView(Activity activity, String viewName) {
        trackEvent(activity, viewName);
    }

    @Override
    public void trackEvent(Context context, String eventName, JSONObject json) {
        trackEvent(context, eventName);
    }

    @Override
    public void trackEvent(Activity activity, String eventName, JSONObject json) {
        trackEvent(activity, eventName);
    }

    @Override
    public void trackEvent(Activity activity, String eventName) {
        String screenName = activity.getClass().getSimpleName();
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
                .setCategory(screenName)
                .setAction(eventName)
                .setLabel("application state")
                .setValue(1);
        mTracker.send(builder.build());
    }

    @Override
    public void trackEvent(Context context, String eventName) {
        String screenName = context.getClass().getSimpleName();
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
                .setCategory(screenName)
                .setAction(eventName)
                .setLabel("application state")
                .setValue(1);
        mTracker.send(builder.build());
    }

    @Override
    public void trackEventWithConnectionSpeed(Activity activity, String eventName) {
        String screenName = activity.getClass().getSimpleName();
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
                .setCategory(screenName)
                .setAction(eventName)
                .setLabel("connection speed")
                .setValue(1);
        mTracker.send(builder.build());
    }

    @Override
    public void trackViewCampaign(Activity activity, String eventName, Campaign campaign) {
        double credits = AppUtility.calculateCredits(activity, campaign);
        double dollar = AppUtility.calculateDollar(activity, credits);

        Product product = new Product()
                .setName(campaign.getTitle() + ":" + campaign.getDescription())
                .setPrice(dollar)
                .setQuantity(1);

        ProductAction productAction = new ProductAction(ProductAction.ACTION_PURCHASE)
                .setTransactionId(campaign.getId());

        // Add the transaction data to the event.
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
                .setCategory("In-App Passport")
                .setAction(eventName)
                .addProduct(product)
                .setProductAction(productAction);

        builder.set("TITLE", campaign.getTitle());
        builder.set("DESCRIPTION", campaign.getDescription());
        builder.set("ID", campaign.getId());
        builder.set("CREDIT_VALUE", credits + "");
        builder.set("DOLLAR_VALUE", AppUtility.calculateDollar(activity, credits) + "");
        builder.set("OFFER_URL", campaign.getUrl());
        builder.set("PREVIEW_URL", campaign.getPrevUrl());
        builder.set("THUMB_URL", campaign.getThumbnailUrl());

        // Send the transaction data with the event.
        mTracker.send(builder.build());

        //resets
        builder.set("TITLE", null);
        builder.set("DESCRIPTION", null);
        builder.set("ID", null);
        builder.set("CREDIT_VALUE", null);
        builder.set("DOLLAR_VALUE", null);
        builder.set("OFFER_URL", null);
        builder.set("PREVIEW_URL", null);
        builder.set("THUMB_URL", null);
        builder.build();
    }

    @Override
    public void sendTrackedInformation(UserInfo userInfo) {
        mTracker.set("&uid", userInfo.getUserId()); // Set the user ID using signed-in user_id.
    }

    @Override
    public void trackComputeAvailability(UserInfo userInfo){
        HitBuilders.EventBuilder builder = getNodeInfo(userInfo);
        builder.setNonInteraction(true);
        mTracker.send(builder.build());
    }

    private HitBuilders.EventBuilder getNodeInfo(UserInfo user){
        HitBuilders.EventBuilder params = new HitBuilders.EventBuilder();
        params.setCategory("NODE_INFORMATION");

        if (null != appStartWith) {
            params.set(UNOCEROS_NETWORK_RUNNING_ON, appStartWith);
        }

        Context context = AppController.getInstance().getApplicationContext();
        boolean canCompute = SyncUtility.canCompute(context);
        //boolean isComputing = SyncUtility.isComputing(context);
        //boolean isActive = (canCompute && !isComputing);
        //params.set("COMPUTE_ENGINE_STATUS", isActive ? "YES" : "NO");
        params.set("MEETS_COMPUTE_THRESHOLD", canCompute ? "YES" : "NO");
        //params.set("IS_COMPUTING", isComputing ? "YES" : "NO");
        params.set("BATTERY_LEVEL", SyncUtility.batteryLevel(context) + "%");
        params.set("IS_CHARGING", SyncUtility.isCharging(context) ? "YES" : "NO");
        params.set("APP_VERSION", AppUtility.getVersionName(AppController.getInstance()));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        params.set("TIME_ZONE", TimeZone.getDefault().getID());
        params.set("TIME_UTC", sdf.format(new Date()));

        params.set("PRIMARY_KEY", UUID.randomUUID().toString());
        params.set(UNOCEROS_NODE_ID, user.getUserId());

        return params;
    }

    public void trackCustomDimension(Context context, String dimension) {
        String screenName = context.getClass().getSimpleName();
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder()
                        .setCustomDimension(1, dimension)
                        .build()
        );
    }
}
