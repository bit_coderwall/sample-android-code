package com.unoceros.consumer.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Amit
 */
public class PushMessage implements Parcelable, Serializable {
    private String messageId;
    private String headline;
    private String details = "Oops, no details yet, please check back soon!!";
    private String label;
    private Long timestamp;
    private Boolean isNew = false;
    private int color;

    public PushMessage(){}
    public PushMessage(String messageId, String headline, String details){
        this.messageId = messageId;
        this.headline = headline;
        this.details = details;
    }
    
    public PushMessage(Parcel in){
        readFromParcel(in);
    }
    
    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean getIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public String toString(){
        return this.headline;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getMessageId());
        dest.writeString(getHeadline());
        dest.writeString(getDetails());
        dest.writeString(getLabel());
        dest.writeLong(getTimestamp());
        dest.writeInt(getColor());
    }

    public void readFromParcel(Parcel in) {
        setMessageId(in.readString());
        setHeadline(in.readString());
        setDetails(in.readString());
        setLabel(in.readString());
        setTimestamp(in.readLong());
        setColor(in.readInt());
    }

    public static final Parcelable.Creator<PushMessage> CREATOR = new Parcelable.Creator<PushMessage>() {
        public PushMessage createFromParcel(Parcel in) {
            return new PushMessage(in);
        }

        public PushMessage[] newArray(int size) {
            return new PushMessage[size];
        }
    };

    @Override
    public int hashCode(){
        return getMessageId().hashCode();
    }

    @Override
    public boolean equals(Object o){
        if (o instanceof PushMessage){
            return getMessageId().equalsIgnoreCase(((PushMessage) o).getMessageId());
        }
        return false;
    }
}
