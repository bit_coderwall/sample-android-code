package com.unoceros.consumer.fragment.message;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.unoceros.consumer.R;
import com.unoceros.consumer.adapter.PushMessageListAdapter;
import com.unoceros.consumer.model.PushMessage;
import com.unoceros.consumer.model.db.PushMessagesDAO;
import com.unoceros.consumer.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amit
 */
public class HeadlinesFragment extends ListFragment {

    public static final String TAG = "HeadlinesFragment";
    
    private OnHeadlineSelectedListener mCallback;

    private List<PushMessage> pushMessageList;
    private PushMessageListAdapter listAdapter;

    private PushMessagesDAO mPersistentDao;
    
    public HeadlinesFragment() {
    }
    
    public static HeadlinesFragment newInstance(boolean fromPush, Bundle args){
        HeadlinesFragment headline = new HeadlinesFragment();
        if(args == null)
            args = new Bundle();
        args.putBoolean("fromPush", fromPush);
        headline.setArguments(args);
        return headline;
    }

    // Container Activity must implement this interface
    public interface OnHeadlineSelectedListener {
        public void onMessageSelected(int position, PushMessage messages);
    }

    @Override 
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // retain this fragment
        setRetainInstance(true);

        // Notify the system to allow an options menu for this fragment.
        setHasOptionsMenu(true);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.message_center_headlines, container, false);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setBackgroundColor(getResources().getColor(R.color.brand_blue)); //Color.parseColor(AppUtility.LIST_BG_HEX_COLOR_TRANSPARENT));

        // No data initially
        Bundle bundle = getArguments();
        if(bundle != null)
            setEmptyText(bundle.getString("emptyDefault", "Loading ..."));
        else
            setEmptyText("Loading ...");
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Activity activity = getActivity();

        //create persistent datastore
        mPersistentDao = new PushMessagesDAO(activity);
        mPersistentDao.open();
        
        // Create an array adapter for the list view, using the Ipsum headlines
        // array
        pushMessageList = new ArrayList<>();
        listAdapter = new PushMessageListAdapter(activity, pushMessageList, true);
        setListAdapter(listAdapter);

        //remove scrollbars
        ListView listView = getListView();
        listView.setVerticalScrollBarEnabled(false);
        listView.setHorizontalScrollBarEnabled(false);

        //register context menu for list view
        registerForContextMenu(listView);

        //populate view
        populatedAdapter(true);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == android.R.id.list) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;

            PushMessage message = pushMessageList.get(info.position);
            menu.setHeaderTitle(message.getHeadline());
            String[] menuItems = new String[]{"Delete"}; //getResources().getStringArray(R.array.menu);
            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(android.view.MenuItem item) {
        //Note how this callback is using the fully-qualified class name
        //Toast.makeText(this, "Got click: " + item.toString(), Toast.LENGTH_SHORT).show();

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = new String[]{"Delete"}; //getResources().getStringArray(R.array.menu);
        String menuItemName = menuItems[menuItemIndex];

        PushMessage message = pushMessageList.get(info.position);
        Log.d(TAG, String.format("Selected %s for item %s", menuItemName, message));

        //remove from datastore
        removeFromDatastore(message);
        
        //re-populate/refresh list view
        populatedAdapter(false);
        
        return true;
    }
    
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // Send the event to the host activity
        PushMessage messages = (PushMessage)getListView().getItemAtPosition(position);
        //PushMessage messages = listAdapter.getItem(position);
        mCallback.onMessageSelected(position, messages);

        // Set the item as checked to be highlighted when in two-pane layout
        getListView().setItemChecked(position, true);
    }

    @Override
    public void onStart() {
        super.onStart();

        // When in two-pane layout, set the listview to highlight the selected
        // list item
        // (We do this during onStart because at the point the listview is
        // available.)
        if (getFragmentManager().findFragmentById(R.id.article_fragment) != null) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    public void onResume() {
        super.onResume();

        mPersistentDao.open();
    }

    public void onPause(){
        super.onPause();

        mPersistentDao.close();
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    private void populatedAdapter(boolean onCreate){
        //clear the data store
        listAdapter.clearData();
        // notify data changes to list adapter
        listAdapter.notifyDataSetChanged();

        pushMessageList = mPersistentDao.getAll();

        Bundle bundle = getArguments();
        if(bundle != null && bundle.getParcelable("pushMessage") != null) {
            PushMessage push = bundle.getParcelable("pushMessage");
            if(pushMessageList != null && pushMessageList.contains(push)){
                //mark as new
                int idx = pushMessageList.indexOf(push);
                pushMessageList.get(idx).setIsNew(true);
            }else if(pushMessageList != null){
                pushMessageList.add(push);
            }
        }

        if(pushMessageList == null)
            return; //sanity check

        for(PushMessage message : pushMessageList){
            listAdapter.addData(message);
        }

        if(pushMessageList.size() <= 0 && bundle != null)
            setEmptyText(bundle.getString("emptyMessage", "No Messages Found ..."));
        else
            setEmptyText("No Messages Found ...");
        
        // notify data changes to list adapter
        listAdapter.notifyDataSetChanged();
    }

    private void removeFromDatastore(PushMessage message){
        //remove this result from the db
        if(mPersistentDao != null && message != null){
            mPersistentDao.deleteByID(message.getMessageId());
        }
    }
}
