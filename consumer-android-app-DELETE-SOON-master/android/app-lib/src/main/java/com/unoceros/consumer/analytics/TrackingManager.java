package com.unoceros.consumer.analytics;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.flurry.android.FlurryAgent;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.util.Log;

import org.json.JSONObject;

public class TrackingManager implements ITracker {

    protected static final String TAG = "TrackingManager";

    protected static TrackingManager manager;

    public final static String OFFER_CLICKED = "offer_click";
    public final static String OFFER_PURCHASED = "offer_purchase";

    private static HasOfferTracker hasOfferTracker;
    //private static MixPanelTracker mixPanelTracker;
    private static FlurryTracker flurryTracker;
    private static GoogleTracker googleTracker;

    private static boolean isInitialized = false;

    protected TrackingManager(Application applicationContext) {
        if (hasOfferTracker == null)
            hasOfferTracker = HasOfferTracker.getInstance(applicationContext);
        //if(mixPanelTracker == null)
        //   mixPanelTracker = MixPanelTracker.getInstance(applicationContext);
        if(flurryTracker == null)
            flurryTracker = FlurryTracker.getInstance(applicationContext);
        if(googleTracker == null)
            googleTracker = GoogleTracker.getInstance(applicationContext);

        isInitialized = true;
    }

    private static void sanityMethodCheck(Activity activity, String methodName) {
        if (activity == null)
            throw new NullPointerException("[" + methodName + "] Activity for tracking manager cannot be null");

        if (!isInitialized)
            getInstance(activity.getApplication());
    }

    public static TrackingManager getInstance(Application applicationContext) {
        if (manager == null)
            manager = new TrackingManager(applicationContext);

        return manager;
    }

    public static TrackingManager getTracker() throws Exception {
        if (manager == null)
            throw new NullPointerException("Tracker is not yet initialized, call getInstance() first.");
        return manager;
    }

    public void trackAppStart(Activity activity) {
        sanityMethodCheck(activity, "trackAppStart");

        Log.d(TAG, "Tracking app start: "+activity);
        hasOfferTracker.trackAppStart(activity);
        //mixPanelTracker.trackAppStart(activity);
        flurryTracker.trackAppStart(activity);
        googleTracker.trackAppStart(activity);
    }

    public void trackPageView(Activity activity, String viewName) {
        sanityMethodCheck(activity, "trackPageView");

        Log.d(TAG, "Tracking pageview: " + viewName);
        hasOfferTracker.trackPageView(activity, viewName);
        //mixPanelTracker.trackPageView(activity, viewName);
        flurryTracker.trackPageView(activity, viewName);
        googleTracker.trackPageView(activity, viewName);
    }

    public void trackEvent(Context context, String eventName, JSONObject json) {
        Log.d(TAG, "Tracking events: " + eventName);

        hasOfferTracker.trackEvent(context, eventName, json);
        //mixPanelTracker.trackEvent(context, eventName, json);
        flurryTracker.trackEvent(context, eventName, json);
        googleTracker.trackEvent(context, eventName, json);
    }

    public void trackEvent(Activity activity, String eventName, JSONObject json) {
        sanityMethodCheck(activity, "trackStartActivity");

        Log.d(TAG, "Tracking events: " + eventName);
        hasOfferTracker.trackEvent(activity, eventName, json);
        //mixPanelTracker.trackEvent(activity, eventName, json);
        flurryTracker.trackEvent(activity, eventName, json);
        googleTracker.trackEvent(activity, eventName, json);
    }

    public void trackEvent(Context context, String eventName) {
        Log.d(TAG, "Tracking events: " + eventName);

        hasOfferTracker.trackEvent(context, eventName);
        //mixPanelTracker.trackEvent(context, eventName);
        flurryTracker.trackEvent(context, eventName);
        googleTracker.trackEvent(context, eventName);
    }

    public void trackEvent(Activity activity, String eventName) {
        sanityMethodCheck(activity, "trackEvent");

        Log.d(TAG, "Tracking events: " + eventName);
        hasOfferTracker.trackEvent(activity, eventName);
        //mixPanelTracker.trackEvent(activity, eventName);
        flurryTracker.trackEvent(activity, eventName);
        googleTracker.trackEvent(activity, eventName);
    }

    public void trackViewCampaign(Activity activity, String eventName, Campaign campaign) {
        sanityMethodCheck(activity, "trackViewCampaign");

        Log.d(TAG, "Tracking campaign: " + campaign +", for event: "+eventName);
        hasOfferTracker.trackViewCampaign(activity, eventName, campaign);
        //mixPanelTracker.trackViewCampaign(activity, eventName, campaign);
        flurryTracker.trackViewCampaign(activity, eventName, campaign);
        googleTracker.trackViewCampaign(activity, eventName, campaign);
    }

    public void trackStartActivity(Activity activity) {
        sanityMethodCheck(activity, "trackStartActivity");

        Log.d(TAG, "Tracking activity start");
        hasOfferTracker.trackStartActivity(activity);
        //mixPanelTracker.trackStartActivity(activity);
        flurryTracker.trackStartActivity(activity);
        googleTracker.trackStartActivity(activity);
    }

    public void trackResumeActivity(Activity activity) {
        sanityMethodCheck(activity, "trackResumeActivity");

        Log.d(TAG, "Tracking activity resume");
        hasOfferTracker.trackResumeActivity(activity);
        //mixPanelTracker.trackResumeActivity(activity);
        flurryTracker.trackResumeActivity(activity);
        googleTracker.trackResumeActivity(activity);
    }

    public void trackStopActivity(Activity activity) {
        sanityMethodCheck(activity, "trackStopActivity");

        Log.d(TAG, "Tracking activity stop");
        hasOfferTracker.trackStopActivity(activity);
        //mixPanelTracker.trackStopActivity(activity);
        flurryTracker.trackStopActivity(activity);
        googleTracker.trackStopActivity(activity);
    }

    public void trackDestroyActivity(Activity activity) {
        sanityMethodCheck(activity, "trackDestroyActivity");

        Log.d(TAG, "Tracking activity destroy");
        hasOfferTracker.trackDestroyActivity(activity);
        //mixPanelTracker.trackDestroyActivity(activity);
        flurryTracker.trackDestroyActivity(activity);
        googleTracker.trackDestroyActivity(activity);
    }

    public void trackAppState(Activity activity, String states) {
        sanityMethodCheck(activity, "trackAppState");

        Log.d(TAG, "Tracking application state: " + states);
        hasOfferTracker.trackAppState(activity, states);
        //mixPanelTracker.trackAppState(activity, states);
        flurryTracker.trackAppState(activity, states);
        googleTracker.trackAppState(activity, states);
    }

    public void trackEventWithConnectionSpeed(Activity activity, String eventName) {
        sanityMethodCheck(activity, "trackEventWithConnectionSpeed");

        Log.d(TAG, "Tracking connection speed: " + eventName);
        hasOfferTracker.trackEventWithConnectionSpeed(activity, eventName);
        //mixPanelTracker.trackEventWithConnectionSpeed(activity, eventName);
        flurryTracker.trackEventWithConnectionSpeed(activity, eventName);
        googleTracker.trackEventWithConnectionSpeed(activity, eventName);
    }

    public void sendTrackedInformation(UserInfo userInfo){
        if(userInfo != null) {
            Log.d(TAG, "Sending User Info to tracker: " + userInfo.getUserId());

            hasOfferTracker.sendTrackedInformation(userInfo);
            //mixPanelTracker.sendTrackedInformation(userInfo);
            flurryTracker.sendTrackedInformation(userInfo);
            googleTracker.sendTrackedInformation(userInfo);
        }
    }

    public void trackComputeAvailability(UserInfo userInfo){
        if(userInfo != null) {
            Log.d(TAG, "Sending User Info to tracker: " + userInfo.getUserId());

            hasOfferTracker.sendTrackedInformation(userInfo);
            //mixPanelTracker.sendTrackedInformation(userInfo);
            flurryTracker.sendTrackedInformation(userInfo);
            googleTracker.sendTrackedInformation(userInfo);
        }
    }


}
