package com.unoceros.consumer.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.unoceros.consumer.HistoryActivityStream;
import com.unoceros.consumer.OfferListActivity;
import com.unoceros.consumer.R;
import com.unoceros.consumer.model.DepositStream;
import com.unoceros.consumer.services.ApplicationServices;
import com.unoceros.consumer.services.CreditServices;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.util.SyncUtility;
import com.unoceros.consumer.views.DeviceActivityStreamItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * Created by Amit
 */
public class ProgressFragment extends BaseFragment {

    public static final String TAG = "ProgressFragment";


    public static ProgressFragment newInstance(boolean forceSync){
        ProgressFragment bf = new ProgressFragment();
        Bundle option = bf.getArguments();
        if(option == null)
            option = new Bundle();
        option.putBoolean("forceSync", forceSync);
        bf.setArguments(option);
        return bf;
    }

    public static ProgressFragment newInstance(Bundle option){
        ProgressFragment bf = new ProgressFragment();
        bf.setArguments(option);
        return bf;
    }

    public ProgressFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_progress1, container, false);

        return view;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Context
        final Activity activity = getActivity();

        //make an intent request to populate the activity stream
        Intent streamIntent = new Intent(AppUtility.ACTION_GET_ACTIVITY_STREAMING);
        streamIntent.putExtra("limit", 5);
        streamIntent.putExtra("offset", 0);
        streamIntent.putExtra("order", "desc");
        streamIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        activity.sendBroadcast(streamIntent);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();

        //register local service
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mActivityStreamingReceiver, new IntentFilter("activity-streaming"));

        //populate the progress view without forcing sync
        populateTopView();
    }

    @Override
    public void onPause() {
        super.onPause();

        //unregister local service
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mActivityStreamingReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    private void populateTopView(){
        final Activity activity = getActivity();
        CreditServices creditServices = ApplicationServices.getInstance(activity.getApplication()).getCreditService();

        double overallCredits =  creditServices.getOverallBalance();
        double todayCredits =  creditServices.getTodayDeposit();
        double totalCredits = creditServices.getTotalBalance();
        double dailyAverage = creditServices.getDailyAverage();
        double networkAverage = creditServices.getNetworkAverage();

        //Today's Balance
        ((TextView)activity.findViewById(R.id.reserve_dailynew_counter_textview)).setText(""+todayCredits);

        //Total Balance
        ((TextView)activity.findViewById(R.id.reserve_total_counter_textview)).setText(""+totalCredits);

        //Daily Average
        ((TextView)activity.findViewById(R.id.reserve_dailyavg_counter_textview)).setText(""+dailyAverage);

        //Network Average
        ((TextView)activity.findViewById(R.id.reserve_netavg_counter_textview)).setText(""+networkAverage);

        //Overall Total Balance
        ((TextView)activity.findViewById(R.id.rewards_progress_overall_credits)).setText("" + overallCredits);

        //Last sync
        //Format: Last Sync: Sep 22 10:01 PM
        Date currentDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd hh:mm a");
        ((TextView) activity.findViewById(R.id.rewards_progress_last_sync)).setText("Last Sync: " + sdf.format(currentDate));


        //Alerts
        TextView alertText = ((TextView)activity.findViewById(R.id.rewards_progress_status_alert));
        alertText.setText(getString(R.string.alert_low_battery_level));
        alertText.setVisibility(View.GONE);
        if(!SyncUtility.canCompute(getActivity())) {
            alertText.setVisibility(View.VISIBLE);
        }

        //Calculate Tier Summary
        double tierRate;
        double tierCreditsRemainder;
        double tierHoursRemainder;
        String tierLevel;
        String nextTierLevel;
        boolean eliteMember = false;

        double standardTierMax = 2100;
        double silverTierMax = 3100;
        double goldTierMax = 4100;
        int tierColor; //R.color.blunoceros;

        if(overallCredits < standardTierMax) {
            //Bronze
            tierColor = R.color.bronze_overlay;
            tierLevel = getString(R.string.tier_bronze_label);
            nextTierLevel = getString(R.string.tier_silver_label);
            tierRate = AppUtility.getCreditRate(activity, false); //50;
            tierCreditsRemainder = standardTierMax - overallCredits;
            tierHoursRemainder = tierCreditsRemainder / tierRate;

            //progress bar
            int tierProgress = 0;
            if (overallCredits > 0)
                tierProgress = (int) Math.round(overallCredits / standardTierMax * 100);
            ((ProgressBar)activity.findViewById(R.id.rewards_bronze_tier_progress_bar)).setProgress(tierProgress);
            ((ProgressBar)activity.findViewById(R.id.rewards_silver_tier_progress_bar)).setProgress(0);
            ((ProgressBar)activity.findViewById(R.id.rewards_gold_tier_progress_bar)).setProgress(0);
        }else if(overallCredits < silverTierMax){
            //silver
            tierColor = R.color.silver_overlay;
            tierLevel = getString(R.string.tier_silver_label);
            nextTierLevel = getString(R.string.tier_gold_label);
            tierRate = AppUtility.getCreditRate(activity, false); //60;
            tierCreditsRemainder = silverTierMax - overallCredits;
            tierHoursRemainder = tierCreditsRemainder / tierRate;

            //progress bar
            int tierProgress = 0;
            if (overallCredits > 0) {
                tierProgress = (int)Math.round((overallCredits-standardTierMax) / (silverTierMax-standardTierMax) * 100);;
            }
            ((ProgressBar)activity.findViewById(R.id.rewards_bronze_tier_progress_bar)).setProgress(100);
            ((ProgressBar)activity.findViewById(R.id.rewards_silver_tier_progress_bar)).setProgress(tierProgress);
            ((ProgressBar)activity.findViewById(R.id.rewards_gold_tier_progress_bar)).setProgress(0);
        }else if(overallCredits < goldTierMax){
            //gold
            tierColor = R.color.gold_overlay;
            tierLevel = getString(R.string.tier_gold_label);
            nextTierLevel = getString(R.string.tier_elite_label);
            tierRate = AppUtility.getCreditRate(activity, false); //70;
            tierCreditsRemainder = goldTierMax - overallCredits;
            tierHoursRemainder = tierCreditsRemainder / tierRate;

            //progress bar
            int tierProgress = 0;
            if (overallCredits > 0)
                tierProgress = (int) Math.round((overallCredits-silverTierMax) / (goldTierMax-silverTierMax) * 100);
            ((ProgressBar)activity.findViewById(R.id.rewards_bronze_tier_progress_bar)).setProgress(100);
            ((ProgressBar)activity.findViewById(R.id.rewards_silver_tier_progress_bar)).setProgress(100);
            ((ProgressBar)activity.findViewById(R.id.rewards_gold_tier_progress_bar)).setProgress(tierProgress);
        }else{
            //the user is an elite member, defaults to gold regardless
            eliteMember = true;
            tierColor = R.color.gold_overlay;
            tierLevel = getString(R.string.tier_elite_label);
            nextTierLevel = getString(R.string.tier_elite_label);
            tierRate = AppUtility.getCreditRate(activity, false); //70;
            tierCreditsRemainder = goldTierMax - overallCredits;
            tierHoursRemainder = tierCreditsRemainder / tierRate;

            ((ProgressBar)activity.findViewById(R.id.rewards_bronze_tier_progress_bar)).setProgress(100);
            ((ProgressBar)activity.findViewById(R.id.rewards_silver_tier_progress_bar)).setProgress(100);
            ((ProgressBar)activity.findViewById(R.id.rewards_gold_tier_progress_bar)).setProgress(100);
        }

        //Tier Level/Status
        TextView levelStatus = ((TextView)activity.findViewById(R.id.rewards_progress_status));
        levelStatus.setText(getString(R.string.tier_level, tierLevel));
        levelStatus.setTextColor(getResources().getColor(tierColor));

        //Tier summary
        tierCreditsRemainder = Math.round(tierCreditsRemainder * 100.0) / 100.0;
        tierHoursRemainder = Math.round(tierHoursRemainder * 100.0) / 100.0;
        String tierSummary = getString(R.string.tier_level_progress_summary,
                tierCreditsRemainder, tierHoursRemainder, nextTierLevel);
        TextView tierSummaryText = ((TextView)activity.findViewById(R.id.rewards_progress_tier_summary));
        tierSummaryText.setText("" + tierSummary);
        if(eliteMember)
            tierSummaryText.setVisibility(View.GONE);

        //Special Offer Btn
        Button specialOffers = (Button) activity.findViewById(R.id.rewards_progress_special_offers);
        specialOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //launch a new activity
                Intent marketPlaceIntent = new Intent(getActivity(), OfferListActivity.class);
                Bundle marketPlaceBundle = new Bundle();
                marketPlaceBundle.putString("title", getString(R.string.title_activity_offer_list));
                //marketPlaceBundle.putInt("position", position);
                marketPlaceBundle.putBoolean("isCampaignList", true);
                marketPlaceIntent.putExtras(marketPlaceBundle);
                startActivity(marketPlaceIntent);
            }
        });
    }

    private void populateActivityStream(Activity activity, ArrayList<DepositStream> parcelableStream) {
        ProgressBar activityStreamProgress = (ProgressBar) activity.findViewById(R.id.rewards_activity_stream_progress_bar);
        LinearLayout activityStreamContainer = (LinearLayout) activity.findViewById(R.id.rewards_activity_stream_container);
        activityStreamContainer.removeAllViews();

        activityStreamProgress.setVisibility(View.VISIBLE);
        activityStreamContainer.setVisibility(View.GONE);

        Button activityStreamLoadMoreBtn = (Button) activity.findViewById(R.id.rewards_activity_more_stream_button);
        activityStreamLoadMoreBtn.setEnabled(false);
        activityStreamLoadMoreBtn.setClickable(false);

        if(parcelableStream != null && parcelableStream.size() > 0) {
            //preserve insertion order
            LinkedHashMap<String, Double> creditMap = new LinkedHashMap<String, Double>();
            LinkedHashMap<String, Double> hourMap = new LinkedHashMap<String, Double>();

            for (DepositStream stream : parcelableStream) {
                String date = stream.getDate();
                double credits = stream.getAmount();
                double hrs = (stream.getAmount() * 60) / stream.getRate();

                Double dailyCredits = creditMap.get(date);
                Double dailyHours = hourMap.get(date);
                if (dailyCredits == null || dailyCredits == 0) {
                    creditMap.put(date, credits);
                    hourMap.put(date, hrs);
                } else {
                    dailyCredits = credits + dailyCredits;
                    dailyHours = hrs + dailyHours;
                    creditMap.put(date, dailyCredits);
                    hourMap.put(date, dailyHours);
                }
            }

            for (String date : creditMap.keySet()) {
                Double credits = creditMap.get(date);
                credits = Math.round(credits * 100.0) / 100.0; //round to 2 decimal places

                Double hrs = hourMap.get(date);
                hrs = Math.round(hrs / 60 * 100.0) / 100.0; //round to 2 decimal places

                DeviceActivityStreamItem activityStreamData = new DeviceActivityStreamItem(activity);
                activityStreamData.configData(date, credits + "", hrs + "");

                //add each activity stream data to the container
                activityStreamContainer.addView(activityStreamData);
            }

            //enable load more button
            activityStreamLoadMoreBtn.setEnabled(true);
            activityStreamLoadMoreBtn.setClickable(true);
            activityStreamLoadMoreBtn.setVisibility(View.VISIBLE);

            //no more activity
            ((TextView)activity.findViewById(R.id.rewards_activity_no_more_text)).setVisibility(View.GONE);
        }else{
            //disable load more button
            activityStreamLoadMoreBtn.setEnabled(false);
            activityStreamLoadMoreBtn.setClickable(false);
            activityStreamLoadMoreBtn.setVisibility(View.GONE);

            try {
                DeviceActivityStreamItem activityStreamData = new DeviceActivityStreamItem(activity);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date = sdf.parse(new Date().toString());
                activityStreamData.configData(date.toString(), "0.0", "0.0");

                //add each activity stream data to the container
                activityStreamContainer.addView(activityStreamData);
            }catch (Exception e){
                Log.e(TAG, e.getMessage(), e);
            }

            //no more activity
            ((TextView)activity.findViewById(R.id.rewards_activity_no_more_text)).setVisibility(View.VISIBLE);
        }

        activityStreamProgress.setVisibility(View.GONE);
        activityStreamContainer.setVisibility(View.VISIBLE);

        activityStreamLoadMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //launch history streaming activity
                Intent historyIntent = new Intent(getActivity(), HistoryActivityStream.class);
                startActivity(historyIntent);
            }
        });
    }

    private BroadcastReceiver mActivityStreamingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ArrayList<DepositStream> parcelableStream = intent.getParcelableArrayListExtra("parcelableStream");
            populateTopView();
            populateActivityStream(getActivity(), parcelableStream);
        }
    };
}
