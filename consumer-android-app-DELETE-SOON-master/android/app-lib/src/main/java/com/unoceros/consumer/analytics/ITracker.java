package com.unoceros.consumer.analytics;

import android.app.Activity;
import android.content.Context;

import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.UserInfo;

import org.json.JSONObject;

public interface ITracker {

    public void trackAppStart(Activity activity);

    public void trackStartActivity(Activity activity);

    public void trackResumeActivity(Activity activity);

    public void trackStopActivity(Activity activity);

    public void trackDestroyActivity(Activity activity);

    public void trackAppState(Activity activity, String states);

    public void trackPageView(Activity activity, String viewName);

    public void trackEvent(Context context, String eventName, JSONObject json);

    public void trackEvent(Activity activity, String eventName, JSONObject json);

    public void trackEvent(Activity activity, String eventName);

    public void trackEvent(Context context, String eventName);

    public void trackEventWithConnectionSpeed(Activity activity, String eventName);

    public void trackViewCampaign(Activity activity, String eventName, Campaign campaign);
    
    public void sendTrackedInformation(UserInfo userInfo);

    public void trackComputeAvailability(UserInfo userInfo);
}
