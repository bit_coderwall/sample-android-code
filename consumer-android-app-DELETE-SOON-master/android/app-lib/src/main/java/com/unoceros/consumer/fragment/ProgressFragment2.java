package com.unoceros.consumer.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.unoceros.consumer.HistoryActivityStream2;
import com.unoceros.consumer.OfferListActivity;
import com.unoceros.consumer.R;
import com.unoceros.consumer.UserPreferenceActivity;
import com.unoceros.consumer.adapter.ActivityStreamingAdapter;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.DepositStream;
import com.unoceros.consumer.model.db.ClaimedRewardsDAO;
import com.unoceros.consumer.services.ApplicationServices;
import com.unoceros.consumer.services.CreditServices;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.util.SyncUtility;
import com.unoceros.consumer.views.FeedImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Amit
 */
public class ProgressFragment2 extends BaseFragment {

    public static final String TAG = "ProgressFragment";
    public static final int MAX_DISPLAY_DAYS = 3;
    public static final String PERSIST_FILENAME = "stream1.tmp";

    private ArrayList<DepositStream> mParcelableStream;
    private ClaimedRewardsDAO mPersistentDao;

    public static ProgressFragment2 newInstance(boolean forceSync){
        ProgressFragment2 bf = new ProgressFragment2();
        Bundle option = bf.getArguments();
        if(option == null)
            option = new Bundle();
        option.putBoolean("forceSync", forceSync);
        bf.setArguments(option);
        return bf;
    }

    public static ProgressFragment2 newInstance(Bundle option){
        ProgressFragment2 bf = new ProgressFragment2();
        bf.setArguments(option);
        return bf;
    }

    public ProgressFragment2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_slickblue_main, container, false);
        setHasOptionsMenu(true);
        
        return view;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Context
        final Activity activity = getActivity();

        Bundle option = getArguments();
        boolean forceSync = option.getBoolean("forceSync", false);

        //create persistent datastore
        mPersistentDao = new ClaimedRewardsDAO(activity);
        mPersistentDao.open();
        
        //use stored version temporary
        boolean useCache = false;
        mParcelableStream = AppUtility.readFromInternalStorage(activity, ProgressFragment2.PERSIST_FILENAME);
        if(mParcelableStream != null) {
            useCache = true;
        }

        if(savedInstanceState != null && !useCache) {
            mParcelableStream = savedInstanceState.getParcelableArrayList("parcelableStream");
            if(mParcelableStream != null){
                useCache = true;
            }
        }

        if(!useCache) {
            //make an intent request to populate the activity stream
            loadStream(activity, forceSync);
        } else {
            //populate view with saved states
            if(!forceSync)
                populateActivityStream(activity, mParcelableStream);
            
            boolean isExpired = AppUtility.isStoredStreamExpired(activity, ProgressFragment2.PERSIST_FILENAME);
            if(forceSync || isExpired)
                loadStream(activity, forceSync);
        }
    }

    private void loadStream(Activity activity, boolean forceSync){
        Intent streamIntent = new Intent(AppUtility.ACTION_GET_ACTIVITY_STREAMING);
        streamIntent.putExtra("limit", 5);
        streamIntent.putExtra("offset", 0);
        streamIntent.putExtra("order", "desc");
        streamIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        activity.sendBroadcast(streamIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();

        //register local service
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mActivityStreamingReceiver, new IntentFilter("activity-streaming"));

        //populate the progress view without forcing sync
        populateTopView();
    }

    @Override
    public void onPause() {
        super.onPause();

        //unregister local service
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mActivityStreamingReceiver);

        mPersistentDao.close();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.status_menu, menu);

        // Set menu items
        menu.findItem(R.id.menu_item_sync);
        
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        int i = item.getItemId();
        if(i == android.R.id.home){
            // app icon in action bar clicked; go to previous activity or home
            return true;
        } else if (i == R.id.menu_item_sync) {
            ProgressBar activityStreamProgress = (ProgressBar) getActivity().findViewById(R.id.progress_timeline_stream_progress_bar);
            LinearLayout activityStreamContainer = (LinearLayout) getActivity().findViewById(R.id.progress_timeline_layout);
            activityStreamContainer.removeAllViews();

            activityStreamProgress.setVisibility(View.VISIBLE);
            activityStreamContainer.setVisibility(View.GONE);
            //sync progress fragment
            //simulate syncing
            new AsyncTask<Void, Integer, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    return null;
                }
                @Override
                protected void onPostExecute(Void status) {
                    try {
                        Thread.sleep(3500);
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage(), e);
                    } finally {
                        loadStream(getActivity(), true);
                    }
                }
            }.execute();
            
        }/* else if (i == R.id.menu_item_settings) {
            //open settings
            Intent pref = new Intent(getActivity(), UserPreferenceActivity.class);
            startActivity(pref);
        }*/
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("parcelableStream", mParcelableStream);
        super.onSaveInstanceState(outState);
    }


    private void populateTopView(){
        final Activity activity = getActivity();
        CreditServices creditServices = ApplicationServices.getInstance(activity.getApplication()).getCreditService();

        double overallCredits =  creditServices.getOverallBalance();
        double todayCredits =  creditServices.getTodayDeposit();
        double totalCredits = creditServices.getTotalBalance();
        double dailyAverage = creditServices.getDailyAverage();
        double networkAverage = creditServices.getNetworkAverage();

        //double dollarToCreditRate = 0.05; //dollar/credits
        double base = activity.getResources().getFraction(R.fraction.unoceros_initial_credit_deposit, 10, 10);
        double totalDollarValue = AppUtility.calculateDollar(activity, overallCredits-base); //Math.round(overallCredits * dollarToCreditRate * 100.0) / 100.0;

        //layout containing progress bar, and credits values
        final LinearLayout topLayout = (LinearLayout)activity.findViewById(R.id.progress_top_layout);

        // This thing can be deleted
        //top progress bar
        final ProgressBar progressBar = (ProgressBar)activity.findViewById(R.id.progress_top_progress_bar);
        progressBar.setVisibility(View.GONE);

        //Total's Balance (Available to spend)
        final TextView availableToSpend = ((TextView)activity.findViewById(R.id.progress_available_to_spend));
        availableToSpend.setText(""+totalCredits);

        //Total Value (Value to date)
        final TextView valueToDate = ((TextView)activity.findViewById(R.id.progress_value_to_date));
        valueToDate.setText("$"+totalDollarValue);

        //Alerts
        TextView alertText = ((TextView)activity.findViewById(R.id.rewards_progress_status_alert));
        alertText.setText(getString(R.string.alert_low_battery_level));
        alertText.setVisibility(View.GONE);
        if(!SyncUtility.canCompute(getActivity())) {
            alertText.setVisibility(View.VISIBLE);
        }

        //Last sync
        //Format: Last Sync: Sep 22 10:01 PM
        Date currentDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd hh:mm a");
        ((TextView) activity.findViewById(R.id.rewards_progress_last_sync)).setText("Last Sync: " + sdf.format(currentDate));

        final ScrollView scrollView = ((ScrollView) activity.findViewById(R.id.progress_bottom_scrollview));
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                //int scrollX = scrollView.getScrollX(); //for horizontalScrollView
                int scrollY = scrollView.getScrollY(); //for verticalScrollView

                int progress = 1000 - scrollY;
                if (progress <= 0)
                    progress = 1;
                float alpha = ((float) progress) / 1000;
                if (alpha > 1)
                    alpha = 1;
                if (alpha < 0.5)
                    alpha = 0.0f;

                //Log.d(TAG, "alpha: "+alpha+", scrollY: "+scrollY);
                topLayout.setAlpha(alpha);
            }
        });
        
        ImageButton redeemBtn = (ImageButton)activity.findViewById(R.id.main_redeem_logo_button);
        redeemBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent marketPlaceIntent = new Intent(activity, OfferListActivity.class);
                Bundle marketPlaceBundle = new Bundle();
                marketPlaceBundle.putString("title", activity.getString(R.string.title_activity_offer_list));
                marketPlaceBundle.putInt("position", 0);
                marketPlaceBundle.putBoolean("isCampaignList", true);
                marketPlaceIntent.putExtras(marketPlaceBundle);
                startActivity(marketPlaceIntent);
            }
        });
    }

    /**
     * 0 being transparent and 255 being opaque.
     * @param textView
     * @param alpha
     * @return
     */
    private boolean setTextAlpha(TextView textView, int alpha){
        textView.setTextColor(textView.getTextColors().withAlpha(alpha));
        textView.setHintTextColor(textView.getHintTextColors().withAlpha(alpha));
        textView.setLinkTextColor(textView.getLinkTextColors().withAlpha(alpha));
        if(textView.getBackground() != null)
            textView.getBackground().setAlpha(alpha);
        return true;
    }

    private void populateActivityStream2(Activity activity, ArrayList<DepositStream> parcelableStream) {
        ProgressBar activityStreamProgress = (ProgressBar) activity.findViewById(R.id.progress_timeline_stream_progress_bar);

        ImageButton activityStreamLoadMoreBtn = (ImageButton) activity.findViewById(R.id.progress_timeline_more_stream_button);
        activityStreamLoadMoreBtn.setEnabled(false);
        activityStreamLoadMoreBtn.setClickable(false);

        //the list view holding the streaming items
        ListView listView = ((ListView)activity.findViewById(R.id.rewards_activity_stream_list));
        listView.setEmptyView(activityStreamProgress);

        if(parcelableStream != null && parcelableStream.size() > 0){
            //create an empty adapter
            ActivityStreamingAdapter listAdapter = new ActivityStreamingAdapter(activity, R.layout.slickblue_list_history_credits_and_offers_timeline, parcelableStream, MAX_DISPLAY_DAYS);

            //remove scrollbars
            listView.setVerticalScrollBarEnabled(false);
            listView.setHorizontalScrollBarEnabled(false);
            listView.setAdapter(listAdapter);

            //enable load more button
            activityStreamLoadMoreBtn.setEnabled(true);
            activityStreamLoadMoreBtn.setClickable(true);
            activityStreamLoadMoreBtn.setVisibility(View.VISIBLE);
        }else{
            //disable load more button
            activityStreamLoadMoreBtn.setEnabled(false);
            activityStreamLoadMoreBtn.setClickable(false);
            activityStreamLoadMoreBtn.setVisibility(View.GONE);

            try {
                //create an empty adapter
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String dateStr = sdf.format(new Date());

                //create default stream object
                DepositStream streamItem = new DepositStream();
                streamItem.setRate(0.0);
                streamItem.setAmount(0.0);
                streamItem.setDate(dateStr);
                parcelableStream = new ArrayList<DepositStream>();
                parcelableStream.add(streamItem);
                ActivityStreamingAdapter listAdapter = new ActivityStreamingAdapter(activity, R.layout.slickblue_list_history_credits_and_offers_timeline, parcelableStream, MAX_DISPLAY_DAYS);

                //remove scrollbars
                listView.setVerticalScrollBarEnabled(false);
                listView.setHorizontalScrollBarEnabled(false);
                listView.setAdapter(listAdapter);
            }catch (Exception e){
                Log.e(TAG, e.getMessage(), e);
            }
        }

        setListViewHeightBasedOnChildren(listView);
        //activityStreamProgress.setVisibility(View.GONE);
        activityStreamLoadMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //launch history streaming activity
                Intent historyIntent = new Intent(getActivity(), HistoryActivityStream2.class);
                startActivity(historyIntent);
            }
        });
    }

    /**** Method for Setting the Height of the ListView dynamically.
     **** Hack to fix the issue of not showing all the items of the ListView
     **** when placed inside a ScrollView  ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void populateActivityStream(Activity activity, ArrayList<DepositStream> parcelableStream) {
        ProgressBar activityStreamProgress = (ProgressBar) activity.findViewById(R.id.progress_timeline_stream_progress_bar);
        LinearLayout activityStreamContainer = (LinearLayout)activity.findViewById(R.id.progress_timeline_layout);
        activityStreamContainer.removeAllViews();

        activityStreamProgress.setVisibility(View.VISIBLE);
        activityStreamContainer.setVisibility(View.GONE);

        ImageButton activityStreamLoadMoreBtn = (ImageButton) activity.findViewById(R.id.progress_timeline_more_stream_button);
        activityStreamLoadMoreBtn.setEnabled(false);
        activityStreamLoadMoreBtn.setClickable(false);

        //loader of feed images
        ImageLoader imageLoader = AppController.getInstance().getImageLoader();

        if(parcelableStream != null && parcelableStream.size() > 0) {
            //preserve insertion order
            LinkedHashMap<String, Double> creditMap = new LinkedHashMap<String, Double>();
            LinkedHashMap<String, Double> hourMap = new LinkedHashMap<String, Double>();

            for (DepositStream stream : parcelableStream) {
                String date = stream.getDate();
                double credits = stream.getAmount();
                double hrs = (stream.getAmount() * 60) / stream.getRate();

                Double dailyCredits = creditMap.get(date);
                Double dailyHours = hourMap.get(date);
                if (dailyCredits == null || dailyCredits == 0) {
                    creditMap.put(date, credits);
                    hourMap.put(date, hrs);
                } else {
                    dailyCredits = credits + dailyCredits;
                    dailyHours = hrs + dailyHours;
                    creditMap.put(date, dailyCredits);
                    hourMap.put(date, dailyHours);
                }
            }

            int daysCounter = 0;
            for (String dateStr : creditMap.keySet()) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
                    SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
                    Date date = sdf.parse(dateStr);
                    String monthStr = monthFormat.format(date);
                    String dayStr = dayFormat.format(date);

                    Double credits = creditMap.get(dateStr);
                    credits = Math.round(credits * 100.0) / 100.0; //round to 2 decimal places

                    Double hrs = hourMap.get(dateStr);
                    hrs = Math.round(hrs / 60 * 100.0) / 100.0; //round to 2 decimal places

                    View activityStreamData = activity.getLayoutInflater().inflate(R.layout.slickblue_list_history_credits_and_offers_timeline, null);
                    TextView monthText = (TextView)activityStreamData.findViewById(R.id.streaming_month_timeline);
                    TextView dayText = (TextView)activityStreamData.findViewById(R.id.streaming_day_timeline);
                    TextView creditText = (TextView)activityStreamData.findViewById(R.id.streaming_daily_credits);
                    TextView hourText = (TextView)activityStreamData.findViewById(R.id.streaming_daily_hours);
                    monthText.setText(monthStr);
                    dayText.setText(dayStr);
                    creditText.setText(credits+"");
                    hourText.setText(hrs + "");

                    List<Campaign> history = mPersistentDao.retrieveByDate(dateStr); //OfferDetailFragment.retrieveFromDataStore(activity, Const.DATASTORE_HISTORY_NAME);
                    //Log.d(TAG, "history items: "+history+", date: "+dateStr);

                    ViewGroup rewardsHistoryLayout = (ViewGroup)activityStreamData.findViewById(R.id.streaming_rightcolumn_history);
                    rewardsHistoryLayout.removeAllViews();
                    if(history != null && history.size() > 0 ){
                        for(int i = 0; i < history.size(); i++) {
                            View rewardsView = activity.getLayoutInflater().inflate(R.layout.list_item_with_image_title_desc2_small, null);
                            FeedImageView thumb = (FeedImageView) rewardsView.findViewById(R.id.thumbnail);

                            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                            params.setMargins(0, 10, 0, 0); // llp.setMargins(left, top, right, bottom);
                            thumb.setLayoutParams(params);

                            Campaign campaign = history.get(i);
                            thumb.setImageUrl(campaign.getThumbnailUrl(), imageLoader);
                            thumb.requestLayout();

                            rewardsHistoryLayout.addView(rewardsView);
                        }
                    }else{
                        View rewardsView = activity.getLayoutInflater().inflate(R.layout.list_item_history_no_claimed_rewards, null);
                        TextView text = (TextView) rewardsView.findViewById(R.id.streaming_no_activity_indicator);
                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        params.setMargins(0, 57, 0, 0); // llp.setMargins(left, top, right, bottom);
                        rewardsView.setLayoutParams(params);
                        rewardsHistoryLayout.addView(rewardsView);
                    }

                    //Hidden for slickblue version of app
                    View lineSeperator = activityStreamData.findViewById(R.id.list_item_line_seperator);
                    lineSeperator.setVisibility(View.GONE);

                    //add each activity stream data to the container
                    activityStreamContainer.addView(activityStreamData);

                    daysCounter++;
                    if(daysCounter >= MAX_DISPLAY_DAYS)
                        break;
                }catch (Exception e){
                    Log.e(TAG, e.getMessage(), e);
                }
            }

            //enable load more button
            activityStreamLoadMoreBtn.setEnabled(true);
            activityStreamLoadMoreBtn.setClickable(true);
            activityStreamLoadMoreBtn.setVisibility(View.VISIBLE);
        } else {
            //disable load more button
            activityStreamLoadMoreBtn.setEnabled(false);
            activityStreamLoadMoreBtn.setClickable(false);
            activityStreamLoadMoreBtn.setVisibility(View.GONE);

            //load default empty view, perhaps this is the first view after installation???
            try {
                SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
                SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
                Date date = new Date();
                String monthStr = monthFormat.format(date);
                String dayStr = dayFormat.format(date);

                View activityStreamData = activity.getLayoutInflater().inflate(R.layout.slickblue_list_history_credits_and_offers_timeline, null);
                TextView monthText = (TextView)activityStreamData.findViewById(R.id.streaming_month_timeline);
                TextView dayText = (TextView)activityStreamData.findViewById(R.id.streaming_day_timeline);
                TextView creditText = (TextView)activityStreamData.findViewById(R.id.streaming_daily_credits);
                TextView hourText = (TextView)activityStreamData.findViewById(R.id.streaming_daily_hours);
                monthText.setText(monthStr);
                dayText.setText(dayStr);
                creditText.setText("0.0");
                hourText.setText("0.0");

                List<Campaign> history = mPersistentDao.retrieveByDate(null); //OfferDetailFragment.retrieveFromDataStore(activity, Const.DATASTORE_HISTORY_NAME);
                ViewGroup rewardsHistoryLayout = (ViewGroup)activityStreamData.findViewById(R.id.streaming_rightcolumn_history);
                rewardsHistoryLayout.removeAllViews();
                if(history != null && history.size() > 0 ){
                    for(int i = 0; i < history.size(); i++) {
                        View rewardsView = activity.getLayoutInflater().inflate(R.layout.list_item_with_image_title_desc2_small, null);
                        FeedImageView thumb = (FeedImageView) rewardsView.findViewById(R.id.thumbnail);

                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                        params.setMargins(0, 57, 0, 0); // llp.setMargins(left, top, right, bottom);
                        thumb.setLayoutParams(params);

                        Campaign campaign = history.get(i);
                        thumb.setImageUrl(campaign.getThumbnailUrl(), imageLoader);
                        thumb.requestLayout();

                        rewardsHistoryLayout.addView(rewardsView);
                    }
                }else{
                    View rewardsView = activity.getLayoutInflater().inflate(R.layout.list_item_history_no_claimed_rewards, null);
                    TextView text = (TextView) rewardsView.findViewById(R.id.streaming_no_activity_indicator);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0, 57, 0, 0); // llp.setMargins(left, top, right, bottom);
                    rewardsView.setLayoutParams(params);
                    rewardsHistoryLayout.addView(rewardsView);
                }

                //hidden for slickblue version of app
                //View lineSeperator = activityStreamData.findViewById(R.id.list_item_line_seperator);
                //lineSeperator.setVisibility(View.GONE);

                //add each activity stream data to the container
                activityStreamContainer.addView(activityStreamData);
            }catch (Exception e){
                Log.e(TAG, e.getMessage(), e);
            }
        }

        activityStreamProgress.setVisibility(View.GONE);
        activityStreamContainer.setVisibility(View.VISIBLE);

        activityStreamLoadMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //launch history streaming activity
                Intent historyIntent = new Intent(getActivity(), HistoryActivityStream2.class);
                startActivity(historyIntent);
            }
        });
    }

    private BroadcastReceiver mActivityStreamingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean success = intent.getBooleanExtra("success", false);
            populateTopView();

            if(success) {
                ArrayList<DepositStream> parcelableStream = intent.getParcelableArrayListExtra("parcelableStream");
                populateActivityStream(getActivity(), parcelableStream);
                mParcelableStream = parcelableStream;
                
                //store activity stream temporary
                AppUtility.writeToInternalStorage(context, ProgressFragment2.PERSIST_FILENAME, parcelableStream);
            }else{
                String errorMsg = intent.getStringExtra("message");
                errorMsg = "Sorry, error occurred while updating your activities. "+errorMsg;
                Toast.makeText(context, errorMsg, Toast.LENGTH_LONG).show();

                ArrayList<DepositStream> parcelableStream = AppUtility.readFromInternalStorage(context, ProgressFragment2.PERSIST_FILENAME);
                populateActivityStream(getActivity(), parcelableStream);
            }
        }
    };
}
