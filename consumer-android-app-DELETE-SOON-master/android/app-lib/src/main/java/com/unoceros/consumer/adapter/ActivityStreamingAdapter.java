package com.unoceros.consumer.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.unoceros.consumer.R;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.model.Campaign;
import com.unoceros.consumer.model.DepositStream;
import com.unoceros.consumer.model.StreamingHistoryItem;
import com.unoceros.consumer.model.db.ClaimedRewardsDAO;
import com.unoceros.consumer.util.Log;
import com.unoceros.consumer.views.FeedImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Ejiro.
 */
public class ActivityStreamingAdapter extends BaseAdapter{

    private static final String TAG = "ActivityStreamingAdapter";

    private Context context;
    private List<StreamingHistoryItem> listItems;
    private int layoutResID;
    private LayoutInflater inflater;
    private ImageLoader imageLoader;
    private ClaimedRewardsDAO mPersistentDao;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public ActivityStreamingAdapter(Context context, int resource, List<DepositStream> parcelableStream, int maxItem) {
        this.context = context;
        this.layoutResID = resource;
        this.inflater = ((Activity) context).getLayoutInflater();
        this.imageLoader = AppController.getInstance().getImageLoader();
        this.mPersistentDao = new ClaimedRewardsDAO(context);

        //preserve insertion order
        LinkedHashMap<String, Double> creditMap = new LinkedHashMap<String, Double>();
        LinkedHashMap<String, Double> hourMap = new LinkedHashMap<String, Double>();
        listItems = new ArrayList<StreamingHistoryItem>();

        if(parcelableStream != null && parcelableStream.size() > 0) {
            for (DepositStream stream : parcelableStream) {
                String date = stream.getDate();
                double credits = stream.getAmount();
                double hrs = (stream.getAmount() * 60) / stream.getRate();

                Double dailyCredits = creditMap.get(date);
                Double dailyHours = hourMap.get(date);
                if (dailyCredits == null || dailyCredits == 0) {
                    creditMap.put(date, credits);
                    hourMap.put(date, hrs);
                } else {
                    dailyCredits = credits + dailyCredits;
                    dailyHours = hrs + dailyHours;
                    creditMap.put(date, dailyCredits);
                    hourMap.put(date, dailyHours);
                }
            }

            mPersistentDao.open();
            int daysCounter = 0;
            for (String dateStr : creditMap.keySet()) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
                    SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
                    Date date = sdf.parse(dateStr);
                    String monthStr = monthFormat.format(date);
                    String dayStr = dayFormat.format(date);

                    Double credits = creditMap.get(dateStr);
                    credits = Math.round(credits * 100.0) / 100.0; //round to 2 decimal places

                    Double hrs = hourMap.get(dateStr);
                    hrs = Math.round(hrs / 60 * 100.0) / 100.0; //round to 2 decimal places

                    StreamingHistoryItem streamItem = new StreamingHistoryItem();
                    streamItem.setDate(date);
                    streamItem.setMonth(monthStr);
                    streamItem.setDay(dayStr);
                    streamItem.setCredit(credits+"");
                    streamItem.setHour(hrs+"");
                    streamItem.setHistory(mPersistentDao.retrieveByDate(dateStr)); //history); //new ArrayList<Campaign>();
                    listItems.add(streamItem);

                    if(maxItem > 0){
                        daysCounter++;
                        if(daysCounter >= maxItem)
                            break;
                    }
                }catch (Exception e){
                    Log.e(TAG, e.getMessage(), e);
                }
            }

            mPersistentDao.close();
        }
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        //returns the date as string
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        StreamingItemViewsHolder streamingHolder;
        View view = convertView;

        if (view == null) {
            streamingHolder = new StreamingItemViewsHolder();

            view = inflater.inflate(layoutResID, parent, false);
            streamingHolder.monthText = (TextView) view.findViewById(R.id.streaming_month_timeline);
            streamingHolder.dayText = (TextView) view.findViewById(R.id.streaming_day_timeline);
            streamingHolder.creditText = (TextView) view.findViewById(R.id.streaming_daily_credits);
            streamingHolder.hourText = (TextView) view.findViewById(R.id.streaming_daily_hours);
            streamingHolder.rewardsHistoryLayout = (LinearLayout)view.findViewById(R.id.streaming_rightcolumn_history);

            view.setTag(streamingHolder);
        } else {
            streamingHolder = (StreamingItemViewsHolder) view.getTag();
        }

        StreamingHistoryItem streamItem = (StreamingHistoryItem)getItem(position);
        if(streamItem != null){
            streamingHolder.monthText.setText(streamItem.getMonth());
            streamingHolder.dayText.setText(streamItem.getDay());
            streamingHolder.creditText.setText(streamItem.getCredit());
            streamingHolder.hourText.setText(streamItem.getHour());

            streamingHolder.rewardsHistoryLayout.removeAllViews();
            int width_dps = 89;
            int height_dps = 50;
            final float scale = context.getResources().getDisplayMetrics().density;
            
            LinearLayout.LayoutParams rewardsParams = 
                    new LinearLayout.LayoutParams((int) (width_dps * scale + 0.5f), (int) (height_dps * scale + 0.5f));
            rewardsParams.gravity = Gravity.LEFT;
            rewardsParams.setMargins(2, 2, 2, 2);
            
            if(streamItem.getHistory() != null && streamItem.getHistory().size() > 0){
                for(int i = 0; i < streamItem.getHistory().size(); i++){
                    View thumbsView = inflater.inflate(R.layout.list_item_with_image_title_desc2_small, null);
                    FeedImageView thumb = (FeedImageView)thumbsView.findViewById(R.id.thumbnail);

                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    //        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    params.setMargins(0, 0, 0, 0); // llp.setMargins(left, top, right, bottom);
                    thumb.setLayoutParams(params);
                    thumbsView.setLayoutParams(rewardsParams);

                    Campaign campaign = streamItem.getHistory().get(i);
                    thumb.setImageUrl(campaign.getThumbnailUrl(), imageLoader);
                    thumb.requestLayout();

                    streamingHolder.rewardsHistoryLayout.addView(thumbsView);
                    Log.d(TAG, "Adding campaign with uuid: "+campaign.getUuid());
                }
            }else{
                View rewardsView = inflater.inflate(R.layout.list_item_history_no_claimed_rewards, null);
                TextView text = (TextView) rewardsView.findViewById(R.id.streaming_no_activity_indicator);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                //        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 57, 0, 0); // llp.setMargins(left, top, right, bottom);
                rewardsView.setLayoutParams(params);
                streamingHolder.rewardsHistoryLayout.addView(rewardsView);
            }
        }
        return view;
    }


    private static class StreamingItemViewsHolder {
        TextView monthText, dayText, creditText, hourText;
        LinearLayout rewardsHistoryLayout;
    }
}
