package com.unoceros.consumer.social;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.unoceros.consumer.NoNetworkActivity;
import com.unoceros.consumer.R;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.model.DeviceHardware;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.services.ApplicationServices;
import com.unoceros.consumer.services.NetworkRequest;
import com.unoceros.consumer.util.AppUtility;


/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class EmailSignInActivity extends BaseSignInActivity{

    public final static int EMAIL_SIGNIN_REQUEST = 4896;

	/**
	 * A dummy authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.
	 */
	private static final String[] DUMMY_CREDENTIALS = new String[] {
		"foo@example.com:hello", "bar@example.com:world" };

	/**
	 * The default email to populate the email field with.
	 */
	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";
	
	//private static final String REDEEM_CODE_REQUEST = "http://54.200.85.61:8000/redeeminvitecode?token=CnH09qfwnAFnyDgZ9O4f&code=";
	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// Values for email and password at the time of the login attempt.
	private String mEmail;
	private String mEmailConfirm;
	private String mName;
	//private String mInviteCode;

	// UI references.
    private EditText mNameView;
	private EditText mEmailView;
	private EditText mEmailConfirmView;
	//private EditText mInviteCodeView;
	//private EditText mPasswordView;
	//private View mAgeRangeHeaderView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	private int mAgeRange = 1;

    private DeviceHardware hardware;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_up);

        //enable up navigation
        if(getActionBar() != null)
            getActionBar().setDisplayHomeAsUpEnabled(true);

		// Set up the login form.
        mNameView = (EditText) findViewById(R.id.sign_up_login_name);
        mEmail = getIntent().getStringExtra(EXTRA_EMAIL);
		mEmailView = (EditText) findViewById(R.id.sign_up_login_email);
		mEmailView.setText(mEmail);
		mEmailConfirmView = (EditText) findViewById(R.id.sign_up_login_email_confirm);
        mEmailConfirmView.setText(mEmail);

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);
        mLoginStatusView.setVisibility(View.GONE);
        mLoginFormView.setVisibility(View.VISIBLE);

        //hardware reference
        hardware = ApplicationServices.getInstance(this.getApplication()).getDeviceHardware();

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});

	}


    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //check network connectivity
        AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		return true;
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
            default:{
                //
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mNameView.setError(null);
        mEmailConfirmView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mEmailConfirm = mEmailConfirmView.getText().toString();
		mName = mNameView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
        /*
		if (TextUtils.isEmpty(mName)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mName.indexOf(" ") == -1) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}*/

        // Check for a valid name
        if (TextUtils.isEmpty(mName)) {
            mNameView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        }

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
        //} else if (!mEmail.contains("@")) {
        } else if (!AppUtility.isValidateEmail(mEmail)) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}
		
		// Check that the email addresses match
		if (!mEmail.equalsIgnoreCase(mEmailConfirm)) {
			mEmailConfirmView.setError(getString(R.string.error_email_not_matching));
			focusView = mEmailConfirmView;
			cancel = true;
		}
		
		// Check that the user chose an age range.
		if (mAgeRange < 1) {
			// No age range chosen.
			showAgeRangeErrorToast();
			//focusView = mAgeRangeHeaderView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			if(focusView != null)
                focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	private void showAgeRangeErrorToast() {
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, R.string.age_range_error, duration);
		toast.show();
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
			.alpha(show ? 1 : 0)
			.setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginStatusView.setVisibility(show ? View.VISIBLE
							: View.GONE);
				}
			});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
			.alpha(show ? 0 : 1)
			.setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginFormView.setVisibility(show ? View.GONE
							: View.VISIBLE);
				}
			});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}


	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			for (String credential : DUMMY_CREDENTIALS) {
				String[] pieces = credential.split(":");
				if (pieces[0].equals(mEmail)) {
					// Account exists, return true if the password matches.
					return pieces[1].equals(mName);
				}
			}

			// TODO: register the new account here.
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			if (success) {
				AppState.instance().setAgeRange(mAgeRange);
				AppState.instance().setUsername(mName);
				AppState.instance().setEmailAddress(mEmail);

                if (!AppState.instance().isSignedIn())
				    AppState.instance().signIn();

                //((TextView)findViewById(R.id.login_status_message)).setText("Welcome " + mName+ "! One moment please!");
                UserInfo userInfo = AppController.getInstance().getUserInfo();
                userInfo.setUserId(hardware.getUniqueDeviceId().toString());
                userInfo.setUserName(mName);
                userInfo.setUserEmail(mEmail);
                userInfo.setAge(mAgeRange);

                setResult(RESULT_OK);
                affiliateProfileUpdate(userInfo);
                TrackingManager.getInstance(AppController.getInstance()).
                        trackEvent(EmailSignInActivity.this, "Email Sign-up");
                /*
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            Thread.sleep(3500);
                        }catch (Exception e){}
                        //finish();
                    }
                }).start();*/
			} else {
                setResult(RESULT_CANCELED);
                finish();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			//showProgress(false);
            setResult(RESULT_CANCELED);
            finish();
		}
	}

    public void signOut() {

    }

    public void revokeAccess(){

    }
}
