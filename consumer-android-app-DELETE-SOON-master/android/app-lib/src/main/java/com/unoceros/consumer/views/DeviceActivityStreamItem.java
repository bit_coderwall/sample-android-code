package com.unoceros.consumer.views;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.unoceros.consumer.R;

/**
 * Created by Amit
 */
public class DeviceActivityStreamItem extends LinearLayout {

    private static final String TAG = "ExtendedForecastLayout";
    private Context mContext;
    private LayoutInflater mInflater;

    private LinearLayout mRoot;
    private LayoutParams mParams;
    private LinearLayout mActivityData;

    public DeviceActivityStreamItem(Context context) {
        super(context);

        mContext = context;
        mInflater = LayoutInflater.from(mContext);

		/*
		 	android:orientation="vertical"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginRight="5dp"
            android:layout_marginLeft="5dp"
            android:layout_gravity="center"
		 */
        int five_dip = mContext.getResources().getDimensionPixelSize(R.dimen.five_dip);

        mParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        //mParams.setMargins(five_dip, 0, five_dip, 0);

        mRoot = this;
        mRoot.setGravity(Gravity.CENTER);
        mRoot.setOrientation(VERTICAL);
        mRoot.setLayoutParams(mParams);

        //daily container
        mActivityData = (LinearLayout) mInflater.inflate(R.layout.rewards_activity_stream_content, null);
    }

    public void configData(String date, String credits, String hrs){
        TextView dateText = (TextView) mActivityData.findViewById(R.id.rewards_activity_date);
        TextView creditText = (TextView) mActivityData.findViewById(R.id.rewards_activity_credit);
        TextView hourText = (TextView) mActivityData.findViewById(R.id.rewards_activity_hrs);

        dateText.setText(date); //2014-09-30 => yyyy-MM-dd
        creditText.setText(credits);
        hourText.setText("~"+hrs); //approximately

        //add to layout root
        mRoot.addView(mActivityData, mParams);
    }
}
