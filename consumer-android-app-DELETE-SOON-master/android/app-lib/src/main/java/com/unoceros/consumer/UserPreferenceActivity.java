package com.unoceros.consumer;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.MenuItem;

import com.flurry.android.FlurryAgent;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.services.ApplicationServices;
import com.unoceros.consumer.util.AppRater;
import com.unoceros.consumer.util.AppUtility;

/**
 * Created by Amit
 */
public class UserPreferenceActivity extends PreferenceActivity
        implements SharedPreferences.OnSharedPreferenceChangeListener{

    public final static int RESULT_SETTINGS = 2020;
    private UserPreferenceActivity mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.user_preferences);

        mContext = this;

        //enable up navigation
        actionBarSetup();

        // set listeners
        Preference about = findPreference( "prefAbout" );
        about.setSummary("App Version: "+getString(R.string.app_version)+"\nApp UUID: "+AppUtility.getUniqueUserID(this));
        about.setOnPreferenceClickListener( new Preference.OnPreferenceClickListener(){
            @Override
            public boolean onPreferenceClick(Preference preference) {
                startActivity(new Intent(mContext , AboutPreferencesActivity.class));
                return false;
            }
        });

        Preference sd = findPreference( "prefShareApp" );
        sd.setOnPreferenceClickListener( new Preference.OnPreferenceClickListener(){
            @Override
            public boolean onPreferenceClick(Preference preference) {

                //track sharing event
                TrackingManager.getInstance(getApplication()).trackEvent(mContext, "shareApp");

                String title = mContext.getString(R.string.share_title);
                String link = mContext.getString(R.string.share_download_link);
                String desc = mContext.getString(R.string.share_description);

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = (desc == null || desc.isEmpty())?link:desc+"\n"+link;
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, title);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TITLE, title);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                return false;
            }
        });

        Preference sf = findPreference( "prefContact" );
        if(sf != null){
            sf.setOnPreferenceClickListener( new Preference.OnPreferenceClickListener(){
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    String deviceInfo = ApplicationServices.getInstance(mContext.getApplication())
                            .getDeviceHardware().toString();
                    String body =
                            "---------------------------------\n"+
                            mContext.getString(R.string.contact_support_email_body)+"\n"
                            +deviceInfo+"\n"+
                            "---------------------------------";

                    StringBuffer buffer = new StringBuffer();
                    buffer.append("mailto:");
                    buffer.append( mContext.getString(R.string.contact_support_email_address));
                    buffer.append("?subject=");
                    buffer.append(mContext.getString(R.string.contact_support_email_subject));
                    buffer.append("&body=");
                    buffer.append(body);
                    String uriString = buffer.toString().replace(" ", "%20");
                    startActivity(Intent.createChooser(new Intent(Intent.ACTION_SENDTO, Uri.parse(uriString)),
                            "Email Support"));

                    return false;
                }
            });
        }

        Preference rta = findPreference( "prefRateApp" );
        if(rta != null){
            rta.setOnPreferenceClickListener( new Preference.OnPreferenceClickListener(){
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    try{
                        // need to clean up & check whether goog play is installed
                        String packageName = mContext.getPackageName();
                        Intent intent = AppRater.showMarketAppstore(mContext, packageName);
                        if(intent != null)
                            startActivity(intent);
                    }catch(Exception e){
                        //do nothing
                    }
                    return false;
                }
            });
        }

        Preference logout = findPreference( "prefLogout" );
        if(logout != null){
            logout.setOnPreferenceClickListener( new Preference.OnPreferenceClickListener(){
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    if(AppState.instance().isSignedIn())
                        AppState.instance().signOut();
                    startActivity(new Intent(mContext , SignUpSlickblueActivity.class));
                    setResult(RESULT_OK);
                    finish();
                    return false;
                }
            });
        }
    }

    private void openWebView(String title, String url){
        if(TextUtils.isEmpty(title))
            title="Browser";

        Uri link =  Uri.parse(url);
        Bundle webBundle = new Bundle();
        Intent intent = new Intent();
        webBundle.putParcelable("uri", link);
        webBundle.putString(ModalWebViewActivity.EXTRA_TITLE, title);
        intent.setClass(mContext, ModalWebViewActivity.class);
        intent.putExtras(webBundle);
        startActivity(intent);
    }

    /**
     * Sets the Action Bar for new Android versions.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        ActionBar ab = getActionBar();
        if(ab!= null){
            ab.setDisplayHomeAsUpEnabled(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                ab.setTitle(this.getString(R.string.app_name));
                ab.setSubtitle("settings");
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
            default:{
                //
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }


    @Override
    protected void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals("prefNotification")){
        }
    }
}