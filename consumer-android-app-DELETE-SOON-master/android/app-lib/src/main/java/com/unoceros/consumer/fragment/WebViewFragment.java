package com.unoceros.consumer.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.unoceros.consumer.ModalWebViewActivity;
import com.unoceros.consumer.R;

/**
 * Created by Amit
 */
public class WebViewFragment extends BaseFragment {

    public final static String TAG = WebViewFragment.class.getSimpleName();

    public WebViewFragment() {
        // TODO Auto-generated constructor stub
    }

    // newInstance constructor for creating fragment with arguments
    public static WebViewFragment newInstance(String url) {
        WebViewFragment web = new WebViewFragment();
        Bundle args = new Bundle();
        args.putString("url", url);
        web.setArguments(args);
        return web;
    }

    public static WebViewFragment newInstance() {
        WebViewFragment web = new WebViewFragment();
        return web;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_webview, container, false);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WebView webView = (WebView) view.findViewById(R.id.fragment_webView);

        String url = getArguments().getString("url");
        if(url != null && url != "") {
            //webView.loadUrl(url);
            openWebView("Browser", url);
        }else{
            //show unoceros home page
            webView.loadUrl("http://www.unoceros.com");
        }
    }

    public void openWebView(String title, String url){
        Uri link =  Uri.parse(url);
        Bundle webBundle = new Bundle();
        Intent intent = new Intent();
        webBundle.putParcelable("uri", link);
        webBundle.putString(ModalWebViewActivity.EXTRA_TITLE, title);
        intent.setClass(this.getActivity(), ModalWebViewActivity.class);
        intent.putExtras(webBundle);
        startActivity(intent);

        if (mListener != null) {
            mListener.onFragmentInteraction(null);
        }
    }
}
