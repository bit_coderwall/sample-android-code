package com.unoceros.consumer.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.unoceros.consumer.R;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WalkthroughFragmentA#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WalkthroughFragmentA extends Fragment {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment WalkthroughFragmentA.
     */
    public static WalkthroughFragmentA newInstance(Bundle args) {
        WalkthroughFragmentA fragment = new WalkthroughFragmentA();
        fragment.setArguments(args);
        return fragment;
    }

    public WalkthroughFragmentA() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_walkthrough_a, container, false);
        Bundle bundle = getArguments();
        boolean isHowItWorks = true;
        if(bundle != null)
            isHowItWorks = bundle.getBoolean("isHowItWorks");

        TextView header = (TextView) rootView.findViewById(R.id.header);
        TextView text2 = (TextView) rootView.findViewById(R.id.text2);
        //ImageButton rightBtn = (ImageButton) rootView.findViewById(R.id.button_right);
        
        if(isHowItWorks) {
            header.setText(R.string.howitworks);
            text2.setText(R.string.blurb1);
            //rightBtn.setVisibility(View.INVISIBLE);
        }
        
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Activity activity = getActivity();
    }
}
