package com.unoceros.consumer.social;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.unoceros.consumer.NoNetworkActivity;
import com.unoceros.consumer.R;
import com.unoceros.consumer.analytics.TrackingManager;
import com.unoceros.consumer.app.AppController;
import com.unoceros.consumer.app.AppState;
import com.unoceros.consumer.model.UserInfo;
import com.unoceros.consumer.util.AppUtility;
import com.unoceros.consumer.util.Log;

/**
 * Created by Amit
 */
public class GoogleSignInActivity extends BaseSignInActivity{

    private final static String TAG = "GoogleSignInActivity";

    public final static int GOOGLE_SIGNIN_REQUEST = 4894;

    /* Client used to interact with Google APIs. */
    /* Request code used to invoke sign in user interactions. */
    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;

    private Activity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_progress);

        activity = this;
        mSignInClicked = true;

        initGoogleLoginClient();
    }

    private void checkNetworkConnection() {
        if (!AppUtility.haveNetworkConnection(this)) {
            //check network connectivity
            AppUtility.checkNetworkConnection(this, NoNetworkActivity.REQUEST_CODE);
        } else {
            if(mGoogleApiClient != null && !mGoogleApiClient.isConnected())
                mGoogleApiClient.connect();

            if (mGoogleApiClient != null && !mGoogleApiClient.isConnecting()) {
                mSignInClicked = true;
                resolveGoogleSignInErrors();
            }
        }
    }
    private void initGoogleLoginClient(){
        int code = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if(code == ConnectionResult.SUCCESS) {
            mGoogleApiClient = new GoogleApiClient.Builder(activity)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                            Log.d(TAG, "onConnected:: connection succeed: "+bundle);
                            // We've resolved any connection errors.  mGoogleApiClient can be used to
                            // access Google APIs on behalf of the user.
                            Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(new ResultCallback<People.LoadPeopleResult>() {
                                @Override
                                public void onResult(People.LoadPeopleResult loadPeopleResult) {

                                }
                            });

                            try {
                                //try getting user
                                Person user = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                                googleUserIsConnected(user);
                            }catch (Exception e){
                                Log.e(TAG, e.getMessage(), e);
                            }
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            mGoogleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult result) {
                            Log.d(TAG, "onConnectionFailed:: connection failed: "+result);
                            if (!mIntentInProgress) {
                                Log.d(TAG, "onConnectionFailed:: attempt resolution failure");
                                // Store the ConnectionResult so that we can use it later when the user clicks
                                // 'sign-in'.
                                mConnectionResult = result;

                                if (mSignInClicked) {
                                    // The user has already clicked 'sign-in' so we attempt to resolve all
                                    // errors until the user is signed in, or they cancel.
                                    resolveGoogleSignInErrors();
                                } else {
                                    handleGooglePlayServices(mConnectionResult, result.getErrorCode());
                                }
                            }
                            setResult(RESULT_CANCELED);
                            //finish();
                        }
                    })
                    .addApi(Plus.API)
                    .addScope(Plus.SCOPE_PLUS_LOGIN) //.addScope(Plus.SCOPE_PLUS_PROFILE)
                    .build();

        }else{
            handleGooglePlayServices(mConnectionResult, code);
        }
    }


    private boolean handleGooglePlayServices(ConnectionResult conn, int code) {
        Log.d(TAG, "handling google play service issues: " + code);

        boolean reconnect = false;
        switch(code){
            case ConnectionResult.SERVICE_MISSING:
                GooglePlayServicesUtil.getErrorDialog(code, activity, ConnectionResult.SERVICE_MISSING).show();
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                GooglePlayServicesUtil.getErrorDialog(code, activity, ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED).show();
                break;
            case ConnectionResult.SERVICE_DISABLED:
                GooglePlayServicesUtil.getErrorDialog(code, activity, ConnectionResult.SERVICE_DISABLED).show();
                break;
            case ConnectionResult.SIGN_IN_REQUIRED:
                ConnectionResult conn3 = conn!=null?conn:new ConnectionResult(ConnectionResult.SIGN_IN_REQUIRED, null);
                try {
                    conn3.startResolutionForResult(activity, ConnectionResult.SIGN_IN_REQUIRED);
                } catch (IntentSender.SendIntentException e) {
                    Log.e(TAG, e.getMessage(), e);
                    reconnect = true;
                }
                break;
            case ConnectionResult.INVALID_ACCOUNT:
                ConnectionResult conn6 = conn!=null?conn:new ConnectionResult(ConnectionResult.INVALID_ACCOUNT, null);
                try {
                    conn6.startResolutionForResult(activity, ConnectionResult.INVALID_ACCOUNT);
                } catch (IntentSender.SendIntentException e) {
                    Log.e(TAG, e.getMessage(), e);
                    reconnect = true;
                }
                break;
            case ConnectionResult.RESOLUTION_REQUIRED:
                ConnectionResult conn7 = conn!=null?conn:new ConnectionResult(ConnectionResult.RESOLUTION_REQUIRED, null);
                try {
                    conn7.startResolutionForResult(activity, ConnectionResult.RESOLUTION_REQUIRED);
                } catch (IntentSender.SendIntentException e) {
                    Log.e(TAG, e.getMessage(), e);
                    reconnect = true;
                }
                break;
        }


        return reconnect;
    }

    private void resolveGoogleSignInErrors(){
        Log.d(TAG, "resolving google sign in errors: "+mConnectionResult);

        if (mConnectionResult != null && mConnectionResult.hasResolution()) {
            try{
                mIntentInProgress = true;
                boolean reconnect = handleGooglePlayServices(mConnectionResult, mConnectionResult.getErrorCode());

                Log.d(TAG, "can reconnect? "+reconnect);
                if(reconnect){
                    mIntentInProgress = false;
                    mGoogleApiClient.connect();
                }
            } finally{
                if (mGoogleApiClient.isConnected()) {
                    //findViewById(R.id.signup_options_layout).setVisibility(View.GONE);
                    //findViewById(R.id.signup_welcome_layout).setVisibility(View.VISIBLE);
                }
            }
        }else{
            //no resolution, notify the user
            if(!mSignInClicked)
                Toast.makeText(activity, "Issues occurred while communicating with Google", Toast.LENGTH_SHORT).show();
        }
    }

    private void googleUserIsConnected(Person user){
        mSignInClicked = false;
        if (mGoogleApiClient != null && user != null) {
            //Toast.makeText(activity, "Google User is connected!", Toast.LENGTH_LONG).show();

            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            String personName = currentPerson.getDisplayName();
            String personPhoto = currentPerson.getImage() != null ? currentPerson.getImage().getUrl() : "";
            String personId = currentPerson.getId();
            int personAgeRange = 0;
            try{
                personAgeRange = currentPerson.getAgeRange().getMin();
            }catch (Exception e){
                //continue
            }
            String personEmail = Plus.AccountApi.getAccountName(mGoogleApiClient);

            //persist user info
            AppState.instance().setUsername(personName);
            AppState.instance().setEmailAddress(personEmail);
            AppState.instance().setAgeRange(personAgeRange);
            if (!AppState.instance().isSignedIn())
                AppState.instance().signIn();

            //update view
            NetworkImageView profileImageView = (NetworkImageView) activity.findViewById(R.id.login_profile_image);
            profileImageView.setDefaultImageResId(R.drawable.user);
            personPhoto = personPhoto.replace("sz=50", "sz=400");
            profileImageView.setImageUrl(personPhoto, AppController.getInstance().getImageLoader());
            //profileImageView.setVisibility(View.VISIBLE);
            //((TextView)findViewById(R.id.login_status_message)).setText("Welcome " + personName + "! One moment please!");
            //findViewById(R.id.login_progress_bar).setVisibility(View.GONE);

            UserInfo userInfo = AppController.getInstance().getUserInfo();
            userInfo.setUserId(personId);
            userInfo.setUserName(personName);
            userInfo.setUserImageUrl(personPhoto);
            userInfo.setGoogleUserId(personId);
            userInfo.setUserEmail(personEmail);

            setResult(RESULT_OK);
            affiliateProfileUpdate(userInfo);
            TrackingManager.getInstance(AppController.getInstance()).
                    trackEvent(GoogleSignInActivity.this, "Google Sign-up");
            /*
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try{
                        Thread.sleep(3500);
                    }catch (Exception e){}
                    //finish();
                }
            }).start();*/
        }
    }


    public void signOut() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();

            if(AppState.instance().isSignedIn())
                AppState.instance().signOut();

            Log.d(TAG, "sign out google user");
        }
    }

    public void revokeAccess(){
        // Prior to disconnecting, run clearDefaultAccount().
        if (mGoogleApiClient != null) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            // mGoogleApiClient is now disconnected and access has been revoked.
                            // Trigger app logic to comply with the developer policies

                        }
                    });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConnectionResult.SIGN_IN_REQUIRED) {
            Log.d(TAG, "onActivityResult:: sign in required: resultCode: "+resultCode);
            //mResolvingError = false;
            if (resultCode == RESULT_OK) {
                mIntentInProgress = false;
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() && !mGoogleApiClient.isConnected()) {
                    Log.d(TAG, "connecting to google client api again");
                    mGoogleApiClient.connect();
                }
            }
        }else if(requestCode == ConnectionResult.RESOLUTION_REQUIRED){
            Log.d(TAG, "onActivityResult:: resolution required: "+resultCode);
            if (resultCode == RESULT_CANCELED) {
                setResult(RESULT_CANCELED);
                finish();
            }
        }else if(requestCode == ConnectionResult.INVALID_ACCOUNT){
            Log.d(TAG, "onActivityResult:: invalid account");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        TrackingManager.getInstance(AppController.getInstance()).trackStartActivity(this);
    }

    @Override
    public void onStop(){
        super.onStop();
        TrackingManager.getInstance(AppController.getInstance()).trackStopActivity(this);
    }


    public void onResume(){
        super.onResume();

        checkNetworkConnection();
    }

    public void onPause(){
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
