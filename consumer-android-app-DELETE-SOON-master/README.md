Beta 1.0 Specification
===========================

updated to reflect Beta agenda not including compute engine (Beta 2.0)

The purpose of the Beta app is to obtain network capacity for the pending compute engine update

<b>What we know</b><br>
1. Consumers <i>will</i> trade their mobile device's compute time for other items of value.<br>
2. Consumers are comfortable with an explicit definition of what their device will be doing, i.e. the compute engine<br>
3. App publishers/developers are very amenable to place offers within our app<br>

<b>Methodology</b><br>
To achieve our purpose we will implement the following app flow<br>
* Sign up and login<br>
  * Connect expected social logins e.g. facebook, gplus,...
  * Use our own non social login an collect necessary variables i.e. email & password (unless a case can be made for gaining more info that relates back to our purpose)
* After the information sign up screen, the app will message to the user that it will run during periods of user inactivity (see below). The user will be asked to agree that they understand this and click a checkbox on our ToS and privacy policy before completing sign up.
  * The app will run as a constant background process WHEN
      * The device is charging 
      * The device has more than 75% charge
  * Our data sets will be minimized, so the app will not use too much data.
  
* Main UI begins following sing up and acceptance of terms<br>
* In App Credit Tab (Oasis Reserve/ Unicoins/ Bank)
  * Users will initially see the credit accrued within the app, because they are new we will prime the account with enough to redeem for an intial offer in the Marketplace tabe (see Marketplace below) 
  * (explanation: Unoceros credit is an equalizing currency for variable reward values provided by publishers/developers. As an example Pandora may determine the wholesale value of an in app offer that retails for $5 is worth more to them than another developer who also offers their product at a retail value of $5. In this scenario we would require different amounts of device hours to pay for the reward, thereby making our currency the equalizing agent within the app. Another way to say this is we mitigate our counter party risk to the developer by requiring more hours to pay for something that costs us more to acquire.)
  * Unoceros credit will increase in the app on a granular momentary basis e.g. hours, minutes, seconds so the progress bar will be replaced with an ever incrementing value display
      * We should show some daily or weekly statistics of accrual to lay the ground work for some psychology components e.g. gamification
          * We can all provide some input on these stats but simple is the right place to start 

* Marketplace Tab
  * Users will visualize publisher/devleoper offers and relevant data
      * Cost in Unoceros Credit to redeem (gray out if not enough credit)
      * Network wide download count
      * Length of time offer is expected to remain available (publisher may limit availability or time available/ scarcity          is good)
  * Can be an endless scroll setup or cards
  * (HasOffers, the company, may have a useful CRM type backend specifically setup to handle the backend of this with           respect to publisher offers, redemptions, and time durations and a web dashboard)
  * Simple checkout or confirmation feature for the marketplace selection
 
* 3rd Tab (I think some simple network stats or a map displaying all the other network participants would help with some "feel good" aspect plus I think it might make a cool visual. When the compute engine comes into play a big feature request we got during the demo was some project use case information. Some people will be interested in what they contribute to, perhaps not down to the individual level but highlighting really cool things that happen on the network would be really fun to show.) 
    
* In the alpha we launched notifications for milestons, generally those notifications were not appreciated according to the feedback and should be turned off.
   * Currently the launche icon will live in the notifiaction tray since we are a foreground process. We could choose to show some information there, daily ear, or leave it out altogether. A third option could be to set it as user configurable.  
* Dummy compute
   * The alpha created a dummy compute scenario to test some assumptions and identify aspects of UX. Unless there is a convincing argument for this to remain, I beleive we should reduce or remove this aspect. We need to build loyalty and adoption now. We no longer need to test if taxing the system will cause consumers to uninstall.  
    

<b>Metrics</b><br>
The Beta will be instrumented to capture the following metrics:
* Assign a tracakble ID to each individual user so we can eventually profile a devices behaviour over time.
* How many consumers get past the sign up screen. (<i>This tests Assumption 1.<i>)
* The age range of the consumers (assuming consumer truthfulness).
* How many times a consumers has redeemed a reward
* How much CPU time the app has available, based on how much time the app has to run when charging and when the battery is over 75%. Storing the total battery + charging and each individual value.  
* How many times a user acts on a progress notification.
* How long the background app is actually running, i.e., if not 24/7, it means the user is killing our app so that we can't compute.
* Additional metrics relevant to publisher deals will be required and may end up being handled by a third party service integration, such as HasOffer
